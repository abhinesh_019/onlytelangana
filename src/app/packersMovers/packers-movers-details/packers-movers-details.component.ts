import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PackersMoversDetailsService } from './packers-movers-details.service';

@Component({
  selector: 'app-packers-movers-details',
  templateUrl: './packers-movers-details.component.html',
  styleUrls: ['./packers-movers-details.component.css']
})
export class PackersMoversDetailsComponent implements OnInit {

  public clientsDetails:any
  public ids:any
  public servicesOneGets:any
  public servicesOneGetsCount:any
  public servicesOneGetss:any
  public servicesTwoGetDatavehicleName:any
  public servicesTwoGetDataId:any
  public serviceTwoGetss:any
public serviceTwoGetssCount:any
public trackRecordsGetss:any
public trackRecordsGetCounts:any
public fecilitiesGetsCount:any
public fecilitiesGets:any
public fecilitiesGstes:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
// public servicesOneGets:any
public servicesone:any
public servicesTwos:any
public trackRecordsId:any
public trackRecordsName:any
public trackRecordsGet:any
public trackRecordsgetsData:any
 public trackRecordsgetsDatac:any
public servicesTwosc:any
public totalCustomer:any
public totalCustomerc:any
 public fecilitiesGetsc:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
public openshareid:any

postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
  constructor(private router:Router,private route:ActivatedRoute,private packers:PackersMoversDetailsService) { }
public  s="5d232d17a5744b686509ef27"
  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.servicesOneGetsss()
   this.servicesOneGetCount()
   this.trackRecordsGetsMains()
   this.trackRecordsGetsCountsMains()
   this.fecilitiesGetCounts()
   this.fecilitiesGetss()
   this.clientsUpdates()
   this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.packers.usersDetails(this.ids).subscribe((res)=>{
this.clientsDetails=res
 
})
}
servicesOneGetsss(){ 
  this.packers.servicesOneGets(this.ids).subscribe((res)=>{
  this.servicesOneGetss=res
  var dfg = this.servicesOneGetss[0]
  this.servicesOneceCLicks(dfg)
    })
  }
  servicesOneceCLicks(get){
    this.servicesTwoGetDataId=get._id
    this.servicesTwoGetDatavehicleName=get.vehicleName
     
    this.serviceTwoGets()
   this.serviceTwoGetsCount()
  }
  servicesOneGetCount(){ 
    this.packers.servicesOneGetCount(this.ids).subscribe((res)=>{
    this.servicesOneGetsCount=res
      })
    }
    serviceTwoGets(){ 
      this.packers.serviceTwoGets(this.servicesTwoGetDataId).subscribe((res)=>{
      this.serviceTwoGetss=res
        
      })
      }
      serviceTwoGetsCount(){ 
        this.packers.serviceTwoGetsCount(this.servicesTwoGetDataId).subscribe((res)=>{
        this.serviceTwoGetssCount=res
          
        })
        }
   // ******************************************updates present ***************************************
   shareClick(up){
    this.share=!this.share
    this.comments=false
    this.viewcomments=false
    this.replyfrom=false
    this.openshareid=up._id
  }
  commentsClick(up){
    this.comments=!this.comments
    this.share=false
    this.viewcomments=false
    this.commentspostsId=up._id
     this.commentsGettoClints()
     this.commentsGettoClintsCounts()
   }
  
  viewCommentsClick(up){
    this.viewcomments=!this.viewcomments
    this.share=false
    this.viewcommentsid=up._id
  }
  replyFromm(comms){
    this.replyfrom=!this.replyfrom
    this.share=false
    this.replyid=comms._id
    this.replyFromCommentesGet()
    
  }
   clientsUpdates(){   
    this. packers.clientsUpdatesGet(this.ids).subscribe((res)=>{
      this.clientUpdates=res
      console.log(res);
      
     })
   }
   
   clientsUpdatesCount(){   
    this. packers.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
      this.clientUpdatesCount=res
      console.log(res);
      
       })
   }
  
   commentsPoststoClints(){   
    this. packers.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
       })
       this.postComments.descriptions=""
   }
   commentsGettoClints(){   
    this. packers.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
      this.getUsersComments=res
      console.log(res);
     })
   }
  
   commentsGettoClintsCounts(){   
    this. packers.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
      this.getUsersCommentsCounts=res
      console.log(res);
      })
   }
  
   replyFromCommentesGet(){   
    this. packers.replyFromCommentesGet(this.replyid).subscribe((res)=>{
      this.replies=res
      console.log(res);
      })
   }
  
  
    // ***********0verallcomments************
    overallcomment(){
         
       
      this. packers.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
       
       this.commentspost.name="",
       this.commentspost.contactNo="",
       this.commentspost.email="",
       this.commentspost.leaveComment=""
      })
    }
    // ******************************* track records **************
    trackRecordsGetsMains(){ 
      this.packers.trackRecordsGets(this.ids).subscribe((res)=>{
      this.trackRecordsGetss=res
       })
      }
  
      trackRecordsGetsCountsMains(){ 
        this.packers.trackRecordsGetsCounts(this.ids).subscribe((res)=>{
        this.trackRecordsGetCounts=res
         })
        }

        fecilitiesGetss(){ 
          this.packers.fecilitiesGets(this.ids).subscribe((res)=>{
          this.fecilitiesGstes=res
            
          })
          }
          fecilitiesGetCounts(){ 
            this.packers.fecilitiesGetCounts(this.ids).subscribe((res)=>{
            this.fecilitiesGetsCount=res
              
            })
            }
   }
  