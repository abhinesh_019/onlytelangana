import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class PackersMoversDetailsService {

  constructor(private http: HttpClient) { }

  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsGetind/"+id);
  }
  servicesOneGets(id){
    return this.http.get(environment.apiUrl + "/packersMoversServicesGet/"+id);
     }
     servicesOneGetCount(id){
      return this.http.get(environment.apiUrl + "/packersMoversServicesGetCounts/"+id);
       }
       serviceTwoGets(id){
        return this.http.get(environment.apiUrl + "/packersMoversServicesTwoGet/"+id);
         } 
         serviceTwoGetsCount(id){
          return this.http.get(environment.apiUrl + "/packersMoversServicesTwoGetCounts/"+id);
           } 
            //  ************************updates present ***************************
            clientsUpdatesGet(id){
    
              return this.http.get(environment.apiUrl +"/packersMoversupdatesget/"+id);
            }
            
            clientsUpdatesGetCounts(id){
              
              return this.http.get(environment.apiUrl +"/packersMoversupdatesgetCounts/"+id);
            }
            commentsPoststoClints(id,data){
              
              return this.http.post(environment.apiUrl +"/packersMoversupdatesCommentspost/"+id,data);
            }
            commentsGettoClints(id){
              
              return this.http.get(environment.apiUrl +"/packersMoversupdatesCommentsget/"+id);
            }
            commentsGettoClintsCounts(id){
              
              return this.http.get(environment.apiUrl +"/packersMoversupdatesCommentsgetcounts/"+id);
            }
            replyFromCommentesGet(id){
              
              return this.http.get(environment.apiUrl +"/packersMoversupdatesCommentReplysGet/"+id);
            }
          
            overallscommentspost(id,data){
              
              return this.http.post(environment.apiUrl +"/packersMoversUsersComments/"+id,data);
            }
            // ********************************
            trackRecordsGets(id){
              return this.http.get(environment.apiUrl + "/packersMoversServicesTrackRecordsGet/"+id);
               }
               trackRecordsGetsCounts(id){
                return this.http.get(environment.apiUrl + "/packersMoversServicesTrackRecordsGetCounts/"+id);
                 }
                 fecilitiesGets(id){
                  return this.http.get(environment.apiUrl + "/packersMoversfecitiesGet/"+id);
                   }
                   fecilitiesGetCounts(id){
                    return this.http.get(environment.apiUrl + "/packersMoversfecitiesGetCounts/"+id);
                     }
          }
          
