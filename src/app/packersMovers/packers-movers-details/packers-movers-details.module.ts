import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { PackersMoversDetailsComponent } from './packers-movers-details.component';
import { PackersMoversDetailsService } from './packers-movers-details.service';
 
const routes:Routes=[{path:'packersMoversdetails/:_id/:name',component:PackersMoversDetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    PackersMoversDetailsComponent,

  ],
  providers: [PackersMoversDetailsService],
})
export class PackersMoversDetailsModule { }
