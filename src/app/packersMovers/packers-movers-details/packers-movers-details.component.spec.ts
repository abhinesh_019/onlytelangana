import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackersMoversDetailsComponent } from './packers-movers-details.component';

describe('PackersMoversDetailsComponent', () => {
  let component: PackersMoversDetailsComponent;
  let fixture: ComponentFixture<PackersMoversDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackersMoversDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackersMoversDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
