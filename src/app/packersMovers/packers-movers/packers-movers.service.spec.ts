import { TestBed, inject } from '@angular/core/testing';

import { PackersMoversService } from './packers-movers.service';

describe('PackersMoversService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PackersMoversService]
    });
  });

  it('should be created', inject([PackersMoversService], (service: PackersMoversService) => {
    expect(service).toBeTruthy();
  }));
});
