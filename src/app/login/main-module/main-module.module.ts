import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { MainModuleComponent } from './main-module.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import{LoginModule} from'../login/login.module';
import{SignupModule} from'../signup/signup.module'
import { MaterialsModule } from '../shared/materials.module';
import { GolbalService } from '../golbal.service';
import { ForgetpasswordModule } from '../forgetpassword/forgetpassword.module';
import { ResetpasswordModule } from '../resetpassword/resetpassword.module';


const routes: Routes = [
  {
    path: '', component: MainModuleComponent, children:
      [{ path: 'login', redirectTo: 'login', pathMatch: "full" },
      { path: 'login', loadChildren: 'app/login/login/login.module#LoginModule' },
      { path: 'signup', loadChildren: 'app/login/signup/signup.module#SignupModule'},
      { path: 'forgetpassword', loadChildren: 'app/login/forgetpassword/forgetpassword.module#ForgetpasswordModule'},
      { path: 'reset-password/:token', loadChildren: 'app/login/resetpassword/resetpassword.module#ResetpasswordModule'},

      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    LoginModule,
    SignupModule,
    MaterialsModule,
    ForgetpasswordModule,
    ResetpasswordModule,
    HttpClientModule
  ],
  declarations: [
    MainModuleComponent,
  ],
  providers: [GolbalService],
})
export class MainModuleModule { }
