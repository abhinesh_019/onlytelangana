import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { ForgetpasswordComponent } from './forgetpassword.component';
import { MaterialsModule } from '../shared/materials.module';

const routes:Routes=[{path:'',component:ForgetpasswordComponent},]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MaterialsModule
  ],
  declarations: [ForgetpasswordComponent]
})
export class ForgetpasswordModule { }
