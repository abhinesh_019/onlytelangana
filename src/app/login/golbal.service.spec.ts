import { TestBed, inject } from '@angular/core/testing';

import { GolbalService } from './golbal.service';

describe('GolbalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GolbalService]
    });
  });

  it('should be created', inject([GolbalService], (service: GolbalService) => {
    expect(service).toBeTruthy();
  }));
});
