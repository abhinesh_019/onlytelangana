import { TestBed, inject } from '@angular/core/testing';

import { FunctonhallcatService } from './functonhallcat.service';

describe('FunctonhallcatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FunctonhallcatService]
    });
  });

  it('should be created', inject([FunctonhallcatService], (service: FunctonhallcatService) => {
    expect(service).toBeTruthy();
  }));
});
