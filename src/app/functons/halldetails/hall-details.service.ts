import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class HallDetailsService {

  constructor(private http: HttpClient) { }
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/clientsGetind/"+id);
  } 
  ficilities(id){
    
    return this.http.get(environment.apiUrl +"/fecitiesGet/"+id);
  } 
  ficilitiesCount(id){
    
    return this.http.get(environment.apiUrl +"/fecitiesGetCounts/"+id);
  } 
  customers(id){
    
    return this.http.get(environment.apiUrl +"/customersGet/"+id);
  } 
  customersC(id){
    
    return this.http.get(environment.apiUrl +"/customersGetCounts/"+id);
  }


///////////////////////////////////  clients  updates    //////////////////////////////////////////



                 
     
clientsUpdatesGet(id){
    
  return this.http.get(environment.apiUrl +"/updatesget/"+id);
}
clientsUpdatesCnts(id){
  
  return this.http.get(environment.apiUrl +"/updatesgetCounts/"+id);
}
 
commentsPoststoClints(id,data){
  
  return this.http.post(environment.apiUrl +"/updatesCommentspost/"+id,data);
}
commentsGettoClints(id){
  
  return this.http.get(environment.apiUrl +"/updatesCommentsget/"+id);
}
commentsGettoClintsCounts(id){
  
  return this.http.get(environment.apiUrl +"/updatesCommentsgetcounts/"+id);
}
replyFromCommentesGet(id){
  
  return this.http.get(environment.apiUrl +"/updatesCommentReplysGet/"+id);
}

overallscommentspost(id,data){
  
  return this.http.post(environment.apiUrl +"/userscomments/"+id,data);
}
// ********************************
futuresGet(id){
  
  return this.http.get(environment.apiUrl +"/functionalHallAdditionalFutures/"+id);
}

futuresGetTwo(id){
  
  return this.http.get(environment.apiUrl +"/functionalHallAdditionalFuturesTwo/"+id);
}



}
////////////////////////////////////////////////////////////////////////