import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HallDetailsService } from './hall-details.service';

@Component({
  selector: 'app-halldetails',
  templateUrl: './halldetails.component.html',
  styleUrls: ['./halldetails.component.css']
})
export class HalldetailsComponent implements OnInit {
  public hallsClients:any
  public ids:any
  public hallsFec:any
  public hallsFecnt:any
  public hallscus:any
  public hallscusc:any
  ////////////////////////////////////// updates
  public share:any
  public comments:any
  public viewcomments:any
  public viewcommentsid:any
  public replyfrom:any
  public replyid:any
  public clientUpdates:any
  public clientUpdatesCnts:any
  public getUsersComments:any
  public commentspostsId:any
  public getUsersCommentsCounts:any
  public replies:any
  public clientUpdatesCount:any
  public openshareid:any
  public futuresGets:any
  public futuresGetsTwoId:any
  public futuresGetsTwoName:any
  public futuresGetsTwo:any
//   public futuresGets:any
//   public futuresGets:any
//   public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any
//  public futuresGets:any

  formData: FormData = new FormData(); 

  comment={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }

  postComments={
    "descriptions":""
  }
  commentspost={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }

      constructor(private route:ActivatedRoute,private halls:HallDetailsService) { }
      ngOnInit() {
    this.individualdata()
  this.ficilitiesC()
  this.ficilities()
  this.customers()
  this.customersC()
  this.clientsUpdates()
  this.clientsUpdatesCnts()
  this.futuresGet()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
    this.usersDetails()
    
    
}

usersDetails(){ 
this.halls.usersDetails(this.ids).subscribe((res)=>{
this.hallsClients=res

})
}
ficilities(){ 
  this.halls.ficilities(this.ids).subscribe((res)=>{
  this.hallsFec=res
  
  })
  }
  ficilitiesC(){ 
    this.halls.ficilitiesCount(this.ids).subscribe((res)=>{
    this.hallsFecnt=res
    
    })
    }
 
  // *********************************************************
// ********************************************************************** customers **********************************************


   customers(){ 
    this.halls.customers(this.ids).subscribe((res)=>{
    this.hallscus=res
    console.log(res);
    
    })
    }
    customersC(){ 
      this.halls.customersC(this.ids).subscribe((res)=>{
      this.hallscusc=res
      console.log(res);
      
      })
      }

// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.halls.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
    console.log(res);
   })
   
   
 }

 clientsUpdatesCnts(){   
  this.halls.clientsUpdatesCnts(this.ids).subscribe((res)=>{
    this.clientUpdatesCnts=res
    console.log(res);
    
   })
 }
  

 commentsPoststoClints(){   
  this.halls.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.halls.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.halls.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.halls.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }
// ********************************************************************************
// ***********0verallcomments************
overallcomment(){
       
     
  this.halls.overallscommentspost(this.ids,this.comment).subscribe((res)=>{
 
   this.comment.name="",
   this.comment.contactNo="",
   this.comment.email="",
   this.comment.leaveComment=""
  })
}

futuresGet(){   
  this.halls.futuresGet(this.ids).subscribe((res)=>{
    this.futuresGets=res
      var furg = this.futuresGets[0]
this.futuresGetClick(furg)
  })
 }

 futuresGetClick(get?){
this.futuresGetsTwoId=get._id
this.futuresGetsTwoName=get.mainTitle
console.log(this.futuresGetsTwoId);
console.log(this.futuresGetsTwoName);
this.futuresGetTwo()
 }
 
 futuresGetTwo(){   
  this.halls.futuresGetTwo(this.futuresGetsTwoId).subscribe((res)=>{
    this.futuresGetsTwo=res
  })
 }
}

