import { TestBed, inject } from '@angular/core/testing';

import { HallDetailsService } from './hall-details.service';

describe('HallDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HallDetailsService]
    });
  });

  it('should be created', inject([HallDetailsService], (service: HallDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
