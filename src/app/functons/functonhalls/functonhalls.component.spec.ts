import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctonhallsComponent } from './functonhalls.component';

describe('FunctonhallsComponent', () => {
  let component: FunctonhallsComponent;
  let fixture: ComponentFixture<FunctonhallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctonhallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctonhallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
