import { TestBed, inject } from '@angular/core/testing';

import { FunctionhallbriefService } from './functionhallbrief.service';

describe('FunctionhallbriefService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FunctionhallbriefService]
    });
  });

  it('should be created', inject([FunctionhallbriefService], (service: FunctionhallbriefService) => {
    expect(service).toBeTruthy();
  }));
});
