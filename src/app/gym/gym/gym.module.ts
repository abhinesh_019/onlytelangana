import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { SharedModule } from '../../shared/shared.module';
 import { GymdetailsModule } from '../gymdetails/gymdetails.module';
import { GymComponent } from './gym.component';
import { GymService } from '../gym.service';

const routes: Routes = [
  {
    path: '', component: GymComponent, children:
      [
         { path: 'gymDetails/:_id/:name', loadChildren: 'app/gym/gymdetails/gymdetails.module#GymdetailsModule'},

      ]
  }]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    GymdetailsModule
  ],
  declarations: [
    GymComponent
  ],
  providers:[GymService]
})
 
export class GymModule { }
