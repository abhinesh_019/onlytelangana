import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GymService } from '../gym.service';

@Component({
  selector: 'app-gymdetails',
  templateUrl: './gymdetails.component.html',
  styleUrls: ['./gymdetails.component.css']
})
export class GymdetailsComponent implements OnInit {
  public gymuser:any
  public ids:any
  public gymTypes:any
  public gymNames:any
  public gymId:any
  public gymTypesDes:any
  public selectAnimals:any
  public gymTypesDesInd:any
  public clientUpdatesCount:any
  public clientUpdates:any
  public commentspostsId:any
  public getUsersComments:any
  public getUsersCommentsCounts:any
  public openshareid:any
  public viewcommentsid:any
  public replyid:any
  public replies:any
  public gymTypesCnts:any
  public clientUpdatesCnts:any
  public gymuserCat:any   //catageries dropdown
  public genderName:any //catageries dropdown gender 
  public genderId:any //catageries dropdown gender
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public showdetails:boolean=false
  public replyfrom:boolean=false 
  public equipmentsShow:boolean=false 
 public gymServices:any
 public deatilsId:any
 public gymServicesind:any
 public gymServicesCount:any
 public equipments:any
 public equipmentsCount:any
 public equipmentsind:any
 public equipmentsId:any
 public fecilitiesCount:any
 public fecilities:any
public usersgetCount:any
public usersget:any
public usersdetailsId:any
public showUsersDetails:any
public usersgetind:any



  postComments={
    "descriptions":""
  }
  commentspost={
    "name":"",
    "email":"",
    "contactNo":"",
    "leaveComment":"",
      }

      constructor(private route:ActivatedRoute,private gym:GymService) { }
      ngOnInit() {
    this.individualdata()
    this.clientsUpdates()
    this.clientsUpdatesCount()
    this.clientsUpdatesCnts()
    this.usersCat() 
    this.fecilitiesget()
    this.fecilitiesgetCount()
 

  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

this.usersDetails()
}

usersDetails(){ 
this.gym.gymusersonly(this.ids).subscribe((res)=>{
this.gymuser=res

})
}
// ********************************** catageries *************************
usersCat(){ 
  this.gym.gymusersCat(this.ids).subscribe((res)=>{
  this.gymuserCat=res
  var id =this.gymuserCat[0];
  this.catageries(id)
     
  })  }
  catageries(get?){
    this.genderName=get.genders
    this.genderId=get._id
      this.userServices()
      this.userServicesCount()
      this.equipmentsget()
      this.equipmentsgetCount()
      this.usersdetailsgetCount()
      this.usersdetailsget()
  }
  // *********************************************************


// ********************************** our services *************************
userServices(){ 
  this.gym.userServices(this.genderId).subscribe((res)=>{
  this.gymServices=res
   })  
   
}
  clickImageDetails(ser){
     this.deatilsId=ser._id
     this.showdetails=!this.showdetails
     this.equipmentsShow=false
     this.userServicesind()
     
  }
  userServicesind(){ 
    if(this.deatilsId)
    this.gym.userServicesind(this.deatilsId).subscribe((res)=>{
    this.gymServicesind=res
     })  
   }

  userServicesCount(){ 
    if(this.deatilsId)
    this.gym.userServicesCount(this.genderId).subscribe((res)=>{
    this.gymServicesCount=res
     
     })  
  
  }
  // *********************************************************

// ********************************** equipments *************************
equipmentsget(){ 
  this.gym.equipmentsget(this.genderId).subscribe((res)=>{
  this.equipments=res
   
  })  }

  equipmentsgetCount(){ 
    this.gym.equipmentsgetCount(this.genderId).subscribe((res)=>{
    this.equipmentsCount=res
     })  }

       clickEquipmentsDetails(ser){
        this.equipmentsId=ser._id
        this.equipmentsShow=!this.equipmentsShow
        this.showdetails=false
        this.equipmentsgetInd()
         
      }

      equipmentsgetInd(){ 
        this.gym.equipmentsgetInd(this.equipmentsId).subscribe((res)=>{
        this.equipmentsind=res
         })  }
  // *********************************************************

// ********************************** fecilities *************************
fecilitiesget(){ 
  this.gym.fecilitiesget(this.ids).subscribe((res)=>{
  this.fecilities=res
    
  })  }

  fecilitiesgetCount(){ 
    this.gym.fecilitiesgetCount(this.ids).subscribe((res)=>{
    this.fecilitiesCount=res
     }) 
      }

     
  // *********************************************************
// ********************************** users details *************************
usersdetailsget(){ 
  this.gym.usersdetailsget(this.genderId).subscribe((res)=>{
  this.usersget=res
   })  }
  clickUsersDetails(u){
this.usersdetailsId=u
this.showUsersDetails=!this.showUsersDetails
this.usersdetailsgetind()
   }

  usersdetailsgetind(){ 
    this.gym.usersdetailsgetind(this.usersdetailsId).subscribe((res)=>{
    this.usersgetind=res
    
     }) 
    
     
     }
  usersdetailsgetCount(){ 
    this.gym.usersdetailsgetCount(this.genderId).subscribe((res)=>{
    this.usersgetCount=res
     }) 
     }

     
  // *********************************************************


// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.gym.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
    console.log(res);
   })
   
   
 }

 clientsUpdatesCnts(){   
  this.gym.clientsUpdatesCnts(this.ids).subscribe((res)=>{
    this.clientUpdatesCnts=res
   })
 }
 clientsUpdatesCount(){   
  this.gym.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
   })
 }

 commentsPoststoClints(){   
  this.gym.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.gym.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.gym.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.gym.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.gym.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }



}
