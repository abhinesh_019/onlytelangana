import { Component, OnInit } from '@angular/core';
import { TelanganaService } from './telangana.service';

@Component({
  selector: 'app-telangana',
  templateUrl: './telangana.component.html',
  styleUrls: ['./telangana.component.css']
})
export class TelanganaComponent implements OnInit {
public asone:any
public astwo:any
public asthree:any
public asfour:any
public asfive:any
public assix:any
public asseven:any
public aseight:any
public asnine:any
public asten:any
constructor(private swim:TelanganaService) { }

  ngOnInit() {
    this.addsoneget()
    this.addstwoget()
    this.addsthreeget()
    this.addsfourget()
    this.addsfiveget()
    this.addssixget()
    this.addssevenget()
    this.addseightget()
    this.addsnineget()
    this.addstenget()
    

  }
  addsoneget(){
           
    this.swim.addsonegets().subscribe((res)=>{
      this.asone=res
       
      })
   }
   addstwoget(){
           
    this.swim.addstwogets().subscribe((res)=>{
      this.astwo=res
       
      })
   }
   addsthreeget(){
           
    this.swim.addsthreegets().subscribe((res)=>{
      this.asthree=res
      })
   }

   addsfourget(){
           
    this.swim.addsfourgets().subscribe((res)=>{
      this.asfour=res
      })
   }

   addsfiveget(){
           
    this.swim.addsfivegets().subscribe((res)=>{
      this.asfive=res
      })
   }

   addssixget(){
           
    this.swim.addssixgets().subscribe((res)=>{
      this.assix=res
      })
   }

   addssevenget(){
           
    this.swim.addssevengets().subscribe((res)=>{
      this.asseven=res
      })
   }
   addseightget(){
           
    this.swim.addseightgets().subscribe((res)=>{
      this.aseight=res
      })
   }

   addsnineget(){
           
    this.swim.addsninegets().subscribe((res)=>{
      this.asnine=res
      })
   }
   addstenget(){
           
    this.swim.addstengets().subscribe((res)=>{
      this.asten=res
      })
   }
}
