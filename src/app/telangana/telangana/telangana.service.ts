import { Injectable } from '@angular/core';
 import { environment } from '../../../environments/environment.prod';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
@Injectable()
export class TelanganaService {

  constructor(private http: HttpClient) { }
  addsonegets(){
    return this.http.get(environment.apiUrl +"/advertiseOne");

  }
  addstwogets(){
    return this.http.get(environment.apiUrl +"/advertiseTwo");

  }
  addsthreegets(){
    return this.http.get(environment.apiUrl +"/advertiseThree");

  }
  addsfourgets(){
    return this.http.get(environment.apiUrl +"/advertiseFour");

  }
  addsfivegets(){
    return this.http.get(environment.apiUrl +"/advertiseFive");

  }
  addssixgets(){
    return this.http.get(environment.apiUrl +"/advertiseSix");

  }


  addssevengets(){
    return this.http.get(environment.apiUrl +"/advertiseSeven");

  }
  addseightgets(){
    return this.http.get(environment.apiUrl +"/advertiseEight");

  }
  addsninegets(){
    return this.http.get(environment.apiUrl +"/advertiseNine");

  }
  addstengets(){
    return this.http.get(environment.apiUrl +"/advertiseTen");

  }

}
