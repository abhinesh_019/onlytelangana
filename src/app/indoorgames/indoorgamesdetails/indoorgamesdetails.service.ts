import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class IndoorgamesdetailsService {
  constructor(private http: HttpClient) { }

   
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/indoorgamesClientsGetind/"+id);
  }
  
  servicesOne(id){
    return this.http.get(environment.apiUrl + "/indoorgamesServiceGet/"+id);
     }
     servicesOnec(id){
      return this.http.get(environment.apiUrl + "/indoorgamesServiceGetc/"+id);
       }
       
         servicesTwo(id){
          return this.http.get(environment.apiUrl + "/indoorgamesServicesGet/"+id);
           }
           servicesTwoc(id){
            return this.http.get(environment.apiUrl + "/indoorgamesServicesGetCounts/"+id);
             }
             
             clientsUpdatesGet(id){
    
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesget/"+id);
            }
            
            clientsUpdatesGetCounts(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesgetCounts/"+id);
            }
            commentsPoststoClints(id,data){
              
              return this.http.post(environment.apiUrl +"/indoorgamesupdatesCommentspost/"+id,data);
            }
            commentsGettoClints(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesCommentsget/"+id);
            }
            commentsGettoClintsCounts(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesCommentsgetcounts/"+id);
            }
            replyFromCommentesGet(id){
              
              return this.http.get(environment.apiUrl +"/indoorgamesupdatesCommentReplysGet/"+id);
            }
          
            overallscommentspost(id,data){
              
              return this.http.post(environment.apiUrl +"/indoorgamesUsersComments/"+id,data);
            }
            // ********************************
            fecilities(id){
              return this.http.get(environment.apiUrl + "/indoorgamesfecitiesGet/"+id);
             }
             fecilitiesc(id){
              return this.http.get(environment.apiUrl + "/indoorgamesfecitiesGetCounts/"+id);
             }
          }
          