import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndoorgamesdetailsComponent } from './indoorgamesdetails.component';

describe('IndoorgamesdetailsComponent', () => {
  let component: IndoorgamesdetailsComponent;
  let fixture: ComponentFixture<IndoorgamesdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndoorgamesdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndoorgamesdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
