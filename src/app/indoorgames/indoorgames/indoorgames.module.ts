import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { IndoorgamesComponent } from './indoorgames.component';
import { IndoorgamesService } from './indoorgames.service';
import { IndoorgamesdetailsModule } from '../indoorgamesdetails/indoorgamesdetails.module';
 

const routes: Routes = [
  {
    path: '', component: IndoorgamesComponent, children:
      [
      { path: 'indoorgamesdetails/:_id/:name', loadChildren: 'app/indoorgames/indoorgamesdetails/indoorgamesdetails.module#IndoorgamesdetailsModule'},
 
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     IndoorgamesdetailsModule
       ],
  declarations: [
    IndoorgamesComponent
     ],
  providers:[IndoorgamesService]
})

export class IndoorgamesModule { }
