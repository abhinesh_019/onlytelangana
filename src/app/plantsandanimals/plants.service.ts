import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class PlantsService {

  constructor(private http: HttpClient) { }
  locationapi( ){
    return   this.http.get(environment.apiUrl +"/plantsLocations");
   }
   areaapi(id){
    return   this.http.get(environment.apiUrl +"/plantsAreas/"+id);
   }


 plantsusers(id,data){
    
    return this.http.get(environment.apiUrl +"/plantsClients/"+id,{params:data});
  }
 plantsusersForLocations(){
    
    return this.http.get(environment.apiUrl +"/plantsCountsLocations");
  }
 plantsusersonly(id){
    
    return this.http.get(environment.apiUrl +"/plantsClientIdInd/"+id);
  }
  
 plantsuserscounts(id){
    return this.http.get(environment.apiUrl +"/plantsClientsCountsByArea/"+id);
  }
 plantsuserscountstot(){
    
    return this.http.get(environment.apiUrl +"/plantsClientsAll");
  }
 plantsuserscountsarea(id){
    
    return this.http.get(environment.apiUrl +"/plantsCountsAreas/"+id);
  }
 plantsTypes(id){
    
    return this.http.get(environment.apiUrl +"/plantsTypes/"+id);
  }
 plantsListsTypeCnt(id){
    
    return this.http.get(environment.apiUrl +"/plantsTypesdesCont/"+id);
  }
  
 plantsListDetail(id){
    
    return this.http.get(environment.apiUrl +"/plantsTypesdes/"+id);
  }
 plantsListDetailIndv(id){
    
    return this.http.get(environment.apiUrl +"/plantsTypedesInd/"+id);
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/plantsupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/plantsupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/plantsupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/plantsupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/plantsupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/plantsupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/plantsuserscomments/"+id,data);
  }
  // ********************************
  plantsMaintainces(id){
    
    return this.http.get(environment.apiUrl +"/PlantsNecessaryMaintaincesget/"+id);
  }
  plantsMaintaincesCounts(id){
    
    return this.http.get(environment.apiUrl +"/PlantsNecessaryMaintaincesgetCounts/"+id);
  }
  plantsFertilizerss(id){
    
    return this.http.get(environment.apiUrl +"/plantsPlantsFertilizersget/"+id);
  }
  plantsFertilizersCounts(id){
    
    return this.http.get(environment.apiUrl +"/plantsPlantsFertilizersgetCounts/"+id);
  }
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/plantsAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/plantsAddsFiveLocGet/"+id);
  }
}