import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { PlantsComponent } from './plants.component';
import { PlantsService } from '../plants.service';
import { PadetailsModule } from '../padetails/padetails.module';
 
 

const routes: Routes = [
  {
    path: '', component: PlantsComponent, children:
      [
      { path: 'plantsdetails/:_id/:name', loadChildren: 'app/plantsandanimals/padetails/padetails.module#PadetailsModule'},
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     PadetailsModule
    ],
  declarations: [
    PlantsComponent,
  ],
  providers:[PlantsService]
})
 

export class PlantsModule { }
