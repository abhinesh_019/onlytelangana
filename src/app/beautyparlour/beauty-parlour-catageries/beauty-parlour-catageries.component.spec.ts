import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeautyParlourCatageriesComponent } from './beauty-parlour-catageries.component';

describe('BeautyParlourCatageriesComponent', () => {
  let component: BeautyParlourCatageriesComponent;
  let fixture: ComponentFixture<BeautyParlourCatageriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeautyParlourCatageriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeautyParlourCatageriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
