import { Component, OnInit } from '@angular/core';
import { BeautyparlourService } from '../beautyparlour.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parlourdetails',
  templateUrl: './parlourdetails.component.html',
  styleUrls: ['./parlourdetails.component.css']
})
export class ParlourdetailsComponent implements OnInit {
public ids:any
public clientsInd:any
public clientsServices:any
public clientsServicesCount:any
public updatesIds:any
public show:any
public showes:any
public shows:any
public showess:any
public selectedblogIds:any
public adminsupdateCommentsReply:any
public commentsUpdatesId:any
public adminupdates:any
public adminsupdateCountsall:any
public adminsupdateComments:any
public adminsupdateCommentsCount:any
public equipnmentsGet:any
public equipnmentsGetsCount:any
public trainningGetsCount:any
public trainningGet:any
public totalCustomersGetCount:any
public totalCustomersGets:any
public fecilitesGetsCount:any
public fecilitesGets:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any
// public adminsupdateCommentsCount:any

updatesCommentsdata={
  "descriptions":""
}

comments={
  name:"",
  email:"",
  contactNo:"",
  leaveComment:""
}


  constructor(private route:ActivatedRoute,private parlour:BeautyparlourService) { }

  ngOnInit() {
  this.individualdata()
 this.ServicesDetails()
 this.ServicesDetailsCounts()
 this.getupdates()
 this.getupdatesCount()
 this.equipnmentsGets()
 this.equipnmentsGetsCounts()
 this.trainningGets()
 this.trainningGetsCounts()
 this.totalCustomersGet()
 this.totalCustomersGetCounts()
 this.fecilitesGet()
 this.fecilitesGetsCounts()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
 this.clientDetailsIndividuial()
}
clientDetailsIndividuial(){ 
  this.parlour.clientsSeperate(this.ids).subscribe((res)=>{
  this.clientsInd=res
     
  })
  }
  ServicesDetails(){ 
    this.parlour.services(this.ids).subscribe((res)=>{
    this.clientsServices=res
  
    })
    }
    ServicesDetailsCounts(){ 
      this.parlour.servicesCounts(this.ids).subscribe((res)=>{
      this.clientsServicesCount=res
         
      })
      }

// *************get updates****************
getupdates(){
 
  this.parlour.updatesget(this.ids).subscribe((res)=>{
    this.adminupdates=res
    
   
  })
       }
       getupdatesCount(){
 
        this.parlour.updatesgetCount(this.ids).subscribe((res)=>{
          this.adminsupdateCountsall=res
          
         
        })
             }

showicons(){
  this.show=!this.show
  this.shows=false
}
showcomments(item?){
  this.show=false
  this.shows=!this.shows
   this.updatesIds=item._id
    
   this.comm()
 
        }
        updatestoAdmincommentsget(result?){
          this.showes=!this.showes
                   this.commentsUpdatesId=result
                    this.comm()
   this.commCount()
                    }
       comm(){
        this.parlour.commentsgetupdates(this.commentsUpdatesId).subscribe((res)=>{
            this.adminsupdateComments=res
             
           
          })
                } 
                commCount(){
                  this.parlour.commentsgetupdatesCount(this.commentsUpdatesId).subscribe((res)=>{
                      this.adminsupdateCommentsCount=res
                       
                     
                    })
                          }
         updatestoadmincomments(){
         
          this.parlour.commentspostupdates(this.updatesIds,this.updatesCommentsdata).subscribe((res)=>{
           
           
            
          })
          this.updatesCommentsdata.descriptions=""
           
               }
               updatesAdminReply(items){
                this.showess=!this.showess
                this.selectedblogIds=items
                this.parlour.commentsgetupdatesReply(this.selectedblogIds).subscribe((res)=>{
                  this.adminsupdateCommentsReply=res
                  
                })
              } 
// ***********0verallcomments************
overallcomment(){
 
  this.parlour.overallscommentspost(this.ids,this.comments).subscribe((res)=>{
    
  })
 
  this.comments.name="",
  this.comments.contactNo="",
  this.comments.email="",
  this.comments.leaveComment=""
 
 
 }
 equipnmentsGets(){ 
  this.parlour.equipnmentsGets(this.ids).subscribe((res)=>{
  this.equipnmentsGet=res
     
  })
  }

  equipnmentsGetsCounts(){ 
    this.parlour.equipnmentsGetsCounts(this.ids).subscribe((res)=>{
    this.equipnmentsGetsCount=res
       
    })
    }

    trainningGets(){ 
      this.parlour.trainningGets(this.ids).subscribe((res)=>{
      this.trainningGet=res
         
      })
      }
    
      trainningGetsCounts(){ 
        this.parlour.trainningGetsCounts(this.ids).subscribe((res)=>{
        this.trainningGetsCount=res
           
        })
        }

        totalCustomersGet(){ 
          this.parlour.totalCustomersGet(this.ids).subscribe((res)=>{
          this.totalCustomersGets=res
             
          })
          }
        
          totalCustomersGetCounts(){ 
            this.parlour.totalCustomersGetCounts(this.ids).subscribe((res)=>{
            this.totalCustomersGetCount=res
               
            })
            }

            fecilitesGet(){ 
              this.parlour.fecilitesGet(this.ids).subscribe((res)=>{
              this.fecilitesGets=res
                 
              })
              }
            
              fecilitesGetsCounts(){ 
                this.parlour.fecilitesGetsCounts(this.ids).subscribe((res)=>{
                this.fecilitesGetsCount=res
                   
                })
                }
                }
