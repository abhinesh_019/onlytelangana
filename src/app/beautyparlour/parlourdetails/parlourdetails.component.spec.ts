import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParlourdetailsComponent } from './parlourdetails.component';

describe('ParlourdetailsComponent', () => {
  let component: ParlourdetailsComponent;
  let fixture: ComponentFixture<ParlourdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParlourdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParlourdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
