import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BabycareComponent } from './babycare.component';
import { BabycareoverModule } from '../babycareover/babycareover.module';
import { BabycareService } from '../babycare.service';

const routes: Routes = [
  {
    path: '', component: BabycareComponent, children:
      [
        { path: 'babycaredetails/:_id', loadChildren: 'app/babycare/babycareover/babycareover.module#BabycareoverModule'},

      ]
  }]
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    BabycareoverModule
  ],
  declarations: [
    BabycareComponent
  ],
  providers: [BabycareService],
})
export class BabycareModule { }
