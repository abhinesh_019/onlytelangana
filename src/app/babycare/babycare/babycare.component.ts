import { Component, OnInit } from '@angular/core';
import { BabycareService } from '../babycare.service';
import{Router,ActivatedRoute} from'@angular/router';


@Component({
  selector: 'app-babycare',
  templateUrl: './babycare.component.html',
  styleUrls: ['./babycare.component.css']
})
export class BabycareComponent implements OnInit {

  public babycares;
  public babycare;
  public babycar;
  public area:any
  public pincode:any
  public name:any
  public searchString:String;
  searchTerm:String;
  public locationsget:any
  public locations:any
  public locationName:any
public its:any
public locationget:any
public areasAll:any
public selectAreaId:any
public selectArea:any
public polularsLocations:any
public areasCounts:any


public babycaresAddsOnes:any
public babycaresAddsTwos:any
public babycaresAddsThrees:any
public babycaresAddsFours:any
public babycaresAddsOneLocl:any
public babycaresAddsOneLoc:any
public babycaresAddsTwoLoc:any
public babycaresAddsThreeLoc:any
public babycaresAddsFoursLoc:any
public babycaresAddsFivesLoc:any


  constructor(private router:Router,private route:ActivatedRoute,private baby:BabycareService) { }

  ngOnInit() {
    this.getlocations()
    this.babycaredetails(),
    this.babycaredetail(),
    this.babycaredetailes()
    this.popularLocations()
  }

    
  // locations*****************
getlocations(){
  this.baby.locationapi().subscribe((res)=>{
    this.locationsget=res;
     
    
    var id =this.locationsget[0];
     this.location(id)
     
  })}


  location(get?){
    this.locationName=get.locations
    this.its=get._id
     this.allAreas()
    this.babycaressareasAddsOneLoc()
    this.babycaressareasAddsTwoLoc()
    this.babycaressareasAddsThreeLoc()
    this.babycaressareasAddsFourLoc()
    this.babycaressareasAddsFiveLoc()
   }

   allAreas(){
     
    this.baby.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
      this.selectedAre(id)
      this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
    })
   
  }

selectedAre(result){
this.selectAreaId=result._id
this.selectArea=result.area
  
 this.babycaredetails()
this.areaCounts()
}

  babycaredetails(){

  var data:any =  {}
if(this.searchString){
  data.search=this.searchString
}

  this.baby.babycare(this.selectAreaId,data).subscribe((res)=>{
    this.babycares=res
      
  })

}
searchFilter(){
  this.babycaredetails()
}

babycaredetailes(){
  this.baby.babycaredist().subscribe((res)=>{
    this.babycar=res
     
  })
}
babycaredetail(){
  this.baby.babycares().subscribe((res)=>{
    this.babycare=res
     
  })
}
popularLocations(){
  this.baby.polularLocation().subscribe((res)=>{
    this.polularsLocations=res
     
  })
}
areaCounts(){
  this.baby.areaCount(this.selectAreaId).subscribe((res)=>{
    this.areasCounts=res
     
  })
}

// ********************************************************* Adds Areas One ***********************************************************************
 
babycaressareasAddsOnea(){
  this.baby.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOneLoc=res
 
   })
}
 
// ********************************************************* Adds Areas Two ***********************************************************************
 
babycaressareasAddsTwoa(){
  this.baby.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
 

// ********************************************************* Adds Areas Three ***********************************************************************
 
babycaressareasAddsThreea(){
  this.baby.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
 
// ********************************************************* Adds Areas Four ***********************************************************************

 
babycaressareasAddsFoura(){
  this.baby.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
  
// ********************************************************* Adds Areas One ***********************************************************************
 
babycaressareasAddsOneLoc(){
  this.baby.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
 
// ********************************************************* Adds Areas Two ***********************************************************************
 
babycaressareasAddsTwoLoc(){
  this.baby.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
 
// ********************************************************* Adds Areas Three Loc***********************************************************************
 
babycaressareasAddsThreeLoc(){
  this.baby.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
 
// ********************************************************* Adds Areas Four Loc***********************************************************************

 
babycaressareasAddsFourLoc(){
  this.baby.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
 


// ********************************************************* Adds Areas Five Loc***********************************************************************

 
babycaressareasAddsFiveLoc(){
  this.baby.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
 
 
}

