import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BabycareoverComponent } from './babycareover.component';

describe('BabycareoverComponent', () => {
  let component: BabycareoverComponent;
  let fixture: ComponentFixture<BabycareoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BabycareoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BabycareoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
