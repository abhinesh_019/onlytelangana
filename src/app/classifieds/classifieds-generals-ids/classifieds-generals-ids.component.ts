import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClassifiedsGeneralsIdsService } from './classifieds-generals-ids.service';

@Component({
  selector: 'app-classifieds-generals-ids',
  templateUrl: './classifieds-generals-ids.component.html',
  styleUrls: ['./classifieds-generals-ids.component.css']
})
export class ClassifiedsGeneralsIdsComponent implements OnInit {
  public ids:any
  public landingsgetsgenerals:any
  public landingsgeneralsName:any
  public landingsgeneralsNameId:any
  public generalsDetails:any
  public generalsDetailsc:any
  // public ids:any
  // public ids:any
  // public ids:any
  // public ids:any
  // public ids:any
  // public ids:any
  // public ids:any
  constructor(private route:ActivatedRoute,private swim:ClassifiedsGeneralsIdsService) { }

  ngOnInit() {
    this.individualdata()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
 this.getlandingsgenerals()
  }
  getlandingsgenerals(){
    this.swim.landingsgetgenerals(this.ids).subscribe((res)=>{
    this.landingsgetsgenerals=res
    
     var id =this.landingsgetsgenerals[0];
        
    })  
  }
  
  landingsgenerals(get?){
    this.landingsgeneralsName=get.mainTitle
    this.landingsgeneralsNameId=get._id 
this.generalesDetails() 
this.generalesDetailsc()   
   }
   generalesDetails(){
    this.swim.generals(this.landingsgeneralsNameId).subscribe((res)=>{
    this.generalsDetails=res
    console.log(res);
    })  
  }
  generalesDetailsc(){
    this.swim.generalsc(this.landingsgeneralsNameId).subscribe((res)=>{
    this.generalsDetailsc=res
    console.log(res);
    
    })  
  }
}
