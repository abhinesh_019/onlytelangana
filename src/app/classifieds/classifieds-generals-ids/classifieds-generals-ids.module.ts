import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ClassifiedsGeneralsIdsComponent } from './classifieds-generals-ids.component';
import { Routes, RouterModule } from '@angular/router';
import { ClassifiedsGeneralsIdsService } from './classifieds-generals-ids.service';
 


const routes:Routes=[{path:'classifiedsGeneralsIds/:_id/:name',component:ClassifiedsGeneralsIdsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    ClassifiedsGeneralsIdsComponent,

  ],
  providers: [ClassifiedsGeneralsIdsService],
})
export class ClassifiedsGeneralsIdsModule { }
