import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ClassifiedsComponent } from './classifieds.component';
import { ClassifiedsService } from './classifieds.service';
import { ClassifiedSearchJobModule } from '../classifieds-search-jobs/classified-search-job.module';
import { ClassifiedGeneralModule } from '../classified-generals/classified-general.module';
import { ClassifiedsGeneralsIdsModule } from '../classifieds-generals-ids/classifieds-generals-ids.module';
 

const routes: Routes = [
  {
    path: '', component: ClassifiedsComponent, children:
      [
      { path: 'searchjobs', loadChildren: 'app/classifieds/classifieds-search-jobs/classified-search-job.module#ClassifiedSearchJobModule'},
      { path: 'classifiedGeneral', loadChildren: 'app/classifieds/classified-generals/classified-general.module#ClassifiedGeneralModule'},
      { path: 'classifiedsGeneralsIds/:_id/:name', loadChildren: 'app/classifieds/classifieds-generals-ids/classifieds-generals-ids.module#ClassifiedsGeneralsIdsModule'},

    ]
  }]
  
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     ClassifiedSearchJobModule,
     ClassifiedGeneralModule,
     ClassifiedsGeneralsIdsModule
  ],
  declarations: [
    ClassifiedsComponent
     ],
  providers:[ClassifiedsService]
})

export class ClassifiedsModule { }
