import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class ClassifiedGeneralService {

  constructor(private http: HttpClient) { }
  locationsGets(){
    return this.http.get(environment.apiUrl +"/clasdifiedsGeneralsLocations");
                }
}
