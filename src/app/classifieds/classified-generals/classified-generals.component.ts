import { Component, OnInit } from '@angular/core';
import { ClassifiedGeneralService } from './classified-general.service';

@Component({
  selector: 'app-classified-generals',
  templateUrl: './classified-generals.component.html',
  styleUrls: ['./classified-generals.component.css']
})
export class ClassifiedGeneralsComponent implements OnInit {
  public locationName:any
  public locationsget:any
  public its:any
  public locationss:any
  constructor(private swim:ClassifiedGeneralService) { }

  ngOnInit() {
    this.locationsGet()
  }
  locationsGet(){
    this.swim.locationsGets().subscribe((res)=>{
    this.locationsget=res
  
  })  
  }
}
