import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ClassifiedsSearchJobsComponent } from './classifieds-search-jobs.component';
import { ClassifiedSearchJobService } from './classified-search-job.service';
 


const routes:Routes=[{path:'searchjobs',component:ClassifiedsSearchJobsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    ClassifiedsSearchJobsComponent,

  ],
  providers: [ClassifiedSearchJobService],
})
 
export class ClassifiedSearchJobModule { }
