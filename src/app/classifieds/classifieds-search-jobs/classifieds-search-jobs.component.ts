import { Component, OnInit } from '@angular/core';
import { ClassifiedSearchJobService } from './classified-search-job.service';

@Component({
  selector: 'app-classifieds-search-jobs',
  templateUrl: './classifieds-search-jobs.component.html',
  styleUrls: ['./classifieds-search-jobs.component.css']
})
export class ClassifiedsSearchJobsComponent implements OnInit {
  public locationName:any
public locationsgets:any
public its:any
public locationss:any
 public landingsgets:any
public landingsName:any
public landingsNameId:any
public imageName:any
public videoName:any
public adminupdatesp:any
public searchesjobs:any
public statusNameId:any
public statusName:any
public statuseGets:any
public statuseGetsc:any
public searchesjobsc:any

  constructor(private swim:ClassifiedSearchJobService) { }

  ngOnInit() {
    this.getlandings()
   }
   
  getlandings(){
    this.swim.landingsget().subscribe((res)=>{
    this.landingsgets=res
    console.log(res);
    
     var id =this.landingsgets[0];
        
    })  
  }

  landings(get?){
    this.landingsName=get.mainTitle
    this.landingsNameId=get._id
    console.log(this.landingsNameId);
    console.log(this.landingsName);
this.searchejobs()    
this.searchejobsc()
   }
   searchejobs(){
    this.swim.searchdjob(this.landingsNameId).subscribe((res)=>{
    this.searchesjobs=res
     
         
    })  
  }
  searchesjobes(get?){
    this.statusName=get.mainTitle
    this.statusNameId=get._id
  this.statusGet()
  this.statusGetc()
   }
   statusGet(){
    this.swim.statusGets(this.statusNameId).subscribe((res)=>{
    this.statuseGets=res
     
         
    })  
  }
  statusGetc(){
    this.swim.statusGetsc().subscribe((res)=>{
    this.statuseGetsc=res
     })  
  }

  searchejobsc(){
    this.swim.searchdjobc(this.landingsNameId).subscribe((res)=>{
    this.searchesjobsc=res
    })  
  }
}
