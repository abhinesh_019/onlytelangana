import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class ClassifiedSearchJobService {

  constructor(private http: HttpClient) { }
  
     
      
   locationsget(){
    return this.http.get(environment.apiUrl +"/classifidesDate");
                }
                landingsget(){
                  return this.http.get(environment.apiUrl +"/classifidesLands");
                              }
                             
                                 searchdjob(id){
                                  return this.http.get(environment.apiUrl +"/classifideSearchJobsg/"+id);
                                              }
                                              statusGets(id){
                                                return this.http.get(environment.apiUrl +"/classifidesStatus/"+id);
                                                            }
                                                            statusGetsc(){
                                                              return this.http.get(environment.apiUrl +"/classifidesStatusc");
                                                                          }
                                                                          searchdjobc(id){
                                                                            return this.http.get(environment.apiUrl +"/classifideSearchJobsgc/"+id);
                                                                                        }

}
