import { TestBed, inject } from '@angular/core/testing';

import { FireWorksDetailsService } from './fire-works-details.service';

describe('FireWorksDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FireWorksDetailsService]
    });
  });

  it('should be created', inject([FireWorksDetailsService], (service: FireWorksDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
