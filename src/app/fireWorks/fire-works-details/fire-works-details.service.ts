import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class FireWorksDetailsService {

  constructor(private http: HttpClient) { }

  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/fireworksClientsGetind/"+id);
  }

  servicesOne(id){
    return this.http.get(environment.apiUrl + "/fireworksServicesGet/"+id);
     }
     servicesOnec(id){
      return this.http.get(environment.apiUrl + "/fireworksServicesGetCounts/"+id);
       }
     servicesOnes(id){
      return this.http.get(environment.apiUrl + "/fireworksServicesGet/"+id);
       }  
        servicesTwo(id){
            return this.http.get(environment.apiUrl + "/fireworksServicesTwo/"+id);
             }  
             servicesTwoc(id){
              return this.http.get(environment.apiUrl + "/fireworksServicesTwoCounts/"+id);
               }  
                 customersDetailsGet(id){
                      return this.http.get(environment.apiUrl + "/fireworkServicesCustomers/"+id);
                       } 
                       customersDetailsGetc(id){
                        return this.http.get(environment.apiUrl + "/fireworkServicesCustomersCounts/"+id);
                         }
                          totalCustomers(id){
                            return this.http.get(environment.apiUrl + "/fireworksTotalCustomersGet/"+id);
                             }
                             totalCustomersc(id){
                              return this.http.get(environment.apiUrl + "/fireworksServicesTotalCustomersGetCounts/"+id);
                               }
  //  ************************updates present ***************************
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/fireworksupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/fireworksupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/fireworksUsersComments/"+id,data);
  }
  // ********************************
 
}



