import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FireWorksDetailsComponent } from './fire-works-details.component';
import { FireWorksDetailsService } from './fire-works-details.service';
 
const routes:Routes=[{path:'firWorksdetails/:_id/:name',component:FireWorksDetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    FireWorksDetailsComponent,

  ],
  providers: [FireWorksDetailsService],
})
export class FireWorksDetailsModule { }
