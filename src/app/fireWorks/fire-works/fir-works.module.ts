import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FireWorksComponent } from './fire-works.component';
import { FirWorksService } from './fir-works.service';
import { FireWorksDetailsModule } from '../fire-works-details/fire-works-details.module';
 
const routes: Routes = [
  {
    path: '', component: FireWorksComponent, children:
      [
      { path: 'firWorksdetails/:_id/:name', loadChildren: 'app/fireWorks/fire-works-details/fire-works-details.module#FireWorksDetailsModule'},
 
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     FireWorksDetailsModule
     
       ],
  declarations: [
    FireWorksComponent
     ],
  providers:[FirWorksService]
})
export class FirWorksModule { }
