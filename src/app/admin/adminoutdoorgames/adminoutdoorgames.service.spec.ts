import { TestBed, inject } from '@angular/core/testing';

import { AdminoutdoorgamesService } from './adminoutdoorgames.service';

describe('AdminoutdoorgamesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminoutdoorgamesService]
    });
  });

  it('should be created', inject([AdminoutdoorgamesService], (service: AdminoutdoorgamesService) => {
    expect(service).toBeTruthy();
  }));
});
