import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminoutdoorgamesService {

  constructor(private http: HttpClient) { }

  usersDetails(id){
    return this.http.get(environment.apiUrl + "/outdoorgamesClientsGetind/"+id);
     }
     servicesOne(id){
      return this.http.get(environment.apiUrl + "/outdoorgamesServiceGet/"+id);
       }
       servicesOnec(id){
        return this.http.get(environment.apiUrl + "/outdoorgamesServiceGetc/"+id);
         }
         updateServices(id,data){
          return this.http.post(environment.apiUrl + "/outdoorgamesServiceCatPost/"+id,data);
           }

           servicesTwo(id){
            return this.http.get(environment.apiUrl + "/outdoorgamesServicesGet/"+id);
             }
             servicesTwoc(id){
              return this.http.get(environment.apiUrl + "/outdoorgamesServicesGetCounts/"+id);
               }
               updateServicesTwo(id,data){
                return this.http.post(environment.apiUrl + "/outdoorgamesServicesPost/"+id,data);
                 }
                       
             clientsUpdatesGet(id){
    
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesget/"+id);
            }
            
            clientsUpdatesGetCounts(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesgetCounts/"+id);
            }
            commentsPoststoClints(id,data){
              
              return this.http.post(environment.apiUrl +"/outdoorgamesupdatesCommentReplysPost/"+id,data);
            }
            commentsGettoClints(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesCommentsget/"+id);
            }
            commentsGettoClintsCounts(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesCommentsgetcounts/"+id);
            }
            replyFromCommentesGet(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesCommentReplysGet/"+id);
            }
            postUpdatesForm(id,data){
              return this.http.post(environment.apiUrl + "/outdoorgamesupdatespostsdata/"+id,data);
               }    
               postFecilities(id,data){
                return this.http.post(environment.apiUrl + "/outdoorgamesfecitiesPost/"+id,data);
                 }
                 fecilities(id){
                  return this.http.get(environment.apiUrl + "/outdoorgamesfecitiesGet/"+id);
                 }
                 fecilitiesc(id){
                  return this.http.get(environment.apiUrl + "/outdoorgamesfecitiesGetCounts/"+id);
                 }
                 generalsCommGet(id) {
                  return this.http.get(environment.apiUrl + "/outdoorgamesUsersComments/"+id);
                }
                generalsCommGetC(id) {
                  return this.http.get(environment.apiUrl + "/outdoorGamesUsersCommentsCounts/"+id);
                }
    }

