import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminoutdoorgamesComponent } from './adminoutdoorgames.component';

describe('AdminoutdoorgamesComponent', () => {
  let component: AdminoutdoorgamesComponent;
  let fixture: ComponentFixture<AdminoutdoorgamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminoutdoorgamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminoutdoorgamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
