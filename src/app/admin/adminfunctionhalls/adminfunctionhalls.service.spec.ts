import { TestBed, inject } from '@angular/core/testing';

import { AdminfunctionhallsService } from './adminfunctionhalls.service';

describe('AdminfunctionhallsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminfunctionhallsService]
    });
  });

  it('should be created', inject([AdminfunctionhallsService], (service: AdminfunctionhallsService) => {
    expect(service).toBeTruthy();
  }));
});
