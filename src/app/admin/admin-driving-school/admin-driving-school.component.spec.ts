import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDrivingSchoolComponent } from './admin-driving-school.component';

describe('AdminDrivingSchoolComponent', () => {
  let component: AdminDrivingSchoolComponent;
  let fixture: ComponentFixture<AdminDrivingSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDrivingSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDrivingSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
