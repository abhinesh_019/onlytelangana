import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminDrivingSchoolComponent } from './admin-driving-school.component';
import { AdminDrivingSchoolService } from './admin-driving-school.service';
 
const routes:Routes=[{path:'admidrivingSchools',component:AdminDrivingSchoolComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminDrivingSchoolComponent,

  ],
  providers: [AdminDrivingSchoolService],
})
export class AdminDrivingSchoolModule { }
