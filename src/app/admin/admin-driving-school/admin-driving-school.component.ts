import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
 import { AdminDrivingSchoolService } from './admin-driving-school.service';

@Component({
  selector: 'app-admin-driving-school',
  templateUrl: './admin-driving-school.component.html',
  styleUrls: ['./admin-driving-school.component.css']
})
export class AdminDrivingSchoolComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false
  public serviceTwoPosts:boolean=false

  public usersID:any
public clientsInd:any
public viewsProfiles:any
public viewsNoti:any
public servicesOneGet:any
public servicevehicleName:any
public servicesIds:any
public servicesOneGets:any
public servicevehicleNames:any
public servicesone:any
public servicesTwos:any
public trackRecordsId:any
public trackRecordsName:any
public trackRecordsGet:any
public trackRecordsgetsData:any
public servicesOneGetc:any
public trackRecordsgetsDatac:any
public servicesTwosc:any
public totalCustomer:any
public totalCustomerc:any
public fecilitiesGets:any
public fecilitiesGetsc:any
public commentspostsId:any
  public viewcommentsid:any
  public clientUpdates:any
  public replyid:any
  public clientUpdatesCount:any
   public getUsersComments:any
  public getUsersCommentsCounts:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public replies:any
  public openshareid:any
public imageNameupdates:any
  public videoNames:any
  public gcomm:any
  public gcommc:any
  // public videoNames:any
  // public videoNames:any
  // public videoNames:any
  // public videoNames:any
  // public videoNames:any
  postComments={
    "descriptions":""
  }
ser={
  vehicleName:" ",
  vehicleType:"",
  vehicleImg:"",
  
}

totalCus={
  customerName:" ",
  place:"",
  customersImg:"",
 }
 fec={
  fecilitiesName:" ",
  fecilitiesDescriptions:"",
  fecilitiesImg:"",
 }
tracks={
  customerName:" ",
  dateOfJoin:"",
  shift:"",
  timmings:"",
   place:"",
  customersImg:"",
}
ups={
  images:"",
    title:"",
    description:"",
    viedoes:"",
}
serc={
 
  learningPeriods1:"",
    learningPeriods2:"",
    learningPeriods3:"",
    providence:"",
    shifts:"",
    timmings:"",
    hoursOfOperations:"",
    routesCoveredFrom:"",
    routesCoveredTo:"",
    trainerName:"",
    trainerExp:"",
    trainerImg:"" 
}
formData: FormData = new FormData(); 


constructor(private route:ActivatedRoute,private driving:AdminDrivingSchoolService) { }
 ngOnInit() {
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.servicesOne()
this.servicesOnec()
this.servicesOneForPostTwo()
this.servicesTrackRecods()
this.totalCustomersc()
this.totalCustomers()
this.fecilitiesGet()
this.fecilitiesGetc()
this.clientsUpdatesCount()
this.clientsUpdates()
this.generalCommGet()
this.generalCommGetc()
  }
  usersDetails(){ 
    this.driving.usersDetails(this.usersID).subscribe((res)=>{
    this.clientsInd=res
     
    })
    }
    
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }
    // ************************************************ Services one **********************************

    handleFileInputUpdatesI($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
       
    }
    
    updateServices(){
      this.formData.append('vehicleName',this.ser.vehicleName);
  this.formData.append('vehicleType',this.ser.vehicleType);
  
 this.driving.updateServices(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.ser.vehicleName="",
     this.ser.vehicleType="",
     this.ser.vehicleImg=""
   }

   servicesOne(){ 
    this.driving.servicesOne(this.usersID).subscribe((res)=>{
    this.servicesOneGet=res 
    console.log(res);
    
    })
    }
    servicesOnec(){ 
      this.driving.servicesOnec(this.usersID).subscribe((res)=>{
      this.servicesOneGetc=res 
      console.log(res);
      
      })
      }
    ServicesOneClick(vec){
      this.servicesIds=vec._id
      this.servicevehicleName=vec.vehicleName
      console.log(this.servicevehicleName);
      console.log(this.servicesIds);
       this.servicesTwo()
       this.servicesTwoc()
       this.trackRecords()
       this.trackRecordsc()
    }

    servicesOneForPostTwo(){ 
      this.driving.servicesOnes(this.usersID).subscribe((res)=>{
      this.servicesOneGets=res 
      var id=this.servicesOneGets[0]
      console.log(res);
      this.vechilesNamesClicks(id)
      })
      } 
      vechilesNamesClicks(get?){
        this.servicesone=get._id
        this.servicevehicleNames=get.vehicleName
        console.log(this.servicevehicleName);
        console.log(this.servicesIds);
      }

      servicesTrackRecods(){ 
        this.driving.servicesOnes(this.usersID).subscribe((res)=>{
        this.trackRecordsGet=res 
        var id=this.trackRecordsGet[0]
        console.log(res);
        this.TrackRecodsClicks(id)
        })
        } 
        TrackRecodsClicks(get?){
          this.trackRecordsId=get._id
          this.trackRecordsName=get.vehicleName
          console.log(this.servicevehicleName);
          console.log(this.servicesIds);
        }

      // ******************************** services two *********************
      handleFileInputUpdatesT($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
       
    }
    
    updateServicesTwo(){
    
      this.formData.append('learningPeriods1',this.serc.learningPeriods1);
      this.formData.append('learningPeriods2',this.serc.learningPeriods2);
      this.formData.append('learningPeriods3',this.serc.learningPeriods3); 
      this.formData.append('providence',this.serc.providence);
      this.formData.append('shifts',this.serc.shifts);
      this.formData.append('timmings',this.serc.timmings);
      this.formData.append('hoursOfOperations',this.serc.hoursOfOperations);
      this.formData.append('routesCoveredFrom',this.serc.routesCoveredFrom);
      this.formData.append('routesCoveredTo',this.serc.routesCoveredTo);
      this.formData.append('trainerName',this.serc.trainerName);
      this.formData.append('trainerExp',this.serc.trainerExp);
   
 this.driving.updateServicesTwo(this.servicesone,this.formData).subscribe((res)=>{
  
     }) 
     this.serc.hoursOfOperations="",
     this.serc.learningPeriods1="",
     this.serc.learningPeriods2="",
     this.serc.learningPeriods3="",
     this.serc.providence="",
     this.serc.routesCoveredFrom="",
     this.serc.routesCoveredTo="",
     this.serc.shifts="",
     this.serc.timmings="",
     this.serc.trainerExp="",
     this.serc.trainerImg="",
     this.serc.trainerName=""
     
   }
   servicesTwo(){ 
    this.driving.servicesTwo(this.servicesIds).subscribe((res)=>{
    this.servicesTwos=res 
    console.log(res);
    
    })
    }
    servicesTwoc(){ 
      this.driving.servicesTwoc(this.servicesIds).subscribe((res)=>{
      this.servicesTwosc=res 
      console.log(res);
      
      })
      }
    // ****************************** track records *************************
      handleFileTracks($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
       
    }
    
    trcakRecordsPosts(){
      
      this.formData.append('customerName',this.tracks.customerName);
      this.formData.append('dateOfJoin',this.tracks.dateOfJoin);
      this.formData.append('shift',this.tracks.shift);
      this.formData.append('selectedAsVehicle',this.trackRecordsName);
      this.formData.append('timmings',this.tracks.timmings);
      this.formData.append('place',this.tracks.place);
     
   
 this.driving.trcakRecordsPosts(this.trackRecordsId,this.formData).subscribe((res)=>{
  
     })
     this.tracks.customerName="",
     this.tracks.dateOfJoin="",
     this.tracks.shift="",
     this.tracks.place="",
     this.tracks.timmings=""
    }
   trackRecords(){ 
    this.driving.trackRecords(this.servicesIds).subscribe((res)=>{
    this.trackRecordsgetsData=res 
    console.log(res);
    
    })
    }
    trackRecordsc(){ 
      this.driving.trackRecordsc(this.servicesIds).subscribe((res)=>{
      this.trackRecordsgetsDatac=res 
      console.log(res);
      
      })
      }

      // ****************************** total no of customers *************************
      handleFileCustomersTotal($event:any,img) {
        let imageFile= File = $event.target.files[0];
        this.formData.append(img, imageFile);
         
      }
      
      totalCustamers(){
        
        this.formData.append('customerName',this.totalCus.customerName);
        this.formData.append('place',this.totalCus.place);
        
       
     
   this.driving.totalCustamers(this.usersID,this.formData).subscribe((res)=>{
    
       })
       this.tracks.customerName="",
       this.tracks.place=""
      
      }
     totalCustomers(){ 
      this.driving.totalCustomers(this.usersID).subscribe((res)=>{
      this.totalCustomer=res 
      console.log(res);
      
      })
      }
      totalCustomersc(){ 
        this.driving.totalCustomersc(this.usersID).subscribe((res)=>{
        this.totalCustomerc=res 
        console.log(res);
        
        })
        }
         // ****************************** Fecilities*************************
         handleFileFecilities($event:any,img) {
        let imageFile= File = $event.target.files[0];
        this.formData.append(img, imageFile);
         
      }
      
      feiciliesPosts(){
        
        this.formData.append('fecilitiesName',this.fec.fecilitiesName);
        this.formData.append('fecilitiesDescriptions',this.fec.fecilitiesDescriptions);
        
       
     
   this.driving.feiciliesPosts(this.usersID,this.formData).subscribe((res)=>{
    
       })
       this.fec.fecilitiesName="",
       this.fec.fecilitiesDescriptions=""
       this.fec.fecilitiesImg=""
      }
     fecilitiesGet(){ 
      this.driving.fecilitiesGet(this.usersID).subscribe((res)=>{
      this.fecilitiesGets=res 
      console.log(res);
      
      })
      }
      fecilitiesGetc(){ 
        this.driving.fecilitiesGetc(this.usersID).subscribe((res)=>{
        this.fecilitiesGetsc=res 
        console.log(res);
        
        })
        }
           // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.driving.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.driving.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.driving.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.driving.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.driving.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.driving.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }
  // ***************************************************updades **************************************
  handleFileInputUpdates($event:any,images){
    this.imageNameupdates= File = $event.target.files[0];
   }
  handleFileInputUpdate($event:any,viedoes){
    this.videoNames= File = $event.target.files[0];
    this.formData.append(viedoes,this.videoNames);
  }
  
  postUpdatesForm(){
  this.formData.append('images',this.imageNameupdates);
  this.formData.append('title',this.ups.title);
  this.formData.append('description',this.ups.description);
   
   this.driving.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
      console.log('assssss',res);
      })
       this.ups.title="",
       this.ups.description="",
       this.ups.images="",
       this.ups.viedoes=""
  }
  generalCommGet(){ 
    this.driving.generalsCommGet(this.usersID).subscribe((res)=>{
    this.gcomm=res
       })
    }
  generalCommGetc(){ 
    this.driving.generalsCommGetC(this.usersID).subscribe((res)=>{
    this.gcommc=res
      })
    }
  }