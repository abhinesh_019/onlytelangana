import { TestBed, inject } from '@angular/core/testing';

import { AdminDrivingSchoolService } from './admin-driving-school.service';

describe('AdminDrivingSchoolService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminDrivingSchoolService]
    });
  });

  it('should be created', inject([AdminDrivingSchoolService], (service: AdminDrivingSchoolService) => {
    expect(service).toBeTruthy();
  }));
});
