import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminindoorgamesComponent } from './adminindoorgames.component';

describe('AdminindoorgamesComponent', () => {
  let component: AdminindoorgamesComponent;
  let fixture: ComponentFixture<AdminindoorgamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminindoorgamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminindoorgamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
