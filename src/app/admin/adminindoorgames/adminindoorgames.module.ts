import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminindoorgamesComponent } from './adminindoorgames.component';
import { AdminindoorgamesService } from './adminindoorgames.service';
 

const routes:Routes=[{path:'adminindoor',component:AdminindoorgamesComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminindoorgamesComponent,

  ],
  providers: [AdminindoorgamesService],
})
export class AdminindoorgamesModule { }
