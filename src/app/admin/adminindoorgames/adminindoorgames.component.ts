import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminindoorgamesService } from './adminindoorgames.service';

@Component({
  selector: 'app-adminindoorgames',
  templateUrl: './adminindoorgames.component.html',
  styleUrls: ['./adminindoorgames.component.css']
})
export class AdminindoorgamesComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false
  public divhide:boolean=true

   public usersID:any
public indoorgamesClients:any
public viewsProfiles:any
public viewsNoti:any
public imagesName:any
public servicesOneGet:any
public servicesOneGetc:any
public servicesId:any
public imageFiles:any
public serviceGameName:any
public servicesOneGets:any
public servicesTwoGet:any
public servicesTwoGetc:any
public servicesIds:any
public serviceGameNames:any
public openshareid:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any


postComments={
  "descriptions":""
}
public imageNameupdates:any
public videoNames:any
public imgFec:any
public fecilitiesGet:any
public fecilitiesGetc:any
public gcommc:any
public gcomm:any
 // public usersID:any


formData: FormData = new FormData(); 

ser={
  gameName:" ",
  aboutGame:"",
  NoOfPlayers:"",
  trainersName:"",
  trainersYearsOfExp:"",
  trainersImg:"",
   gameImg:"",
}
sers={
  gamePlayerNames1:"",
  gamePlayerNames2:"",
  gamePlayerNames3:"",
  gamePlayerNames4:"",
  gamePlayerTtile:"",
  anyDescriptoions:"",
  gameNamesImg1:"",
}
ups={
  images:"",
    title:"",
    description:"",
    viedoes:"",
}
fec={

  fecilitiesName:"",
  fecilitiesImg:"",
  fecilitiesDescriptions:"",
}
constructor(private route:ActivatedRoute,private indoor:AdminindoorgamesService) { }
 ngOnInit() {
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.servicesOne()
this.servicesOnec()
this.servicesOnes()
this.clientsUpdates()
this.clientsUpdatesCount()
this.fecilities()
this.fecilitiesc()
this.generalCommGet()
this.generalCommGetc()
  }
  usersDetails(){ 
    this.indoor.usersDetails(this.usersID).subscribe((res)=>{
    this.indoorgamesClients=res
     })
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
     }
    
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
     }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
     }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.divhide=true
      }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
     }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }

    // ************************************************serilities **********************************

    handleFileInputUpdatesI($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
       
    }
    
    updateServices(){
      this.formData.append('gameName',this.ser.gameName);
  this.formData.append('aboutGame',this.ser.aboutGame);
  this.formData.append('NoOfPlayers',this.ser.NoOfPlayers);
  this.formData.append('trainersName',this.ser.trainersName);
  this.formData.append('trainersYearsOfExp',this.ser.trainersYearsOfExp); 
 this.indoor.updateServices(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.ser.aboutGame="",
     this.ser.gameImg="",
     this.ser.gameName="",
     this.ser.NoOfPlayers="",
     this.ser.trainersImg="",
     this.ser.trainersName="",
     this.ser.trainersYearsOfExp=""
   }

   servicesOne(){ 
    this.indoor.servicesOne(this.usersID).subscribe((res)=>{
    this.servicesOneGet=res 
    })
    }
    ServicesOneClick(ser){
      this.servicesIds=ser._id
      this.serviceGameNames=ser.gameName
      this.servicesTwo()
      this.servicesTwoc()
    }
    servicesOnes(){ 
      this.indoor.servicesOne(this.usersID).subscribe((res)=>{
      this.servicesOneGets=res 
      var id=this.servicesOneGets[0]
        this.gameNames(id)
      })
      }
    servicesOnec(){ 
      this.indoor.servicesOnec(this.usersID).subscribe((res)=>{
      this.servicesOneGetc=res
  })
      }
       
      gameNames(get?){
        this.servicesId=get._id
        this.serviceGameName=get.gameName
       }
     
       // ************************************************services two **********************************

    handleFileInputUpdatesT($event:any,img) {
      let imageFiles= File = $event.target.files[0];
      this.formData.append(img, imageFiles);
    }
    
    updateServicesTwo(){
      this.formData.append('gameName',this.serviceGameName);
      this.formData.append('gamePlayerNames1',this.sers.gamePlayerNames1);
  this.formData.append('gamePlayerNames2',this.sers.gamePlayerNames2);
  this.formData.append('gamePlayerNames3',this.sers.gamePlayerNames3);
  this.formData.append('gamePlayerNames4',this.sers.gamePlayerNames4);
  this.formData.append('gamePlayerTtile',this.sers.gamePlayerTtile); 
  this.formData.append('anyDescriptoions',this.sers.anyDescriptoions); 
 this.indoor.updateServicesTwo(this.servicesId,this.formData).subscribe((res)=>{
  
     })
     this.ser.aboutGame="",
     this.ser.gameImg="",
     this.ser.gameName="",
     this.ser.NoOfPlayers="",
     this.ser.trainersImg="",
     this.ser.trainersName="",
     this.ser.trainersYearsOfExp=""
   }

   servicesTwo(){ 
    this.indoor.servicesTwo(this.servicesIds).subscribe((res)=>{
    this.servicesTwoGet=res
    })
    }

    servicesTwoc(){ 
      this.indoor.servicesTwoc(this.servicesIds).subscribe((res)=>{
      this.servicesTwoGetc=res
       
      })
      }
   // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
 
}
 clientsUpdates(){   
  this.indoor.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.indoor.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.indoor.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.indoor.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.indoor.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.indoor.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
   })
 }
 // ***************************************************updades **************************************
 handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 }
handleFileInputUpdate($event:any,viedoes){
  this.videoNames= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoNames);
}

postUpdatesForm(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('description',this.ups.description);
 
 this.indoor.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
     })
     this.ups.title="",
     this.ups.description="",
     this.ups.images="",
     this.ups.viedoes=""
}
// ************************************************fecilities **********************************
handleFileFec($event:any,images){
  this.imgFec= File = $event.target.files[0];
  }

  postFecilities(){
  
  this.formData.append('fecilitiesImg',this.imgFec);
 this.formData.append('fecilitiesName',this.fec.fecilitiesName);
 this.formData.append('fecilitiesDescriptions',this.fec.fecilitiesDescriptions);
 
 this.indoor.postFecilities(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.fec.fecilitiesDescriptions="",
     this.fec.fecilitiesImg="",
     this.fec.fecilitiesName=""
   }

   fecilities(){ 
    this.indoor.fecilities(this.usersID).subscribe((res)=>{
    this.fecilitiesGet=res
 
    })
    }

    fecilitiesc(){ 
      this.indoor.fecilitiesc(this.usersID).subscribe((res)=>{
      this.fecilitiesGetc=res
       
      })
      }
      generalCommGet(){ 
        this.indoor.generalsCommGet(this.usersID).subscribe((res)=>{
        this.gcomm=res
           })
        }
      generalCommGetc(){ 
        this.indoor.generalsCommGetC(this.usersID).subscribe((res)=>{
        this.gcommc=res
          })
        }
}