import { TestBed, inject } from '@angular/core/testing';

import { AdminswimmingService } from './adminswimming.service';

describe('AdminswimmingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminswimmingService]
    });
  });

  it('should be created', inject([AdminswimmingService], (service: AdminswimmingService) => {
    expect(service).toBeTruthy();
  }));
});
