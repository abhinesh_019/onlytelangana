import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminswimmingComponent } from './adminswimming.component';

describe('AdminswimmingComponent', () => {
  let component: AdminswimmingComponent;
  let fixture: ComponentFixture<AdminswimmingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminswimmingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminswimmingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
