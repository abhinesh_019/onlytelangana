import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminanimalsService } from './adminanimals.service';

@Component({
  selector: 'app-adminanimals',
  templateUrl: './adminanimals.component.html',
  styleUrls: ['./adminanimals.component.css']
})
export class AdminanimalsComponent implements OnInit {
 
  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public marriageDetailsClients:any
public viewsProfiles:any
public viewsNoti:any
public plantsFertilizersGet:any
public PlantsEquipmentsGet:any
public imageNameupdates:any
public videoNames:any
public typesOfPlantsGets:any
public plantsDetailsId:any
public plantsDetailsName:any
public gcommc:any
public gcomm:any
public PlantsDescriptionsGet:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any

fer={
  foodType:"",
  foodQuantity:"",
  foodTimmings:"",
  foodPrecations:"",
  foodStorages:"",
  foodImages1:"",
  foodImages2:"",
  foodImages3:"",
  foodImagesDes1:"",
  foodImagesDes2:"",
  foodImagesDes3:"",
}
equ={
  nessariesImages:"",
  necessariesName:"",

}

ups={
  images:"",
    title:"",
    descriptions:"",
    viedoes:"",
}
tr={
  animalsTypes:"",
}
des={
  name:"",
  age:"",
  food:"",
  descriptions:"",
  gender:"",
  precautions:"",
  price:"",
  images1:"",
  images2:"",
  images3:"",
  images1des:"",
  images2des:"",
  images3des:"",
}
formData: FormData = new FormData(); 

public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public typesPlants:boolean=false

  public animalsuser:any
  public ids:any
  public animalsTypes:any
  public animalsNames:any
  public animalsId:any
  public animalsTypesDes:any
  public selectAnimals:any
  public animalsTypesDesInd:any
  public clientUpdatesCount:any
  public clientUpdates:any
  public commentspostsId:any
  public getUsersComments:any
  public getUsersCommentsCounts:any
  public openshareid:any
  public viewcommentsid:any
  public replyid:any
  public replies:any
  public animalsTypesCnts:any
  public clientUpdatesCnts:any
  public plantsMaintaincs:any
  public plantsMaintaincsC:any
  public plantsFertilizerss:any
  public plantsFertilizersC:any
 public typesOfPlantsMainGets:any
 public plantsDetailsMainId:any
 public plantsDetailsMainName:any
 public plantsDetailsNecessaryId:any
 public plantsDetailsNecessaryName:any
 public typesOfPlantsNecessaryGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any
//  public typesOfPlantsMainGets:any

  postComments={
    "descriptions":""
  }
constructor(private route:ActivatedRoute,private animals:AdminanimalsService) { }
 ngOnInit() {
 
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
 this.typesOfPlantsNecessaryGet()
this.PlantsEquipmentsGets()
this.clientsUpdates()
this.clientsUpdatesCount()
this.typesOfPlantsGet()
this.typesOfPlantsMainGet()
this.generalCommGet()
this.generalCommGetc()
  }
  usersDetails(){ 
    this.animals.usersDetails(this.usersID).subscribe((res)=>{
    this.marriageDetailsClients=res;
    
    console.log(res);
     })
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    typesOfPlants(){
      this.typesPlants=!this.typesPlants
      this.showmains=false
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.typesPlants=false

    }

// **********************************************animals food  ************************/

handleFileInputUpdatesOne($event:any,img) {
      let imageFileOne= File = $event.target.files[0];
      this.formData.append(img,imageFileOne);
        
    }
    handleFileInputUpdatesTwo($event:any,img) {
      let imageFileTwo= File = $event.target.files[0];
      this.formData.append(img,imageFileTwo);
        
    }
    handleFileInputUpdatesThree($event:any,img) {
      let imageFileThree= File = $event.target.files[0];
      this.formData.append(img,imageFileThree);
        
    }
    
    postPlantsDetails(){
      this.formData.append('foodType',this.fer.foodType);
     this.formData.append('foodQuantity',this.fer.foodQuantity);
    this.formData.append('foodTimmings',this.fer.foodTimmings);
    this.formData.append('foodPrecations',this.fer.foodPrecations);
    this.formData.append('foodStorages',this.fer.foodStorages);
    this.formData.append('foodImagesDes1',this.fer.foodImagesDes1);
    this.formData.append('foodImagesDes2',this.fer.foodImagesDes2);
    this.formData.append('foodImagesDes3',this.fer.foodImagesDes3);
    this.animals.postPlantsDetails(this.plantsDetailsMainId,this.formData).subscribe((res)=>{
    
     })
     this.fer.foodType="",
     this.fer.foodQuantity="",
     this.fer.foodTimmings="",
     this.fer.foodPrecations="",
     this.fer.foodStorages="",
     this.fer.foodImagesDes1="",
     this.fer.foodImagesDes2="",
     this.fer.foodImagesDes3="",
     this.fer.foodImages1="",
     this.fer.foodImages2="",
     this.fer.foodImages3=""
    }

    plantsFertilizersGets(){ 
      this.animals.plantsFertilizersGets(this.plantsDetailsMainId).subscribe((res)=>{
      this.plantsFertilizersGet=res
      console.log(res);
       })
      }
      
// **********************************************necessaries  ************************/

handleFileInputUpdatesequ1($event:any,img) {
  let imageFileNecessaryOne= File = $event.target.files[0];
  this.formData.append(img,imageFileNecessaryOne);
    
}
 

postPlantsEquipmentsDetails(){
  this.formData.append('necessariesName',this.equ.necessariesName);
 
this.animals.postPlantsEquipmentsDetails(this.plantsDetailsNecessaryId,this.formData).subscribe((res)=>{

 })
 this.equ.necessariesName="",
 this.equ.nessariesImages=""
 
}
typesOfPlantsNecessaryGet(){   
  this.animals.typesOfPlantsGet(this.usersID).subscribe((res)=>{
    this.typesOfPlantsNecessaryGets=res
    var fruty=this.typesOfPlantsNecessaryGets[0]
this.typesOfPlantsNecessaryGetClick(fruty)
    })
 }
 typesOfPlantsNecessaryGetClick(get?){
this.plantsDetailsNecessaryId=get._id
this.plantsDetailsNecessaryName=get.animalsTypes
this.PlantsEquipmentsGets()
 }
PlantsEquipmentsGets(){ 
  this.animals.PlantsEquipmentsGets(this.plantsDetailsNecessaryId).subscribe((res)=>{
  this.PlantsEquipmentsGet=res
    })
  }



// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.animals.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.animals.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.animals.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.animals.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.animals.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.animals.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }

 // ***************************************************updades **************************************
 handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 }
handleFileInputUpdate($event:any,viedoes){
  this.videoNames= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoNames);
}

postUpdatesForm(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('descriptions',this.ups.descriptions);
 
 this.animals.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
    console.log('assssss',res);
    })
     this.ups.title="",
     this.ups.descriptions="",
     this.ups.images="",
     this.ups.viedoes=""
}

 // *************************************************** Types of animals **************************************
 typesOfPlantsPosts(){   
  this.animals.typesOfPlantsPosts(this.usersID,this.tr).subscribe((res)=>{
     })
     this.tr.animalsTypes =""
 }
 typesOfPlantsGet(){   
  this.animals.typesOfPlantsGet(this.usersID).subscribe((res)=>{
    this.typesOfPlantsGets=res
    var fruty=this.typesOfPlantsGets[0]
this.typesOfPlantsGetClick(fruty)
    })
 }
 typesOfPlantsGetClick(get?){
this.plantsDetailsId=get._id
this.plantsDetailsName=get.animalsTypes
this.PlantsDescriptionsGets()
 }
 typesOfPlantsMainGet(){   
  this.animals.typesOfPlantsGet(this.usersID).subscribe((res)=>{
    this.typesOfPlantsMainGets=res
    var fruty=this.typesOfPlantsMainGets[0]
this.typesOfPlantsMainGetClick(fruty)
    })
 }
 typesOfPlantsMainGetClick(get?){
this.plantsDetailsMainId=get._id
this.plantsDetailsMainName=get.animalsTypes
 this.plantsFertilizersGets()
 }

 
// *************************************************** description for types of animals **************************************
 
handleFileInputDescriptionsTypeOne($event:any,img) {
  let imageFilePlantsOne= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsOne);
    
}
handleFileInputDescriptionsTypeTwo($event:any,img) {
  let imageFilePlantsTwo= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsTwo);
    
}
handleFileInputDescriptionsTypeThree($event:any,img) {
  let imageFilePlantsThree= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsThree);
    
}

postPlantsDescriptionsDetails(){
  this.formData.append('name',this.des.name);
 this.formData.append('age',this.des.age);
 this.formData.append('food',this.des.food);
this.formData.append('precautions',this.des.precautions);
this.formData.append('descriptions',this.des.descriptions);
this.formData.append('gender',this.des.gender);
this.formData.append('price',this.des.price);
this.formData.append('images1des',this.des.images1des);
this.formData.append('images2des',this.des.images2des);
this.formData.append('images3des',this.des.images3des);
this.animals.postPlantsDescriptionsDetails(this.plantsDetailsId,this.formData).subscribe((res)=>{

 })
 this.des.name="",
 this.des.age="",
 this.des.food="",
 this.des.precautions="",
 this.des.descriptions="",
 this.des.gender="",
 this.des.price="",
 this.des.images1="",
 this.des.images2="",
 this.des.images3="",
 this.des.images1des="",
 this.des.images2des="",
 this.des.images3des=""

}

PlantsDescriptionsGets(){ 
  this.animals.PlantsDescriptionsGets(this.plantsDetailsId).subscribe((res)=>{
  this.PlantsDescriptionsGet=res
  console.log(res);
   })
  }
  generalCommGet(){ 
    this.animals.generalsCommGet(this.usersID).subscribe((res)=>{
    this.gcomm=res
       })
    }
  generalCommGetc(){ 
    this.animals.generalsCommGetC(this.usersID).subscribe((res)=>{
    this.gcommc=res
      })
    }
  }
