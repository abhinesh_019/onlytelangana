import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminanimalsService {

  constructor(private http: HttpClient) { }
 
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/animalsClientInd/"+id);
  }
  postPlantsDetails(id,data){
    return this.http.post(environment.apiUrl + "/animalsfoodpostsdata/"+id,data);
     }

     plantsFertilizersGets(id){
    
      return this.http.get(environment.apiUrl +"/animalsfoodget/"+id);
    }

    postPlantsEquipmentsDetails(id,data){
      return this.http.post(environment.apiUrl + "/animalsNecessariesPosts/"+id,data);
       }
  
       PlantsEquipmentsGets(id){
      
        return this.http.get(environment.apiUrl +"/animalsNecessariesGet/"+id);
      }
 
      clientsUpdatesGet(id){
    
        return this.http.get(environment.apiUrl +"/animalsupdatesget/"+id);
      }
      
      clientsUpdatesGetCounts(id){
        
        return this.http.get(environment.apiUrl +"/animalsupdatesgetCounts/"+id);
      }
      commentsPoststoClints(id,data){
        
        return this.http.post(environment.apiUrl +"/animalsupdatesCommentReplysPost/"+id,data);
      }
      commentsGettoClints(id){
        
        return this.http.get(environment.apiUrl +"/animalsupdatesCommentsget/"+id);
      }
      commentsGettoClintsCounts(id){
        
        return this.http.get(environment.apiUrl +"/animalsupdatesCommentsgetcounts/"+id);
      }
      replyFromCommentesGet(id){
        
        return this.http.get(environment.apiUrl +"/animalsupdatesCommentReplysGet/"+id);
      }
      postUpdatesForm(id,data){
        
        return this.http.post(environment.apiUrl +"/animalsupdatesgetpostsdata/"+id,data);
      }
      typesOfPlantsPosts(id,data){
        
        return this.http.post(environment.apiUrl +"/animalsTypes/"+id,data);
      }
      typesOfPlantsGet(id){
        
        return this.http.get(environment.apiUrl +"/animalsTypes/"+id);
      }

      postPlantsDescriptionsDetails(id,data){
        
        return this.http.post(environment.apiUrl +"/animalsTypesdesc/"+id,data);
      }
      PlantsDescriptionsGets(id){
        
        return this.http.get(environment.apiUrl +"/animalsTypesdes/"+id);
      }

      generalsCommGet(id) {
        return this.http.get(environment.apiUrl + "/animalsuserscomments/"+id);
      }
      generalsCommGetC(id) {
        return this.http.get(environment.apiUrl + "/animalsuserscommentsCounts/"+id);
      }
}
