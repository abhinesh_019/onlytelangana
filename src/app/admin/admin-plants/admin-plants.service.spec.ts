import { TestBed, inject } from '@angular/core/testing';

import { AdminPlantsService } from './admin-plants.service';

describe('AdminPlantsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminPlantsService]
    });
  });

  it('should be created', inject([AdminPlantsService], (service: AdminPlantsService) => {
    expect(service).toBeTruthy();
  }));
});
