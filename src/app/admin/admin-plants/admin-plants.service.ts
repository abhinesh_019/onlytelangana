import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminPlantsService {

  constructor(private http: HttpClient) { }
 
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/plantsClientIdInd/"+id);
  }
  postPlantsDetails(id,data){
    return this.http.post(environment.apiUrl + "/plantsPlantsFertilizersgetpostsdata/"+id,data);
     }

     plantsFertilizersGets(id){
    
      return this.http.get(environment.apiUrl +"/plantsPlantsFertilizersget/"+id);
    }

    postPlantsEquipmentsDetails(id,data){
      return this.http.post(environment.apiUrl + "/PlantsNecessaryMaintaincespostsdata/"+id,data);
       }
  
       PlantsEquipmentsGets(id){
      
        return this.http.get(environment.apiUrl +"/PlantsNecessaryMaintaincesget/"+id);
      }




      clientsUpdatesGet(id){
    
        return this.http.get(environment.apiUrl +"/plantsupdatesget/"+id);
      }
      
      clientsUpdatesGetCounts(id){
        
        return this.http.get(environment.apiUrl +"/plantsupdatesgetCounts/"+id);
      }
      commentsPoststoClints(id,data){
        
        return this.http.post(environment.apiUrl +"/plantsupdatesCommentReplysPost/"+id,data);
      }
      commentsGettoClints(id){
        
        return this.http.get(environment.apiUrl +"/plantsupdatesCommentsget/"+id);
      }
      commentsGettoClintsCounts(id){
        
        return this.http.get(environment.apiUrl +"/plantsupdatesCommentsgetcounts/"+id);
      }
      replyFromCommentesGet(id){
        
        return this.http.get(environment.apiUrl +"/plantsupdatesCommentReplysGet/"+id);
      }
      postUpdatesForm(id,data){
        
        return this.http.post(environment.apiUrl +"/plantsupdatesgetpostsdata/"+id,data);
      }
      typesOfPlantsPosts(id,data){
        
        return this.http.post(environment.apiUrl +"/plantsTypes/"+id,data);
      }
      typesOfPlantsGet(id){
        
        return this.http.get(environment.apiUrl +"/plantsTypes/"+id);
      }

      postPlantsDescriptionsDetails(id,data){
        
        return this.http.post(environment.apiUrl +"/plantsTypesdesc/"+id,data);
      }
      PlantsDescriptionsGets(id){
        
        return this.http.get(environment.apiUrl +"/plantsTypesdes/"+id);
      }

      generalsCommGet(id) {
        return this.http.get(environment.apiUrl + "/plantsuserscomments/"+id);
      }
      generalsCommGetC(id) {
        return this.http.get(environment.apiUrl + "/plantsuserscommentsCounts/"+id);
      }
}
