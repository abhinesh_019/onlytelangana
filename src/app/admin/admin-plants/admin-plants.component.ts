import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminPlantsService } from './admin-plants.service';

@Component({
  selector: 'app-admin-plants',
  templateUrl: './admin-plants.component.html',
  styleUrls: ['./admin-plants.component.css']
})
export class AdminPlantsComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public marriageDetailsClients:any
public viewsProfiles:any
public viewsNoti:any
public plantsFertilizersGet:any
public PlantsEquipmentsGet:any
public imageNameupdates:any
public videoNames:any
public typesOfPlantsGets:any
public plantsDetailsId:any
public plantsDetailsName:any
public gcommc:any
public gcomm:any
public PlantsDescriptionsGet:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any

fer={
  fertilizersName:"",
  fertilizersPrecautions:"",
  descriptions:"",
  price:"",
  images1:"",
  images2:"",
  images3:"",
}
equ={
  EquipmentName:"",
  precautions:"",
  descriptions:"",
  price:"",
  images1:"",
  images2:"",
  images3:"",

}

ups={
  images:"",
    title:"",
    descriptions:"",
    viedoes:"",
}
tr={
  plantsTypes:"",
}
des={
    name:"",
    age:"",
    precautions:"",
    descriptions:"",
    price:"",
    images1:"",
    images2:"",
    images3:"",
}
formData: FormData = new FormData(); 

public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public typesPlants:boolean=false

  public animalsuser:any
  public ids:any
  public animalsTypes:any
  public animalsNames:any
  public animalsId:any
  public animalsTypesDes:any
  public selectAnimals:any
  public animalsTypesDesInd:any
  public clientUpdatesCount:any
  public clientUpdates:any
  public commentspostsId:any
  public getUsersComments:any
  public getUsersCommentsCounts:any
  public openshareid:any
  public viewcommentsid:any
  public replyid:any
  public replies:any
  public animalsTypesCnts:any
  public clientUpdatesCnts:any
  public plantsMaintaincs:any
  public plantsMaintaincsC:any
  public plantsFertilizerss:any
  public plantsFertilizersC:any
 
  postComments={
    "descriptions":""
  }
constructor(private route:ActivatedRoute,private plants:AdminPlantsService) { }
 ngOnInit() {
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.plantsFertilizersGets()
this.PlantsEquipmentsGets()
this.clientsUpdates()
this.clientsUpdatesCount()
this.typesOfPlantsGet()
this.generalCommGet()
this.generalCommGetc()
  }
  usersDetails(){ 
    this.plants.usersDetails(this.usersID).subscribe((res)=>{
    this.marriageDetailsClients=res
    console.log(res);
     })
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    typesOfPlants(){
      this.typesPlants=!this.typesPlants
      this.showmains=false
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
      this.typesPlants=false

    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.typesPlants=false

    }

// **********************************************fertilizers and maintainces ************************/

handleFileInputUpdatesOne($event:any,img) {
      let imageFileOne= File = $event.target.files[0];
      this.formData.append(img,imageFileOne);
        
    }
    handleFileInputUpdatesTwo($event:any,img) {
      let imageFileTwo= File = $event.target.files[0];
      this.formData.append(img,imageFileTwo);
        
    }
    handleFileInputUpdatesThree($event:any,img) {
      let imageFileThree= File = $event.target.files[0];
      this.formData.append(img,imageFileThree);
        
    }
    
    postPlantsDetails(){
      this.formData.append('descriptions',this.fer.descriptions);
     this.formData.append('fertilizersName',this.fer.fertilizersName);
    this.formData.append('fertilizersPrecautions',this.fer.fertilizersPrecautions);
    this.formData.append('images1',this.fer.images1);
    this.formData.append('images2',this.fer.images2);
    this.formData.append('images3',this.fer.images3);
    this.formData.append('price',this.fer.price);
    this.plants.postPlantsDetails(this.usersID,this.formData).subscribe((res)=>{
    
     })
     this.fer.descriptions="",
     this.fer.fertilizersName="",
     this.fer.fertilizersPrecautions="",
     this.fer.images1="",
     this.fer.images2="",
     this.fer.images2="",
     this.fer.images3="",
     this.fer.price=""
    }

    plantsFertilizersGets(){ 
      this.plants.plantsFertilizersGets(this.usersID).subscribe((res)=>{
      this.plantsFertilizersGet=res
      console.log(res);
       })
      }
// **********************************************necessaries and equipments ************************/

handleFileInputUpdatesequ1($event:any,img) {
  let imageFileNecessaryOne= File = $event.target.files[0];
  this.formData.append(img,imageFileNecessaryOne);
    
}
handleFileInputUpdatesequ2($event:any,img) {
  let imageFileNecessaryTwo= File = $event.target.files[0];
  this.formData.append(img,imageFileNecessaryTwo);
    
}
handleFileInputUpdatesequ3($event:any,img) {
  let imageFileNecessaryThree= File = $event.target.files[0];
  this.formData.append(img,imageFileNecessaryThree);
    
}

postPlantsEquipmentsDetails(){
  this.formData.append('descriptions',this.equ.descriptions);
 this.formData.append('EquipmentName',this.equ.EquipmentName);
this.formData.append('precautions',this.equ.precautions);
this.formData.append('images1',this.equ.images1);
this.formData.append('images2',this.equ.images2);
this.formData.append('images3',this.equ.images3);
this.formData.append('price',this.equ.price);
this.plants.postPlantsEquipmentsDetails(this.usersID,this.formData).subscribe((res)=>{

 })
 this.equ.descriptions="",
 this.equ.EquipmentName="",
 this.equ.precautions="",
 this.equ.images1="",
  this.equ.images2="",
 this.equ.images3="",
 this.equ.price=""
}

PlantsEquipmentsGets(){ 
  this.plants.PlantsEquipmentsGets(this.usersID).subscribe((res)=>{
  this.PlantsEquipmentsGet=res
  console.log(res);
   })
  }



// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.plants.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.plants.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.plants.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.plants.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.plants.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.plants.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }

 // ***************************************************updades **************************************
 handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 }
handleFileInputUpdate($event:any,viedoes){
  this.videoNames= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoNames);
}

postUpdatesForm(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('descriptions',this.ups.descriptions);
 
 this.plants.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
    console.log('assssss',res);
    })
     this.ups.title="",
     this.ups.descriptions="",
     this.ups.images="",
     this.ups.viedoes=""
}

 // *************************************************** Types of Plants **************************************
 typesOfPlantsPosts(){   
  this.plants.typesOfPlantsPosts(this.usersID,this.tr).subscribe((res)=>{
     })
     this.tr.plantsTypes =""
 }
 typesOfPlantsGet(){   
  this.plants.typesOfPlantsGet(this.usersID).subscribe((res)=>{
    this.typesOfPlantsGets=res
    var fruty=this.typesOfPlantsGets[0]
this.typesOfPlantsGetClick(fruty)
    })
 }
 typesOfPlantsGetClick(get?){
this.plantsDetailsId=get._id
this.plantsDetailsName=get.plantsTypes
this.PlantsDescriptionsGets()
 }

// *************************************************** description for types of plants **************************************
 
handleFileInputDescriptionsTypeOne($event:any,img) {
  let imageFilePlantsOne= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsOne);
    
}
handleFileInputDescriptionsTypeTwo($event:any,img) {
  let imageFilePlantsTwo= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsTwo);
    
}
handleFileInputDescriptionsTypeThree($event:any,img) {
  let imageFilePlantsThree= File = $event.target.files[0];
  this.formData.append(img,imageFilePlantsThree);
    
}

postPlantsDescriptionsDetails(){
  this.formData.append('name',this.des.name);
 this.formData.append('age',this.des.age);
this.formData.append('precautions',this.des.precautions);
this.formData.append('descriptions',this.des.descriptions);
this.formData.append('images2',this.des.images2);
this.formData.append('images3',this.des.images3);
this.formData.append('price',this.des.price);
this.plants.postPlantsDescriptionsDetails(this.plantsDetailsId,this.formData).subscribe((res)=>{

 })
 this.des.name="",
 this.des.age="",
 this.des.precautions="",
 this.des.images1="",
 this.des.descriptions="",
 this.des.images2="",
 this.des.images3="",
 this.des.price=""
}

PlantsDescriptionsGets(){ 
  this.plants.PlantsDescriptionsGets(this.plantsDetailsId).subscribe((res)=>{
  this.PlantsDescriptionsGet=res
  console.log(res);
   })
  }
  generalCommGet(){ 
    this.plants.generalsCommGet(this.usersID).subscribe((res)=>{
    this.gcomm=res
       })
    }
  generalCommGetc(){ 
    this.plants.generalsCommGetC(this.usersID).subscribe((res)=>{
    this.gcommc=res
      })
    }
  }