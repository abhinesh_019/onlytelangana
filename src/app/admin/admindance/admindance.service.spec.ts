import { TestBed, inject } from '@angular/core/testing';

import { AdmindanceService } from './admindance.service';

describe('AdmindanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdmindanceService]
    });
  });

  it('should be created', inject([AdmindanceService], (service: AdmindanceService) => {
    expect(service).toBeTruthy();
  }));
});
