import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdmindanceService } from './admindance.service';

@Component({
  selector: 'app-admindance',
  templateUrl: './admindance.component.html',
  styleUrls: ['./admindance.component.css']
})
export class AdmindanceComponent implements OnInit {
  public showmains:boolean=false
  public updatesShows:boolean=false
  public serviceShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
   public serviceType:any
  public usersID:any
  public musicTypeId:any
  public musicTypeName:any
  public imageNameupdates:any
  public videoName:any
  public servicegets:any
  public servicegetsc:any
  public imageName:any
  public catget:any
  public  musClients:any
  public fecilities:any
  public fecilitiesc:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public openshareid:any
  public clientUpdates:any
  public clientUpdatesCount:any
  public commentspostsId:any
  public getUsersComments:any
  public getUsersCommentsCounts:any
   public viewcommentsid:any
   public replies:any
   public replyid:any
   public musicDetailsClients:any
   public gcomm:any
   public gcommc:any
   public servicesCatageriesGets:any
   public servicesCatageriesGetName:any
   public servicesCatageriesGetId:any
   public videoNamees:any
   public masterImgs:any
   public serviceTwoGets:any
   public servicegetsTwoc:any
   public imageNameCat:any
   public catageriesGetss:any
   public catageriesDanceName:any
   public catageriesDanceNameId:any
   public viewsProfiles:any
   public viewsNoti:any
  //  public servicesCatageriesGetId:any
  //  public servicesCatageriesGetId:any
  //  public servicesCatageriesGetId:any
  //  public servicesCatageriesGetId:any
   
  sec={
    danceType:"",
    
  }
  ser={
       masterName:"",
       masterYearOfExp:"",
      descriptions:"",
      masterImg:"",
      images:"",
      viedeo:"",
  }
  fec={
    name:"",
       importantsKey:"",
      descriptions:"",
      images:"",
   }
  
   ups={
    images:"",
      title:"",
      descriptions:"",
      viedoes:"",
  }
  postComments={
    "descriptions":""
  }
  danceCat={
    danceCatatgeries:"",
    danceImg:"",
  }
  
  formData: FormData = new FormData(); 
  
  
  constructor(private route:ActivatedRoute,private mus:AdmindanceService) { }
  
  
    ngOnInit() {
       this.usersID=localStorage.getItem('ads');

        this.usersDetails()
        this.fecilitiesGet()
       this.fecilitiesGetC()
       this.clientsUpdates()
       this.clientsUpdatesCount()
       this.generalCommGet()
       this.generalCommGetc()
       this.servicesCatageriesGet()
       this.CatageriesGet()
     }
     viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
  
  usersDetails(){ 
  this.mus.usersDetails(this.usersID).subscribe((res)=>{
  this.musicDetailsClients=res
  
  
  })
  }
  
  
  // <!-- **************************************************************** create services catageries *************************************************** -->
  
  
  serviceTypesCat(){ 
   this.mus.serviceTypesCat(this.catageriesDanceNameId,this.sec).subscribe((res)=>{
    })
   this.sec.danceType=""
      
                     }

                     
                     servicesCatageriesGet(){
                      this.mus.servicesCatageriesGet(this.catageriesDanceNameId).subscribe((res)=>{
                        this.servicesCatageriesGets=res
                        var serCat=this.servicesCatageriesGets[0]
                        this.servicesCatageriesGetClick(serCat)
                         console.log(res);
                          })
                        }
                        servicesCatageriesGetClick(get){
this.servicesCatageriesGetId=get._id
this.servicesCatageriesGetName=get.danceType
console.log(this.servicesCatageriesGetId);
console.log(this.servicesCatageriesGetName);
this.serviceTwoGet()
                         }


                                       
      // <!-- ***************************************************************************************************************** -->
     // <!-- **********************************  servies two   ******************************************************************************* -->
      handleFileInputUpdate($event:any,images){
    this.imageNameupdates= File = $event.target.files[0];
    }
   handleFileInputUpdat($event:any,viedoes){
    this.masterImgs= File = $event.target.files[0];
     }
     handleFileInputUpdatessV($event:any,viedoes){
    this.videoNamees= File = $event.target.files[0];
    }
    updatesPostData(){
      this.formData.append('masterName',this.ser.masterName);
      this.formData.append('masterYearOfExp',this.ser.masterYearOfExp);
      this.formData.append('descriptions',this.ser.descriptions);
      this.formData.append('masterImg',this.masterImgs);
      this.formData.append('viedeo',this.videoNamees);
      this.formData.append('images',this.imageNameupdates);
      this.mus.updatesPostData(this.servicesCatageriesGetId,this.formData).subscribe((res)=>{
    
       })
            this.ser.masterName="",
            this.ser.masterYearOfExp="",
            this.ser.masterName="",
            this.ser.descriptions="",
            this.ser.masterImg=""
            this.ser.images=""
            this.ser.viedeo=""
  }
 
  
  
  serviceTwoGet(){   
    this.mus.serviceTwoGet(this.servicesCatageriesGetId).subscribe((res)=>{
       this.serviceTwoGets=res
   
       
      })
   }
  
 
  
        // <!-- ***************************************************************************************************************** -->

  // <!-- **************************************************************** create Fecilities*************************************************** -->
  handleFileInputFec($event:any,images){
    this.imageName= File = $event.target.files[0];
   
  }
  
  updatesPostDataFec(){ 
    this.formData.append('images',this.imageName);
  this.formData.append('name',this.fec.name);
  this.formData.append('importantsKey',this.fec.importantsKey);
  this.formData.append('descriptions',this.fec.descriptions);
   this.mus.updatesPostDataFec(this.usersID,this.formData).subscribe((res)=>{
     
   })
   this.fec.name="",
   this.fec.importantsKey="",
   this.fec.descriptions="",
     this.fec.images=""
                     }
                     fecilitiesGet(){   
                      this.mus.fecilitiesGet(this.usersID).subscribe((res)=>{
                        this.fecilities=res
                         })
                     }
                     fecilitiesGetC(){   
                      this.mus.fecilitiesGetC(this.usersID).subscribe((res)=>{
                        this.fecilitiesc=res
                         })
                     }
                                       
      // <!-- ***************************************************************************************************************** -->
     

  
     
    
    mainMenuShow(){
  this.showmains=!this.showmains
  this.updatesShows=false
  this.serviceShows=false
  this.onlyCommentsShows=false
  this.ImagesShows=false
  this.ProductsShows=false
     }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.serviceShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
     }
    ServiceShow(){
      this.serviceShows=!this.serviceShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
     }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.serviceShows=false
      this.ImagesShows=false
      this.ProductsShows=false
     }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ProductsShows=false
     }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.serviceShows=false
      this.ImagesShows=false
     }
    // ***************************************************updades **************************************
    handleFileInputUpdatesI($event:any,images){
    this.imageNameupdates= File = $event.target.files[0];
   
  }
  
  handleFileInputUpdatesV($event:any,viedoes){
    this.videoName= File = $event.target.files[0];
    this.formData.append(viedoes,this.videoName);
   
   
  }
  
  updatesPostDatas(){
  this.formData.append('images',this.imageNameupdates);
  this.formData.append('title',this.ups.title);
  this.formData.append('descriptions',this.ups.descriptions);
  
   this.mus.updatesPostDatas(this.usersID,this.formData).subscribe((res)=>{
      
       })
       this.ups.title="",
       this.ups.descriptions="",
       this.ups.images="",
       this.ups.viedoes=""
  }
  
  // ******************************************updates present ***************************************
  shareClick(up){
    this.share=!this.share
    this.comments=false
    this.viewcomments=false
    this.replyfrom=false
    this.openshareid=up._id
  }
  commentsClick(up){
    this.comments=!this.comments
    this.share=false
    this.viewcomments=false
    this.commentspostsId=up._id
     this.commentsGettoClints()
     this.commentsGettoClintsCounts()
   }
  
  viewCommentsClick(up){
    this.viewcomments=!this.viewcomments
    this.share=false
    this.viewcommentsid=up._id
  }
  replyFromm(comms){
    this.replyfrom=!this.replyfrom
    this.share=false
    this.replyid=comms._id
    this.replyFromCommentesGet()
  
  }
   clientsUpdates(){   
    this.mus.clientsUpdatesGet(this.usersID).subscribe((res)=>{
      this.clientUpdates=res
     })
   }
   
   clientsUpdatesCount(){   
    this.mus.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
      this.clientUpdatesCount=res
       })
   }
  
   commentsPoststoClints(){   
    this.mus.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
      
    })
       this.postComments.descriptions=""
   }
   commentsGettoClints(){   
    this.mus.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
      this.getUsersComments=res
     })
   }
  
   commentsGettoClintsCounts(){   
    this.mus.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
      this.getUsersCommentsCounts=res
      })
   }
  
   replyFromCommentesGet(){   
    this.mus.replyFromCommentesGet(this.replyid).subscribe((res)=>{
      this.replies=res
    
      
      })
   }
   generalCommGet(){ 
    this.mus.generalsCommGet(this.usersID).subscribe((res)=>{
    this.gcomm=res
       })
    }
  
    generalCommGetc(){ 
      this.mus.generalsCommGetC(this.usersID).subscribe((res)=>{
      this.gcommc=res
        })
      }

       // <!-- **************************************************************** dance catagories************************************************** -->
  handleFileInputCat($event:any,images){
    this.imageNameCat= File = $event.target.files[0];
   
  }
  
  updatesPostDataCat(){ 
    this.formData.append('danceImg',this.imageNameCat);
  this.formData.append('danceCatatgeries',this.danceCat.danceCatatgeries);
 
   this.mus.updatesPostDataCats(this.usersID,this.formData).subscribe((res)=>{
     
   })

   this.danceCat.danceCatatgeries="",
     this.danceCat.danceImg=""
                     }

                     CatageriesGet(){   
                      this.mus.CatageriesGets(this.usersID).subscribe((res)=>{
                        this.catageriesGetss=res
                         })
                     }
                     catageriesGetClick(get){
                       this.catageriesDanceName=get.danceCatatgeries
                       this.catageriesDanceNameId=get._id
                       this. servicesCatageriesGet()
                     }
   
                                       
      // <!-- ***************************************************************************************************************** -->
     

  }
  
