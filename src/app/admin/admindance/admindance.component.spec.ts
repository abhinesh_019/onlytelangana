import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmindanceComponent } from './admindance.component';

describe('AdmindanceComponent', () => {
  let component: AdmindanceComponent;
  let fixture: ComponentFixture<AdmindanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmindanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmindanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
