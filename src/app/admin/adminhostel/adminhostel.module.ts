import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminhostelComponent } from './adminhostel.component';
import { AdminhostelService } from './adminhostel.service';
 

const routes:Routes=[{path:'adminhostels',component:AdminhostelComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminhostelComponent,

  ],
  providers: [AdminhostelService],
})
export class AdminhostelModule { }
