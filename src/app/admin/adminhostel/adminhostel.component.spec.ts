import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminhostelComponent } from './adminhostel.component';

describe('AdminhostelComponent', () => {
  let component: AdminhostelComponent;
  let fixture: ComponentFixture<AdminhostelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminhostelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminhostelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
