import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminhostelService } from './adminhostel.service';

@Component({
  selector: 'app-adminhostel',
  templateUrl: './adminhostel.component.html',
  styleUrls: ['./adminhostel.component.css']
})
export class AdminhostelComponent implements OnInit {
  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false
  public usersID:any
  public hostelClients:any
  public imgFec:any
  public fecilitiesGet:any
  public fecilitiesGetc:any
  public viewsProfiles:any
  public viewsNoti:any
  public commentspostsId:any
  public viewcommentsid:any
  public clientUpdates:any
  public replyid:any
  public clientUpdatesCount:any
   public getUsersComments:any
  public getUsersCommentsCounts:any
  public share:boolean=false
  public comments:boolean=false
  public viewcomments:boolean=false
  public replyfrom:boolean=false 
  public replies:any
  public openshareid:any
public imageNameupdates:any
  public videoNames:any

  public gcomm:any
  public gcommc:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  // public ProductsViewShows:any
  
postComments={
  "descriptions":""
}
ups={
  images:"",
    title:"",
    descriptions:"",
    viedoes:""
}
formData: FormData = new FormData(); 

fec={

  fecilitiesName:"",
  fecilitiesImg:"",
  fecilitiesDescriptions:"",
}

  constructor(private route:ActivatedRoute,private hostels:AdminhostelService) { }

  
    ngOnInit() {
      this.usersID=localStorage.getItem('ads');
      this.usersDetails()
      this.fecilities()
      this.fecilitiesc()
      this.clientsUpdates()
      this.clientsUpdatesCount()
      this.generalCommGet()
      this.generalCommGetc()
    }
  
    mainMenuShow(){
  this.showmains=!this.showmains
  this.updatesShows=false
  this.viewupdatesShows=false
  this.onlyCommentsShows=false
  this.ImagesShows=false
  this.ProductsShows=false
  this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }
    usersDetails(){ 
      this.hostels.usersDetails(this.usersID).subscribe((res)=>{
      this.hostelClients=res
        })
      }

      // ************************************************fecilities **********************************
handleFileFec($event:any,images){
  this.imgFec= File = $event.target.files[0];
  }

  postFecilities(){
  
  this.formData.append('fecilitiesImg',this.imgFec);
 this.formData.append('fecilitiesName',this.fec.fecilitiesName);
 this.formData.append('fecilitiesDescriptions',this.fec.fecilitiesDescriptions);
 
 this.hostels.postFecilities(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.fec.fecilitiesDescriptions="",
     this.fec.fecilitiesImg="",
     this.fec.fecilitiesName=""
   }

   fecilities(){ 
    this.hostels.fecilities(this.usersID).subscribe((res)=>{
    this.fecilitiesGet=res
    console.log(this.fecilitiesGet);
    
    })
    }

    fecilitiesc(){ 
      this.hostels.fecilitiesc(this.usersID).subscribe((res)=>{
      this.fecilitiesGetc=res 
      console.log(this.fecilitiesGetc);   
      })
      }
      viewProfile(){
        this.viewsProfiles=!this.viewsProfiles
        this.viewsNoti=false
      }
      viewnoti(){
        this.viewsNoti=!this.viewsNoti
        this.viewsProfiles=false
      }

       // ******************************************updates present ***************************************
  handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 
}

handleFileInputUpdate($event:any,viedoes){
  this.videoNames= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoNames);
 
 
}

postUpdatesForm(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('descriptions',this.ups.descriptions);
 

 

 this.hostels.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
  
    })
     this.ups.title="",
     this.ups.descriptions="",
     this.ups.images="",
     this.ups.viedoes=""
}

shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
 
  
}
 clientsUpdates(){   
  this.hostels.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.hostels.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.hostels.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.hostels.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.hostels.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.hostels.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
  
    
    })
 }


 generalCommGet(){ 
  this.hostels.generalsCommGet(this.usersID).subscribe((res)=>{
  this.gcomm=res
     })
  }

  generalCommGetc(){ 
    this.hostels.generalsCommGetC(this.usersID).subscribe((res)=>{
    this.gcommc=res
      })
    }
  }
  
