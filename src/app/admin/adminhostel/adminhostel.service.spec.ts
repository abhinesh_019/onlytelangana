import { TestBed, inject } from '@angular/core/testing';

import { AdminhostelService } from './adminhostel.service';

describe('AdminhostelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminhostelService]
    });
  });

  it('should be created', inject([AdminhostelService], (service: AdminhostelService) => {
    expect(service).toBeTruthy();
  }));
});
