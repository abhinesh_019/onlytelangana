import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminPackersMoversService } from './admin-packers-movers.service';

@Component({
  selector: 'app-admin-packers-movers',
  templateUrl: './admin-packers-movers.component.html',
  styleUrls: ['./admin-packers-movers.component.css']
})
export class AdminPackersMoversComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false

  public usersID:any
public marriageDetailsClients:any
public viewsProfiles:any
public viewsNoti:any
public serviceOneGets:any
public serviceOneToTwoClickName:any
public serviceOneToTwoClickId:any
public serviceOneToTwoClickIdsMains:any
public serviceOneToTwoClickNamesMains:any
public serviceTwoGets:any
public serviceTwoGetsCount:any
public fecilitiesGets:any
public fecilitiesGetsCount:any
public trackRecordsGet:any
public trackRecordsGetCounts:any

 
public fireWorksClients:any
public servicesOneGet:any
public servicevehicleName:any
public servicesIds:any
public servicesOneGets:any
public servicevehicleNames:any
public servicesone:any
public servicesTwos:any
public trackRecordsId:any
public trackRecordsName:any
 public trackRecordsgetsData:any
public servicesOneGetc:any
public trackRecordsgetsDatac:any
public servicesTwosc:any
public totalCustomer:any
public totalCustomerc:any
 public fecilitiesGetsc:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
public openshareid:any
public imageNameupdates:any
public videoNames:any
public gcomm:any
public gcommc:any
public fireWorksDetails:any
public serviceTwoGetc:any
public fireWorksDetailsId:any
public fireWorksDetailsName:any
public cusotmersgetDetails:any
public cusotmersgetDetailsc:any

// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
catOne={
  vehicleName:"",
  vehicleCapacity:"",
  vehicleDescriptions:"",
  driverName:"",
  driverYearOfExp:"",
  DriverImg:"",
  vehicleImg:"",
}
catTwo={
  vehicleName:"",
  CustomerName:"",
  CustomerPlace:"",
  customerStartingPlace:"",
  customerEndingPlace:"",
  totalDistance:"",
  totalTimeTaken:"",
  itemName:"",
  totalPrice:"",
  customerImg:"",
  itemgLoadingImg1:"",
  itemgLoadingImg2:"",
  itemgLoadingImg3:"",
  itemgLoadingImg4:"",
}
fec={
  fecilitiesName:"",
  fecilitiesImg:"",
  fecilitiesDescriptions:""
}

tec={
  customerName: "",
  place: "",
  customersImg: ""
}


postComments={
  "descriptions":""
}
ups={
  images:"",
    title:"",
    description:"",
    viedoes:"",
}
formData: FormData = new FormData(); 


constructor(private route:ActivatedRoute,private packers:AdminPackersMoversService) { }
 ngOnInit() {
    this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.serviceOneGet()
this.fecilitiesGet()
this.fecilitiesGetCount()
this.trackRecordsGets()
this.trackRecordsGetsCounts()
this.clientsUpdates()
this.clientsUpdatesCount()
this.generalCommGet()
this.generalCommGetc()
   }
  usersDetails(){ 
    this.packers.usersDetails(this.usersID).subscribe((res)=>{
    this.marriageDetailsClients=res
  
    })
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }
     // ************************************************ Services one **********************************

     handleFileInputUpdatesI($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img,imageFile);
      }
    
      postUpdatesForm(){
      this.formData.append('vehicleName',this.catOne.vehicleName);
  this.formData.append('vehicleCapacity',this.catOne.vehicleDescriptions);
  this.formData.append('vehicleDescriptions',this.catOne.vehicleDescriptions);
  this.formData.append('driverName',this.catOne.driverName);
  this.formData.append('driverYearOfExp',this.catOne.driverYearOfExp);
  
 this.packers.updateServicesOne(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.catOne.vehicleName="",
     this.catOne.vehicleCapacity="",
     this.catOne.vehicleDescriptions="",
     this.catOne.driverName=""
     this.catOne.driverYearOfExp=""
     this.catOne.DriverImg=""
     this.catOne.vehicleImg=""
   }

   serviceOneGet(){ 
    this.packers.serviceOneGet(this.usersID).subscribe((res)=>{
    this.serviceOneGets=res
    var vsd=this.serviceOneGets[0]
    this.serviceOneCLick(vsd)
   })
    }
    serviceOneCLick(get){
this.serviceOneToTwoClickId=get._id
this.serviceOneToTwoClickName=get.vehicleName
this.serviceTwoGet()
    }



     // ************************************************ ServicesTwo **********************************

     handleFileInputUpdatesTwo($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img,imageFile);
      }
    
      postUpdatesFormTwo(){
      this.formData.append('vehicleName',this.catTwo.vehicleName);
  this.formData.append('CustomerName',this.catTwo.CustomerName);
  this.formData.append('CustomerPlace',this.catTwo.CustomerPlace);
  this.formData.append('customerStartingPlace',this.catTwo.customerStartingPlace);
  this.formData.append('customerEndingPlace',this.catTwo.customerEndingPlace);
  this.formData.append('totalDistance',this.catTwo.totalDistance);
  this.formData.append('totalTimeTaken',this.catTwo.totalTimeTaken);
  this.formData.append('itemName',this.catTwo.itemName);
  this.formData.append('totalPrice',this.catTwo.totalPrice);
 this.packers.postUpdatesFormTwo(this.serviceOneToTwoClickId,this.formData).subscribe((res)=>{
  
     })
     
     this.catTwo.vehicleName="",
     this.catTwo.CustomerName="",
     this.catTwo.CustomerPlace="",
     this.catTwo.customerStartingPlace=""
     this.catTwo.customerEndingPlace=""
     this.catTwo.totalDistance=""
     this.catTwo.itemName=""
     this.catTwo.totalPrice=""
     this.catTwo.totalTimeTaken=""
     this.catTwo.itemgLoadingImg1=""
     this.catTwo.itemgLoadingImg2=""
     this.catTwo.itemgLoadingImg3=""
     this.catTwo.itemgLoadingImg4=""
      

   }

   serviceTwoGet(){ 
    this.packers.serviceTwoGet(this.serviceOneToTwoClickId).subscribe((res)=>{
    this.serviceTwoGets=res
      
    })
    }
    serviceTwoGetCount(){ 
      this.packers.serviceTwoGetCount(this.serviceOneToTwoClickId).subscribe((res)=>{
      this.serviceTwoGetsCount=res
        var dfd = this.serviceTwoGetsCount[0]

      })
      }

        // ************************************************ fecilities **********************************

     handleFileInputFecilities($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img,imageFile);
      }
    
      postUpdatesFecilities(){
      this.formData.append('fecilitiesDescriptions',this.fec.fecilitiesDescriptions);
  this.formData.append('fecilitiesName',this.fec.fecilitiesName);
  
  
 this.packers.postUpdatesFecilities(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.fec.fecilitiesDescriptions="",
     this.fec.fecilitiesName="",
     this.fec.fecilitiesImg=""
   }

  fecilitiesGet(){ 
    this.packers.fecilitiesGet(this.usersID).subscribe((res)=>{
    this.fecilitiesGets=res
    console.log(res);
   
    })
    }
    fecilitiesGetCount(){ 
      this.packers.fecilitiesGetCount(this.usersID).subscribe((res)=>{
      this.fecilitiesGetsCount=res
      console.log(res);
   
      })
      }
    // ************************************************ track records **********************************

     handleFileInputTrackRecords($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img,imageFile);
      }
    
      postUpdatesTrackRecords(){
      this.formData.append('customerName',this.tec.customerName);
  this.formData.append('place',this.tec.place);
  
  
 this.packers.postUpdatesTrackRecords(this.usersID,this.formData).subscribe((res)=>{
  
     })
     this.tec.place="",
     this.tec.customersImg="",
     this.tec.customerName=""
   }

   trackRecordsGets(){ 
    this.packers.trackRecordsGets(this.usersID).subscribe((res)=>{
    this.trackRecordsGet=res
     })
    }

    trackRecordsGetsCounts(){ 
      this.packers.trackRecordsGetsCounts(this.usersID).subscribe((res)=>{
      this.trackRecordsGetCounts=res
       })
      }
       // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
 
}
 clientsUpdates(){   
  this.packers.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
 
   })
 }
 
 clientsUpdatesCount(){   
  this.packers.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.packers.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.packers.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.packers.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.packers.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
 })
 }
 
 generalCommGet(){ 
  this.packers.generalsCommGet(this.usersID).subscribe((res)=>{
  this.gcomm=res
     })
  }
generalCommGetc(){ 
  this.packers.generalsCommGetC(this.usersID).subscribe((res)=>{
  this.gcommc=res
    })
  }
// ***************************************************updades **************************************
handleFileInputUpdat($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
 }
handleFileInputUpdate($event:any,viedoes){
  this.videoNames= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoNames);
}

postUpdatesForms(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('description',this.ups.description);
 
 this.packers.postUpdatesForms(this.usersID,this.formData).subscribe((res)=>{
    
    })
     this.ups.title="",
     this.ups.description="",
     this.ups.images="",
     this.ups.viedoes=""
}
 
  }
  




