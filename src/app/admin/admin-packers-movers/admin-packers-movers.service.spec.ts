import { TestBed, inject } from '@angular/core/testing';

import { AdminPackersMoversService } from './admin-packers-movers.service';

describe('AdminPackersMoversService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminPackersMoversService]
    });
  });

  it('should be created', inject([AdminPackersMoversService], (service: AdminPackersMoversService) => {
    expect(service).toBeTruthy();
  }));
});
