import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPackersMoversComponent } from './admin-packers-movers.component';

describe('AdminPackersMoversComponent', () => {
  let component: AdminPackersMoversComponent;
  let fixture: ComponentFixture<AdminPackersMoversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPackersMoversComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPackersMoversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
