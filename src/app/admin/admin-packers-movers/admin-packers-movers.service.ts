import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminPackersMoversService {

  
  constructor(private http: HttpClient) { }
 
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversClientsGetind/"+id);
  }
  updateServicesOne(id,data){
    return this.http.post(environment.apiUrl + "/packersMoversServicesPost/"+id,data);
     }
     serviceOneGet(id){
      return this.http.get(environment.apiUrl + "/packersMoversServicesGet/"+id);
       } 

       postUpdatesFormTwo(id,data){
        return this.http.post(environment.apiUrl + "/packersMoversServicesTwoPost/"+id,data);
         }
         serviceTwoGet(id){
          return this.http.get(environment.apiUrl + "/packersMoversServicesTwoGet/"+id);
           } 
           serviceTwoGetCount(id){
            return this.http.get(environment.apiUrl + "/packersMoversServicesTwoGetCounts/"+id);
             }

             postUpdatesFecilities(id,data){
              return this.http.post(environment.apiUrl + "/packersMoversfecitiesPost/"+id,data);
               }
               fecilitiesGet(id){
                return this.http.get(environment.apiUrl + "/packersMoversfecitiesGet/"+id);
                 }
                 fecilitiesGetCount(id){
                  return this.http.get(environment.apiUrl + "/packersMoversfecitiesGetCounts/"+id);
                   }


                   postUpdatesTrackRecords(id,data){
                    return this.http.post(environment.apiUrl + "/packersMoversServicesTrackRecordsPost/"+id,data);
                     }
                     trackRecordsGets(id){
                      return this.http.get(environment.apiUrl + "/packersMoversServicesTrackRecordsGet/"+id);
                       }
                       trackRecordsGetsCounts(id){
                        return this.http.get(environment.apiUrl + "/packersMoversServicesTrackRecordsGetCounts/"+id);
                         }
                            //  ************************updates present ***************************
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/packersMoversupdatesCommentReplysPost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/packersMoversupdatesCommentReplysGet/"+id);
  }

  generalsCommGet(id) {
    return this.http.get(environment.apiUrl + "/packersMoversUsersComments/"+id);
  }
  generalsCommGetC(id) {
    return this.http.get(environment.apiUrl + "/packersMoversUsersCommentsCounts/"+id);
  }
 
     postUpdatesForms(id,data){
      return this.http.post(environment.apiUrl + "/packersMoversupdatespostsdata/"+id,data);
       } 
     
    }