import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminProfessionalInstituionalComponentsService {

  constructor(private http: HttpClient) { }
 
  usersDetail(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstclientsGetind/"+id);
  }
  postPlantsDescriptionsDetail(id,data){
    return this.http.post(environment.apiUrl + "/professionalInstupdatesAboutUspostsdata/"+id,data);
     }

     aboutUsDetais(id){
    
      return this.http.get(environment.apiUrl +"/professionalInstAboutUsget/"+id);
    }
    OurPhilosophys(id,data){
      return this.http.post(environment.apiUrl + "/professionalInstPhilosophy/"+id,data);
       }
       getPhilosophys(id){
    
        return this.http.get(environment.apiUrl +"/professionalInstPhilosophy/"+id);
      }
      posWhyInsts(id,data){
        return this.http.post(environment.apiUrl + "/professionalInstJourneyWhyTwo/"+id,data);
         }
         getWhyInsts(id){
    
          return this.http.get(environment.apiUrl +"/professionalInstJourneyWhyTwo/"+id);
        }
        infraStructurePost(id,data){
          return this.http.post(environment.apiUrl + "/professionalInfraStructure/"+id,data);
           }
        getinfraStructure(id){
    
          return this.http.get(environment.apiUrl +"/professionalInfraStructure/"+id);
        }


        boardsDirectorsPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalBoardDirectors/"+id,data);
           }
           getboardsDirector(id){
    
          return this.http.get(environment.apiUrl +"/professionalBoardDirectors/"+id);
        }


        AwardsAndRewardsPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalAwardsRewards/"+id,data);
           }
           getAwardsAndRewards(id){
    
          return this.http.get(environment.apiUrl +"/professionalAwardsRewards/"+id);
        }

        catageriesOnePost(id,data){
          return this.http.post(environment.apiUrl + "/professionalSerivesOnep/"+id,data);
           }
           getcatageriesOne(id){
    
          return this.http.get(environment.apiUrl +"/professionalSerivesOne/"+id);
        }

        catageriesTwoPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalSerivesTwop/"+id,data);
           }
           getcatageriesTwo(id){
    
          return this.http.get(environment.apiUrl +"/professionalSerivesTwo/"+id);
        }

        trackRecordsYears(id,data){
          return this.http.post(environment.apiUrl + "/professionalInstyear/"+id,data);
           }
           getTrackRecordsYears(id){
    
          return this.http.get(environment.apiUrl +"/professionalInstyear/"+id);
        }

        branchesPosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalBranchesp/"+id,data);
           }
           getBranches(id){
    
          return this.http.get(environment.apiUrl +"/professionalBranches/"+id);
        }

        syllabusDetailsp(id,data){
          return this.http.post(environment.apiUrl + "/professionalSyllabusOneP/"+id,data);
           }
           syllabusDetailsGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalsyllabusOne/"+id);
        }

        syllabusTwoDetailsp(id,data){
          return this.http.post(environment.apiUrl + "/professionalsyllabusTwoP/"+id,data);
           }
           syllabusTwoDetailsGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalsyllabusTwo/"+id);
        }
        ElegibilitiesPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalEligiblitiesP/"+id,data);
           }
           elegibilitiesGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalEligiblities/"+id);
        }

        organizationsPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalimpOrganizationsOneP/"+id,data);
           }
           organizationsGets(id){
    
          return this.http.get(environment.apiUrl +"/professionalimpOrganizationsOne/"+id);
        }

        importantNotificationsPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalImportantNotificationsP/"+id,data);
           }
           importantNotificationsGets(id){
    
          return this.http.get(environment.apiUrl +"/professionalImportantNotifications/"+id);
        }
        importantNotificationsGetsc(id){
    
          return this.http.get(environment.apiUrl +"/professionalImportantNotificationsCount/"+id);
        }
        
        materialsTwoPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalMaterialsP/"+id,data);
           }
           getmaterials(id){
    
          return this.http.get(environment.apiUrl +"/professionalMaterials/"+id);
        }

        newBatchPosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalNewBatchOneP/"+id,data);
           }
           newBatchGets(id){
    
          return this.http.get(environment.apiUrl +"/professionalNewBatchOne/"+id);
        }

        admissionsPosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalAdminProcessP/"+id,data);
           }
           admissionsGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalAdminProcess/"+id);
        }

        freeStructurePosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalFeeStructureTwoP/"+id,data);
           }
           freeStructureGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalFeeStructureTwo/"+id);
        }
        

        batchesPost(id,data){
          return this.http.post(environment.apiUrl + "/professionalBatchP/"+id,data);
           }
           batchesGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalBatchGet/"+id);
        }

        facultyPosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalfacultyDetailsp/"+id,data);
           }
           facultyGets(id){
    
          return this.http.get(environment.apiUrl +"/professionalfacultyDetails/"+id);
        }

        testAnaysisPosts(id,data){
          return this.http.post(environment.apiUrl + "/professionaltestAnalysisP/"+id,data);
           }
           testAnaysisGets(id){
    
          return this.http.get(environment.apiUrl +"/professionaltestAnalysis/"+id);
        }

        computerLabPosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalComputerLabP/"+id,data);
           }
           computerLabGets(id){
    
          return this.http.get(environment.apiUrl +"/professionalComputerLab/"+id);
        }

        libraryOnePosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalliraryOneP/"+id,data);
           }
           libraryOneGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalliraryOneGet/"+id);
        }


        libraryTwoPosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalliraryTwoPost/"+id,data);
           }
           libraryTwoGet(id){
    
          return this.http.get(environment.apiUrl +"/professionalliraryTwoGet/"+id);
        }

        computerLabThreePosts(id,data){
          return this.http.post(environment.apiUrl + "/professionalliraryThreeP/"+id,data);
           }
           computerLabThreeGets(id){
    
          return this.http.get(environment.apiUrl +"/professionalliraryThree/"+id);
        }



        trackRecordsPost(id,data){
          return this.http.post(environment.apiUrl + "/professionaltrackRecordP/"+id,data);
           }
           trackRecordsGets(id){
    
          return this.http.get(environment.apiUrl +"/professionaltrackRecord/"+id);
        }
        postsUpdata(id,data){
                    
          return this.http.post(environment.apiUrl +"/professionalInstupdatesgetpostsdata/"+id,data);
        }
        clientsUpdatesGet(id){
        
          return this.http.get(environment.apiUrl +"/professionalInstupdatesget/"+id);
        }
        
        clientsUpdatesGetCounts(id){
          
          return this.http.get(environment.apiUrl +"/professionalInstupdatesgetCounts/"+id);
        }
        commentsPoststoClints(id,data){
          
          return this.http.post(environment.apiUrl +"/professionalInstupdatesCommentReplysPost/"+id,data);
        }
        commentsGettoClints(id){
          
          return this.http.get(environment.apiUrl +"/professionalInstupdatesCommentsget/"+id);
        }
        commentsGettoClintsCounts(id){
          
          return this.http.get(environment.apiUrl +"/professionalInstupdatesCommentsgetcounts/"+id);
        }
        replyFromCommentesGet(id){
          
          return this.http.get(environment.apiUrl +"/professionalInstupdatesCommentReplysGet/"+id);
        }
      
        overallscommentspost(id){
      
          return this.http.get(environment.apiUrl +"/professionalInstuserscomments/"+id);
        }
        
        onlyCommentsGetCount(id){
          
          return this.http.get(environment.apiUrl +"/professionalInstuserscommentsCounts/"+id);
        }
      }
