import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminProfecssionalInstutionalComponent } from './admin-profecssional-instutional.component';
import { AdminProfessionalInstituionalComponentsService } from './admin-professional-instituional-components.service';
 
const routes:Routes=[{path:'adminProfessionalInstitutions',component:AdminProfecssionalInstutionalComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminProfecssionalInstutionalComponent,

  ],
  providers: [AdminProfessionalInstituionalComponentsService],
})
export class AdminProfessionalInstituionalComponentsModule { }
