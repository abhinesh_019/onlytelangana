import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminfightComponent } from './adminfight.component';

describe('AdminfightComponent', () => {
  let component: AdminfightComponent;
  let fixture: ComponentFixture<AdminfightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminfightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminfightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
