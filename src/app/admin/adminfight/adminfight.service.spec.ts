import { TestBed, inject } from '@angular/core/testing';

import { AdminfightService } from './adminfight.service';

describe('AdminfightService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminfightService]
    });
  });

  it('should be created', inject([AdminfightService], (service: AdminfightService) => {
    expect(service).toBeTruthy();
  }));
});
