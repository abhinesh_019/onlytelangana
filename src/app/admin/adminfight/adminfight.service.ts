import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminfightService {
  constructor(private http: HttpClient) { }
  
  usersDetails(id){
    return this.http.get(environment.apiUrl + "/fightsClientsgetId/"+id);
     
  }
  postsServices(id,data){
    return this.http.post(environment.apiUrl + "/fightsServicesClients/"+id,data);
     }

     fightServices(id){
      return this.http.get(environment.apiUrl + "/fightServicesget/"+id);
       
    }
    fightServicesc(id){
      return this.http.get(environment.apiUrl + "/fightServicesCntget/"+id);
     }

     postFecilities(id,data){
      return this.http.post(environment.apiUrl + "/fightsfecitiesPost/"+id,data);
       }
       fecilities(id){
        return this.http.get(environment.apiUrl + "/fightsfecitiesGet/"+id);
       }
       fecilitiesc(id){
        return this.http.get(environment.apiUrl + "/fightsfecitiesGetCounts/"+id);
       }
       postUpdatesForm(id,data){
        return this.http.post(environment.apiUrl + "/fightsupdatespostsdata/"+id,data);
         }
         clientsUpdatesGet(id){
    
          return this.http.get(environment.apiUrl +"/fightsupdatesget/"+id);
        }
        
        clientsUpdatesGetCounts(id){
          
          return this.http.get(environment.apiUrl +"/fightsupdatesgetCounts/"+id);
        }
        commentsPoststoClints(id,data){
          
          return this.http.post(environment.apiUrl +"/fightsupdatesCommentReplysPost/"+id,data);
        }
        commentsGettoClints(id){
          
          return this.http.get(environment.apiUrl +"/fightsupdatesCommentsget/"+id);
        }
        commentsGettoClintsCounts(id){
          
          return this.http.get(environment.apiUrl +"/fightsupdatesCommentsgetcounts/"+id);
        }
        replyFromCommentesGet(id){
          
          return this.http.get(environment.apiUrl +"/fightsupdatesCommentReplysGet/"+id);
        }
      
        generalsCommGet(id) {
          return this.http.get(environment.apiUrl + "/fightseuserscomments/"+id);
        }
        generalsCommGetC(id) {
          return this.http.get(environment.apiUrl + "/fightseuserscommentsCounts/"+id);
        }
      }