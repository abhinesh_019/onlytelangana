import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminmarriagebuerauComponent } from './adminmarriagebuerau.component';
import { AdminmarriagebuerauService } from './adminmarriagebuerau.service';
 
const routes:Routes=[{path:'marriagesB',component:AdminmarriagebuerauComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    AdminmarriagebuerauComponent,

  ],
  providers: [AdminmarriagebuerauService],
})
export class AdminmarriagebuerauModule { }
