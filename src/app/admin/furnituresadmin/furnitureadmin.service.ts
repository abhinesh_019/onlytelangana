import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class FurnitureadminService {
  constructor(private http: HttpClient) { }
  
  postDegrees(id,data) {
    return this.http.post(environment.apiUrl + "/furnituresOnlyCustomersReviewReviewPost/"+id,data);
  }
  customersGetsForProducts(id){
    return this.http.get(environment.apiUrl +"/furnituresCustomerReviewget/"+id);

  }
  customersGetsForProductsCount(id){
    return this.http.get(environment.apiUrl +"/furnituresCustomerReviewGetCounts/"+id);

  }
  updatePostDataCustomers(id,data) {
    return this.http.post(environment.apiUrl + "/furnituresCustomerReviewpostsdata/"+id,data);
  }
  
  furnitureTypresPosts(id,data) {
    console.log(data);
    return this.http.post(environment.apiUrl + "/furnitureTypespostss/"+id,data);
  }
  furnitureTypeGets(id){
    return this.http.get(environment.apiUrl +"/furnitureTypesGet/"+id);

  }
   // ***************products posts***********
  
   postProducts(id,data){
    return this.http.post(environment.apiUrl + "/furnituresproductspostss/"+id,data);
     
  }
  productsServiceTwos(id) {
    return this.http.get(environment.apiUrl + "/furnituresproducts/"+id);
  }
  postsdataupFurnitures(id,data){
    return this.http.post(environment.apiUrl + "/furnituresupdatespostsdata/"+id,data);
     
  }
  UpdatedFurnituresReply(id,data){
    return this.http.post(environment.apiUrl + "/furnituresupdatespostsdata/"+id,data);
    
  }

  overallscommentsGet(id){
     
    
    return this.http.get(environment.apiUrl +"/furnituresuserscomments/"+id);
    
  }

  overallscommentspostCounts(id){
     
    
    return this.http.get(environment.apiUrl +"/furnituresuserscommentsCounts/"+id);
    
  }

  furnitureusers(id,data){
    
    return this.http.get(environment.apiUrl +"/furnituresusers/"+id,{params:data});
  }
  furnitureusersonly(id){
    
    return this.http.get(environment.apiUrl +"/furnitures/"+id);
  }
  
  furnitureuserscounts(id){
    return this.http.get(environment.apiUrl +"/furnituresusersCountsByArea/"+id);
  }
  furnitureuserscountstot(){
    
    return this.http.get(environment.apiUrl +"/furnituresuserstotcout");
  }
  furnitureuserscountsarea(id){
    
    return this.http.get(environment.apiUrl +"/furnituresuserscountarea/"+id);
  }

  

  // ********************************
 

 
  // ***************************************

  // ****************overall comments*****************
  overallscommentspost(id,data){
     
    
    return this.http.post(environment.apiUrl +"/furnituresuserscomments/"+id,data);
    
  }

  // *********************product descriptions**************
  products(id) {
    return this.http.get(environment.apiUrl +"/furnituresproducts/"+id);
  }
  productsCounts(id){
    return this.http.get(environment.apiUrl +"/furnituresproductsCounts/"+id);

  }
// *****************  updates*********************
  Getupdates(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesget/"+id);
  }
  GetupdatesCounts(id){
    return this.http.get(environment.apiUrl +"/furnituresUpdatesGetCounts/"+id);
  }
  //****updates comments  */
  commentspostupdates(id,data){
    return this.http.post(environment.apiUrl +"/furnituresupdatesCommentspost/"+id,data);

  }
  commentsgetupdates(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesCommentsget/"+id);

  }
  commentsgetupdatesCounts(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesCommentsgetcounts/"+id);

  }
  
  commentsgetupdatesReply(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesCommentReplysGet/"+id);

  }
  commentsUpdatesAdminReply(id,data){
   
    
    return this.http.post(environment.apiUrl +"/furnituresupdatesCommentReplysPost/"+id,data);

    
  }
  

}
