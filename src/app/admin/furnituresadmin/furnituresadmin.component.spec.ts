import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FurnituresadminComponent } from './furnituresadmin.component';

describe('FurnituresadminComponent', () => {
  let component: FurnituresadminComponent;
  let fixture: ComponentFixture<FurnituresadminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FurnituresadminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FurnituresadminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
