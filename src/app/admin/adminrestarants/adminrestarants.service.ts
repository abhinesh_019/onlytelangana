import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminrestarantsService {

  constructor(private http: HttpClient) { }

  individualcat(id){
    return this.http.get(environment.apiUrl +"/restaurantsClientsGetind/"+id);
 }
  
  foodTypeOne(id){
     return this.http.get(environment.apiUrl +"/restaurantsFoodType/"+id);
  }
 
  foodCatOne(id,data){
                    
    return this.http.post(environment.apiUrl +"/restaurantsFoodType/"+id,data);
  }


  foodTypeDishGet(id){
    return this.http.get(environment.apiUrl +"/restaurantsFoodTypeDish/"+id);
 }

 foodCatDish(id,data){
                   
   return this.http.post(environment.apiUrl +"/restaurantsFoodTypeDish/"+id,data);
 }



  foodTypeTwoGet(id){
    return this.http.get(environment.apiUrl +"/restaurantsFoodTypeTwo/"+id);
 }
 foodTypeTwoGetC(id){
  return this.http.get(environment.apiUrl +"/restaurantsFoodTypeTwoC/"+id);
}
 foodCatTwo(id,data){
                   
   return this.http.post(environment.apiUrl +"/restaurantsFoodTypeTwo/"+id,data);
 }
 foodServices(id){
  return this.http.get(environment.apiUrl +"/restaurantsServicesGetOne/"+id);
}
foodServicesC(id){
  return this.http.get(environment.apiUrl +"/restaurantsServicesGetCountsOne/"+id);
}
foodSerivesPosts(id,data){
                 
 return this.http.post(environment.apiUrl +"/restaurantsServicesPostOne/"+id,data);
}


postUpdatesForm(id,data){
  return this.http.post(environment.apiUrl + "/restaurantsupdatesgetpostsdata/"+id,data);
   }
clientsUpdatesGet(id){
    
  return this.http.get(environment.apiUrl +"/restaurantsupdatesget/"+id);
}

clientsUpdatesGetCounts(id){
  
  return this.http.get(environment.apiUrl +"/restaurantsupdatesgetCounts/"+id);
}
commentsPoststoClints(id,data){
  
  return this.http.post(environment.apiUrl +"/restaurantsupdatesCommentReplysPost/"+id,data);
}
commentsGettoClints(id){
  
  return this.http.get(environment.apiUrl +"/restaurantsupdatesCommentsget/"+id);
}
commentsGettoClintsCounts(id){
  
  return this.http.get(environment.apiUrl +"/restaurantsupdatesCommentsgetcounts/"+id);
}
replyFromCommentesGet(id){
  
  return this.http.get(environment.apiUrl +"/restaurantsupdatesCommentReplysGet/"+id);
}
generalsCommGet(id) {
  return this.http.get(environment.apiUrl + "/restaurantsuserscomments/"+id);
}
generalsCommGetC(id) {
  return this.http.get(environment.apiUrl + "/restaurantsuserscommentsCounts/"+id);
}
 
}
