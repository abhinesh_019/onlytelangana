import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminrestarantsService } from './adminrestarants.service';

@Component({
  selector: 'app-adminrestarants',
  templateUrl: './adminrestarants.component.html',
  styleUrls: ['./adminrestarants.component.css']
})
export class AdminrestarantsComponent implements OnInit {
  formData: FormData = new FormData(); 

  public showmains:boolean=false
public updatesShows:boolean=false
public viewupdatesShows:boolean=false
 public ImagesShows:boolean=false
public ProductsShows:boolean=false
 public usersID:any
public foodTypeOnes:any
public imageName:any
public selectFoodOneName:any
public selectFoodOneId:any
public selectFoodTwoName:any
public selectFoodTwoId:any
public foodTypeTwos:any
public foodSevices:any
public selectFoodServicesName:any
public selectFoodServicesId:any
public imageNames:any
public foodTypeDishs:any
public selectFoodDishName:any
public selectFoodDishId:any
public imageNameDish:any
public foodSevicesC:any
public foodTypeTwosC:any
public resClients:any
public imageNamess:any
public imageNameupdates:any
public videoNames:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public openshareid:any
public clientUpdates:any
public clientUpdatesCount:any
public commentspostsId:any
public getUsersComments:any
public getUsersCommentsCounts:any
public viewcommentsid:any
public replyid:any
public replies:any
public animalsTypesCnts:any
public clientUpdatesCnts:any
public gcomm:any
public gcommc:any
public viewsProfiles:any
public viewsNoti:any

postComments={
  "descriptions":""
}
fd={
  foodType1:"",
  images:""
}
ftd={
  foodDish:"",
  images:"",
}
fdTypes={
  foodType1:"",
   images:"",
 }

fds={
  foodName:"",
  foodType:"",
  foodCat:"",
  images:"",
  foodQuantity:"",
  foodPrice:"",
     foodDescriptions:"",
   foodAdvantages:"",
   chefMasterName:"",
   YearsOfExp:"",
  }

  ups={
    images:"",
      title:"",
      descriptions:"",
      viedoes:"",
  }

constructor(private route:ActivatedRoute,private adminFunc:AdminrestarantsService) { }

  ngOnInit() {
    this.usersID=localStorage.getItem('ads');
    this.usersDetails()
    this.foodTypeOneGet()
    this.clientsUpdates()
    this.clientsUpdatesCount()
    this.generalCommGet()
    this.generalCommGetc()
  }
  usersDetails(){ 
    this.adminFunc.individualcat(this.usersID).subscribe((res)=>{
 
      this.resClients=res
      
      
        })
  }
  viewProfile(){
    this.viewsProfiles=!this.viewsProfiles
    this.viewsNoti=false
  }
  viewnoti(){
    this.viewsNoti=!this.viewsNoti
    this.viewsProfiles=false

  }
  mainMenuShow(){
this.showmains=!this.showmains
this.updatesShows=false
this.viewupdatesShows=false
 this.ImagesShows=false
this.ProductsShows=false
   }
  updatesShow(){
    this.updatesShows=!this.updatesShows
    this.showmains=false
    this.viewupdatesShows=false
     this.ImagesShows=false
    this.ProductsShows=false
   }
  viewupdatesShow(){
    this.viewupdatesShows=!this.viewupdatesShows
    this.updatesShows=false
    this.showmains=false
     this.ImagesShows=false
    this.ProductsShows=false
   }
  
  ImagesShow(){
    
    this.ImagesShows=!this.ImagesShows
    this.updatesShows=false
    this.showmains=false
     this.viewupdatesShows=false
    this.ProductsShows=false
   }
  ProductsShow(){
    this.ProductsShows=!this.ProductsShows
    this.updatesShows=false
    this.showmains=false
     this.viewupdatesShows=false
    this.ImagesShows=false
   }
  

// *********************************************food types one ******************************************
 

    handleFileInputFoodTypeOne($event:any,images){
      this.imageName= File = $event.target.files[0];
   }
   foodCatOne(){
      this.formData.append('images',this.imageName);
      this.formData.append('foodType1',this.fd.foodType1);
        this.adminFunc.foodCatOne(this.usersID,this.formData).subscribe((res)=>{
            })
           this.fd.foodType1="",
           this.fd.images=""
        }

        foodTypeOneGet(){ 
          this.adminFunc.foodTypeOne(this.usersID).subscribe((res)=>{
          this.foodTypeOnes=res
          var id=this.foodTypeOnes[0]
          this.selectFoodType(id)
           })
          }
          selectFoodType(fd?){
      this.selectFoodOneId=fd._id
      this.selectFoodOneName=fd.foodType1
      this.foodTypeDishGet()
         }


// ***********************************************************************************************

// *********************************************food types Dish ******************************************
foodTypeDishGet(){ 
  this.adminFunc.foodTypeDishGet(this.selectFoodOneId).subscribe((res)=>{
  this.foodTypeDishs=res 
  this.selectFoodType()

   })
  }
  selectFoodTypeDish(fd?){
this.selectFoodDishId=fd._id
this.selectFoodDishName=fd.foodDish

this.foodTypeTwoGet()
this.foodTypeTwoGetC()
  }

  
  handleFileInputFoodTypeDish($event:any,images){
    this.imageNameDish= File = $event.target.files[0];
 }
 foodCatDish(){
    this.formData.append('images',this.imageNameDish);
    this.formData.append('foodDish',this.ftd.foodDish);
       this.adminFunc.foodCatDish(this.selectFoodOneId,this.formData).subscribe((res)=>{
          })
         this.ftd.foodDish="",
         this.ftd.images="" 
      }
// ***********************************************************************************************

// *********************************************food types Two Catageries******************************************
foodTypeTwoGet(){ 
  this.adminFunc.foodTypeTwoGet(this.selectFoodDishId).subscribe((res)=>{
  this.foodTypeTwos=res
  var id=this.foodTypeTwos[0]
  this.selectFoodTypeTwo(id)
   })
  }
  foodTypeTwoGetC(){ 
    this.adminFunc.foodTypeTwoGetC(this.selectFoodDishId).subscribe((res)=>{
    this.foodTypeTwosC=res
    var id=this.foodTypeTwosC[0]
      })
    }
  selectFoodTypeTwo(fd?){
this.selectFoodTwoId=fd._id
this.selectFoodTwoName=fd.foodType1
 this.foodServices()
 this.foodServicesC()
  }
  handleFileInputFoodTypeTwo($event:any,images){
    this.imageNamess= File = $event.target.files[0];
 }
 foodCatTwo(){
    this.formData.append('images',this.imageNamess);
    this.formData.append('foodType1',this.selectFoodOneName);
     

      this.adminFunc.foodCatTwo(this.selectFoodDishId,this.formData).subscribe((res)=>{
          })

         this.fdTypes.foodType1="",
        
           this.fdTypes.images=""
      }
// ***********************************************************************************************

// *********************************************food types Services ******************************************
foodServices(){ 
  this.adminFunc.foodServices(this.selectFoodTwoId).subscribe((res)=>{
  this.foodSevices=res 
    })
  }
  
  foodServicesC(){ 
    this.adminFunc.foodServicesC(this.selectFoodTwoId).subscribe((res)=>{
    this.foodSevicesC=res 
      })
    }

  handleFileInputFoodServices($event:any,images){
    this.imageNames= File = $event.target.files[0];
 }
 foodSerivesPosts(){
    this.formData.append('images',this.imageNames);
    this.formData.append('foodName',this.fds.foodName);
    this.formData.append('foodQuantity',this.fds.foodQuantity);
    this.formData.append('foodPrice',this.fds.foodPrice);
     this.formData.append('foodDescriptions',this.fds.foodDescriptions);
    this.formData.append('foodAdvantages',this.fds.foodAdvantages);  
    this.formData.append('chefMasterName',this.fds.chefMasterName);
    this.formData.append('YearsOfExp',this.fds.YearsOfExp);
    this.formData.append('foodCat',this.selectFoodTwoName);
       this.adminFunc.foodSerivesPosts(this.selectFoodTwoId,this.formData).subscribe((res)=>{
          })
 this.fds.foodAdvantages=""; 
 this.fds.foodCat="";  
 this.fds.foodDescriptions="";  
 this.fds.foodName="";  
 this.fds.foodPrice="";  
 this.fds.foodQuantity="";  
  this.fds.images="";  
 this.fds.chefMasterName="";
 this.fds.YearsOfExp="";
      }
// ***********************************************************************************************

// ******************************************updates present ***************************************
handleFileInputUpdates($event:any,images){
  this.imageNameupdates= File = $event.target.files[0];
}

handleFileInputUpdate($event:any,viedoes){
  this.videoNames= File = $event.target.files[0];
  this.formData.append(viedoes,this.videoNames);
  
}

postUpdatesForm(){
this.formData.append('images',this.imageNameupdates);
this.formData.append('title',this.ups.title);
this.formData.append('descriptions',this.ups.descriptions);
 

 

 this.adminFunc.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
  
    })
     this.ups.title="",
     this.ups.descriptions="",
     this.ups.images="",
     this.ups.viedoes=""
}

shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet() 
}

 clientsUpdates(){   
  this.adminFunc.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.adminFunc.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.adminFunc.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.adminFunc.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.adminFunc.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.adminFunc.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res 
    })
 }


  // ***********0verallcomments************
  generalCommGet(){ 
    this.adminFunc.generalsCommGet(this.usersID).subscribe((res)=>{
    this.gcomm=res
    console.log(res);
    
       })
    }
  
    generalCommGetc(){ 
      this.adminFunc.generalsCommGetC(this.usersID).subscribe((res)=>{
      this.gcommc=res
        })
      }
}
