import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminrealestateComponent } from './adminrealestate.component';
import { AdminrealestateService } from './adminrealestate.service';
 


const routes:Routes=[
  {path:'adminrealestate',component:AdminrealestateComponent},

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    AdminrealestateComponent,

  ],
  providers: [AdminrealestateService],
})
export class AdminrealestateModule { }
