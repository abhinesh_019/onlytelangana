import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminrealestateComponent } from './adminrealestate.component';

describe('AdminrealestateComponent', () => {
  let component: AdminrealestateComponent;
  let fixture: ComponentFixture<AdminrealestateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminrealestateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminrealestateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
