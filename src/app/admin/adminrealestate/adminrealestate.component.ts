import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
 import { AdminrealestateService } from './adminrealestate.service';

@Component({
  selector: 'app-adminrealestate',
  templateUrl: './adminrealestate.component.html',
  styleUrls: ['./adminrealestate.component.css']
})
export class AdminrealestateComponent implements OnInit {
  formData: FormData = new FormData(); 

  public showmains:boolean=false
  public updatesShows:boolean=false
  public propertydetails:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false
  public propertyShowPost:boolean=false
  public usersID:any
  public viewsProfiles:any
  public viewsNoti:any
 public createAreaPostRes:any
 public createAreaGetRes:any
 public cAnameId:any
 public cAname:any
 public imageName:any
 public createRealesateTypeGetRes:any
 public realEstateTypeName:any
 public realEstateTypeId:any
 public getBusinessTypeRes:any
 public getBusinessTypeResId:any
 public getBusinessTypeResName:any
 public getBusinessRoomTypeId:any
 public getBusinessRoomTypeName:any
 public getBusinessRoomTypeRes:any
 public roomName:any
 public roomId:any
public basicDetailsViews:any
public sectionsList:any
public propertyDetailsId:any
public outerViewForm:boolean=false
public innerView:boolean=false
public OnlyouterView:boolean=false
public laForm:boolean=false
public amenities:boolean=false 
public overViewsForm:boolean=false
public basicDetailsOuterViews:any
public basicDetailsInnerViews:any
public basicDetailsInnerViewsC:any
public locationsAdvanages:any
public locationsAdvanagesC:any
public basicDetailsOuterViewsC:any
public AmenitiesGet:any
public overviewsGet:any
public clientGet:any
 public videoName:any
 public share:boolean=false
 public comments:boolean=false
 public viewcomments:boolean=false
 public replyfrom:boolean=false 
 public openshareid:any
 public viewcommentsid:any
 public replyid:any
 public clientUpdates:any
 public clientUpdatesCount:any
 public commentspostsId:any
 public postComments:any
 public getUsersComments:any
 public getUsersCommentsCounts:any
 public replies:any
 public ids:any
public onlyComments:any
public onlyCommentsC:any
public realestateType:any
public realestateTypeArea:any
public realestateTypeBusinessLogic:any
public realestatelr:any


cl={
  clientArea:""
}
re={
  realestateType:"",
  images:""
}
be={
  Businesype:"",
  images:""
}
beT={
  businesTypeRooms:"",
  images:""
}

postupdatedataBabycares={
  images:"",
  description:"",
  title:"",
  viedoes:""
}


  constructor(private route:ActivatedRoute,private real:AdminrealestateService) { }
  property={
    area:"",
    reType:"",
    reTypeBussinessType:"",
    propertyTypeBussinessLogic:"",
     propertyArea:"",
     propertyName:"",
     shortDesc:"",  
     longDesc:"", 
     furnituresDetails:"",
     price:"", 
     locations:"", 
     landmark:"", 
     lat:"", 
     long:"", 
     status:"", 
     floor:"", 
     verifications:"", 
     optionalDescriptions:"",
    outerFsideNme:"",
    outerFsideDes:"",
    outerBsideNme:"",
    outerBsideDes:"",
    outerLsideNme:"",
    outerLsideDes:"",
    outerRsideNme:"",
    outerRsideDes:"",
    outerFsideimg:"",
    outerBsideimg:"",
    outerLsideimg:"",
    outerRsideimg:"",

    innerName1:"",
    innerName2:"",
    innerName3:"",
    innerName4:"",
    innerName5:"",
    innerName6:"",
    innerName7:"",
    innerName8:"",
    innerName9:"",
    innerName10:"",
    innerName11:"",
    innerName12:"",
    innerName13:"",
    innerName14:"",
    innerName15:"",
    innerName16:"",
    innerName17:"",
    innerName18:"",
    innerName19:"",
    innerName20:"",

    innerNdes1:"",
    innerNdes2:"",
    innerNdes3:"",
    innerNdes4:"",
    innerNdes5:"",
    innerNdes6:"",
    innerNdes7:"",
    innerNdes8:"",
    innerNdes9:"",
    innerNdes10:"",
    innerNdes11:"",
    innerNdes12:"",
    innerNdes13:"",
    innerNdes14:"",
    innerNdes15:"",
    innerNdes16:"",
    innerNdes17:"",
    innerNdes18:"",
    innerNdes19:"",
    innerNdes20:"",

    innerNimg1:"",
    innerNimg2:"",
    innerNimg3:"",
    innerNimg4:"",
    innerNimg5:"",
    innerNimg6:"",
    innerNimg7:"",
    innerNimg8:"",
    innerNimg9:"",
    innerNimg10:"",
    innerNimg11:"",
    innerNimg12:"",
    innerNimg13:"",
    innerNimg14:"",
    innerNimg15:"",
    innerNimg16:"",
    innerNimg17:"",
    innerNimg18:"",
    innerNimg19:"",
    innerNimg20:"",


    lAdvantagesName1:"",
    lAdvantagesName2:"",
    lAdvantagesName3:"",
    lAdvantagesName4:"",
    lAdvantagesName5:"",
    lAdvantagesName6:"",
    lAdvantagesName7:"",
    lAdvantagesName8:"",
    lAdvantagesName9:"",
    lAdvantagesName10:"",
    lAdvantagesName11:"",
    lAdvantagesName12:"",
    lAdvantagesName13:"",
    lAdvantagesName14:"",
    lAdvantagesName15:"",
    lAdvantagesName16:"",
    lAdvantagesName17:"",
    lAdvantagesName18:"",
    lAdvantagesName19:"",
    lAdvantagesName20:"",

    lAdvantagesImg1:"",
    lAdvantagesImg2:"",
    lAdvantagesImg3:"",
    lAdvantagesImg4:"",
    lAdvantagesImg5:"",
    lAdvantagesImg6:"",
    lAdvantagesImg7:"",
    lAdvantagesImg8:"",
    lAdvantagesImg9:"",
    lAdvantagesImg10:"",
    lAdvantagesImg11:"",
    lAdvantagesImg12:"",
    lAdvantagesImg13:"",
    lAdvantagesImg14:"",
    lAdvantagesImg15:"",
    lAdvantagesImg16:"",
    lAdvantagesImg17:"",
    lAdvantagesImg18:"",
    lAdvantagesImg19:"",
    lAdvantagesImg20:"",


    amenitiesNames1:"",
    amenitiesNames2:"",
    amenitiesNames3:"",
    amenitiesNames4:"",
    amenitiesNames5:"",
    amenitiesNames6:"",
    amenitiesNames7:"",
    amenitiesNames8:"",
    amenitiesNames9:"",
    amenitiesNames10:"",
    amenitiesNames11:"",
    amenitiesNames12:"",
    amenitiesNames13:"",
    amenitiesNames14:"",
    amenitiesNames15:"",
    amenitiesNames16:"",
    amenitiesNames17:"",
    amenitiesNames18:"",
    amenitiesNames19:"",
    amenitiesNames20:"",


    amenitiesImg1:"",
    amenitiesImg2:"",
    amenitiesImg3:"",
    amenitiesImg4:"",
    amenitiesImg5:"",
    amenitiesImg6:"",
    amenitiesImg7:"",
    amenitiesImg8:"",
    amenitiesImg9:"",
    amenitiesImg10:"",
    amenitiesImg11:"",
    amenitiesImg12:"",
    amenitiesImg13:"",
    amenitiesImg14:"",
    amenitiesImg15:"",
    amenitiesImg16:"",
    amenitiesImg17:"",
    amenitiesImg18:"",
    amenitiesImg19:"",
    amenitiesImg20:"",
   }
 overviews={

  overview1:"",
  overview2:"",
  overview3:"",
  overview4:"",
  overview5:"",
  overview6:"",
  overview7:"",
  overview8:"",
  overview9:"",
  overview10:"",
  overview11:"",
  overview12:"",
  overview13:"",
  overview14:"",
  overview15:"",
  overview16:"",
  overview17:"",
  overview18:"",
  overview19:"",
  overview20:"",

 } 
 postComment={
  "descriptions":""
}
    ngOnInit() {
      this.usersID=localStorage.getItem('ads');
      console.log(this.usersID);
      
 this.createAreaGet()
 this.getClientDetails()
 this.clientsUpdates()
 this.overallcomment()
this.overallcommentC()
this.clientsUpdatesCount()
    }
     getClientDetails(){
      this.real.getClientDetails(this.usersID).subscribe((res)=>{
        this.clientGet=res
       
      }) }


    selectSection(sections){
      console.log("sections",sections);
      this.sectionsList=sections;
     
      }
    
         // / ***************************************post main  Property details  ********************************  
     handleFileInputOne($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile);
   
}

PostsmainPropertyBusinessDetails(){
  
   
   
      this.formData.append('area',this.cAname);
      this.formData.append('realestatType',this.realEstateTypeName);
      this.formData.append('realestatTypeBusinessLogic',this.getBusinessTypeResName);
      this.formData.append('realestatTypeBusinessLogicRoom',this.roomName);
      this.formData.append('propertyArea',this.property.propertyArea);
      this.formData.append('propertyName',this.property.propertyName);
      this.formData.append('shortDesc',this.property.shortDesc);
      this.formData.append('longDesc',this.property.longDesc);
      this.formData.append('furnituresDetails',this.property.furnituresDetails);
      this.formData.append('price',this.property.price);
      this.formData.append('locations',this.property.locations);
      this.formData.append('landmark',this.property.landmark);
      this.formData.append('lat',this.property.lat);
      this.formData.append('long',this.property.long);
      this.formData.append('status',this.property.status);
      this.formData.append('floor',this.property.floor);
      this.formData.append('verifications',this.property.verifications);
      this.formData.append('optionalDescriptions',this.property.optionalDescriptions);
           
        this.real.postMainData(this.roomId,this.formData).subscribe((res)=>{
          }) 
      
  
  
  }


  
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    mainMenuShow(){
  this.showmains=!this.showmains
  this.updatesShows=false
  this.propertydetails=false
  this.onlyCommentsShows=false
  this.ImagesShows=false
  this.ProductsShows=false
  this.ProductsViewShows=false
  this.propertyShowPost=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.propertydetails=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.propertyShowPost=false
      console.log(this.updatesShows);
      
    }
    propertyDetailsShow(){
      this.propertydetails=!this.propertydetails
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.propertyShowPost=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.propertydetails=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.propertyShowPost=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.propertydetails=false
      this.ProductsShows=false
      this.ProductsViewShows=false
      this.propertyShowPost=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.propertydetails=false
      this.ImagesShows=false
      this.ProductsViewShows=false
      this.propertyShowPost=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.propertydetails=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.propertyShowPost=false
    }

    CreateProperty(){
      this.propertyShowPost=!this.propertyShowPost
      this.ProductsViewShows=false
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.propertydetails=false
      this.ImagesShows=false
      this.ProductsShows=false
    }
    // <!-- ****************************************************************create area*************************************************** -->

    createAreaPost(){
      this.real.createAreaPost(this.usersID,this.cl).subscribe((res)=>{
        this.createAreaPostRes=res
        this.cl.clientArea=""
        
        
      })
           }

           createAreaGet(){
            this.real.createAreaGet(this.usersID).subscribe((res)=>{
              this.createAreaGetRes=res
              var id= this.createAreaGetRes[0]
              this.clientAreaClick(id)
            }) }
                 clientAreaClick(result){
this.cAnameId=result._id
this.cAname=result.clientArea
  this.createRealesateTypeGet()
                 }
               
    // <!-- ***************************************************************************************************************** -->
 
  
// <!-- **************************************************************** create real estate type *************************************************** -->
handleFileInputRET($event:any,images){
  this.imageName= File = $event.target.files[0];

}
createRealesateTypePost(){ 
this.formData.append('images',this.imageName);
this.formData.append('realestateType',this.re.realestateType); 
this.real.createRealesateTypePost(this.cAnameId,this.formData).subscribe((res)=>{
 })
     this.re.realestateType="",
   this.re.images=""
                   }

   createRealesateTypeGet(){
      this.real.createRealesateTypeGet(this.cAnameId).subscribe((res)=>{
        this.createRealesateTypeGetRes=res
  
    var ids=this.createRealesateTypeGetRes[0]
   
    this.clientrealEstatetypeClick(ids)
      })
      
           }
           clientrealEstatetypeClick(result){
            this.realEstateTypeId=result._id
            this.realEstateTypeName=result.realestateType
            this.getBusinessType()
           }
           
    // <!-- ***************************************************************************************************************** -->
  // <!-- **************************************************************** create real estate type business *************************************************** -->
  handleFileInputRETB($event:any,images){
  this.imageName= File = $event.target.files[0];

}
createRealesateBusinesTypePost(){ 
this.formData.append('images',this.imageName);
this.formData.append('Businesype',this.be.Businesype); 
this.real.createRealesateBusinesTypePost(this.realEstateTypeId,this.formData).subscribe((res)=>{
 })
     this.be.Businesype="",
   this.re.images=""
                   }

   getBusinessType(){
      this.real.getBusinessType(this.realEstateTypeId).subscribe((res)=>{
        this.getBusinessTypeRes=res
        var idss=this.getBusinessTypeRes[0]
        this.getBusinessTypeClick(idss)
      })
      
           }
           getBusinessTypeClick(result){
            this.getBusinessTypeResId=result._id
            this.getBusinessTypeResName=result.Businesype
              this.getBusinessRoomTypeGet()
           }
           
    // <!-- ***************************************************************************************************************** -->
  // <!-- ****************************************************************create real estate type business room*************************************************** -->
 
  createRealesateBusinesRoomTypePost(){ 
    
   this.real.createRealesateBusinesRoomTypePost(this.getBusinessTypeResId,this.beT).subscribe((res)=>{
   })
       this.beT.businesTypeRooms=""
   
                     }
  
                     getBusinessRoomTypeGet(){
                      this.real.getBusinessRoomType(this.getBusinessTypeResId).subscribe((res)=>{
                        this.getBusinessRoomTypeRes=res
                      var ids=this.getBusinessRoomTypeRes[0]
                      this.getBusinessRoomTypeClick(ids)
                      })
                      
                           }
                           getBusinessRoomTypeClick(result){
                            this.roomId=result._id
                            this.roomName=result.businesTypeRooms
                             this.basicProperty()
                             this.getAreaForBusiness()
                             this.getRealEstateBusinessLogic()
                             this.getRealEstateBusinessLogicRoom()
                             this.getRealEstateType()
                           
                           }
                           
                           getAreaForBusiness(){
                            this.real.getAreaForBusiness(this.roomId).subscribe((res)=>{
                              this.realestateTypeArea=res
                           })
                              }
                              getRealEstateType(){
                                this.real.getRealEstateType(this.roomId).subscribe((res)=>{
                                  this.realestateType=res
                                })
                                  }
                                  getRealEstateBusinessLogic(){
                                    this.real.getRealEstateBusinessLogic(this.roomId).subscribe((res)=>{
                                      this.realestateTypeBusinessLogic=res
                                    })
                                      }
                                      getRealEstateBusinessLogicRoom(){
                                        this.real.getRealEstateBusinessLogicRoom(this.roomId).subscribe((res)=>{
                                          this.realestatelr=res
                                         
                                        })
                                          }
      // <!-- ***************************************************************************************************************** -->
                  // <!-- *********************************************** BASIC PROPERT DETAILS ****************************************************************** -->

                  basicProperty(){
                    this.real.basicProperty(this.roomId).subscribe((res)=>{
                      this.basicDetailsViews=res
                       })

                         }
                        // <!-- ***************************************************************************************************************** -->
                     
   // <!-- *********************************************** BASIC PROPERT DETAILS outer view ****************************************************************** -->
   viewAllDetails(bp){
    this.propertyDetailsId=bp._id
    this.OnlyouterView=!this.OnlyouterView
     this.basicPropertyOuterVIew()
     this.basicPropertyInnerGetView()
     this.getLocationsAdvantages()
    this.getAmenities()
    this.getOverviews()
  }


   handleFileInputOuterVIew($event:any,img) {
    let imageFile= File = $event.target.files[0];
    this.formData.append(img, imageFile);
     
  }
  
  PostsmainOuterView(){
      
  
            this.formData.append('outerFsideNme',this.property.outerFsideNme);
            this.formData.append('outerBsideNme',this.property.outerBsideNme);
            this.formData.append('outerRsideNme',this.property.outerRsideNme);
            this.formData.append('outerLsideNme',this.property.outerLsideNme);
  
            this.formData.append('outerFsideDes',this.property.outerFsideDes);
            this.formData.append('outerBsideDes',this.property.outerBsideDes);
            this.formData.append('outerRsideDes',this.property.outerRsideDes);
            this.formData.append('outerLsideDes',this.property.outerLsideDes);
        
         
          this.real.postMainDataOuterView(this.propertyDetailsId,this.formData).subscribe((res)=>{
             
          }) 
     
    }
      


   basicPropertyOuterVIew(){
    this.real.basicPropertyOuterVIew(this.propertyDetailsId).subscribe((res)=>{
      this.basicDetailsOuterViews=res
    
    })
         }
          
        // <!-- ***************************************************************************************************************** -->
      // ******************************************************** post and get INNER VIEW PROPERTY **********************************

  // post inner deatils
    handleFileInputInnerView($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
       
    }
    
    PostsmainInnerView(){
        
    
            this.formData.append('innerName1',this.property.innerName1);
            this.formData.append('innerName2',this.property.innerName2);
            this.formData.append('innerName3',this.property.innerName3);
            this.formData.append('innerName4',this.property.innerName4);
            this.formData.append('innerName5',this.property.innerName5);
            this.formData.append('innerName6',this.property.innerName6);
            this.formData.append('innerName7',this.property.innerName7);
            this.formData.append('innerName8',this.property.innerName8);
            this.formData.append('innerName9',this.property.innerName9);
            this.formData.append('innerName10',this.property.innerName10);
            this.formData.append('innerName11',this.property.innerName11);
            this.formData.append('innerName12',this.property.innerName12);
            this.formData.append('innerName13',this.property.innerName13);
            this.formData.append('innerName14',this.property.innerName14);
            this.formData.append('innerName15',this.property.innerName15);
            this.formData.append('innerName16',this.property.innerName16);
            this.formData.append('innerName17',this.property.innerName17);
            this.formData.append('innerName18',this.property.innerName18);
            this.formData.append('innerName19',this.property.innerName19);
            this.formData.append('innerName20',this.property.innerName20);
            
            this.formData.append('innerNdes1',this.property.innerNdes1);
            this.formData.append('innerNdes2',this.property.innerNdes2);
            this.formData.append('innerNdes3',this.property.innerNdes3);
            this.formData.append('innerNdes4',this.property.innerNdes4);
            this.formData.append('innerNdes5',this.property.innerNdes5);
            this.formData.append('innerNdes6',this.property.innerNdes6);
            this.formData.append('innerNdes7',this.property.innerNdes7);
            this.formData.append('innerNdes8',this.property.innerNdes8);
            this.formData.append('innerNdes9',this.property.innerNdes9);
            this.formData.append('innerNdes10',this.property.innerNdes10);
            this.formData.append('innerNdes11',this.property.innerNdes11);
            this.formData.append('innerNdes12',this.property.innerNdes12);
            this.formData.append('innerNdes13',this.property.innerNdes13);
            this.formData.append('innerNdes14',this.property.innerNdes14);
            this.formData.append('innerNdes15',this.property.innerNdes15);
            this.formData.append('innerNdes16',this.property.innerNdes16);
            this.formData.append('innerNdes17',this.property.innerNdes17);
            this.formData.append('innerNdes18',this.property.innerNdes18);
            this.formData.append('innerNdes19',this.property.innerNdes19);
            this.formData.append('innerNdes20',this.property.innerNdes20);
  
          
           
            this.real.postMainDataInnerView(this.propertyDetailsId,this.formData).subscribe((res)=>{
             
            }) 
       
      }


      basicPropertyInnerGetView(){
        this.real.basicPropertyInnerVIew(this.propertyDetailsId).subscribe((res)=>{
          this.basicDetailsInnerViews=res
           
        })
             }
            

            //  *********************************************************************************************************************************************
       // ******************************************************** post locations advantages **********************************

  // post inner deatils
  handleFileInputla($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
       
    }
    
    PostsLocationsDetails(){
        
    
          this.formData.append('lAdvantagesName1',this.property.lAdvantagesName1);
          this.formData.append('lAdvantagesName2',this.property.lAdvantagesName2);
          this.formData.append('lAdvantagesName3',this.property.lAdvantagesName3);
          this.formData.append('lAdvantagesName4',this.property.lAdvantagesName4);
          this.formData.append('lAdvantagesName5',this.property.lAdvantagesName5);
          this.formData.append('lAdvantagesName6',this.property.lAdvantagesName6);
          this.formData.append('lAdvantagesName7',this.property.lAdvantagesName7);
          this.formData.append('lAdvantagesName8',this.property.lAdvantagesName8);
          this.formData.append('lAdvantagesName9',this.property.lAdvantagesName9);
          this.formData.append('lAdvantagesName10',this.property.lAdvantagesName10);
          this.formData.append('lAdvantagesName11',this.property.lAdvantagesName11);
          this.formData.append('lAdvantagesName12',this.property.lAdvantagesName12);
          this.formData.append('lAdvantagesName13',this.property.lAdvantagesName13);
          this.formData.append('lAdvantagesName14',this.property.lAdvantagesName14);
          this.formData.append('lAdvantagesName15',this.property.lAdvantagesName15);
          this.formData.append('lAdvantagesName16',this.property.lAdvantagesName16);
          this.formData.append('lAdvantagesName17',this.property.lAdvantagesName17);
          this.formData.append('lAdvantagesName18',this.property.lAdvantagesName18);
          this.formData.append('lAdvantagesName19',this.property.lAdvantagesName19);
          this.formData.append('lAdvantagesName20',this.property.lAdvantagesName20);
   
          
           
            this.real.postLocationsAdvantages(this.propertyDetailsId,this.formData).subscribe((res)=>{
               
            }) 
       
      }


      getLocationsAdvantages(){
        this.real.getLocationsAdvantages(this.propertyDetailsId).subscribe((res)=>{
          this.locationsAdvanages=res
          
        })
             }
           

            //  *********************************************************************************************************************************************
       
       // ******************************************************** post amenities**********************************

  // post inner deatils
  handleFileInputAm($event:any,img) {
    let imageFile= File = $event.target.files[0];
    this.formData.append(img, imageFile);
     
  }
 
 
  PostsAmenities(){
          this.formData.append('amenitiesNames1',this.property.amenitiesNames1);
          this.formData.append('amenitiesNames2',this.property.amenitiesNames2);
          this.formData.append('amenitiesNames3',this.property.amenitiesNames3);
          this.formData.append('amenitiesNames4',this.property.amenitiesNames4);
          this.formData.append('amenitiesNames5',this.property.amenitiesNames5);
          this.formData.append('amenitiesNames6',this.property.amenitiesNames6);
          this.formData.append('amenitiesNames7',this.property.amenitiesNames7);
          this.formData.append('amenitiesNames8',this.property.amenitiesNames8);
          this.formData.append('amenitiesNames9',this.property.amenitiesNames9);
          this.formData.append('amenitiesNames10',this.property.amenitiesNames10);
          this.formData.append('amenitiesNames11',this.property.amenitiesNames11);
          this.formData.append('amenitiesNames12',this.property.amenitiesNames12);
          this.formData.append('amenitiesNames13',this.property.amenitiesNames13);
          this.formData.append('amenitiesNames14',this.property.amenitiesNames14);
          this.formData.append('amenitiesNames15',this.property.amenitiesNames15);
          this.formData.append('amenitiesNames16',this.property.amenitiesNames16);
          this.formData.append('amenitiesNames17',this.property.amenitiesNames17);
          this.formData.append('amenitiesNames18',this.property.amenitiesNames18);
          this.formData.append('amenitiesNames19',this.property.amenitiesNames19);
          this.formData.append('amenitiesNames20',this.property.amenitiesNames20);
      
          this.real.postAmenities(this.propertyDetailsId,this.formData).subscribe((res)=>{
              
          }) 
     
    }


    getAmenities(){
      this.real.getAmenities(this.propertyDetailsId).subscribe((res)=>{
        this.AmenitiesGet=res
        
      })
           }
           

          //  *********************************************************************************************************************************************
     
       // ******************************************************** post OverViews **********************************

  // post inner deatils
  
 
  PostsOverviews(){
            
          this.real.PostsOverviews(this.propertyDetailsId,this.overviews).subscribe((res)=>{
            }) 
     
    }


    getOverviews(){
      this.real.getOverviews(this.propertyDetailsId).subscribe((res)=>{
        this.overviewsGet=res
       
      })
           }
           

          //  *********************************************************************************************************************************************
     
      
            propertyDetailsClickId(){
           console.log(this.propertyDetailsId);
          this.outerViewForm=!this.outerViewForm
          this.innerView=false
          this.laForm=false
          this.amenities=false
          this.overViewsForm=false
        }
        innerViwPropertyDetailsClickId(){
          this.innerView=!this.innerView
          this.outerViewForm=false
          this.laForm=false
          this.amenities=false
          this.overViewsForm=false
        }
        propertyDetailsForLAClickId(){
          this.laForm=!this.laForm
          this.innerView=false
          this.outerViewForm=false
          this.amenities=false
          this.overViewsForm=false
        }

        propertyAmenitiesGetClickId(){
          this.amenities=!this.amenities
          this.laForm=false
          this.innerView=false
          this.outerViewForm=false
          this.overViewsForm=false
        }

        propertyDetailsForOverViewsClickId(){
          this.overViewsForm=!this.overViewsForm
          this.amenities=false
          this.laForm=false
          this.innerView=false
          this.outerViewForm=false
        }
                
      // **********updates*******
      handleFileInputRealEstateI($event:any,images){
  this.imageName= File = $event.target.files[0];
  
  
}

handleFileInputRealEstate($event:any,viedoes){
  this.videoName= File = $event.target.files[0];

 console.log(this.videoName);
 
}
realEstateUpdatePostData( ){


  let formData: FormData = new FormData();
  formData.append('images', this.imageName);
  formData.append('viedoes', this.videoName);
  formData.append('description', this.postupdatedataBabycares.description);
  formData.append('title', this.postupdatedataBabycares.title);


  this.usersID=localStorage.getItem('ads');
  console.log(formData)

  this.real.realEstateUpdatePostData(this.usersID,formData).subscribe((res)=>{
     console.log('assssss',res);
    console.log(this.usersID);
    
      })
      this.postupdatedataBabycares.title="",
      this.postupdatedataBabycares.description="",
      this.postupdatedataBabycares.images="",
      this.postupdatedataBabycares.viedoes=""
}


// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.real.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
    console.log(res);
    console.log(this.usersID);
    })
 }
 
 clientsUpdatesCount(){   
  this.real.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.real.commentsPoststoClints(this.replyid,this.postComment).subscribe((res)=>{
     })
     this.postComment.descriptions=""
 }
 commentsGettoClints(){   
  this.real.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.real.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.real.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }

 overallcomment(){
this.real.overallscommentspostbaby(this.usersID).subscribe((res)=>{
 this.onlyComments=res
  console.log(this.onlyComments);
  
})
}
overallcommentC(){
  this.real.overallcommentC(this.usersID).subscribe((res)=>{
   this.onlyCommentsC=res
    console.log(this.onlyCommentsC);
    
  })
  }
      }

