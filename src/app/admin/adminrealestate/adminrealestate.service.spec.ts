import { TestBed, inject } from '@angular/core/testing';

import { AdminrealestateService } from './adminrealestate.service';

describe('AdminrealestateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminrealestateService]
    });
  });

  it('should be created', inject([AdminrealestateService], (service: AdminrealestateService) => {
    expect(service).toBeTruthy();
  }));
});
