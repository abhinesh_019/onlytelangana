import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmintutionsComponent } from './admintutions.component';

describe('AdmintutionsComponent', () => {
  let component: AdmintutionsComponent;
  let fixture: ComponentFixture<AdmintutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmintutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmintutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
