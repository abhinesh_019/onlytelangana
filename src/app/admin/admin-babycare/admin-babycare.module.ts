import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
  import { AdminBabycareComponent } from './admin-babycare.component';
import { AdminbabycareService } from './adminbabycare.service';


const routes:Routes=[{path:'adminBabycare',component:AdminBabycareComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    AdminBabycareComponent,

  ],
  providers: [AdminbabycareService],
})
export class AdminBabycareModule { }
