import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { AdminbabycareService } from './adminbabycare.service';

@Component({
  selector: 'app-admin-babycare',
  templateUrl: './admin-babycare.component.html',
  styleUrls: ['./admin-babycare.component.css']
})
export class AdminBabycareComponent implements OnInit {
  formData: FormData = new FormData(); 

 public usersID:any
 public datas:any
public showmains:boolean=false
public updatesShows:boolean=false
public viewupdatesShows:boolean=false
public onlyCommentsShows:boolean=false
public ImagesShows:boolean=false
public viewsProfiles:boolean=false
public viewsNoti:boolean=false
public programInfo:boolean=false

public imageName:any
public videoName:any
public adminupdatesp:any
public adminupdates:any
public onlyComments:any
public show:any
public shows:any
public updatesup:any
public adminsupdates:any
public showes:any
public selectedblogId:any
public updatescommentsget:any
public showess:any
public updatesIdss:any
public selectedblogIds:any
public updatescommentsgets:any
public updatesCommentsdataReply:any
 public adminShows:any
 public replyId:any
 public adminsupdateComments:any
 public adminsupdateCommentsCounts:any
 public adminsupdateCommentsCountsall:any
public adminCommentsCountsall:any
public programInfromationsGets:any
public productsItemsListName:any
public productsItemsListId:any
 public programInfromationsGetsTwo:any
public fecilitiesGets:any
public fecilitiesGetsCount:any
public fecilitiese:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any
// public productsItemsListName:any


//  *mains***

 public adminsupdateCommentsReply

//  **********mains*********

postupdatedataBabycares={
  images:"",
  description:"",
  title:"",
  viedoes:""
}
updatesCommentsdataReplys={
  
  descriptions:"",
   
}

pro={
  programInformation:""
}
serTwo={
  information:"",
    descriptions:"",
    images:"",
}

fec={
  fecilitiesName: "",
  fecilitiesImg: "",
  fecilitiesDescriptions: "",
}
  constructor(private baby:AdminbabycareService) { }

  ngOnInit() {
    this.usersID=localStorage.getItem('ads');
    this. overallcomment()
    this.getupdates()
    this.individualdata()
    this.programInfromationsGet()
    this.fecilitiesGet()
    this.fecilitiesGetCount()
  }

  // ********************nav bar ********************
  viewProfile(){
    this.viewsProfiles=!this.viewsProfiles
    this.viewsNoti=false
  }
  viewnoti(){
    this.viewsNoti=!this.viewsNoti
    this.viewsProfiles=false
  }
  // **************************navbar end********************
  individualdata(){
this.baby.getbabycares(this.usersID).subscribe((res)=>{
this.datas=res
})
}

// **********updates*******

handleFileInputBabycares($event:any,images){
  this.imageName= File = $event.target.files[0];
  // this.formData.append(images, imageFile);
}
handleFileInputBabycaresv($event:any,viedoes){
  this.videoName= File = $event.target.files[0];
}
FurnituresUpdatePostData( ){
  let formData: FormData = new FormData();
  formData.append('images', this.imageName);
  formData.append('viedoes', this.videoName);
  formData.append('description', this.postupdatedataBabycares.description);
  formData.append('title', this.postupdatedataBabycares.title);


  this.usersID=localStorage.getItem('ads');
  
  this.baby.postsdataupBabycares(this.usersID,formData).subscribe((res)=>{
     this.adminupdatesp=res
   
      })
      this.postupdatedataBabycares.description="",
      this.postupdatedataBabycares.images="",
      this.postupdatedataBabycares.viedoes="",
      this.postupdatedataBabycares.title=""
}


// **************************
  mainMenuShow(){
this.showmains=!this.showmains
this.updatesShows=false
this.viewupdatesShows=false
this.onlyCommentsShows=false
this.ImagesShows=false
this.programInfo=false
this.fecilitiese=false

  }
  updatesShow(){
    this.updatesShows=!this.updatesShows
    this.showmains=false
    this.viewupdatesShows=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
    this.programInfo=false
    this.fecilitiese=false

  }
  Feicilities(){
    this.fecilitiese=!this.fecilitiese
    this.viewupdatesShows=false
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
     this.programInfo=false
  }
  viewupdatesShow(){
    this.viewupdatesShows=!this.viewupdatesShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
     this.programInfo=false
     this.fecilitiese=false
  }

  programInfrmations(){
    this.programInfo=!this.programInfo
    this.viewupdatesShows=false
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.ImagesShows=false
    this.fecilitiese=false

  }
  onlyCommentsShow(){
    this.onlyCommentsShows=!this.onlyCommentsShows
    this.updatesShows=false
    this.showmains=false
    this.viewupdatesShows=false
    this.ImagesShows=false
    this.programInfo=false
    this.fecilitiese=false

  }
  ImagesShow(){
    
    this.ImagesShows=!this.ImagesShows
    this.updatesShows=false
    this.showmains=false
    this.onlyCommentsShows=false
    this.viewupdatesShows=false
    this.programInfo=false
    this.fecilitiese=false

  }
   overallcomment(){
     this.usersID=localStorage.getItem('ads');
 this.baby.overallscommentspostbaby(this.usersID).subscribe((res)=>{
    this.onlyComments=res
   })
}
// *************get updates****************
getupdates(){
 
  this.baby.adminBabyUpdatesGet(this.usersID).subscribe((res)=>{
    this.adminupdates=res
   
  })
       }

       showicons(){
        this.show=!this.show
        this.shows=false
      }

  showcomments(item){
  
        this.shows=!this.shows
        this.show=false
          this.updatesup=item._id
       this.comm()
this.updatesCounts()             }
             commentsReplyId(result){
              this.updatesCommentsdataReplys.descriptions=""
              this.adminShows=!this.adminShows
this.replyId=result
 

            }  

            comm(){
             this.baby.commentsgetupdate(this.updatesup).subscribe((res)=>{
                  this.adminsupdateComments=res
                 
                  })
    }

    updatesAdminCommentsReply(){
 this.baby.commentsUpdatesAdminReply(this.replyId,this.updatesCommentsdataReplys).subscribe((res)=>{
         
       })
 this.updatesCommentsdataReplys.descriptions=""
  
             }
             updatesAdminReply(items?){
              this.showess=!this.showess
              this.selectedblogIds=items
              this.baby.commentsgetupdates(this.selectedblogIds).subscribe((res)=>{
                this.adminsupdateCommentsReply=res
               
               
              })
            }


updatesCounts(){
  this.baby.updatesCommmentsCount(this.updatesup).subscribe((res)=>{
    this.adminsupdateCommentsCounts=res
  
   
  })
}

// updatesallCounts(){
//   this.baby.updatesCommmentsallCount(this.usersID).subscribe((res)=>{
//     this.adminsupdateCommentsCountsall=res
//   
   
//   })
// }
//  alloverComments(){
//    this.baby.updatesCommmentsOverAll(this.usersID).subscribe((res)=>{
//     this.adminCommentsCountsall=res
//   
   
//   })
//  }


programInfromations(){
  this.baby.programInfromations(this.usersID,this.pro).subscribe((res)=>{
        
        })
  this.pro.programInformation=""
   
              }

              programInfromationsGet(){
                this.baby.programInfromationsGet(this.usersID).subscribe((res)=>{
                  this.programInfromationsGets=res
                  var vsd=this.programInfromationsGets[0]
this.programInfromationsClick(vsd)
               
                 })
              }

              programInfromationsClick(pro){
this.productsItemsListId=pro._id
this.productsItemsListName=pro.information
this.programInfromationsTwoGet()
              }
               
// *************************************************services two *************************************************8
              handleFileInputServicesTwo($event:any,img) {
                let imageFileSerTwo= File = $event.target.files[0];
                this.formData.append(img, imageFileSerTwo);
               }
              
              updateServicesTwo(){
                this.formData.append('information',this.serTwo.information);
            this.formData.append('descriptions',this.serTwo.descriptions);
            
           this.baby.updateServices(this.productsItemsListId,this.formData).subscribe((res)=>{
            
               })
               this.serTwo.information="",
               this.serTwo.descriptions="",
               this.serTwo.images=""
             }

             programInfromationsTwoGet(){
              this.baby.programInfromationsTwoGet(this.productsItemsListId).subscribe((res)=>{
                this.programInfromationsGetsTwo=res
                var fdf=this.programInfromationsGetsTwo[0]
             
               })
            }


                          
// *************************************************fecilities*************************************************8
handleFileInputFecilities($event:any,img) {
  let imageFileSerFecilities= File = $event.target.files[0];
  this.formData.append(img,imageFileSerFecilities);
 }

 fecilities(){
  this.formData.append('fecilitiesDescriptions',this.fec.fecilitiesDescriptions);
this.formData.append('fecilitiesName',this.fec.fecilitiesName);

this.baby.fecilities(this.usersID,this.formData).subscribe((res)=>{

 })
 this.serTwo.information="",
 this.serTwo.descriptions="",
 this.serTwo.images=""
}

fecilitiesGet(){
this.baby.fecilitiesGet(this.usersID).subscribe((res)=>{
  this.fecilitiesGets=res
 
 })
}
fecilitiesGetCount(){
  this.baby.fecilitiesGetCount(this.usersID).subscribe((res)=>{
    this.fecilitiesGetsCount=res
    var fdf=this.fecilitiesGetsCount[0]
  
   })
  }
}
