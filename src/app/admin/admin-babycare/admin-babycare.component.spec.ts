import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBabycareComponent } from './admin-babycare.component';

describe('AdminBabycareComponent', () => {
  let component: AdminBabycareComponent;
  let fixture: ComponentFixture<AdminBabycareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBabycareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBabycareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
