import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class AdminbabycareService {

  constructor(private http: HttpClient) { }
 
  getbabycares(id){
    
    return this.http.get(environment.apiUrl +"/babycare/"+id);
  }
  postsdataupBabycares(id,data){
    return this.http.post(environment.apiUrl + "/babyCareupdatespost/"+id,data);
     }
     adminBabyUpdatesGet(id){
    
      return this.http.get(environment.apiUrl +"/babyCareupdates/"+id);
    }

    commentsgetupdate(id){
    
      return this.http.get(environment.apiUrl +"/babyCareupdatesuserscomments/"+id);
    }

    
     
    commentsUpdatesAdminReply(id,data){
      return this.http.post(environment.apiUrl + "/babycareupdatesuserscommentsreply/"+id,data);
       }
       overallscommentspostbaby(id){
    
        return this.http.get(environment.apiUrl +"/babycarecomments/"+id);
      }

      commentsgetupdates(id){
    
        return this.http.get(environment.apiUrl +"/babycareupdatesuserscommentsreply/"+id);
      } 
      updatesCommmentsCount(id){
    
        return this.http.get(environment.apiUrl +"/babyCareupdatescounts/"+id);
      } 

      programInfromations(id,data){
        return this.http.post(environment.apiUrl + "/babyCareServices/"+id,data);
         }
         programInfromationsGet(id){
        return this.http.get(environment.apiUrl +"/babyCareServices/"+id);
            }
            programInfromationsTwoGet(id){
              return this.http.get(environment.apiUrl +"/babyCareServicesTwoGet/"+id);
                  }

                  updateServices(id,data){
                    return this.http.post(environment.apiUrl + "/babyCareServicesTwoPost/"+id,data);
                     }
                     fecilities(id,data){
                      return this.http.post(environment.apiUrl + "/babyCarefecitiesPost/"+id,data);
                       }
                       fecilitiesGet(id){
                        return this.http.get(environment.apiUrl +"/babyCarefecitiesGet/"+id);
                            }
                            fecilitiesGetCount(id){
                              return this.http.get(environment.apiUrl +"/babyCarefecitiesGetCounts/"+id);
                                  }
}
