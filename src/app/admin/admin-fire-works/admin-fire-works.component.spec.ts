import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFireWorksComponent } from './admin-fire-works.component';

describe('AdminFireWorksComponent', () => {
  let component: AdminFireWorksComponent;
  let fixture: ComponentFixture<AdminFireWorksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFireWorksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFireWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
