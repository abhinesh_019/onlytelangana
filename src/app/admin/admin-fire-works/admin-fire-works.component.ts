import { Component, OnInit } from '@angular/core';
import { AdminfireWorksService } from './adminfire-works.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-fireWorks-works',
  templateUrl: './admin-fire-works.component.html',
  styleUrls: ['./admin-fire-works.component.css']
})
export class AdminFireWorksComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false
  public usersID:any
public fireWorksClients:any
public viewsProfiles:any
public viewsNoti:any
public clientsDetails:any
public ids:any
public clientsInd:any
public servicesOneGet:any
public servicevehicleName:any
public servicesIds:any
public servicesOneGets:any
public servicevehicleNames:any
public servicesone:any
public servicesTwos:any
public trackRecordsId:any
public trackRecordsName:any
public trackRecordsGet:any
public trackRecordsgetsData:any
public servicesOneGetc:any
public trackRecordsgetsDatac:any
public servicesTwosc:any
public totalCustomer:any
public totalCustomerc:any
public fecilitiesGets:any
public fecilitiesGetsc:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any
public openshareid:any
public imageNameupdates:any
public videoNames:any
public gcomm:any
public gcommc:any
public fireWorksDetails:any
public serviceTwoGetc:any
public fireWorksDetailsId:any
public fireWorksDetailsName:any
public cusotmersgetDetails:any
public cusotmersgetDetailsc:any
public imageSeriveOne:any
public fireworksServicesOneGets:any
public fireWorksSerivesIds:any
public fireWorksSerivesName:any
public imageSeriveTwo:any
public fireworksServicesTwoGets:any
public fireWorksSerivesTwoIds:any
public fireWorksSerivesTwoName:any
public fireworksServicesTwoGetsC:any
public fireworksServicesOneGetsC:any
public fireworksServicesThreeGets:any
public imagecustomers:any
public fireworksServicesThreeGetsC:any
public imagecustomerst:any
public s:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any
// public usersID:any


formData: FormData = new FormData(); 

postComments={
  "descriptions":""
}
ups={
  images:"",
    title:"",
    description:"",
    viedoes:"",
}
serOne={
  typeOfFiewWorks:"",
    FiewWorksDescriptions:"",
    FiewWorksImg:"",
}
serTwo={
  nameOfFireWorks:"",
     fireWorksDescriptions:"",
    fireWorksPrice:"",
    fireWorksImg:"",
}

serThree={
  customerName:"",
      place:"",
     itemsDescriptions:"",
     itemsImg:"",
     customersImg:"",
}
constructor(private route:ActivatedRoute,private fireWorks:AdminfireWorksService) { }
 ngOnInit() {
     this.usersID=localStorage.getItem('ads');
this.usersDetails()
 this.clientsUpdates()
 this.clientsUpdatesCount()
 this.generalCommGet()
 this.generalCommGetc()
 this.fireworksServicesOneGet()
 this.fireworksServicesOneGetC()
  }


  // ***************************************************updades **************************************
  handleFileInputUpdates($event:any,images){
    this.imageNameupdates= File = $event.target.files[0];
   }
  handleFileInputUpdate($event:any,viedoes){
    this.videoNames= File = $event.target.files[0];
    this.formData.append(viedoes,this.videoNames);
  }
  
  postUpdatesForm(){
  this.formData.append('images',this.imageNameupdates);
  this.formData.append('title',this.ups.title);
  this.formData.append('description',this.ups.description);
   
   this.fireWorks.postUpdatesForm(this.usersID,this.formData).subscribe((res)=>{
     
      })
       this.ups.title="",
       this.ups.description="",
       this.ups.images="",
       this.ups.viedoes=""
  }
  usersDetails(){ 
    this.fireWorks.usersDetails(this.usersID).subscribe((res)=>{
    this.fireWorksClients=res
   
    
    })
    }
    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
    
    mainMenuShow(){
      this.showmains=!this.showmains
      this.updatesShows=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    viewupdatesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }

     // ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
 
  
}
 clientsUpdates(){   
  this.fireWorks.clientsUpdatesGet(this.usersID).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.fireWorks.clientsUpdatesGetCounts(this.usersID).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.fireWorks.commentsPoststoClints(this.replyid,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.fireWorks.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.fireWorks.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.fireWorks.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
  
    
    })
 }
 generalCommGet(){ 
  this.fireWorks.generalsCommGet(this.usersID).subscribe((res)=>{
  this.gcomm=res
     })
  }
generalCommGetc(){ 
  this.fireWorks.generalsCommGetC(this.usersID).subscribe((res)=>{
  this.gcommc=res
    })
  }
  


// *************************************************** fireworksServicesOne **************************************
  handleFileInputSeriveOne($event:any,images){
    this.imageSeriveOne= File = $event.target.files[0];
   }
  
  
   fireworksServicesOne(){
  this.formData.append('FiewWorksImg',this.imageSeriveOne);
  this.formData.append('typeOfFiewWorks',this.serOne.typeOfFiewWorks);
  this.formData.append('FiewWorksDescriptions',this.serOne.FiewWorksDescriptions);
   
   this.fireWorks.fireworksServicesOne(this.usersID,this.formData).subscribe((res)=>{
     
      })
       this.serOne.typeOfFiewWorks="",
       this.serOne.FiewWorksDescriptions="",
       this.serOne.FiewWorksImg=""
  }

  fireworksServicesOneGet(){
    this.fireWorks.fireworksServicesOneGet(this.usersID).subscribe((res)=>{
      this.fireworksServicesOneGets=res;
        })
      }
fireworksServicesOneGetClick(get){
this.fireWorksSerivesIds=get._id;
this.fireWorksSerivesName=get.typeOfFiewWorks;
console.log(this.fireWorksSerivesIds);

this.fireworksServicesTwoGet();
this.fireworksServicesTwoGetC();
}
fireworksServicesOneGetC(){
  this.fireWorks.fireworksServicesOneGetC(this.usersID).subscribe((res)=>{
    this.fireworksServicesOneGetsC=res;
      })
    }
// *************************************************** fireworksServicesTwo **************************************
fireworksServicesTwoGet(){
  this.fireWorks.fireworksServicesTwoGet(this.fireWorksSerivesIds).subscribe((res)=>{
    this.fireworksServicesTwoGets=res
    console.log(this.fireWorksSerivesIds);
    
  console.log(res);
  })
    }

    fireworksServicesTwoGetClick(get){
      this.fireWorksSerivesTwoIds=get._id;
         this.fireWorksSerivesTwoName=get.nameOfFireWorks;
      this.fireworksServicesThreeGet();
      this.fireworksServicesThreeGetC();
      }
 
 fireworksServicesTwoGetC(){
  this.fireWorks.fireworksServicesTwoGetC(this.fireWorksSerivesIds).subscribe((res)=>{
    this.fireworksServicesTwoGetsC=res
 
      })
    }
    handleFileInputSeriveTwo($event:any,images){
      this.imageSeriveTwo= File = $event.target.files[0];
     }
    
    
     fireworksServicesTwo(){
    this.formData.append('fireWorksImg',this.imageSeriveTwo);
    this.formData.append('nameOfFireWorks',this.serTwo.nameOfFireWorks);
    this.formData.append('fireWorksDescriptions',this.serTwo.fireWorksDescriptions);
    this.formData.append('fireWorksPrice',this.serTwo.fireWorksPrice);

     this.fireWorks.fireworksServicesTwo(this.fireWorksSerivesIds,this.formData).subscribe((res)=>{
       
        })
         this.serTwo.nameOfFireWorks="",
         this.serTwo.fireWorksDescriptions="",
         this.serTwo.fireWorksPrice="",
         this.serTwo.fireWorksImg=""
        }



      // *************************************************** fireworksServicesThree **************************************
  
       handleFileInputSeriveThrees($event:any,images){
        this.imagecustomers= File = $event.target.files[0];
       }
       handleFileInputSeriveThree($event:any,images){
        this.imagecustomerst= File = $event.target.files[0];
       }
  
       fireworksServicesThree(){
      this.formData.append('itemsImg',this.imagecustomerst);
      this.formData.append('customersImg',this.imagecustomers);
      this.formData.append('customerName',this.serThree.customerName);
      this.formData.append('place',this.serThree.place);
      this.formData.append('itemsDescriptions',this.serThree.itemsDescriptions);
      this.formData.append('itemsDescriptions',this.serThree.itemsDescriptions);

        this.fireWorks.fireworksServicesThree(this.fireWorksSerivesTwoIds,this.formData).subscribe((res)=>{
          console.log('assssss',res);
          })
           this.serThree.itemsImg="",
           this.serThree.customerName="",
           this.serThree.place="",
           this.serThree.customersImg="",
           this.serThree.itemsDescriptions=""
       }
  
  
       fireworksServicesThreeGet(){
        this.fireWorks.fireworksServicesThreeGet(this.fireWorksSerivesTwoIds).subscribe((res)=>{
          this.fireworksServicesThreeGets=res
       console.log(res);
       
            })
          }
  
  
          fireworksServicesThreeGetC(){
            this.fireWorks.fireworksServicesThreeGetC(this.fireWorksSerivesTwoIds).subscribe((res)=>{
              this.fireworksServicesThreeGetsC=res
           console.log(res);
                })
              }

  }



