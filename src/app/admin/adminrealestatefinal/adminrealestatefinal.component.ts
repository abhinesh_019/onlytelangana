import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminrealestatefinalService } from './adminrealestatefinal.service';

@Component({
  selector: 'app-adminrealestatefinal',
  templateUrl: './adminrealestatefinal.component.html',
  styleUrls: ['./adminrealestatefinal.component.css']
})
export class AdminrealestatefinalComponent implements OnInit {
  formData: FormData = new FormData(); 

public ids:any
public realPropertyDetailsInd:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
 public sectionsList:any
public propertyDetailsId:any
public outerViewForm:boolean=false
public innerView:boolean=false
public OnlyouterView:boolean=false
public laForm:boolean=false
public amenities:boolean=false 
public overViewsForm:boolean=false
public basicDetailsOuterViews:any
public basicDetailsInnerViews:any
public basicDetailsInnerViewsC:any
public locationsAdvanages:any
public locationsAdvanagesC:any
public basicDetailsOuterViewsC:any
public AmenitiesGet:any
public overviewsGet:any
public clientGet:any
 
constructor(private route:ActivatedRoute,private real:AdminrealestatefinalService) { }
property={
  area:"",
  reType:"",
  reTypeBussinessType:"",
  propertyTypeBussinessLogic:"",
   propertyArea:"",
   propertyName:"",
   shortDesc:"",  
   longDesc:"", 
   furnituresDetails:"",
   price:"", 
   locations:"", 
   landmark:"", 
   lat:"", 
   long:"", 
   status:"", 
   floor:"", 
   verifications:"", 
   optionalDescriptions:"",
  outerFsideNme:"",
  outerFsideDes:"",
  outerBsideNme:"",
  outerBsideDes:"",
  outerLsideNme:"",
  outerLsideDes:"",
  outerRsideNme:"",
  outerRsideDes:"",
  outerFsideimg:"",
  outerBsideimg:"",
  outerLsideimg:"",
  outerRsideimg:"",

  innerName1:"",
  innerName2:"",
  innerName3:"",
  innerName4:"",
  innerName5:"",
  innerName6:"",
  innerName7:"",
  innerName8:"",
  innerName9:"",
  innerName10:"",
  innerName11:"",
  innerName12:"",
  innerName13:"",
  innerName14:"",
  innerName15:"",
  innerName16:"",
  innerName17:"",
  innerName18:"",
  innerName19:"",
  innerName20:"",

  innerNdes1:"",
  innerNdes2:"",
  innerNdes3:"",
  innerNdes4:"",
  innerNdes5:"",
  innerNdes6:"",
  innerNdes7:"",
  innerNdes8:"",
  innerNdes9:"",
  innerNdes10:"",
  innerNdes11:"",
  innerNdes12:"",
  innerNdes13:"",
  innerNdes14:"",
  innerNdes15:"",
  innerNdes16:"",
  innerNdes17:"",
  innerNdes18:"",
  innerNdes19:"",
  innerNdes20:"",

  innerNimg1:"",
  innerNimg2:"",
  innerNimg3:"",
  innerNimg4:"",
  innerNimg5:"",
  innerNimg6:"",
  innerNimg7:"",
  innerNimg8:"",
  innerNimg9:"",
  innerNimg10:"",
  innerNimg11:"",
  innerNimg12:"",
  innerNimg13:"",
  innerNimg14:"",
  innerNimg15:"",
  innerNimg16:"",
  innerNimg17:"",
  innerNimg18:"",
  innerNimg19:"",
  innerNimg20:"",


  lAdvantagesName1:"",
  lAdvantagesName2:"",
  lAdvantagesName3:"",
  lAdvantagesName4:"",
  lAdvantagesName5:"",
  lAdvantagesName6:"",
  lAdvantagesName7:"",
  lAdvantagesName8:"",
  lAdvantagesName9:"",
  lAdvantagesName10:"",
  lAdvantagesName11:"",
  lAdvantagesName12:"",
  lAdvantagesName13:"",
  lAdvantagesName14:"",
  lAdvantagesName15:"",
  lAdvantagesName16:"",
  lAdvantagesName17:"",
  lAdvantagesName18:"",
  lAdvantagesName19:"",
  lAdvantagesName20:"",

  lAdvantagesImg1:"",
  lAdvantagesImg2:"",
  lAdvantagesImg3:"",
  lAdvantagesImg4:"",
  lAdvantagesImg5:"",
  lAdvantagesImg6:"",
  lAdvantagesImg7:"",
  lAdvantagesImg8:"",
  lAdvantagesImg9:"",
  lAdvantagesImg10:"",
  lAdvantagesImg11:"",
  lAdvantagesImg12:"",
  lAdvantagesImg13:"",
  lAdvantagesImg14:"",
  lAdvantagesImg15:"",
  lAdvantagesImg16:"",
  lAdvantagesImg17:"",
  lAdvantagesImg18:"",
  lAdvantagesImg19:"",
  lAdvantagesImg20:"",


  amenitiesNames1:"",
  amenitiesNames2:"",
  amenitiesNames3:"",
  amenitiesNames4:"",
  amenitiesNames5:"",
  amenitiesNames6:"",
  amenitiesNames7:"",
  amenitiesNames8:"",
  amenitiesNames9:"",
  amenitiesNames10:"",
  amenitiesNames11:"",
  amenitiesNames12:"",
  amenitiesNames13:"",
  amenitiesNames14:"",
  amenitiesNames15:"",
  amenitiesNames16:"",
  amenitiesNames17:"",
  amenitiesNames18:"",
  amenitiesNames19:"",
  amenitiesNames20:"",


  amenitiesImg1:"",
  amenitiesImg2:"",
  amenitiesImg3:"",
  amenitiesImg4:"",
  amenitiesImg5:"",
  amenitiesImg6:"",
  amenitiesImg7:"",
  amenitiesImg8:"",
  amenitiesImg9:"",
  amenitiesImg10:"",
  amenitiesImg11:"",
  amenitiesImg12:"",
  amenitiesImg13:"",
  amenitiesImg14:"",
  amenitiesImg15:"",
  amenitiesImg16:"",
  amenitiesImg17:"",
  amenitiesImg18:"",
  amenitiesImg19:"",
  amenitiesImg20:"",
 }
overviews={

overview1:"",
overview2:"",
overview3:"",
overview4:"",
overview5:"",
overview6:"",
overview7:"",
overview8:"",
overview9:"",
overview10:"",
overview11:"",
overview12:"",
overview13:"",
overview14:"",
overview15:"",
overview16:"",
overview17:"",
overview18:"",
overview19:"",
overview20:"",

}
  ngOnInit() {
    this.ids=this.route.snapshot.params['_id'];
    this.propertyDetailsId=this.ids
console.log(this.ids);
this.usersDetails()
this.viewAllDetails()
  }
  usersDetails(){ 
    this.real.usersDetails(this.ids).subscribe((res)=>{
      this.realPropertyDetailsInd=res
     })
  }
            
   // <!-- *********************************************** BASIC PROPERT DETAILS outer view ****************************************************************** -->
   viewAllDetails(){
  
    this.basicPropertyOuterVIew()
     this.basicPropertyInnerGetView()
     this.getLocationsAdvantages()
    this.getAmenities()
    this.getOverviews()
  }


   handleFileInputOuterVIew($event:any,img) {
    let imageFile= File = $event.target.files[0];
    this.formData.append(img, imageFile);
    console.log(imageFile);
    
  }
  
  PostsmainOuterView(){
      
  
            this.formData.append('outerFsideNme',this.property.outerFsideNme);
            this.formData.append('outerBsideNme',this.property.outerBsideNme);
            this.formData.append('outerRsideNme',this.property.outerRsideNme);
            this.formData.append('outerLsideNme',this.property.outerLsideNme);
  
            this.formData.append('outerFsideDes',this.property.outerFsideDes);
            this.formData.append('outerBsideDes',this.property.outerBsideDes);
            this.formData.append('outerRsideDes',this.property.outerRsideDes);
            this.formData.append('outerLsideDes',this.property.outerLsideDes);
        
         
          this.real.postMainDataOuterView(this.propertyDetailsId,this.formData).subscribe((res)=>{
             console.log(res);
            
          }) 
     
    }
      


   basicPropertyOuterVIew(){
    this.real.basicPropertyOuterVIew(this.propertyDetailsId).subscribe((res)=>{
      this.basicDetailsOuterViews=res
    
    })
         }
          
        // <!-- ***************************************************************************************************************** -->
      // ******************************************************** post and get INNER VIEW PROPERTY **********************************

  // post inner deatils
    handleFileInputInnerView($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
       
    }
    
    PostsmainInnerView(){
        
    
            this.formData.append('innerName1',this.property.innerName1);
            this.formData.append('innerName2',this.property.innerName2);
            this.formData.append('innerName3',this.property.innerName3);
            this.formData.append('innerName4',this.property.innerName4);
            this.formData.append('innerName5',this.property.innerName5);
            this.formData.append('innerName6',this.property.innerName6);
            this.formData.append('innerName7',this.property.innerName7);
            this.formData.append('innerName8',this.property.innerName8);
            this.formData.append('innerName9',this.property.innerName9);
            this.formData.append('innerName10',this.property.innerName10);
            this.formData.append('innerName11',this.property.innerName11);
            this.formData.append('innerName12',this.property.innerName12);
            this.formData.append('innerName13',this.property.innerName13);
            this.formData.append('innerName14',this.property.innerName14);
            this.formData.append('innerName15',this.property.innerName15);
            this.formData.append('innerName16',this.property.innerName16);
            this.formData.append('innerName17',this.property.innerName17);
            this.formData.append('innerName18',this.property.innerName18);
            this.formData.append('innerName19',this.property.innerName19);
            this.formData.append('innerName20',this.property.innerName20);
            
            this.formData.append('innerNdes1',this.property.innerNdes1);
            this.formData.append('innerNdes2',this.property.innerNdes2);
            this.formData.append('innerNdes3',this.property.innerNdes3);
            this.formData.append('innerNdes4',this.property.innerNdes4);
            this.formData.append('innerNdes5',this.property.innerNdes5);
            this.formData.append('innerNdes6',this.property.innerNdes6);
            this.formData.append('innerNdes7',this.property.innerNdes7);
            this.formData.append('innerNdes8',this.property.innerNdes8);
            this.formData.append('innerNdes9',this.property.innerNdes9);
            this.formData.append('innerNdes10',this.property.innerNdes10);
            this.formData.append('innerNdes11',this.property.innerNdes11);
            this.formData.append('innerNdes12',this.property.innerNdes12);
            this.formData.append('innerNdes13',this.property.innerNdes13);
            this.formData.append('innerNdes14',this.property.innerNdes14);
            this.formData.append('innerNdes15',this.property.innerNdes15);
            this.formData.append('innerNdes16',this.property.innerNdes16);
            this.formData.append('innerNdes17',this.property.innerNdes17);
            this.formData.append('innerNdes18',this.property.innerNdes18);
            this.formData.append('innerNdes19',this.property.innerNdes19);
            this.formData.append('innerNdes20',this.property.innerNdes20);
  
          
           
            this.real.postMainDataInnerView(this.propertyDetailsId,this.formData).subscribe((res)=>{
             
            }) 
       
      }


      basicPropertyInnerGetView(){
        this.real.basicPropertyInnerVIew(this.propertyDetailsId).subscribe((res)=>{
          this.basicDetailsInnerViews=res
           
        })
             }
            

            //  *********************************************************************************************************************************************
       // ******************************************************** post locations advantages **********************************

  // post inner deatils
  handleFileInputla($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
      console.log(imageFile);
      
    }
    
    PostsLocationsDetails(){
        
    
          this.formData.append('lAdvantagesName1',this.property.lAdvantagesName1);
          this.formData.append('lAdvantagesName2',this.property.lAdvantagesName2);
          this.formData.append('lAdvantagesName3',this.property.lAdvantagesName3);
          this.formData.append('lAdvantagesName4',this.property.lAdvantagesName4);
          this.formData.append('lAdvantagesName5',this.property.lAdvantagesName5);
          this.formData.append('lAdvantagesName6',this.property.lAdvantagesName6);
          this.formData.append('lAdvantagesName7',this.property.lAdvantagesName7);
          this.formData.append('lAdvantagesName8',this.property.lAdvantagesName8);
          this.formData.append('lAdvantagesName9',this.property.lAdvantagesName9);
          this.formData.append('lAdvantagesName10',this.property.lAdvantagesName10);
          this.formData.append('lAdvantagesName11',this.property.lAdvantagesName11);
          this.formData.append('lAdvantagesName12',this.property.lAdvantagesName12);
          this.formData.append('lAdvantagesName13',this.property.lAdvantagesName13);
          this.formData.append('lAdvantagesName14',this.property.lAdvantagesName14);
          this.formData.append('lAdvantagesName15',this.property.lAdvantagesName15);
          this.formData.append('lAdvantagesName16',this.property.lAdvantagesName16);
          this.formData.append('lAdvantagesName17',this.property.lAdvantagesName17);
          this.formData.append('lAdvantagesName18',this.property.lAdvantagesName18);
          this.formData.append('lAdvantagesName19',this.property.lAdvantagesName19);
          this.formData.append('lAdvantagesName20',this.property.lAdvantagesName20);
   
          
           
            this.real.postLocationsAdvantages(this.propertyDetailsId,this.formData).subscribe((res)=>{
               console.log(res);
               console.log(this.propertyDetailsId);
            }) 
       
      }


      getLocationsAdvantages(){
        this.real.getLocationsAdvantages(this.propertyDetailsId).subscribe((res)=>{
          this.locationsAdvanages=res
          console.log(this.locationsAdvanages);
          console.log(this.propertyDetailsId);
        })
             }
           

            //  *********************************************************************************************************************************************
       
       // ******************************************************** post amenities**********************************

  // post inner deatils
  handleFileInputAm($event:any,img) {
    let imageFile= File = $event.target.files[0];
    this.formData.append(img, imageFile);
    console.log(imageFile);
    
  }
 
 
  PostsAmenities(){
          this.formData.append('amenitiesNames1',this.property.amenitiesNames1);
          this.formData.append('amenitiesNames2',this.property.amenitiesNames2);
          this.formData.append('amenitiesNames3',this.property.amenitiesNames3);
          this.formData.append('amenitiesNames4',this.property.amenitiesNames4);
          this.formData.append('amenitiesNames5',this.property.amenitiesNames5);
          this.formData.append('amenitiesNames6',this.property.amenitiesNames6);
          this.formData.append('amenitiesNames7',this.property.amenitiesNames7);
          this.formData.append('amenitiesNames8',this.property.amenitiesNames8);
          this.formData.append('amenitiesNames9',this.property.amenitiesNames9);
          this.formData.append('amenitiesNames10',this.property.amenitiesNames10);
          this.formData.append('amenitiesNames11',this.property.amenitiesNames11);
          this.formData.append('amenitiesNames12',this.property.amenitiesNames12);
          this.formData.append('amenitiesNames13',this.property.amenitiesNames13);
          this.formData.append('amenitiesNames14',this.property.amenitiesNames14);
          this.formData.append('amenitiesNames15',this.property.amenitiesNames15);
          this.formData.append('amenitiesNames16',this.property.amenitiesNames16);
          this.formData.append('amenitiesNames17',this.property.amenitiesNames17);
          this.formData.append('amenitiesNames18',this.property.amenitiesNames18);
          this.formData.append('amenitiesNames19',this.property.amenitiesNames19);
          this.formData.append('amenitiesNames20',this.property.amenitiesNames20);
      
          this.real.postAmenities(this.propertyDetailsId,this.formData).subscribe((res)=>{
             console.log(res);
             console.log(this.propertyDetailsId);
          }) 
          this.property.amenitiesNames1="",
          this.property.amenitiesNames2="",
          this.property.amenitiesNames3="",
          this.property.amenitiesNames4="",
          this.property.amenitiesNames5="",
          this.property.amenitiesNames6="",
          this.property.amenitiesNames7="",
          this.property.amenitiesNames8="",
          this.property.amenitiesNames9="",
          this.property.amenitiesNames10="",
          this.property.amenitiesNames11="",
          this.property.amenitiesNames12="",
          this.property.amenitiesNames13="",
          this.property.amenitiesNames14="",
          this.property.amenitiesNames15="",
          this.property.amenitiesNames16="",
          this.property.amenitiesNames17="",
          this.property.amenitiesNames18="",
          this.property.amenitiesNames19="",
          this.property.amenitiesNames20=""
    }


    getAmenities(){
      this.real.getAmenities(this.propertyDetailsId).subscribe((res)=>{
        this.AmenitiesGet=res
        console.log(this.AmenitiesGet);
        console.log(this.propertyDetailsId);
      })
           }
           

          //  *********************************************************************************************************************************************
     
       // ******************************************************** post OverViews **********************************

  // post inner deatils
  
 
  PostsOverviews(){
            
          this.real.PostsOverviews(this.propertyDetailsId,this.overviews).subscribe((res)=>{
             console.log(res);
             console.log(this.propertyDetailsId);
          }) 

        

          this.overviews.overview1="",
          this.overviews.overview2="",
          this.overviews.overview3="",
          this.overviews.overview4="",
          this.overviews.overview5="",
          this.overviews.overview6="",
          this.overviews.overview7="",
          this.overviews.overview8="",
          this.overviews.overview9="",
          this.overviews.overview10="",
          this.overviews.overview11="",
          this.overviews.overview12="",
          this.overviews.overview13="",
          this.overviews.overview14="",
          this.overviews.overview15="",
          this.overviews.overview16="",
          this.overviews.overview17="",
          this.overviews.overview18="",
          this.overviews.overview19="",
          this.overviews.overview20=""
            
          
     
    }


    getOverviews(){
      this.real.getOverviews(this.propertyDetailsId).subscribe((res)=>{
        this.overviewsGet=res
        console.log(this.overviewsGet);
        console.log(this.propertyDetailsId);
      })
           }
           

          //  *********************************************************************************************************************************************
     
      
            propertyDetailsClickId(){
           this.outerViewForm=!this.outerViewForm
          this.innerView=false
          this.laForm=false
          this.amenities=false
          this.overViewsForm=false
        }
        innerViwPropertyDetailsClickId(){
          this.innerView=!this.innerView
          this.outerViewForm=false
          this.laForm=false
          this.amenities=false
          this.overViewsForm=false
        }
        propertyDetailsForLAClickId(){
          this.laForm=!this.laForm
          this.innerView=false
          this.outerViewForm=false
          this.amenities=false
          this.overViewsForm=false
        }

        propertyAmenitiesGetClickId(){
          this.amenities=!this.amenities
          this.laForm=false
          this.innerView=false
          this.outerViewForm=false
          this.overViewsForm=false
        }

        propertyDetailsForOverViewsClickId(){
          this.overViewsForm=!this.overViewsForm
          this.amenities=false
          this.laForm=false
          this.innerView=false
          this.outerViewForm=false
        }
                      }


