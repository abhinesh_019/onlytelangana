import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminrealestatefinalService {

  constructor(private http: HttpClient) { }

  usersDetails(id){
    return this.http.get(environment.apiUrl + "/realestateBusinesMainGetsInd/"+id);
     
  }
  
  getClientDetails(id){
    return this.http.get(environment.apiUrl + "/realestateClientsGetind/"+id);
     
  }
  createAreaPost(id,data){
    return this.http.post(environment.apiUrl + "/realestateclientsAreaPost/"+id,data);
     
  }
  createAreaGet(id){
    return this.http.get(environment.apiUrl + "/realestateAreaGet/"+id);
     
  }

  createRealesateTypePost(id,data){
    return this.http.post(environment.apiUrl + "/realestateTypesPost/"+id,data);
     
  }
  createRealesateTypeGet(id){
    return this.http.get(environment.apiUrl + "/realestatefecitiesGets/"+id);
     
  }

  createRealesateBusinesTypePost(id,data){
    return this.http.post(environment.apiUrl + "/realestateTypesBusinesPost/"+id,data);
     
  }
  getBusinessType(id){
    return this.http.get(environment.apiUrl + "/realestateBusinesGets/"+id);
     
  }

  createRealesateBusinesRoomTypePost(id,data){
    return this.http.post(environment.apiUrl + "/realestateTypesBusinesRoomsPost/"+id,data);
     
  }
  getBusinessRoomType(id){
    return this.http.get(environment.apiUrl + "/realestateBusinesRoomsGets/"+id);
     
  }
  postMainData(id,data){
    return this.http.post(environment.apiUrl + "/realestateTypesBusinesMainPost/"+id,data);
     
  }
  postMainDataOuterView(id,data){
    return this.http.post(environment.apiUrl + "/realestateOuterviewPost/"+id,data);
     
  }
  postMainDataInnerView(id,data){
    return this.http.post(environment.apiUrl + "/realestateInnerViewsost/"+id,data);
     
  }
  postLocationsAdvantages(id,data){
    return this.http.post(environment.apiUrl + "/realestatelocationAdvantagesPost/"+id,data);
     
  }
  postAmenities(id,data){
    return this.http.post(environment.apiUrl + "/realestateAmenitiesPost/"+id,data);
     
  }
  PostsOverviews(id,data){
    return this.http.post(environment.apiUrl + "/realestateOverviewspost/"+id,data);
     
  }
  basicProperty(id){
    return this.http.get(environment.apiUrl + "/realestateBusinesMainGets/"+id);
     
  }
  basicPropertyOuterVIew(id){
    return this.http.get(environment.apiUrl + "/realestateOuterviewGets/"+id);
     
  }
   
  basicPropertyInnerVIew(id){
    return this.http.get(environment.apiUrl + "/realestateInnerViewsGets/"+id);
     
  }
   
  getLocationsAdvantages(id){
    return this.http.get(environment.apiUrl + "/realestatelocationAdvantagesGets/"+id);
     
  }
  

  getAmenities(id){
    return this.http.get(environment.apiUrl + "/realestateAmenitiesGets/"+id);
     
  }
  getOverviews(id){
    return this.http.get(environment.apiUrl + "/realestateOverviewsGets/"+id);
     
  }

}


