import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminrealestatefinalComponent } from './adminrealestatefinal.component';
import { AdminrealestatefinalService } from './adminrealestatefinal.service';
 

const routes:Routes=[
  {path:'adminrealestatefinal/:_id/:name',component:AdminrealestatefinalComponent},

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    AdminrealestatefinalComponent,

  ],
  providers: [AdminrealestatefinalService],
})
export class AdminrealestatefinalModule { }
