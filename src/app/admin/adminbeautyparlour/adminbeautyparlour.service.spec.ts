import { TestBed, inject } from '@angular/core/testing';

import { AdminbeautyparlourService } from './adminbeautyparlour.service';

describe('AdminbeautyparlourService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminbeautyparlourService]
    });
  });

  it('should be created', inject([AdminbeautyparlourService], (service: AdminbeautyparlourService) => {
    expect(service).toBeTruthy();
  }));
});
