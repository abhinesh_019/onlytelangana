import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminbeautyparlourService {

  constructor(private http: HttpClient) { }
  individualUsers(id){ return this.http.get(environment.apiUrl +"/bpClientsgetId/"+id)}
  postsUpdata(id,data){
    return this.http.post(environment.apiUrl + "/updatesPostes/"+id,data);
     
  }
  updatesget(id) {
    return this.http.get(environment.apiUrl + "/bpupdates/"+id);
  }
  updatesgetCount(id) {
    return this.http.get(environment.apiUrl + "/updatesCount/"+id);
  }

  commentsgetupdates(id) {
    return this.http.get(environment.apiUrl + "/bpupdatesComments/"+id);
  }
  commentsgetupdatesCount(id) {
    return this.http.get(environment.apiUrl + "/updatesCommentsCount/"+id);
  }
  commentspostupdates(id,data) {
    return this.http.post(environment.apiUrl +"/updatesPostesCommentsReply/"+id,data);
  }

  commentsgetupdatesReply(id) {
    return this.http.get(environment.apiUrl + "/bpupdatesCommentsReply/"+id);
  }

  servicesgetPostes(id,data) {
    return this.http.post(environment.apiUrl + "/bpClientsService/"+id,data);
  }
  servicesget(id) {
    return this.http.get(environment.apiUrl + "/clientServices/"+id);
  }
  servicesgetcount(id) {
    return this.http.get(environment.apiUrl + "/clientServicesCounts/"+id);
  }

  equipmentsPosts(id,data) {
    return this.http.post(environment.apiUrl + "/beaautyParloureEquipmentsPost/"+id,data);
  }

  equipmentsGet(id) {
    return this.http.get(environment.apiUrl + "/beaautyParloureEquipmentsGet/"+id);
  }
  equipmentsGetC(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourEquipmentsGetCounts/"+id);
  }
  generalsCommGet(id) {
    return this.http.get(environment.apiUrl + "/bpGeneralCommentsGet/"+id);
  }
  generalsCommGetC(id) {
    return this.http.get(environment.apiUrl + "/bpGeneralCommentsGetCounts/"+id);
  }

  postsTraine(id,data) {
    return this.http.post(environment.apiUrl + "/bpTrainingPosts/"+id,data);
  }

  trainingsG(id) {
    return this.http.get(environment.apiUrl + "/bpTrainingGet/"+id);
  }

  trainingsGC(id) {
    return this.http.get(environment.apiUrl + "/bpTrainingGetCounts/"+id);
  }
 
  totalcustomers(id,data) {
    return this.http.post(environment.apiUrl + "/beaautyParlourTotalCustomersPost/"+id,data);
  }

  totalcustomersGet(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourTotalCustomersGet/"+id);
  }

  totalcustomersGetCounts(id) {
    return this.http.get(environment.apiUrl + "/beaautyParloureTotalCustomersCounts/"+id);
  }

  fecilitiesPosts(id,data) {
    return this.http.post(environment.apiUrl + "/beaautyParlourecitiesPost/"+id,data);
  }

  fecilitiesGet(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourecitiesGet/"+id);
  }

  fecilitiesGetCounts(id) {
    return this.http.get(environment.apiUrl + "/beaautyParlourecitiesGetCounts/"+id);
  }
}

