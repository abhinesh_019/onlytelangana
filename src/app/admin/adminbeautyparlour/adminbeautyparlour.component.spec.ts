import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminbeautyparlourComponent } from './adminbeautyparlour.component';

describe('AdminbeautyparlourComponent', () => {
  let component: AdminbeautyparlourComponent;
  let fixture: ComponentFixture<AdminbeautyparlourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminbeautyparlourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminbeautyparlourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
