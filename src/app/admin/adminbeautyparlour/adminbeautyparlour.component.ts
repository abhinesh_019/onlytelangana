import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminbeautyparlourService } from './adminbeautyparlour.service';

@Component({
  selector: 'app-adminbeautyparlour',
  templateUrl: './adminbeautyparlour.component.html',
  styleUrls: ['./adminbeautyparlour.component.css']
})
export class AdminbeautyparlourComponent implements OnInit {

  public showmains:boolean=false
  public updatesShows:boolean=false
  public viewupdatesShows:boolean=false
  public onlyCommentsShows:boolean=false
  public ImagesShows:boolean=false
  public ProductsShows:boolean=false
  public ProductsViewShows:boolean=false
  public viewsProfiles:boolean=false
  public viewsNoti:boolean=false
public usersID:any
public mainuser:any
public imageName:any
public videoName:any
 public clientsServices:any
 public clientsServicesCount:any
public updatesIds:any
public show:any
public showes:any
public shows:any
public showess:any
public selectedblogIds:any
public adminsupdateCommentsReply:any
public commentsUpdatesId:any
public adminupdates:any
public adminsupdateCountsall:any
public adminsupdateComments:any
public adminsupdateCommentsCount:any
public equGet:any
public equGetc:any
public gcommc:any
public gcomm:any
public tgetsc:any
public tgets:any
public imageNameCustomers:any
public totalcustomersGetCount:any
public totalcustomersGets:any
public imageNamefec:any
public fecilitiesGetCount:any
public fecilitiesGets:any
public imageNameCustomerst:any
public imageNameEquipment:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any
// public imageNameCustomers:any

 updatesCommentsdata={
  descriptions:""
}

equipments={
  equipnments:"",
  usages:"",
  description:"",
  images:""
}
postupdatedata={
  images:"",
  title:'',
  descriptions:"",
  viedoes:""
}

trainings={
  images:"",
  tclassname:'',
  tclassDescription:"",
  trainnerName:"",
   yearsOfExp:'',
  Shifts:"",
  ShiftsTimmingsFrom:"",
  ShiftsTimmingsTo:""
}

postupdatedataServices={
  images:"",
  servicesName:'',
  descriptions:"",
 }

 fec={
  fecilitiesName:"",
  fecilitiesImg:"",
  fecilitiesDescriptions:"",
 }

totCus={
  customersName:"",
  customersImg:"",
  customersPlace:"",
}
formData: FormData = new FormData(); 
    constructor(private route:ActivatedRoute,private parlour:AdminbeautyparlourService) { }
  
    ngOnInit() {
      this.usersID=localStorage.getItem('ads');
this.usersDetails()
this.getupdates()
this.getupdatesCount()
this.ServicesDetails()
this.ServicesDetailsCounts()
this.EquipmentsGet()
this.EquipmentsGetc()
this.generalCommGet()
this.generalCommGetc()
this.traineeGetc()
this.traineeGet()
this.fecilitiesGet()
this.fecilitiesGetCounts()
this.totalcustomersGet()
this.totalcustomersGetCounts()
    }
    usersDetails(){ 
      this.parlour.individualUsers(this.usersID).subscribe((res)=>{
        this.mainuser=res 
        
         
      })
    }

    handleFileInputFurnitures($event:any,images){
      this.imageName= File = $event.target.files[0];
   }

    handleFileInputsFurnitures($event:any,viedoes){
      this.videoName= File = $event.target.files[0];
      this.formData.append(viedoes,this.videoName);
      
   }
  
   UpdatePostData( ){
    this.formData.append('images',this.imageName);
    this.formData.append('title',this.postupdatedata.title);
    this.formData.append('descriptions',this.postupdatedata.descriptions);
   
     this.parlour.postsUpdata(this.usersID,this.formData).subscribe((res)=>{
        
         })
         this.postupdatedata.title="",
         this.postupdatedata.descriptions="",
         this.postupdatedata.images="",
         this.postupdatedata.viedoes=""
   }

    viewProfile(){
      this.viewsProfiles=!this.viewsProfiles
      this.viewsNoti=false
    }
    viewnoti(){
      this.viewsNoti=!this.viewsNoti
      this.viewsProfiles=false
  
    }
  
    mainMenuShow(){
  this.showmains=!this.showmains
  this.updatesShows=false
  this.viewupdatesShows=false
  this.onlyCommentsShows=false
  this.ImagesShows=false
  this.ProductsShows=false
  this.ProductsViewShows=false
    }
    updatesShow(){
      this.updatesShows=!this.updatesShows
      this.showmains=false
      this.viewupdatesShows=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ServicesShow(){
      this.viewupdatesShows=!this.viewupdatesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    onlyCommentsShow(){
      this.onlyCommentsShows=!this.onlyCommentsShows
      this.updatesShows=false
      this.showmains=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ImagesShow(){
      
      this.ImagesShows=!this.ImagesShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ProductsShows=false
      this.ProductsViewShows=false
    }
    ProductsShow(){
      this.ProductsShows=!this.ProductsShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsViewShows=false
    }
    ProductsViewShow(){
      this.ProductsViewShows=!this.ProductsViewShows
      this.updatesShows=false
      this.showmains=false
      this.onlyCommentsShows=false
      this.viewupdatesShows=false
      this.ImagesShows=false
      this.ProductsShows=false
    }
  
// *************get updates****************
getupdates(){
 
  this.parlour.updatesget(this.usersID).subscribe((res)=>{
    this.adminupdates=res
    
  })
       }
       getupdatesCount(){
 
        this.parlour.updatesgetCount(this.usersID).subscribe((res)=>{
          this.adminsupdateCountsall=res
          
        })
             }

showicons(){
  this.show=!this.show
  this.shows=false
}
showcomments(item?){
  this.show=false
  this.shows=!this.shows
   this.updatesIds=item._id
    
   this.comm()
 
        }
        updatestoAdmincommentsget(result?){
          this.showes=!this.showes
                   this.commentsUpdatesId=result
                    this.comm()
   this.commCount()
                     }
       comm(){
        this.parlour.commentsgetupdates(this.commentsUpdatesId).subscribe((res)=>{
            this.adminsupdateComments=res
           })
                } 
                commCount(){
                  this.parlour.commentsgetupdatesCount(this.commentsUpdatesId).subscribe((res)=>{
                      this.adminsupdateCommentsCount=res
                       
                     
                    })
                          }
        
               updatesAdminReply(items){
                this.showess=!this.showess
                this.selectedblogIds=items
               this.s()
              } 
              s(){
                this.parlour.commentsgetupdatesReply(this.selectedblogIds).subscribe((res)=>{
                  this.adminsupdateCommentsReply=res
                })
              }
              updatestoadmincomments(){
         
                this.parlour.commentspostupdates(this.selectedblogIds,this.updatesCommentsdata).subscribe((res)=>{
                  
                })
                this.updatesCommentsdata.descriptions=""
                 
                     }


                    //  ********************8services ***********************************8
                 
    handleFileInputBp($event:any,images){
      this.imageName= File = $event.target.files[0];
 
    }
    PostsServices(){ 
   this.formData.append('images',this.imageName);
   this.formData.append('servicesName',this.postupdatedataServices.servicesName);
   this.formData.append('descriptions',this.postupdatedataServices.descriptions);
   this.parlour.servicesgetPostes(this.usersID,this.formData).subscribe((res)=>{
     })
         this.postupdatedataServices.servicesName="",
         this.postupdatedataServices.descriptions="",
         this.postupdatedataServices.images=""
                       }

                    ServicesDetails(){ 
                      this.parlour.servicesget(this.usersID).subscribe((res)=>{
                      this.clientsServices=res
                    
                      })
                      }
                      ServicesDetailsCounts(){ 
                        this.parlour.servicesgetcount(this.usersID).subscribe((res)=>{
                        this.clientsServicesCount=res
                            })
                        }
        // ******************************EquipmentsPosts ************************************

     handleFileInputEquipmentsPosts($event:any,images){
        this.imageNameEquipment= File = $event.target.files[0];
     }
   
     EquipmentsPostsPosts(){
     
  this.formData.append('images',this.imageNameEquipment);
  this.formData.append('description',this.equipments.description);
  this.formData.append('equipnments',this.equipments.equipnments);
  this.formData.append('usages',this.equipments.usages);

     
       this.parlour.equipmentsPosts(this.usersID,this.formData).subscribe((res)=>{
          
           })
           this.equipments.images="",
           this.equipments.description="",
           this.equipments.equipnments="",
           this.equipments.usages=""
     }
                         EquipmentsGet(){ 
                           this.parlour.equipmentsGet(this.usersID).subscribe((res)=>{
                          this.equGet=res
                              })
                          }
                          EquipmentsGetc(){ 
                            this.parlour.equipmentsGetC(this.usersID).subscribe((res)=>{
                            this.equGetc=res
                            
                                })
                            }
                            generalCommGet(){ 
                              this.parlour.generalsCommGet(this.usersID).subscribe((res)=>{
                              this.gcomm=res
                                 })
                              }
                              generalCommGetc(){ 
                                this.parlour.generalsCommGetC(this.usersID).subscribe((res)=>{
                                this.gcommc=res
                                  })
                                }


    handleFileInputTrainee($event:any,images){
      this.imageName= File = $event.target.files[0];
   }
 
   PostsTrainee( ){
    this.formData.append('images',this.imageName);
    this.formData.append('tclassname',this.trainings.tclassname);
    this.formData.append('tclassDescription',this.trainings.tclassDescription);
    this.formData.append('trainnerName',this.trainings.trainnerName);
    this.formData.append('yearsOfExp',this.trainings.yearsOfExp);
    this.formData.append('Shifts',this.trainings.Shifts);
    this.formData.append('ShiftsTimmingsFrom',this.trainings.ShiftsTimmingsFrom);
    this.formData.append('ShiftsTimmingsTo',this.trainings.ShiftsTimmingsTo);
   
     this.parlour.postsTraine(this.usersID,this.formData).subscribe((res)=>{
        
         })
         this.trainings.tclassname="",
         this.trainings.tclassDescription="",
         this.trainings.trainnerName="",
         this.trainings.yearsOfExp="",
         this.trainings.Shifts="",
         this.trainings.ShiftsTimmingsFrom="",
         this.trainings.ShiftsTimmingsTo=""
   }

   traineeGet(){ 
    this.parlour.trainingsG(this.usersID).subscribe((res)=>{
    this.tgets=res
    
      })
    }

    traineeGetc(){ 
      this.parlour.trainingsGC(this.usersID).subscribe((res)=>{
      this.tgetsc=res
      
        })
      }


      // ******************************fecilities************************************
      handleFileInputfecilities($event:any,images){
        this.imageNameCustomerst= File = $event.target.files[0];
     }
   
     fecilitiesPosts(){
     
  this.formData.append('fecilitiesImg',this.imageNameCustomers);
  this.formData.append('fecilitiesName',this.fec.fecilitiesName);
  this.formData.append('fecilitiesDescriptions',this.fec.fecilitiesDescriptions);
      
     
       this.parlour.fecilitiesPosts(this.usersID,this.formData).subscribe((res)=>{
          
           })
           this.fec.fecilitiesDescriptions="",
           this.fec.fecilitiesName="",
           this.fec.fecilitiesImg=""
     }
     fecilitiesGet(){ 
      this.parlour.fecilitiesGet(this.usersID).subscribe((res)=>{
      this.fecilitiesGets=res
 
        })
      }
  
      fecilitiesGetCounts(){ 
        this.parlour.fecilitiesGetCounts(this.usersID).subscribe((res)=>{
        this.fecilitiesGetCount=res
 
          })
        }
   // ******************************totalCustomers************************************
   handleFileInputtotalCustomers($event:any,images){
    this.imageNameCustomers= File = $event.target.files[0];
 }

 totalCustomersPostes(){

  this.formData.append('customersImg',this.imageNameCustomerst);
  this.formData.append('customersName',this.totCus.customersName);
  this.formData.append('customersPlace',this.totCus.customersPlace);
  this.parlour.totalcustomers(this.usersID,this.formData).subscribe((res)=>{
         })
        this.totCus.customersName="",
       this.totCus.customersPlace="",
       this.totCus.customersImg=""
 }
  totalcustomersGet(){ 
      this.parlour.totalcustomersGet(this.usersID).subscribe((res)=>{
      this.totalcustomersGets=res
     
 })
      }
  
      totalcustomersGetCounts(){ 
        this.parlour.totalcustomersGetCounts(this.usersID).subscribe((res)=>{
        this.totalcustomersGetCount=res
  })
        }
    }

  
  

