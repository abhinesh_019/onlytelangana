import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment.prod';
@Injectable()
export class AdminService {
 
  constructor(private http: HttpClient) { }
  loginService(login) {
    
    return this.http.post(environment.apiUrl +"/login",login)
  
  }
  updatesdata(id) {
    return this.http.get(environment.apiUrl + "/updates/"+id);
  }
  commentsget(id) {
    return this.http.get(environment.apiUrl + "/babycarecomments/"+id);
  }
  postsdataup(id,data) {
    return this.http.post(environment.apiUrl + "/updates/"+id,data);
  }
 
 


  
// *********************************************************************************babycares**********************************************************
  getbabycares(id) {
    return this.http.get(environment.apiUrl + "/babycares/"+id);
  }
  postsdataupBabycares(id,data){
    return this.http.post(environment.apiUrl + "/updatespost/"+id,data);
   
  }

  overallscommentspostbaby(id){
     
    
    return this.http.get(environment.apiUrl +"/babycarecomments/"+id);
    
  }

  adminBabyUpdatesGet(id){
    return this.http.get(environment.apiUrl +"/updates/"+id);

  }
   
   commentsUpdatesAdminReply(id,data){
   
    
    return this.http.post(environment.apiUrl +"/updatesuserscommentsreply/"+id,data);

    
  }
  
  commentsgetupdates(id){
    return this.http.get(environment.apiUrl +"/updatesuserscommentsreply/"+id);

  }
  commentsgetupdate(id){
    return this.http.get(environment.apiUrl +"/updatesuserscomments/"+id);

  }
  updatesCommmentsCount(id){
    return this.http.get(environment.apiUrl +"/updatesuserscommentscount/"+id);

  }
  updatesCommmentsallCount(id){
    return this.http.get(environment.apiUrl +"/updatescounts/"+id);

  }
  updatesCommmentsOverAll(id){
    return this.http.get(environment.apiUrl +"/babycarecommentscounts/"+id);

  }
   
  updatePostDataCustomers(id,data){
      return this.http.post(environment.apiUrl +"/updatesuserscommentsreply/"+id,data);
   }
   customersGetsForProducts(id){
    return this.http.get(environment.apiUrl +"/updatesuserscommentsreply/"+id);
    }
 }





 
