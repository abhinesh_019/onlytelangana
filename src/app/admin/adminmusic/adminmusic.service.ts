import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()
export class AdminmusicService {

  constructor(private http: HttpClient) { }
  
  usersDetails(id){
    return this.http.get(environment.apiUrl + "/musicClientsGetind/"+id);
     
  }
  updatesPostDataFec(id,data){
    return this.http.post(environment.apiUrl + "/musicfecitiesPost/"+id,data);
     
  }
  serviceTypesCat(id,data){
    return this.http.post(environment.apiUrl + "/musicServicesPost/"+id,data);
     
  }
  serviceOneGet(id){
    return this.http.get(environment.apiUrl + "/musicServicesGet/"+id);
     
  }

  servicesTwoPosts(id,data){
    return this.http.post(environment.apiUrl + "/musicServicesTwoPost/"+id,data);
     
  }

  serviceTwoGet(id){
    return this.http.get(environment.apiUrl + "/musicServicesTwoGet/"+id);
     
  }
  fecilitiesGet(id){
    return this.http.get(environment.apiUrl + "/musicfecitiesGet/"+id);
     
  }
  fecilitiesGetC(id){
    return this.http.get(environment.apiUrl + "/musicfecitiesGetCounts/"+id);
     
  }

  serviceTypeDataGet(id){
    return this.http.get(environment.apiUrl + "/musicServiceGet/"+id);
     
  }

  updatesPostDatas(id,data){
    return this.http.post(environment.apiUrl + "/musicupdatespostsdata/"+id,data);
  }

  postsServices(id,data){
    return this.http.post(environment.apiUrl + "/musicServicesPost/"+id,data);
  }

  serviceDataGet(id){
    return this.http.get(environment.apiUrl + "/musicServicesGet/"+id);
     
  }
  serviceDataGetC(id){
    return this.http.get(environment.apiUrl + "/musicServicesGetCounts/"+id);
     
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/musicupdatesCommentReplysPost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/musicupdatesCommentReplysGet/"+id);
  }
  generalsCommGet(id) {
    return this.http.get(environment.apiUrl + "/musiceuserscomments/"+id);
  }
  generalsCommGetC(id) {
    return this.http.get(environment.apiUrl + "/musicCeuserscommentsCounts/"+id);
  }
  updatesPostDataCats(id,data){
    return this.http.post(environment.apiUrl + "/musicCatageriesPosts/"+id,data);
     
  }
  CatageriesGets(id){
    return this.http.get(environment.apiUrl + "/musicCatageriesGets/"+id);
     
  }
}