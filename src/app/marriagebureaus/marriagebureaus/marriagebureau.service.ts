import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
  
@Injectable()
export class MarriagebureauService {

  constructor(private http: HttpClient) { }
  locationapi( ){
    return   this.http.get(environment.apiUrl +"/marriagesLocations");
   }
   areaapi(id){
    return   this.http.get(environment.apiUrl +"/marriagesAreas/"+id);
   }


 marriageusers(id,data){
    
    return this.http.get(environment.apiUrl +"/marriagesClientsget/"+id,{params:data});
  }
  marriageusersForSingleAreas(id){
    return   this.http.get(environment.apiUrl +"/marriagesClientsgetAreaSingleCnt/"+id);
   }
  marriageuserscountstot(){
    return   this.http.get(environment.apiUrl +"/marriagesClientsAllCount");
   }
   marriageusersForLocations(){
    return   this.http.get(environment.apiUrl +"/marriagesClientsAllCountLocations");
   }
   marriageusersForMainAreas(id){
    return   this.http.get(environment.apiUrl +"/marriagesClientsAllCountMainArea/"+id);
   }
   marriageusersForSubAreas(id){
    return   this.http.get(environment.apiUrl +"/marriagesClientsAreas/"+id);
   }
   babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/marriagesAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/marriagesAddsFiveLocGet/"+id);
  }
}