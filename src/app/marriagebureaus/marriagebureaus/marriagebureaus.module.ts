import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { MarriagebureausComponent } from './marriagebureaus.component';
import { MarriagebureausdetailsModule } from '../marriagebureausdetails/marriagebureausdetails.module';
import { MarriagebureauService } from './marriagebureau.service';
 

const routes: Routes = [
  {
    path: '', component: MarriagebureausComponent, children:
      [
      { path: 'marriagebureausdetails/:_id/:name', loadChildren: 'app/marriagebureaus/marriagebureausdetails/marriagebureausdetails.module#MarriagebureausdetailsModule'},
 
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     MarriagebureausdetailsModule
      ],
  declarations: [
    MarriagebureausComponent
     ],
  providers:[MarriagebureauService]
})


export class MarriagebureausModule { }
