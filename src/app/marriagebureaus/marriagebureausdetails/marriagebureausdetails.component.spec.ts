import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarriagebureausdetailsComponent } from './marriagebureausdetails.component';

describe('MarriagebureausdetailsComponent', () => {
  let component: MarriagebureausdetailsComponent;
  let fixture: ComponentFixture<MarriagebureausdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarriagebureausdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarriagebureausdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
