import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MarriagebureausdetailsComponent } from './marriagebureausdetails.component';
import { MarriagebureaudetailsService } from './marriagebureaudetails.service';
 


const routes:Routes=[{path:'marriagebureausdetails/:_id/:name',component:MarriagebureausdetailsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    MarriagebureausdetailsComponent,

  ],
  providers: [MarriagebureaudetailsService],
})
export class MarriagebureausdetailsModule { }
