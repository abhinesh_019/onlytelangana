import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MarriagebureaudetailsService } from './marriagebureaudetails.service';

@Component({
  selector: 'app-marriagebureausdetails',
  templateUrl: './marriagebureausdetails.component.html',
  styleUrls: ['./marriagebureausdetails.component.css']
})
export class MarriagebureausdetailsComponent implements OnInit {
public ids:any
public marClients:any
public cateCat:any
public castecatName:any
public castecatId:any
public msincateCat:any
public smsincateCat:any
public smaincastecatName:any
public smaincastecatId:any
public maincastecatId:any
public maincastecatName:any
public trackRec:any
public trackRecc:any
public openshareid:any
public commentspostsId:any
public viewcommentsid:any
public clientUpdates:any
public replyid:any
public clientUpdatesCount:any
 public getUsersComments:any
public getUsersCommentsCounts:any
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public replies:any


postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
   
  constructor(private router:Router,private route:ActivatedRoute,private mar:MarriagebureaudetailsService) { }
  ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.casteCatGet()
   this.clientsUpdates()
   this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
 }

usersDetails(){ 
this.mar.usersDetails(this.ids).subscribe((res)=>{
this.marClients=res
 
})
}

casteCatGet(){ 
  this.mar.casteCatGet(this.ids).subscribe((res)=>{
  this.cateCat=res
  var id=this.cateCat[0]
  this.casteCatClick(id)  
  })
   }

   casteCatClick(c?){
this.castecatName=c.casteType
this.castecatId=c._id
this.maincasteCatGet()
   }
   maincasteCatGet(){ 
    this.mar.maincasteCatGet(this.castecatId).subscribe((res)=>{
    this.msincateCat=res
  
    })
     }
     mainCasteCatClick(c){
      this.maincastecatName=c.MainCaste
      this.maincastecatId=c._id
       this.subcasteCatGet()
       this.tracksRecords()
       this.tracksRecordsc()
           }
     subcasteCatGet(){ 
      this.mar.smaincasteCatGet(this.maincastecatId).subscribe((res)=>{
      this.smsincateCat=res
       console.log(res);
       
      })
       }
  
       subCasteCatClick(c){
           this.smaincastecatName=c.MainCaste
          this.smaincastecatId=c._id
         
                          }
                          tracksRecords(){
                            this.mar.tracksRecords(this.maincastecatId).subscribe((res)=>{
                              this.trackRec=res
                              console.log(res);
                               })
                        }
                        tracksRecordsc(){
                          this.mar.tracksRecordsc(this.maincastecatId).subscribe((res)=>{
                            this.trackRecc=res
                            console.log(res);
                             })
                        }

// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid);
  
}
 clientsUpdates(){   
  this.mar.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.mar.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.mar.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.mar.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.mar.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.mar.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res);
    
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.mar.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }



}

