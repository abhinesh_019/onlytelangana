import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class FurnituresService {
  constructor(private http: HttpClient) { }
  
  locationapi(){
    
    return this.http.get(environment.apiUrl +"/furnitureslocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAreas/"+id);
  }
  indoorUserscountAllMainAreas(){
    
    return this.http.get(environment.apiUrl +"/furnituresCountAreas");
  }
  indoorUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/furnituresClientsCountDist");
  }
  furnitureusers(id,data){
    
    return this.http.get(environment.apiUrl +"/furnituresusers/"+id,{params:data});
  }
  furnitureusersonly(id){
    
    return this.http.get(environment.apiUrl +"/furnitures/"+id);
  }
  
  furnitureuserscounts(id){
    return this.http.get(environment.apiUrl +"/furnituresusersCountsByArea/"+id);
  }
  furnitureuserscountstot(){
    
    return this.http.get(environment.apiUrl +"/furnituresuserstotcout");
  }
  furnitureuserscountsarea(id){
    
    return this.http.get(environment.apiUrl +"/furnituresuserscountarea/"+id);
  }

  

  // ********************************
 

 
  // ***************************************

  // ****************overall comments*****************
  overallscommentspost(id,data){
     
    
    return this.http.post(environment.apiUrl +"/furnituresuserscomments/"+id,data);
    
  }

  // *********************product descriptions**************
  furnitureTypeGets(id){
    return this.http.get(environment.apiUrl +"/furnitureTypesGet/"+id);

  }
  products(id) {
    return this.http.get(environment.apiUrl +"/furnituresproducts/"+id);
  }
  productsCounts(id){
    return this.http.get(environment.apiUrl +"/furnituresproductsCounts/"+id);

  }
// *****************  updates*********************
  Getupdates(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesget/"+id);
  }
  GetupdatesCounts(id){
    return this.http.get(environment.apiUrl +"/furnituresUpdatesGetCounts/"+id);
  }
  //****updates comments  */
  commentspostupdates(id,data){
    return this.http.post(environment.apiUrl +"/furnituresupdatesCommentspost/"+id,data);

  }
  commentsgetupdates(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesCommentsget/"+id);

  }
  commentsgetupdatesCounts(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesCommentsgetcounts/"+id);

  }
  
  commentsgetupdatesReply(id){
    return this.http.get(environment.apiUrl +"/furnituresupdatesCommentReplysGet/"+id);

  }
  commentsUpdatesAdminReply(id,data){
   
    
    return this.http.post(environment.apiUrl +"/furnituresupdatesCommentReplysPost/"+id,data);

    
  }
  
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/furnituresAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/furnituresAddsFourLocGet/"+id);
  }
  babycaresAddsFivel(id){
        
    return this.http.get(environment.apiUrl +"/furnituresAddsFiveLocGet/"+id);
  }
}