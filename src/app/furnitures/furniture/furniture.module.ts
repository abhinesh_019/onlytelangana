import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
 import { FurnituredetailsModule } from '../furnituredetails/furnituredetails.module';
import { FurnitureComponent } from '../furniture/furniture.component';
import { FurnituresService } from '../furnitures.service';

const routes: Routes = [
  {
    path: '', component: FurnitureComponent, children:
      [
      { path: 'furnituredetail/:_id/:name', loadChildren: 'app/furnitures/furnituredetails/furnituredetails.module#FurnituredetailsModule'},
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     FurnituredetailsModule  
    ],
  declarations: [
    FurnitureComponent,
  ],
  providers:[FurnituresService]
})
 
export class FurnitureModule { }
