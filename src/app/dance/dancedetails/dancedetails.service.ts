import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class DancedetailsService {

  constructor(private http: HttpClient) { }
  usersDetails(id){
    return   this.http.get(environment.apiUrl +"/danceClientsGetind/"+id);
   }
   serviceTypeDataGet(id){
    return this.http.get(environment.apiUrl + "/danceServicesGet/"+id);
     
  }
 
  serviceDataGet(id){
    return this.http.get(environment.apiUrl + "/danceServicesTwoGet/"+id);
     
  }
  serviceDataGetC(id){
    return this.http.get(environment.apiUrl + "/danceServicesGetCounts/"+id);
     
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/danceupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/danceupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/daneceuserscomments/"+id,data);
  }
  // ********************************
  fecilitiesGet(id){
    return this.http.get(environment.apiUrl + "/dancefecitiesGet/"+id);
     
  }
  fecilitiesGetC(id){
    return this.http.get(environment.apiUrl + "/dancefecitiesGetCounts/"+id);
     
  }
  CatageriesGets(id){
    return this.http.get(environment.apiUrl + "/danceCatageriesGets/"+id);
     
  }
}

