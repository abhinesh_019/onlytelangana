import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { DanceComponent } from './dance.component';
import { DancedetailsModule } from '../dancedetails/dancedetails.module';
import { DancemainService } from './dancemain.service';

const routes: Routes = [
  {
    path: '', component: DanceComponent, children:
      [
      { path: 'dancedetails/:_id/:name', loadChildren: 'app/dance/dancedetails/dancedetails.module#DancedetailsModule'},
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     DancedetailsModule
  ],
  declarations: [
    DanceComponent
     ],
  providers:[DancemainService]
})

 
export class DanceModule { }
