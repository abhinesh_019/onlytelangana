import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class DancemainService {

  constructor(private http: HttpClient) { }

  locationapi(){
    
    return this.http.get(environment.apiUrl +"/danceLocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/areasDacnce/"+id);
  }
  musicUsers(id,data){
    
    return this.http.get(environment.apiUrl +"/danceClientsget/"+id,{params:data});
  }
  musicUserscountsAll(){
    
    return this.http.get(environment.apiUrl +"/danceClientsAllCount");
  }

  musicUserscountAllMainAreas(){
    
    return this.http.get(environment.apiUrl +"/danceClientsCountAreas");
  }

  musicUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/danceClientsCountDist");
  }

  musicUserscountAllAreas(id){
    
    return this.http.get(environment.apiUrl +"/danceClientsCount/"+id);
  }
  musicUserscountAllAreasover(id){
    
    return this.http.get(environment.apiUrl +"/danceClientsAreas/"+id);
  }

  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/danceAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/danceAddsFiveLocGet/"+id);
  }
}