import { Component, OnInit } from '@angular/core';
import { ProfessionalInstitutionsCatageriesService } from './professional-institutions-catageries.service';

@Component({
  selector: 'app-professional-institutions-catageries',
  templateUrl: './professional-institutions-catageries.component.html',
  styleUrls: ['./professional-institutions-catageries.component.css']
})
export class ProfessionalInstitutionsCatageriesComponent implements OnInit {

  public catList:any
  
  constructor(private fh:ProfessionalInstitutionsCatageriesService) { }
   

  ngOnInit() {
    this.getcatagories()
    
  }
  
  getcatagories(){
    this.fh.catapigetdetails().subscribe((res)=>{
  this.catList=res
  console.log(this.catList);
   
          })  }
         
     
}