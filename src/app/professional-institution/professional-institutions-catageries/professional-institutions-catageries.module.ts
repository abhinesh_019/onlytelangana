import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { SharedModule } from '../../shared/shared.module';
import { ProfessionalInstitutionsCatageriesComponent } from './professional-institutions-catageries.component';
import { ProfessionalInstitutionsCatageriesService } from './professional-institutions-catageries.service';
import { ProfessionalInstitutionsModule } from '../professional-institutions/professional-institutions.module';
import { ProfessionalInstiutionsDetailsModulesModule } from '../professional-instiutions-details/professional-instiutions-details-modules.module';
 
const routes: Routes = [
  {
    path: '', component: ProfessionalInstitutionsCatageriesComponent, children:
      [
      { path: 'professionalInstituions/:_id/:name', loadChildren: 'app/professional-institution/professional-institutions/professional-institutions.module#ProfessionalInstitutionsModule'},
      { path: 'professionalInstituionsDetails/:_id/:name', loadChildren: 'app/professional-institution/professional-instiutions-details/professional-instiutions-details-modules.module#ProfessionalInstiutionsDetailsModulesModule'},

       ]
  }]
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    ProfessionalInstitutionsModule,
    ProfessionalInstiutionsDetailsModulesModule,
     SharedModule
   ],
   
  declarations: [
    ProfessionalInstitutionsCatageriesComponent,
     
  ],
  providers:[ProfessionalInstitutionsCatageriesService]
})
export class ProfessionalInstitutionsCatageriesModule { }
