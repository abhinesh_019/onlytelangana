import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalInstiutionsDetailsComponent } from './professional-instiutions-details.component';

describe('ProfessionalInstiutionsDetailsComponent', () => {
  let component: ProfessionalInstiutionsDetailsComponent;
  let fixture: ComponentFixture<ProfessionalInstiutionsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionalInstiutionsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalInstiutionsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
