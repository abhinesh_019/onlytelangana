import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
  
@Injectable()
export class ProfessionalInstiutionsDetailsModulesService {

  constructor(private http: HttpClient) { }
 
  usersDetails(id){
    return   this.http.get(environment.apiUrl +"/professionalInstclientsGetind/"+id);
   }
   aboutUsDetais(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstAboutUsget/"+id);
  }
  getPhilosophys(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstPhilosophy/"+id);
  }
  getWhyInsts(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstJourneyWhyTwo/"+id);
  }
  getinfraStructure(id){
    
    return this.http.get(environment.apiUrl +"/professionalInfraStructure/"+id);
  }
  getAwardsAndRewards(id){
    
    return this.http.get(environment.apiUrl +"/professionalAwardsRewards/"+id);
  }
  getboardsDirector(id){
    
    return this.http.get(environment.apiUrl +"/professionalBoardDirectors/"+id);
  }
  getcatageriesTwo(id){
    
    return this.http.get(environment.apiUrl +"/professionalSerivesTwo/"+id);
  }
  getcatageriesOne(id){
    
    return this.http.get(environment.apiUrl +"/professionalSerivesOne/"+id);
  }
  getTrackRecordsYears(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstyear/"+id);
  }
  getBranches(id){
    
    return this.http.get(environment.apiUrl +"/professionalBranches/"+id);
  }
  syllabusDetailsGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalsyllabusOne/"+id);
  }
  syllabusTwoDetailsGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalsyllabusTwo/"+id);
  }
  elegibilitiesGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalEligiblities/"+id);
  }
  organizationsGets(id){
    
    return this.http.get(environment.apiUrl +"/professionalimpOrganizationsOne/"+id);
  }
  importantNotificationsGets(id){
    
    return this.http.get(environment.apiUrl +"/professionalImportantNotifications/"+id);
  }
  importantNotificationsGetsc(id){

    return this.http.get(environment.apiUrl +"/professionalImportantNotificationsCount/"+id);
  }
  getmaterials(id){
    
    return this.http.get(environment.apiUrl +"/professionalMaterials/"+id);
  }
  newBatchGets(id){
    
    return this.http.get(environment.apiUrl +"/professionalNewBatchOne/"+id);
  }
  admissionsGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalAdminProcess/"+id);
  }
  freeStructureGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalFeeStructureTwo/"+id);
  }
  batchesGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalBatchGet/"+id);
  }
  facultyGets(id){
    
    return this.http.get(environment.apiUrl +"/professionalfacultyDetails/"+id);
  }
  testAnaysisGets(id){
    
    return this.http.get(environment.apiUrl +"/professionaltestAnalysis/"+id);
  }
  computerLabGets(id){
    
    return this.http.get(environment.apiUrl +"/professionalComputerLab/"+id);
  }
  libraryOneGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalliraryOneGet/"+id);
  }
  libraryTwoGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalliraryTwoGet/"+id);
  }
  computerLabThreeGets(id){
    
    return this.http.get(environment.apiUrl +"/professionalliraryThree/"+id);
  }
  trackRecordsGets(id){
    
    return this.http.get(environment.apiUrl +"/professionaltrackRecord/"+id);
  }

  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/professionalInstupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/professionalInstuserscomments/"+id,data);
  }
  // ********************************

}

