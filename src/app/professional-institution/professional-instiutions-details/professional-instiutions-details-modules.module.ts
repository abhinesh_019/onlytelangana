import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ProfessionalInstiutionsDetailsComponent } from './professional-instiutions-details.component';
import { ProfessionalInstiutionsDetailsModulesService } from './professional-instiutions-details-modules.service';
 

const routes:Routes=[
  {path:'professionalInstituionsDetails/:_id/:name',component:ProfessionalInstiutionsDetailsComponent},

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule,
     
    

  ],
  declarations: [
    ProfessionalInstiutionsDetailsComponent,

  ],
  providers: [ProfessionalInstiutionsDetailsModulesService],
})
export class ProfessionalInstiutionsDetailsModulesModule { }
