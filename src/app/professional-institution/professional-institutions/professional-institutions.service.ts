import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
     
@Injectable()
export class ProfessionalInstitutionsService {
  constructor(private http: HttpClient) { }
  individualcat(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstCategoriesIds/"+id);
  }  
  usersForLocations(){
    
    return this.http.get(environment.apiUrl +"/professionalInstUsersLocationsGroups");
  } 
  locationsget(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstlocations/"+id);
  } 
  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstAreas/"+id);
  } 


  Client(id,data){
    
    return this.http.get(environment.apiUrl +"/professionalInstlientsget/"+id,{params:data});
  }
  subArea(id){
    
    return this.http.get(environment.apiUrl +"/clientsAreas/"+id);
  }

  Area(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstclientsCount/"+id);
  }

  
 
  catCount(){
    
    return this.http.get(environment.apiUrl +"/professionalInstUsersAllCounts");
  }

  catContCountsGroups(){
    
    return this.http.get(environment.apiUrl +"/professionalInstUsersLocationsGroups");
  }
  catContCountsSoftware(){
    
    return this.http.get(environment.apiUrl +"/professionalInstUsersLocationsSoftware");
  }
  catContCountsBank(){
    
    return this.http.get(environment.apiUrl +"/professionalInstUsersLocationsBanks");
  }
  subAreasCounts(id){
    
    return this.http.get(environment.apiUrl +"/professionalInstclientsAreas/"+id);
  }

  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/professionalAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/professionalAddsFiveLocGet/"+id);
  }
   
}
