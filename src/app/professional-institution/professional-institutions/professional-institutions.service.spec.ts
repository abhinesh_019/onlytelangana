import { TestBed, inject } from '@angular/core/testing';

import { ProfessionalInstitutionsService } from './professional-institutions.service';

describe('ProfessionalInstitutionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfessionalInstitutionsService]
    });
  });

  it('should be created', inject([ProfessionalInstitutionsService], (service: ProfessionalInstitutionsService) => {
    expect(service).toBeTruthy();
  }));
});
