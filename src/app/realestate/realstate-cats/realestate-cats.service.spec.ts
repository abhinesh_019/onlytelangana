import { TestBed, inject } from '@angular/core/testing';

import { RealestateCatsService } from './realestate-cats.service';

describe('RealestateCatsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RealestateCatsService]
    });
  });

  it('should be created', inject([RealestateCatsService], (service: RealestateCatsService) => {
    expect(service).toBeTruthy();
  }));
});
