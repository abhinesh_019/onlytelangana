import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RealestateCatsService } from './realestate-cats.service';

@Component({
  selector: 'app-realstate-cats',
  templateUrl: './realstate-cats.component.html',
  styleUrls: ['./realstate-cats.component.css']
})
export class RealstateCatsComponent implements OnInit {

  public locationsget:any
  public locationName:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public its:any
  public searchString:any
  public realestateUsers:any
  public catCounts:any
  public clientsAreaCounts:any
  public clientsAreaCountsInd:any
  public locationsCountAlls:any
    
 // **********************************************************
 
 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any
  
    constructor(private router:Router,private route:ActivatedRoute,private real:RealestateCatsService) { }
  
    ngOnInit() {
      this.getlocations()
  
    this.ClientsAllCounts()
    }
  
      
    // locations*****************
  getlocations(){
    this.real.locationapi().subscribe((res)=>{
      this.locationsget=res;
     var id =this.locationsget[0];
       this.location(id)
     })}
  
    location(get?){
      this.locationName=get.locations
      this.its=get._id
      this.allAreas()
      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
     }
   
     allAreas(){
       this.real.areaapi(this.its).subscribe((res)=>{
        this.areasAll=res
        var id =this.areasAll[0];
         this.selectedAre(id)
        
       })
     }
  
  selectedAre(result){
  this.selectAreaId=result._id
  this.selectArea=result.area
  this.realestateUsersall()
  this.clientsAreaId()
  this.clientsAreaIdind()
  this.locationsCountsAll()
  this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
    }
   
  realestateUsersall(){
    var data:any =  {}
    if(this.searchString){
      data.search=this.searchString
      }
    this.real.realestateUsersall(this.selectAreaId,data).subscribe((res)=>{
      this.realestateUsers=res
      console.log(res);
      
      })
   }
  
  searchFilter(){
    this.realestateUsersall()
  }
  ClientsAllCounts(){
    this.real.ClientsAllCounts().subscribe((res)=>{
     this.catCounts=res
     })
  }
  clientsAreaId(){
    this.real.clientsAreaId(this.selectAreaId).subscribe((res)=>{
     this.clientsAreaCounts=res
     })
  }
  clientsAreaIdind(){
    this.real.clientsAreaIdind(this.selectAreaId).subscribe((res)=>{
     this.clientsAreaCountsInd=res
      })
  }
  locationsCountsAll(){
    this.real.locationsCountsAll().subscribe((res)=>{
     this.locationsCountAlls=res
    console.log(res);
    
    })
  }
  // ***************************************************************
  
  // ****************************************************

babycaressareasAddsOnea(){
  this.real.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.real.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.real.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.real.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.real.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.real.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.real.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.real.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.real.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}
