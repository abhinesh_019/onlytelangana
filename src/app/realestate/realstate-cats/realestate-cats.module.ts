import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
 import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RealestateModule } from '../realestate/realestate.module';
import { ReestateMainModule } from '../realestate-main/reestate-main.module';
import { RealestateCatsService } from './realestate-cats.service';
import { RealstateCatsComponent } from './realstate-cats.component';

const routes: Routes = [
  {
    path: '', component: RealstateCatsComponent, children:
      [
      { path: 'realestatedetails/:_id/:name', loadChildren: 'app/realestate/realestate/realestate.module#RealestateModule'},
      { path: 'realestateMain/:_id/:name', loadChildren: 'app/realestate/realestate-main/reestate-main.module#ReestateMainModule'},
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     RealestateModule,
     ReestateMainModule
     ],
  declarations: [
    RealstateCatsComponent,
  ],
  providers:[RealestateCatsService]
})
export class RealestateCatsModule { }
