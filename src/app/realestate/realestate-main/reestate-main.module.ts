import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RealestateMainComponent } from './realestate-main.component';
import { ReestateMainService } from './reestate-main.service';
 

const routes:Routes=[
  {path:'realestateMain/:_id/:propertyName',component:RealestateMainComponent},

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    RealestateMainComponent,

  ],
  providers: [ReestateMainService],
})
export class ReestateMainModule { }
