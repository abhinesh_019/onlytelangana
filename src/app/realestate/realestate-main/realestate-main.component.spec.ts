import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealestateMainComponent } from './realestate-main.component';

describe('RealestateMainComponent', () => {
  let component: RealestateMainComponent;
  let fixture: ComponentFixture<RealestateMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealestateMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealestateMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
