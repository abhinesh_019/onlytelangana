import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReestateMainService } from './reestate-main.service';

@Component({
  selector: 'app-realestate-main',
  templateUrl: './realestate-main.component.html',
  styleUrls: ['./realestate-main.component.css']
})
export class RealestateMainComponent implements OnInit {

  formData: FormData = new FormData(); 

public ids:any
public realPropertyDetailsInd:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
// public ids:any
 public sectionsList:any
public propertyDetailsId:any
public outerViewForm:boolean=false
public innerView:boolean=false
public OnlyouterView:boolean=false
public laForm:boolean=false
public amenities:boolean=false 
public overViewsForm:boolean=false
public basicDetailsOuterViews:any
public basicDetailsInnerViews:any
public basicDetailsInnerViewsC:any
public locationsAdvanages:any
public locationsAdvanagesC:any
public basicDetailsOuterViewsC:any
public AmenitiesGet:any
public overviewsGet:any
public clientGet:any
 
constructor(private route:ActivatedRoute,private real:ReestateMainService) { }
property={
  area:"",
  reType:"",
  reTypeBussinessType:"",
  propertyTypeBussinessLogic:"",
   propertyArea:"",
   propertyName:"",
   shortDesc:"",  
   longDesc:"", 
   furnituresDetails:"",
   price:"", 
   locations:"", 
   landmark:"", 
   lat:"", 
   long:"", 
   status:"", 
   floor:"", 
   verifications:"", 
   optionalDescriptions:"",
  outerFsideNme:"",
  outerFsideDes:"",
  outerBsideNme:"",
  outerBsideDes:"",
  outerLsideNme:"",
  outerLsideDes:"",
  outerRsideNme:"",
  outerRsideDes:"",
  outerFsideimg:"",
  outerBsideimg:"",
  outerLsideimg:"",
  outerRsideimg:"",

  innerName1:"",
  innerName2:"",
  innerName3:"",
  innerName4:"",
  innerName5:"",
  innerName6:"",
  innerName7:"",
  innerName8:"",
  innerName9:"",
  innerName10:"",
  innerName11:"",
  innerName12:"",
  innerName13:"",
  innerName14:"",
  innerName15:"",
  innerName16:"",
  innerName17:"",
  innerName18:"",
  innerName19:"",
  innerName20:"",

  innerNdes1:"",
  innerNdes2:"",
  innerNdes3:"",
  innerNdes4:"",
  innerNdes5:"",
  innerNdes6:"",
  innerNdes7:"",
  innerNdes8:"",
  innerNdes9:"",
  innerNdes10:"",
  innerNdes11:"",
  innerNdes12:"",
  innerNdes13:"",
  innerNdes14:"",
  innerNdes15:"",
  innerNdes16:"",
  innerNdes17:"",
  innerNdes18:"",
  innerNdes19:"",
  innerNdes20:"",

  innerNimg1:"",
  innerNimg2:"",
  innerNimg3:"",
  innerNimg4:"",
  innerNimg5:"",
  innerNimg6:"",
  innerNimg7:"",
  innerNimg8:"",
  innerNimg9:"",
  innerNimg10:"",
  innerNimg11:"",
  innerNimg12:"",
  innerNimg13:"",
  innerNimg14:"",
  innerNimg15:"",
  innerNimg16:"",
  innerNimg17:"",
  innerNimg18:"",
  innerNimg19:"",
  innerNimg20:"",


  lAdvantagesName1:"",
  lAdvantagesName2:"",
  lAdvantagesName3:"",
  lAdvantagesName4:"",
  lAdvantagesName5:"",
  lAdvantagesName6:"",
  lAdvantagesName7:"",
  lAdvantagesName8:"",
  lAdvantagesName9:"",
  lAdvantagesName10:"",
  lAdvantagesName11:"",
  lAdvantagesName12:"",
  lAdvantagesName13:"",
  lAdvantagesName14:"",
  lAdvantagesName15:"",
  lAdvantagesName16:"",
  lAdvantagesName17:"",
  lAdvantagesName18:"",
  lAdvantagesName19:"",
  lAdvantagesName20:"",

  lAdvantagesImg1:"",
  lAdvantagesImg2:"",
  lAdvantagesImg3:"",
  lAdvantagesImg4:"",
  lAdvantagesImg5:"",
  lAdvantagesImg6:"",
  lAdvantagesImg7:"",
  lAdvantagesImg8:"",
  lAdvantagesImg9:"",
  lAdvantagesImg10:"",
  lAdvantagesImg11:"",
  lAdvantagesImg12:"",
  lAdvantagesImg13:"",
  lAdvantagesImg14:"",
  lAdvantagesImg15:"",
  lAdvantagesImg16:"",
  lAdvantagesImg17:"",
  lAdvantagesImg18:"",
  lAdvantagesImg19:"",
  lAdvantagesImg20:"",


  amenitiesNames1:"",
  amenitiesNames2:"",
  amenitiesNames3:"",
  amenitiesNames4:"",
  amenitiesNames5:"",
  amenitiesNames6:"",
  amenitiesNames7:"",
  amenitiesNames8:"",
  amenitiesNames9:"",
  amenitiesNames10:"",
  amenitiesNames11:"",
  amenitiesNames12:"",
  amenitiesNames13:"",
  amenitiesNames14:"",
  amenitiesNames15:"",
  amenitiesNames16:"",
  amenitiesNames17:"",
  amenitiesNames18:"",
  amenitiesNames19:"",
  amenitiesNames20:"",


  amenitiesImg1:"",
  amenitiesImg2:"",
  amenitiesImg3:"",
  amenitiesImg4:"",
  amenitiesImg5:"",
  amenitiesImg6:"",
  amenitiesImg7:"",
  amenitiesImg8:"",
  amenitiesImg9:"",
  amenitiesImg10:"",
  amenitiesImg11:"",
  amenitiesImg12:"",
  amenitiesImg13:"",
  amenitiesImg14:"",
  amenitiesImg15:"",
  amenitiesImg16:"",
  amenitiesImg17:"",
  amenitiesImg18:"",
  amenitiesImg19:"",
  amenitiesImg20:"",
 }
overviews={

overview1:"",
overview2:"",
overview3:"",
overview4:"",
overview5:"",
overview6:"",
overview7:"",
overview8:"",
overview9:"",
overview10:"",
overview11:"",
overview12:"",
overview13:"",
overview14:"",
overview15:"",
overview16:"",
overview17:"",
overview18:"",
overview19:"",
overview20:"",

}
  ngOnInit() {
    this.ids=this.route.snapshot.params['_id'];
    this.propertyDetailsId=this.ids
console.log(this.ids);
this.usersDetails()
this.viewAllDetails()
this.basicPropertyOuterVIew()
  }
  usersDetails(){ 
    this.real.usersDetails(this.ids).subscribe((res)=>{
      this.realPropertyDetailsInd=res
     })
  }
            
   // <!-- *********************************************** BASIC PROPERT DETAILS outer view ****************************************************************** -->
   viewAllDetails(){
  
    
     this.basicPropertyInnerGetView()
     this.getLocationsAdvantages()
    this.getAmenities()
    this.getOverviews()
  }


   
      


   basicPropertyOuterVIew(){
    this.real.basicPropertyOuterVIew(this.propertyDetailsId).subscribe((res)=>{
      this.basicDetailsOuterViews=res
    
    })
         }
          
        // <!-- ***************************************************************************************************************** -->
      // ******************************************************** post and get INNER VIEW PROPERTY **********************************

  // post inner deatils
  

      basicPropertyInnerGetView(){
        this.real.basicPropertyInnerVIew(this.propertyDetailsId).subscribe((res)=>{
          this.basicDetailsInnerViews=res
           
        })
             }
            

            //  *********************************************************************************************************************************************
       // ******************************************************** post locations advantages **********************************

  // post inner deatils
  handleFileInputla($event:any,img) {
      let imageFile= File = $event.target.files[0];
      this.formData.append(img, imageFile);
      console.log(imageFile);
      
    }
    
  


      getLocationsAdvantages(){
        this.real.getLocationsAdvantages(this.propertyDetailsId).subscribe((res)=>{
          this.locationsAdvanages=res
          console.log(this.locationsAdvanages);
          console.log(this.propertyDetailsId);
        })
             }
           

            //  *********************************************************************************************************************************************
       
       // ******************************************************** post amenities**********************************

  // post inner deatils
  
 
   

    getAmenities(){
      this.real.getAmenities(this.propertyDetailsId).subscribe((res)=>{
        this.AmenitiesGet=res
        console.log(this.AmenitiesGet);
        console.log(this.propertyDetailsId);
      })
           }
           

          //  *********************************************************************************************************************************************
     
       // ******************************************************** post OverViews **********************************

  // post inner deatils
  
  

    getOverviews(){
      this.real.getOverviews(this.propertyDetailsId).subscribe((res)=>{
        this.overviewsGet=res
        console.log(this.overviewsGet);
        console.log(this.propertyDetailsId);
      })
           }
           

          //  *********************************************************************************************************************************************
     
      
            propertyDetailsClickId(){
           this.outerViewForm=!this.outerViewForm
          this.innerView=false
          this.laForm=false
          this.amenities=false
          this.overViewsForm=false
        }
        innerViwPropertyDetailsClickId(){
          this.innerView=!this.innerView
          this.outerViewForm=false
          this.laForm=false
          this.amenities=false
          this.overViewsForm=false
        }
        propertyDetailsForLAClickId(){
          this.laForm=!this.laForm
          this.innerView=false
          this.outerViewForm=false
          this.amenities=false
          this.overViewsForm=false
        }

        propertyAmenitiesGetClickId(){
          this.amenities=!this.amenities
          this.laForm=false
          this.innerView=false
          this.outerViewForm=false
          this.overViewsForm=false
        }

        propertyDetailsForOverViewsClickId(){
          this.overViewsForm=!this.overViewsForm
          this.amenities=false
          this.laForm=false
          this.innerView=false
          this.outerViewForm=false
        }
                      }



