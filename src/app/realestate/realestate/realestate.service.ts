import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class RealestateService {

  constructor(private http: HttpClient) { }
  usersDetails(id){
    return   this.http.get(environment.apiUrl +"/realestateClientsGetind/"+id);
   }
   clientsArea(id){
    return   this.http.get(environment.apiUrl +"/realestateAreaGet/"+id);
   } 
   reType(id){
    return   this.http.get(environment.apiUrl +"/realestatefecitiesGets/"+id);
   } 
   getBussinessDetails(id){
    return   this.http.get(environment.apiUrl +"/realestateBusinesGets/"+id);
   } 
   getBussinessLogicTypesAll(id){
    return   this.http.get(environment.apiUrl +"/realestateBusinesRoomsGets/"+id);
   }
   getAllRoomDetails(id){
    return   this.http.get(environment.apiUrl +"/realestateBusinesMainGets/"+id);
   }
  //  **********************updadest ****************************************

  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/realestateupdatesget/"+id);
  }
  
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/realestateupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/realestateupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/realestateupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/realestateupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/realestateupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/realestateeuserscomments/"+id,data);
  }
  // ********************************

}

  