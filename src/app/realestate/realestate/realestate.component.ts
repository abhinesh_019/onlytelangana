import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RealestateService } from './realestate.service';

@Component({
  selector: 'app-realestate',
  templateUrl: './realestate.component.html',
  styleUrls: ['./realestate.component.css']
})
export class RealestateComponent implements OnInit {
  public musicDetailsClients:any
  public ids:any
  
  constructor(private router:Router,private route:ActivatedRoute,private real:RealestateService) { }
 public areaClients:any
public clientAreaName:any
public clientAreaNameId:any
public reTypes:any
public reTypesName:any
public reTypesId:any
public getBussinessDetail:any
public getBussnissLogics:boolean=false
public getBussnissLogicsAllDetails:boolean=false
public bussinessLogicaDetailsNames:any
public bussinessLogicaDetailsId:any
public getBussinessLogicTypesAl:any
public logicTypeName:any
public logicTypeId:any
public mainDetails:any
public getAllRoomDetail:any
public mainLogic:any
public clientUpdatesCnts:any
  
public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public openshareid:any
public viewcommentsid:any
public replyid:any
public clientUpdates:any
public clientUpdatesCount:any
public commentspostsId:any
public postComments:any
public getUsersComments:any
public getUsersCommentsCounts:any
public replies:any
// public openshareid:any
// public openshareid:any
// public openshareid:any

postComment={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
   

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.clientsArea()
   this.clientsUpdates()
   this.clientsUpdatesCount()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
}

usersDetails(){ 
this.real.usersDetails(this.ids).subscribe((res)=>{
this.musicDetailsClients=res
})
}

clientsArea(){ 
  this.real.clientsArea(this.ids).subscribe((res)=>{
  this.areaClients=res
 var id=this.areaClients[0];
  this.clientsAreaget(id)
  })
  }
  clientsAreaget(get){
this.clientAreaName=get.clientArea
this.clientAreaNameId=get._id
this.reType()
  }
  
  reType(){ 
    this.real.reType(this.clientAreaNameId).subscribe((res)=>{
    this.reTypes=res
  
     })
    }
    reTypesClickForDetails(get){
      // this.getBussnissLogics=!this.getBussnissLogics
      this.reTypesName=get.realestateType
      this.reTypesId=get._id

this.getBussinessDetails()
    }
    
    getBussinessDetails(){ 
      this.real.getBussinessDetails(this.reTypesId).subscribe((res)=>{
      this.getBussinessDetail=res
      })
      }

      bussinessLogiClickForDetails(get){
        this.getBussnissLogicsAllDetails=!this.getBussnissLogicsAllDetails
       this.bussinessLogicaDetailsId=get._id
        this.bussinessLogicaDetailsNames=get.Businesype 

       this.getBussinessLogicTypesAll()
      }

      getBussinessLogicTypesAll(){ 
        this.real.getBussinessLogicTypesAll(this.bussinessLogicaDetailsId).subscribe((res)=>{
        this.getBussinessLogicTypesAl=res
      var ids=this.getBussinessLogicTypesAl[0]
      this.getBussinessAll(ids)
        })
        }
        getBussinessAll(get){
           this.logicTypeId=get._id
          this.logicTypeName=get.businesTypeRooms
         this.getAllRoomDetails() 

        }
         getAllRoomDetails(){ 
          this.real.getAllRoomDetails(this.logicTypeId).subscribe((res)=>{
          this.getAllRoomDetail=res
          console.log(res);

     
          })
          }
 
// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
}
 clientsUpdates(){   
  this.real.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
    console.log(res);
    console.log(this.ids);
    })
 }
 
 clientsUpdatesCount(){   
  this.real.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.real.commentsPoststoClints(this.commentspostsId,this.postComment).subscribe((res)=>{
     })
     this.postComment.descriptions=""
 }
 commentsGettoClints(){   
  this.real.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.real.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.real.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.real.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }



}
