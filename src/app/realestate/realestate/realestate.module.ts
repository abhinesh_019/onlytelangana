import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { RealestateComponent } from './realestate.component';
import { RealestateService } from './realestate.service';
 



const routes:Routes=[{path:'realestatedetails/:_id/:name',component:RealestateComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
   ],
  declarations: [
    RealestateComponent,

  ],
  providers: [RealestateService],
})
 
export class RealestateModule { }
