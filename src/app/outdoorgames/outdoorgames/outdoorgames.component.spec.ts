import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutdoorgamesComponent } from './outdoorgames.component';

describe('OutdoorgamesComponent', () => {
  let component: OutdoorgamesComponent;
  let fixture: ComponentFixture<OutdoorgamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutdoorgamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutdoorgamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
