import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { OutdoorgamesComponent } from './outdoorgames.component';
import { OutdoorgamesService } from './outdoorgames.service';
import { OutdoordetailsModule } from '../outdoordetails/outdoordetails.module';
 
const routes: Routes = [
  {
    path: '', component: OutdoorgamesComponent, children:
      [
      { path: 'outdoorgamesdetails/:_id/:name', loadChildren: 'app/outdoorgames/outdoordetails/outdoordetails.module#OutdoordetailsModule'},
 
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     OutdoordetailsModule
       ],
  declarations: [
    OutdoorgamesComponent
     ],
  providers:[OutdoorgamesService]
})
export class OutdoorgamesModule { }
