import { TestBed, inject } from '@angular/core/testing';

import { OutdoordetailsService } from './outdoordetails.service';

describe('OutdoordetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OutdoordetailsService]
    });
  });

  it('should be created', inject([OutdoordetailsService], (service: OutdoordetailsService) => {
    expect(service).toBeTruthy();
  }));
});
