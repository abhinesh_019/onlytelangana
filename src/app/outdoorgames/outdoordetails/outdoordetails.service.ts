import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class OutdoordetailsService {
  constructor(private http: HttpClient) { }

   
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/outdoorgamesClientsGetind/"+id);
  }
  
  servicesOne(id){
    return this.http.get(environment.apiUrl + "/outdoorgamesServiceGet/"+id);
     }
     servicesOnec(id){
      return this.http.get(environment.apiUrl + "/outdoorgamesServiceGetc/"+id);
       }
       
         servicesTwo(id){
          return this.http.get(environment.apiUrl + "/outdoorgamesServicesGet/"+id);
           }
           servicesTwoc(id){
            return this.http.get(environment.apiUrl + "/outdoorgamesServicesGetCounts/"+id);
             }
             
             clientsUpdatesGet(id){
    
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesget/"+id);
            }
            
            clientsUpdatesGetCounts(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesgetCounts/"+id);
            }
            commentsPoststoClints(id,data){
              
              return this.http.post(environment.apiUrl +"/outdoorgamesupdatesCommentspost/"+id,data);
            }
            commentsGettoClints(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesCommentsget/"+id);
            }
            commentsGettoClintsCounts(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesCommentsgetcounts/"+id);
            }
            replyFromCommentesGet(id){
              
              return this.http.get(environment.apiUrl +"/outdoorgamesupdatesCommentReplysGet/"+id);
            }
          
            overallscommentspost(id,data){
              
              return this.http.post(environment.apiUrl +"/outdoorgamesUsersComments/"+id,data);
            }
            // ********************************
            fecilities(id){
              return this.http.get(environment.apiUrl + "/outdoorgamesfecitiesGet/"+id);
             }
             fecilitiesc(id){
              return this.http.get(environment.apiUrl + "/outdoorgamesfecitiesGetCounts/"+id);
             }
          }
    
