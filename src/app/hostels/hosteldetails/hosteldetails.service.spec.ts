import { TestBed, inject } from '@angular/core/testing';

import { HosteldetailsService } from './hosteldetails.service';

describe('HosteldetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HosteldetailsService]
    });
  });

  it('should be created', inject([HosteldetailsService], (service: HosteldetailsService) => {
    expect(service).toBeTruthy();
  }));
});
