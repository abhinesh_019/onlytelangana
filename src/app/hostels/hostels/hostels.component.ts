import { Component, OnInit } from '@angular/core';
import { HostelsService } from '../hostels.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-hostels',
  templateUrl: './hostels.component.html',
  styleUrls: ['./hostels.component.css']
})
export class HostelsComponent implements OnInit {

  locationsdata={
    "locations":""
  }
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
public ids:any
public catdeatils:any
  public searchString:any
  public hostelUsers:any
  public usercountAllDist:any
  public popularareas:any

  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
   
  constructor(private hostels:HostelsService,private route:ActivatedRoute) { }
   

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.getlocations()
    this.indoorUserscountAllDist()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id']; 
}
usersDetails(){ 
  this.hostels.individualcat(this.ids).subscribe((res)=>{
    this.catdeatils=res      
  })
}
    getlocations(){
      this.hostels.locationsget(this.ids).subscribe((res)=>{
      this.locationsgets=res
       var id =this.locationsgets[0];      
      this.locations(id)
         
      })  }
      locations(get?){
        this.locationName=get.locations
        this.its=get._id
        this.locationss=get.locations 
        this.allAreas()

      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
      }
      allAreas(){
           
        this.hostels.areaapi(this.its).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0]; 
          this.selectedAreas(id)
          
        })
       
      }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
        this.hostelusers()  
        this.hostelsAreascount()
        this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
      }
      hostelusers(){
        var data:any =  {}
        if(this.searchString){
          data.search=this.searchString
        }
        this.hostels.hostelsusers(this.selectAreaId,data).subscribe((res)=>{
          this.hostelUsers=res
         })
        
      }
    
    searchFilter(){
      this.hostelusers()
    }
    indoorUserscountAllDist(){
      this.hostels.indoorUserscountAllDist().subscribe((res)=>{
        this.usercountAllDist=res
        })
     }
     hostelsAreascount(){
      this.hostels.hostelsAreascounts(this.selectAreaId).subscribe((res)=>{
        this.popularareas=res
      })
    }

    babycaressareasAddsOnea(){
      this.hostels.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
         this.babycaresAddsOnes=res
     
         
       })
    }
    babycaressareasAddsTwoa(){
      this.hostels.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
         this.babycaresAddsTwos=res
      
       })
    }
    babycaressareasAddsThreea(){
      this.hostels.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
         this.babycaresAddsThrees=res
      
       })
    }

    babycaressareasAddsFoura(){
      this.hostels.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
         this.babycaresAddsFours=res

       })
    }

    babycaressareasAddsOneLoc(){
      this.hostels.babycaresAddsOnel(this.its).subscribe((res)=>{
         this.babycaresAddsOneLoc=res 
       })
    }
    babycaressareasAddsTwoLoc(){
      this.hostels.babycaresAddsTwol(this.its).subscribe((res)=>{
         this.babycaresAddsTwoLoc=res
        
       })
    }
    babycaressareasAddsThreeLoc(){
      this.hostels.babycaresAddsThreel(this.its).subscribe((res)=>{
         this.babycaresAddsThreeLoc=res
         
       })
    }
    babycaressareasAddsFourLoc(){
      this.hostels.babycaresAddsFourl(this.its).subscribe((res)=>{
         this.babycaresAddsFoursLoc=res
         
       })
    }
    babycaressareasAddsFiveLoc(){
      this.hostels.babycaresAddsFivel(this.its).subscribe((res)=>{
         this.babycaresAddsFivesLoc=res
        
       })
    }

}

