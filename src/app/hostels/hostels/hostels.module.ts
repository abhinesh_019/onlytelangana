import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { HostelsComponent } from './hostels.component';
import { HostelsService } from '../hostels.service';
 
const routes:Routes=[{path:'hostels/:_id/:name',component:HostelsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    HostelsComponent,

  ],
  providers: [HostelsService],
})
export class HostelsModule { }
