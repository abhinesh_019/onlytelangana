import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';
  
@Injectable()
export class HostelsService {
  constructor(private http: HttpClient) { }
 
  catapigetdetails(){
    return this.http.get(environment.apiUrl +"/hostelsCategorie");

  } 
  individualcat(id){
    return this.http.get(environment.apiUrl +"/hostelsCategories/"+id);

  } 

 locationsget(id){

   return this.http.get(environment.apiUrl +"/hostelsLocations/"+id);
               }
               areaapi(id){
    
                return this.http.get(environment.apiUrl +"/hostelsAreas/"+id);
              }   
              hostelsusers(id,data){
    
                return this.http.get(environment.apiUrl +"/boysHostelsg/"+id,{params:data});
              }
              indoorUserscountAllDist(){
    
                return this.http.get(environment.apiUrl +"/boysHostelsCountsDisticts");
              }
              hostelsAreascounts(id){
    
                return this.http.get(environment.apiUrl +"/boysHostelsCountsAreas/"+id);
              }

              babycaresAddsOnea(id){

                return this.http.get(environment.apiUrl +"/hostelsAddsOneGeta/"+id);
              }
              babycaresAddsTwoa(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsTwoGeta/"+id);
              }
              babycaresAddsThreea(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsThreeGeta/"+id);
              }
              babycaresAddsFoura(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsFourGeta/"+id);
              }
               
              babycaresAddsOnel(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsOneLocGet/"+id);
              }
              babycaresAddsTwol(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsTwoLocGet/"+id);
              }
              babycaresAddsThreel(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsThreeGetLoc/"+id);
              }
              babycaresAddsFourl(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsFourLocGet/"+id);
              }
            
              babycaresAddsFivel(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsFiveLocGet/"+id);
              }              
 }

