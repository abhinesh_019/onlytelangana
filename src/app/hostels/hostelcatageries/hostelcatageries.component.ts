import { Component, OnInit } from '@angular/core';
import { HostelsService } from '../hostels.service';

@Component({
  selector: 'app-hostelcatageries',
  templateUrl: './hostelcatageries.component.html',
  styleUrls: ['./hostelcatageries.component.css']
})
export class HostelcatageriesComponent implements OnInit {

  
  public catList:any
  
  constructor(private hostels:HostelsService) { }
   

  ngOnInit() {
    this.getcatagories()
    
  }

  getcatagories(){
    this.hostels.catapigetdetails().subscribe((res)=>{
  this.catList=res
  console.log(this.catList);
   
          })  }
         
}
