import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvertiseService } from './advertise.service'

@Component({
  selector: 'app-advertise',
  templateUrl: './advertise.component.html',
  styleUrls: ['./advertise.component.css']
})
export class AdvertiseComponent implements OnInit {
   advertises={
    "companyName":"",
    "firstName":"",
    "lastName":"",
    "city":"",
    "mobileNo":"",
    "landLineNo":""
      }
      //     "node-sass": {
      // "version": "4.13.0",
      // "resolved": "https://registry.npmjs.org/node-sass/-/node-sass-4.13.0.tgz",
      // "integrity": "sha512-W1XBrvoJ1dy7VsvTAS5q1V45lREbTlZQqFbiHb3R3OTTCma0XBtuG6xZ6Z4506nR4lmHPTqVRwxT6KgtWC97CA==",
      // "dev": true,
      // "optional": true,
      // "requires": {
      //   "async-foreach": "0.1.3",
      //   "chalk": "1.1.3",
      //   "cross-spawn": "3.0.1",
      //   "gaze": "1.1.3",
      //   "get-stdin": "4.0.1",
      //   "glob": "7.1.6",
      //   "in-publish": "2.0.0",
      //   "lodash": "4.17.15",
      //   "meow": "3.7.0",
      //   "mkdirp": "0.5.1",
      //   "nan": "2.14.0",
      //   "node-gyp": "3.8.0",
      //   "npmlog": "4.1.2",
      //   "request": "2.88.0",
      //   "sass-graph": "2.2.4",
      //   "stdout-stream": "1.4.1",
      //   "true-case-path": "1.0.3"
      // },

      feedbackpost={
        city:"",
        text:""
      }
      constructor(private route:ActivatedRoute,private feeds:AdvertiseService) { }

  ngOnInit() {
  }
  // ***********0verallcomments************
  advertise(){
       
     
    this.feeds.addsPosts(this.advertises).subscribe((res)=>{
     
     this.advertises.companyName="",
     this.advertises.firstName="",
     this.advertises.lastName="",
     this.advertises.city="",
     this.advertises.mobileNo="",
     this.advertises.landLineNo=""
    })
  }

   // ***********0verallcomments************
   overallcomment(){
       
     
    this.feeds.addsPost(this.feedbackpost).subscribe((res)=>{
     
     this.feedbackpost.text="",
    
     this.feedbackpost.city=""
    
    })
  }
}
