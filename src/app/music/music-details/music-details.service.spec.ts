import { TestBed, inject } from '@angular/core/testing';

import { MusicDetailsService } from './music-details.service';

describe('MusicDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MusicDetailsService]
    });
  });

  it('should be created', inject([MusicDetailsService], (service: MusicDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
