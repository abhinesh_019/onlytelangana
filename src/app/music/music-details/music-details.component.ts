import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MusicDetailsService } from './music-details.service';

@Component({
  selector: 'app-music-details',
  templateUrl: './music-details.component.html',
  styleUrls: ['./music-details.component.css']
})
export class MusicDetailsComponent implements OnInit {

 
  public musicDetailsClients:any
public ids:any
public serviceType:any
public musicTypeName:any
public musicTypeId:any
public servicegets:any
public servicegetsc:any
public viewcommentsid:any
public replyid:any
public replies:any
public animalsTypesCnts:any
public clientUpdatesCnts:any

public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public openshareid:any
public clientUpdates:any
public clientUpdatesCount:any
public commentspostsId:any
public getUsersComments:any
public getUsersCommentsCounts:any
public fecilities:any
public fecilitiesc:any
public catageriesGetss:any
public catageriesDanceName:any
public catageriesDanceNameId:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
// public catageriesGetss:any
 
postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }
   

  constructor(private router:Router,private route:ActivatedRoute,private musicDetails:MusicDetailsService) { }

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
   this.fecilitiesGet()
   this.fecilitiesGetC()
   this.clientsUpdates()
   this.clientsUpdatesCount()
   this.CatageriesGet()
    
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.musicDetails.usersDetails(this.ids).subscribe((res)=>{
this.musicDetailsClients=res
console.log(res);

})
}

serviceTypesGet(){   
  this.musicDetails.serviceTypeDataGet(this.catageriesDanceNameId).subscribe((res)=>{
    this.serviceType=res
     var id =this.serviceType[0];
    this.cageries(id)
  })
 }

cageries(get?){
this.musicTypeName=get.musicType
this.musicTypeId=get._id
}
serviceGet(){   
this.musicDetails.serviceDataGet(this.catageriesDanceNameId).subscribe((res)=>{
   this.servicegets=res
   console.log(res);
   
  })
}

serviceGetC(){   
this.musicDetails.serviceDataGetC(this.catageriesDanceNameId).subscribe((res)=>{
  this.servicegetsc=res
  console.log(res);
  })
}


// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid)
  console.log(this.replyfrom)
}

 clientsUpdates(){   
  this.musicDetails.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.musicDetails.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.musicDetails.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.musicDetails.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.musicDetails.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.musicDetails.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res
    console.log(res)
    console.log(this.replyid)
    
    })
 }


  // ***********0verallcomments************
  overallcomment(){
       
     
    this.musicDetails.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }

  fecilitiesGet(){   
    this.musicDetails.fecilitiesGet(this.ids).subscribe((res)=>{
      this.fecilities=res
       })
   }
   fecilitiesGetC(){   
    this.musicDetails.fecilitiesGetC(this.ids).subscribe((res)=>{
      this.fecilitiesc=res
       })
   }
   CatageriesGet(){   
    this.musicDetails.CatageriesGets(this.ids).subscribe((res)=>{
      this.catageriesGetss=res
       })
   }
   catageriesGetClick(get){
     this.catageriesDanceName=get.musicCatatgeries
     this.catageriesDanceNameId=get._id
      this.serviceGet()
     this.serviceGetC()
   }
  
}

