import { Component, OnInit } from '@angular/core';
import { TutionsService } from '../tutions.service';
import { ActivatedRoute } from '@angular/router';
 
@Component({
  selector: 'app-tutionscatageries',
  templateUrl: './tutionscatageries.component.html',
  styleUrls: ['./tutionscatageries.component.css']
})
export class TutionscatageriesComponent implements OnInit {
public catList:any
public ids:any
public catdeatils:any
public locationsgets:any
public locationName:any
public idss:any
public areasAll:any
public selectAreaId:any
public selectArea:any
public AllUsers:any 
public AllUsersCounts:any
public AllUsersCountsAreas:any
public UsersCountsAreas:any
public searchString:String;

 // **********************************************************
 
 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any
 public marriageusercountLocations:any

constructor(private route:ActivatedRoute,private tutions:TutionsService) { }

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.getlocations()
    this.totalCountsUsers()
    this.marriageuserscountLocations()
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];
console.log(this.ids);
}

usersDetails(){ 
this.tutions.catindgetapi(this.ids).subscribe((res)=>{
this.catdeatils=res
})
}

      // *********************** locations ****************
      
         getlocations(){
           this.tutions.locationsget(this.ids).subscribe((res)=>{
           this.locationsgets=res
            var id =this.locationsgets[0];
            this.locations(id)
              
           })  }


           locations(get?){
             this.locationName=get.locations
             this.idss=get._id
              this.allAreas()
              this.babycaressareasAddsOneLoc()
              this.babycaressareasAddsTwoLoc()
              this.babycaressareasAddsThreeLoc()
              this.babycaressareasAddsFourLoc()
              this.babycaressareasAddsFiveLoc()
           }

 // ************************************************************************************************************************************
 
//  **********************************area *************************************

allAreas(){
           
  this.tutions.areaapi(this.idss).subscribe((res)=>{
    this.areasAll=res
    var id =this.areasAll[0];
    
    
    this.selectedAreas(id)
    
  })
 
}

selectedAreas(result){
  this.selectAreaId=result._id
  this.selectArea=result.area
  
  this.usersDetail()
  this.totalCountsUser()
  this.areasCountsall()
  this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
}

// ************************************************************************

//  ********************************** users details *************************************

usersDetail(){
  var data:any =  {}
  if(this.searchString){
    data.search=this.searchString
    }

  this.tutions.usersDetails(this.selectAreaId,data).subscribe((res)=>{
    this.AllUsers=res
     
    
  })
 
}
searchFilter(){
  this.usersDetail()
}

totalCountsUsers(){
  this.tutions.totalUsers().subscribe((res)=>{
    this.AllUsersCounts=res
     
  })
}

totalCountsUser(){
  this.tutions.totalUser(this.selectAreaId).subscribe((res)=>{
    this.AllUsersCountsAreas=res
     
  })
}
areasCountsall(){
  this.tutions.areaCounts(this.selectAreaId).subscribe((res)=>{
    this.UsersCountsAreas=res
    
    console.log(res);
     
    
  })
}

// ****************************************************

babycaressareasAddsOnea(){
  this.tutions.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.tutions.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.tutions.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.tutions.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.tutions.babycaresAddsOnel(this.idss).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.tutions.babycaresAddsTwol(this.idss).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.tutions.babycaresAddsThreel(this.idss).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.tutions.babycaresAddsFourl(this.idss).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.tutions.babycaresAddsFivel(this.idss).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}
marriageuserscountLocations(){
  this.tutions.marriageusersForLocations().subscribe((res)=>{
    this.marriageusercountLocations=res
   })
 }
}
