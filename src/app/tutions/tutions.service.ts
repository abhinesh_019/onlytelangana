import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';
  

@Injectable()

export class TutionsService {
  constructor(private http: HttpClient) { }
  catapigetdetails(){
    
    return this.http.get(environment.apiUrl +"/tutionsCategories");
  }
  
 locationsget(id){
   return this.http.get(environment.apiUrl +"/tutionslocations/"+id);
               }
               areaapi(id){
    
                return this.http.get(environment.apiUrl +"/tutionsAreas/"+id);
              }   
              
              catindgetapi(id){
    
                return this.http.get(environment.apiUrl +"/tutionsCategories/"+id);
              }
              
              usersDetails(id,data){
    
                return this.http.get(environment.apiUrl +"/tutionshUsers/"+id,{params:data});
              }            
              
              totalUsers(){
                return this.http.get(environment.apiUrl +"/tutionshUsersAllCount");

              }
              totalUser(id){
                return this.http.get(environment.apiUrl +"/tutionshUsersCount/"+id);

              }
              areaCounts(id){
                return this.http.get(environment.apiUrl +"/tutionsUsersAreas/"+id);

              }
              
              individualUsers(id){
                return this.http.get(environment.apiUrl +"/tutionshUsersin/"+id);

              }
              ssc(id){
                return this.http.get(environment.apiUrl +"/tutionshUserSSC/"+id);

              }
              int(id){
                return this.http.get(environment.apiUrl +"/tutionshUserInt/"+id);
                     }
                     deg(id){
                      return this.http.get(environment.apiUrl +"/tutionshUserDegree/"+id);
                           }
                    
  overallscommentspost(id,data){
     
    
    return this.http.post(environment.apiUrl +"/tutionspComments/"+id,data);
    
  }
  
  updatesget(id) {
    return this.http.get(environment.apiUrl + "/tutionsUserUpdates/"+id);
  }
  updatesgetCount(id) {
    return this.http.get(environment.apiUrl + "/tutionsUserUpdatesCount/"+id);
  }

  commentsgetupdates(id) {
    return this.http.get(environment.apiUrl + "/tutionsgCommentsUpdatesg/"+id);
  }
  commentsgetupdatesCount(id) {
    return this.http.get(environment.apiUrl + "/tutionsgCommentUpdatesCountg/"+id);
  }
  commentspostupdates(id,data) {
    return this.http.post(environment.apiUrl +"/tutionspCommentsUpdatesp/"+id,data);
  }

  commentsgetupdatesReply(id) {
    return this.http.get(environment.apiUrl + "/tutionsgCommentsReplyUpdatesg/"+id);
  }
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/tutionsAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/tutionsAddsFiveLocGet/"+id);
  }
  marriageusersForLocations(){
    return   this.http.get(environment.apiUrl +"/tutionsClientsAllCountLocations");
   }
 }
