import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TutionsComponent } from './tutions.component';
import { TutionsdetailsModule } from '../tutionsdetails/tutionsdetails.module';
import { SharedModule } from '../../shared/shared.module';
import { TutionsService } from '../tutions.service';
import { TutionscatageriesModule } from '../tutionscatageries/tutionscatageries.module';

const routes: Routes = [
  {
    path: '', component: TutionsComponent, children:
      [
      { path: 'tutionsdetails/:_id/:name', loadChildren: 'app/tutions/tutionsdetails/tutionsdetails.module#TutionsdetailsModule'},
      { path: 'tutionscatagerie/:_id/:name', loadChildren: 'app/tutions/tutionscatageries/tutionscatageries.module#TutionscatageriesModule'},

      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     TutionsdetailsModule,
     SharedModule,
     TutionscatageriesModule
  ],
  declarations: [
    TutionsComponent,
  ],
  providers:[TutionsService]
})
export class TutionsModule { }
