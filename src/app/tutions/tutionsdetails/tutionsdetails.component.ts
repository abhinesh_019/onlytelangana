import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TutionsService } from '../tutions.service';

@Component({
  selector: 'app-tutionsdetails',
  templateUrl: './tutionsdetails.component.html',
  styleUrls: ['./tutionsdetails.component.css']
})
export class TutionsdetailsComponent implements OnInit {
public ids:any
public mainuser:any
public ssc:any
public ints:any
public degree:any
public adminupdates:any
public adminsupdateCommentsCountsall:any
public show:any
public shows:any
public showes:any
public showess:any
public updatesIds:any
public commentsUpdatesId:any
public adminsupdateComments:any
public selectedblogIds:any
public adminsupdateCommentsReply:any
public adminsupdateCommentsCount:any

comments={
  name:"",
  email:"",
  contactNo:"",
  leaveComment:""
}
updatesCommentsdata={
  "descriptions":""
}
  constructor(private route:ActivatedRoute,private tutions:TutionsService) { }

  ngOnInit() {
    this.individualdata()
    this.sscdetails()
    this.intDetaits()
    this.degreeDetaits()
    this.getupdates()
    this.getupdatesCount()

  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

    this.usersDetails() 
}

usersDetails(){ 
  this.tutions.individualUsers(this.ids).subscribe((res)=>{
    this.mainuser=res
    console.log(this.mainuser)
    
  })
}

sscdetails(){ 
  this.tutions.ssc(this.ids).subscribe((res)=>{
    this.ssc=res
   
    
  })
}
intDetaits(){ 
  this.tutions.int(this.ids).subscribe((res)=>{
    this.ints=res
     
  })
}

degreeDetaits(){ 
  this.tutions.deg(this.ids).subscribe((res)=>{
    this.degree=res
    console.log(this.degree);
    
  })
}
 // ***********0verallcomments************
 overallcomment(){
 
 this.tutions.overallscommentspost(this.ids,this.comments).subscribe((res)=>{
   
 })

 this.comments.name="",
 this.comments.contactNo="",
 this.comments.email="",
 this.comments.leaveComment=""


}

// *************get updates****************
getupdates(){
 
  this.tutions.updatesget(this.ids).subscribe((res)=>{
    this.adminupdates=res
    
    console.log(this.adminupdates);
  
  })
       }
       getupdatesCount(){
 
        this.tutions.updatesgetCount(this.ids).subscribe((res)=>{
          this.adminsupdateCommentsCountsall=res
          
          console.log(this.adminsupdateCommentsCountsall);
        
        })
             }

showicons(){
  this.show=!this.show
  this.shows=false
}
showcomments(item?){
  this.show=false
  this.shows=!this.shows
   this.updatesIds=item._id

   
   console.log(this.updatesIds);
   
   this.comm()
 
        }
        updatestoAdmincommentsget(result?){
          this.showes=!this.showes
                   this.commentsUpdatesId=result
                   console.log(result);
                    this.comm()
                    this.commCount()
                    // this.showcomments()
                   }
       comm(){
        this.tutions.commentsgetupdates(this.commentsUpdatesId).subscribe((res)=>{
            this.adminsupdateComments=res
            console.log(this.adminsupdateComments);
            
           
          })
                } 
                commCount(){
                  this.tutions.commentsgetupdatesCount(this.commentsUpdatesId).subscribe((res)=>{
                      this.adminsupdateCommentsCount=res
                      console.log(this.adminsupdateCommentsCount);
                      
                     
                    })
                          }
         updatestoadmincomments(){
         
          this.tutions.commentspostupdates(this.updatesIds,this.updatesCommentsdata).subscribe((res)=>{
           
           
           console.log(res);
           
          })
          this.updatesCommentsdata.descriptions=""
           
               }
               updatesAdminReply(items){
                this.showess=!this.showess
                this.selectedblogIds=items
                this.tutions.commentsgetupdatesReply(this.selectedblogIds).subscribe((res)=>{
                  this.adminsupdateCommentsReply=res
                 console.log(res);
                 
                })
              }
                
}
