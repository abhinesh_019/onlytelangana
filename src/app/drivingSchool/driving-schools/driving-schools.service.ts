import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()

export class DrivingSchoolsService {
  constructor(private http: HttpClient) { }

  locationapi(){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsLocations");
  }

  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsArea/"+id);
  }
  indoorUsers(id,data){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsClientsget/"+id,{params:data});
  }
  indoorUserscountsAll(){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsClientsAllCount");
  }

  indoorUserscountAllMainAreas(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsClientsCountAreas/"+id);
  }

  indoorUserscountAllDist(){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsClientsCountDist");
  }

  indoorUserscountAllAreas(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsClientsCount/"+id);
  }
  indoorUserscountAllAreasover(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsClientsAreas/"+id);
  }
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsAddsFiveLocGet/"+id);
  }
   
}