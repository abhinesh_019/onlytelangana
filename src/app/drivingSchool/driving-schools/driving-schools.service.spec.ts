import { TestBed, inject } from '@angular/core/testing';

import { DrivingSchoolsService } from './driving-schools.service';

describe('DrivingSchoolsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DrivingSchoolsService]
    });
  });

  it('should be created', inject([DrivingSchoolsService], (service: DrivingSchoolsService) => {
    expect(service).toBeTruthy();
  }));
});
