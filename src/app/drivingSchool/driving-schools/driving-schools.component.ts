import { Component, OnInit } from '@angular/core';
import { DrivingSchoolsService } from './driving-schools.service';

@Component({
  selector: 'app-driving-schools',
  templateUrl: './driving-schools.component.html',
  styleUrls: ['./driving-schools.component.css']
})
export class DrivingSchoolsComponent implements OnInit {
  public locationsget:any;
  public indoorsUser:any;
  public its:any;
  public locationget:any
  public furnitureusercount:any
  public furnitureusercounttot:any
  public locationName:String
  public searchString:String;
    searchTerm:String;
    public usercountAllDist:any
    public usercountAllAreas:any
    public usercountAllAreasOver:any
    // *********************************************************
      public areasAll:any
    public selectAreaId:any
    public selectArea:any
  public name;
   public usercountAll:any
   public popularareas:any
   
    public usercountMainAreas:any
    // **********************************************************
 
    public imageAddsOne:any
    public babycaresAddsOnes:any
     
    public babycaresAddsFours:any
    public imageAddsFour:any
    public imageAddsThree:any
    public imageAddsTwo:any
    public babycaresAddsThrees:any
    public babycaresAddsTwos:any
    
    
    public babycaresAddsFivesLoc:any
    public imageAddsFiveLoc:any
    public imageAddsFourLoc:any
    public babycaresAddsFoursLoc:any
    public imageAddsThreeLoc:any
    public babycaresAddsThreeLoc:any
    public imageAddsTwoLoc:any
    public babycaresAddsTwoLoc:any
    public imageAddsOneLoc:any
    public babycaresAddsOneLoc:any
    constructor(private driving:DrivingSchoolsService) { }
  
    ngOnInit() {
    
      this.getlocations()
      this.indoorUserscountAll()
      this.indoorUserscountAllDist()
     
    }
  
  getlocations(){
      this. driving.locationapi().subscribe((res)=>{
        this.locationsget=res;
          var id =this.locationsget[0];
          this.locations(id)
      })
    
    }
     locations(get?){
      this.locationName=get.locations
      this.its=get._id
      this.locationget=get.locations
        this.allAreas()
        this.babycaressareasAddsOneLoc()
        this.babycaressareasAddsTwoLoc()
        this.babycaressareasAddsThreeLoc()
        this.babycaressareasAddsFourLoc()
        this.babycaressareasAddsFiveLoc()
       }
    allAreas(){
       
      this. driving.areaapi(this.its).subscribe((res)=>{
        this.areasAll=res
        var id =this.areasAll[0];
          this.selectedAreas(id)
        
      })
     
    }
  
    selectedAreas(result){
      this.selectAreaId=result._id
      this.selectArea=result.area
        this.indoorUsers()
     this.indoorUserscountAllAreas()
     this.indoorUserscountAllAreasover()
     this.indoorUserscountAllMainAreas() 
 
   this.babycaressareasAddsOnea()
   this.babycaressareasAddsTwoa()
   this.babycaressareasAddsThreea()
   this.babycaressareasAddsFoura()
     }
   
    indoorUsers(){
      var data:any = {}
      if(this.searchString){
        data.search=this.searchString
       }
      this. driving.indoorUsers(this.selectAreaId,data).subscribe((res)=>{
        this.indoorsUser=res
       })
      
    }
  
  searchFilter(){
    this.indoorUsers()
  }
     indoorUserscountAll(){
      this.driving.indoorUserscountsAll().subscribe((res)=>{
        this.usercountAll=res
       })
     }
    indoorUserscountAllMainAreas(){
      this.driving.indoorUserscountAllMainAreas(this.selectAreaId).subscribe((res)=>{
        this.usercountMainAreas=res
        console.log(res);
        
        })
     }
      indoorUserscountAllDist(){
      this.driving.indoorUserscountAllDist().subscribe((res)=>{
        this.usercountAllDist=res
        })
     }
  
     indoorUserscountAllAreas(){
      this.driving.indoorUserscountAllAreas(this.selectAreaId).subscribe((res)=>{
        this.usercountAllAreas=res
        })
     }
     indoorUserscountAllAreasover(){
      this.driving.indoorUserscountAllAreasover(this.selectAreaId).subscribe((res)=>{
        this.usercountAllAreasOver=res
         })
     }
   
  // ****************************************************
  
// ****************************************************

babycaressareasAddsOnea(){
  this.driving.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.driving.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.driving.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.driving.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.driving.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.driving.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.driving.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.driving.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.driving.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}
