import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class DrivingSchoolsDetailsService {

  constructor(private http: HttpClient) { }
 
  usersDetails(id){
    
    return this.http.get(environment.apiUrl +"/drivingSchoolsClientsGetind/"+id);
  }

  servicesOne(id){
    return this.http.get(environment.apiUrl + "/drivingSchoolsServicesGet/"+id);
     }
     servicesOnec(id){
      return this.http.get(environment.apiUrl + "/drivingSchoolsServicesGetCounts/"+id);
       }
       servicesTwo(id){
        return this.http.get(environment.apiUrl + "/drivingSchoolsServicesCustomers/"+id);
         }  
         servicesTwoc(id){
          return this.http.get(environment.apiUrl + "/drivingSchoolsServicesCustomersCounts/"+id);
           } 
           trackRecords(id){
            return this.http.get(environment.apiUrl + "/drivingSchoolsServicestrackRecords/"+id);
             } 
             trackRecordsc(id){
              return this.http.get(environment.apiUrl + "/drivingSchoolsServicestrackRecordsCounts/"+id);
               } 
               totalCustomers(id){
                return this.http.get(environment.apiUrl + "/drivingSchoolsTotalCustomers/"+id);
                 }
                 totalCustomersc(id){
                  return this.http.get(environment.apiUrl + "/drivingSchoolsTotalCustomersCounts/"+id);
                   }
                   fecilitiesGet(id){
                    return this.http.get(environment.apiUrl + "/drivingSchoolsfecitiesGet/"+id);
                     }
                     fecilitiesGetc(id){
                      return this.http.get(environment.apiUrl + "/drivingSchoolsfecitiesGetCounts/"+id);
                       }
                      //  ************************updates present ***************************
                       clientsUpdatesGet(id){
    
                        return this.http.get(environment.apiUrl +"/drivingSchoolsupdatesget/"+id);
                      }
                      
                      clientsUpdatesGetCounts(id){
                        
                        return this.http.get(environment.apiUrl +"/drivingSchoolsupdatesgetCounts/"+id);
                      }
                      commentsPoststoClints(id,data){
                        
                        return this.http.post(environment.apiUrl +"/drivingSchoolsupdatesCommentspost/"+id,data);
                      }
                      commentsGettoClints(id){
                        
                        return this.http.get(environment.apiUrl +"/drivingSchoolsupdatesCommentsget/"+id);
                      }
                      commentsGettoClintsCounts(id){
                        
                        return this.http.get(environment.apiUrl +"/drivingSchoolsupdatesCommentsgetcounts/"+id);
                      }
                      replyFromCommentesGet(id){
                        
                        return this.http.get(environment.apiUrl +"/drivingSchoolsupdatesCommentReplysGet/"+id);
                      }
                    
                      overallscommentspost(id,data){
                        
                        return this.http.post(environment.apiUrl +"/drivingSchoolsUsersComments/"+id,data);
                      }
                      // ********************************
                     
                    }
                    

