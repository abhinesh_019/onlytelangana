import { TestBed, inject } from '@angular/core/testing';

import { DrivingSchoolsDetailsService } from './driving-schools-details.service';

describe('DrivingSchoolsDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DrivingSchoolsDetailsService]
    });
  });

  it('should be created', inject([DrivingSchoolsDetailsService], (service: DrivingSchoolsDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
