import { TestBed, inject } from '@angular/core/testing';

import { GeneralanimalsService } from './generalanimals.service';

describe('GeneralanimalsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralanimalsService]
    });
  });

  it('should be created', inject([GeneralanimalsService], (service: GeneralanimalsService) => {
    expect(service).toBeTruthy();
  }));
});
