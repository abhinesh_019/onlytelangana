import { Component, OnInit } from '@angular/core';
import { GeneralanimalsService } from './generalanimals.service';

@Component({
  selector: 'app-generalanimals',
  templateUrl: './generalanimals.component.html',
  styleUrls: ['./generalanimals.component.css']
})
export class GeneralanimalsComponent implements OnInit {

  locationsdata={
    "locations":""
  }
  areas={
    "area":""
  }
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  
 

   public imageAddsOne:any
   public imageAddsTwo:any
  public imageAddsThree:any
  public imageAddsFour:any
  public imageAddsOneLoc:any
  public imageAddsTwoLoc:any
  public imageAddsThreeLoc:any
  public imageAddsFourLoc:any
  public imageAddsFiveLoc:any
  
  
  public animalsAddsOnes:any
  public animalsAddsTwos:any
  public animalsAddsThrees:any
  public animalsAddsFours:any

  
  public animalsAddsOneLoc:any
  public animalsAddsTwoLoc:any
  public animalsAddsThreeLoc:any
  public animalsAddsFoursLoc:any
  public animalsAddsFivesLoc:any
 
  
  
  // public selectArea:any
  // public selectArea:any
  
  bcmain={
    "name":"",
    "shopname":"",
    "aboutUsDescriptionOne":"",
    "aboutUsDescriptionTwo":"",
     "day1":"",
    "day2":"",
    "day3":"",
    "day4":"",
    "day5":"",
    "day6":"",
    "day7":"",
    "totaltiming":"",
    "hno":"",
    "area":"",
    "landmark":"",
    "city":"",
    "distict":"",
    "state":"",
    "pincode":"",
    "officeno":"",
    "mobileno":"",
    "whatsupno":"",
    "emailid":"",
  "images1":"",
  "images2":"",
  "images3":"",
  "images4":"",
  "images5":"",
  "images6":""
  
  }
  
areaAddsOne={
  "addsOneLink":"",
  "addsOneImg":"",
}
areaAddsTwo={
  "addsTwoLink":"",
  "addsTwoImg":"",
}
areaAddsThree={
  "addsThreeLink":"",
  "addsThreeImg":"",
}
areaAddsFour={
  "addsFourLink":"",
  "addsFourImg":"",
}
 

areaAddsOneLoc={
  "addsOneLinkLoc":"",
  "addsOneImgLoc":"",
}
areaAddsTwoLoc={
  "addsTwoLinkLoc":"",
  "addsTwoImgLoc":"",
}
areaAddsThreeLoc={
  "addsThreeLinkLoc":"",
  "addsThreeImgLoc":"",
}
areaAddsFourLoc={
  "addsFourLinkLoc":"",
  "addsFourImgLoc":"",
}
areaAddsFiveLoc={
  "addsFiveLinkLoc":"",
  "addsFiveImgLoc":"",
}
 

  public formData: any = new FormData();
  
  
  constructor(private animals:GeneralanimalsService) { }

  ngOnInit() {
    this.getlocations()
  }
  postlocations(){
 
    this.animals.locationspost(this.locationsdata).subscribe((res)=>{
     
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.animals.locationsget().subscribe((res)=>{
      this.locationsgets=res
       var id =this.locationsgets[0];
       
      this.locations(id)
         
      })  }
      locations(get?){
        this.locationName=get.locations
        this.its=get._id
        this.locationss=get.locations
        this.allAreas()
        this.animalssareasAddsOneLoc()
        this.animalssareasAddsTwoLoc()
        this.animalssareasAddsThreeLoc()
        this.animalssareasAddsFourLoc()
        this.animalssareasAddsFiveLoc() 
      }
      allAreas(){
           
        this.animals.areaapi(this.its).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0];
           
          this.selectedAreas(id)
          
        })
       
      }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
      this.animalssareasAddsOnea()
      this.animalssareasAddsTwoa()
      this.animalssareasAddsThreea()
      this.animalssareasAddsFoura()   
    }
      
      postarea(){
        this.animals.areapost(this.its,this.areas).subscribe((res)=>{
           this.areas.area=""
        }) 
      }
      
      //  ************************main users details *********************

 handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile);
}


    
postUsers(){
 
  this.formData.append('name',this.bcmain.name);
  this.formData.append('shopname',this.bcmain.shopname);
  this.formData.append('aboutUsDescriptionOne',this.bcmain.aboutUsDescriptionOne);
  this.formData.append('aboutUsDescriptionTwo',this.bcmain.aboutUsDescriptionTwo);
  this.formData.append('day1',this.bcmain.day1);
  this.formData.append('day2',this.bcmain.day2);
  this.formData.append('day3',this.bcmain.day3);
  this.formData.append('day4',this.bcmain.day4);
  this.formData.append('day5',this.bcmain.day5);
  this.formData.append('day6',this.bcmain.day6);
  this.formData.append('day7',this.bcmain.day7);
  this.formData.append('totaltiming',this.bcmain.totaltiming);
  this.formData.append('hno',this.bcmain.hno);
  this.formData.append('area',this.bcmain.area);
  this.formData.append('landmark',this.bcmain.landmark);
  this.formData.append('city',this.bcmain.city);
  this.formData.append('distict',this.bcmain.distict);
  this.formData.append('state',this.bcmain.state);
  this.formData.append('pincode',this.bcmain.pincode);
  this.formData.append('officeno',this.bcmain.officeno);
  this.formData.append('mobileno',this.bcmain.mobileno);
  this.formData.append('whatsupno',this.bcmain.whatsupno);
  this.formData.append('emailid',this.bcmain.emailid);

this.bcmain.name="",
this.bcmain.shopname="",
this.bcmain.aboutUsDescriptionOne="",
this.bcmain.aboutUsDescriptionTwo="",
 this.bcmain.day1="",
this.bcmain.day2="",
this.bcmain.day3="",
this.bcmain.day4="",
this.bcmain.day5="",
this.bcmain.day6="",
this.bcmain.day7="",
this.bcmain.totaltiming="",
this.bcmain.hno="",
this.bcmain.area="",
this.bcmain.landmark="",
this.bcmain.city="",
this.bcmain.distict="",
this.bcmain.state="",
this.bcmain.pincode="",
this.bcmain.officeno="",
this.bcmain.mobileno="",
this.bcmain.whatsupno="",
this.bcmain.emailid="",
this.bcmain.images1="",
this.bcmain.images2="",
this.bcmain.images3="",
this.bcmain.images4="",
this.bcmain.images5="",
this.bcmain.images6="",

    
      this.animals.areapostusers(this.selectAreaId,this.formData).subscribe((res)=>{
         this.areas.area=""
      }) 
  
}
// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.animals.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
animalssareasAddsOnea(){
  this.animals.animalsAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsOnes=res
   })
}
animalssareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.animals.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
animalssareasAddsTwoa(){
  this.animals.animalsAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsTwos=res
   })
}
animalssareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.animals.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
animalssareasAddsThreea(){
  this.animals.animalsAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsThrees=res
   })
}
animalssareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.animals.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
animalssareasAddsFoura(){
  this.animals.animalsAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsFours=res
   })
}
animalssareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.animals.postAddsOnepl(this.its,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
animalssareasAddsOneLoc(){
  this.animals.animalsAddsOnel(this.its).subscribe((res)=>{
     this.animalsAddsOneLoc=res
   })
}
animalssareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.animals.postAddsTwopl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
animalssareasAddsTwoLoc(){
  this.animals.animalsAddsTwol(this.its).subscribe((res)=>{
     this.animalsAddsTwoLoc=res
   })
}
animalssareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.animals.postAddsThreepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
animalssareasAddsThreeLoc(){
  this.animals.animalsAddsThreel(this.its).subscribe((res)=>{
     this.animalsAddsThreeLoc=res
   })
}
animalssareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.animals.postAddsFourpl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
animalssareasAddsFourLoc(){
  this.animals.animalsAddsFourl(this.its).subscribe((res)=>{
     this.animalsAddsFoursLoc=res
   })
}
animalssareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.animals.postAddsFivepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
animalssareasAddsFiveLoc(){
  this.animals.animalsAddsFivel(this.its).subscribe((res)=>{
     this.animalsAddsFivesLoc=res
   })
}
animalssareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.animals.animalsAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}

