import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()


export class GeneralanimalsService {

  constructor(private http: HttpClient) { }

  locationspost(data) {
    return   this.http.post(environment.apiUrl +"/animalsLocations",data);

 
    }

    locationsget(){
       return this.http.get(environment.apiUrl +"/animalsLocations");
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/animalsAreas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsArea/"+id,data);
                   }
                   areapostusers(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsClientspostsdata/"+id,data);
                   }
                   postAddsOne(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsOnePtsa/"+id,data);
                   }
                   postAddsTwo(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsTwoPtsa/"+id,data);
                   }
                   postAddsThree(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsThreePtsa/"+id,data);
                   }
                   postAddsFour(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsFourPtsa/"+id,data);
                   }
                   
                 
                  animalsAddsOneaDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsOneDeletea/"+id);
                  }
                 
                  animalsAddsTwoaDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsTwoDeletea/"+id);
                  }
                  animalsAddsThreeaDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsThreeDeletea/"+id);
                  }
                  animalsAddsFouraDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsFourDeletea/"+id);
                  }
                  animalsAddsOnea(id){
    
                    return this.http.get(environment.apiUrl +"/animalsAddsOneGeta/"+id);
                  }
                  animalsAddsTwoa(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsTwoGeta/"+id);
                  }
                  animalsAddsThreea(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsThreeGeta/"+id);
                  }
                  animalsAddsFoura(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsFourGeta/"+id);
                  }
                   
                  animalsAddsOnel(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsOneLocGet/"+id);
                  }
                  animalsAddsTwol(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsTwoLocGet/"+id);
                  }
                  animalsAddsThreel(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsThreeGetLoc/"+id);
                  }
                  animalsAddsFourl(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsFourLocGet/"+id);
                  }

                  animalsAddsFivel(id){
                    
                    return this.http.get(environment.apiUrl +"/animalsAddsFiveLocGet/"+id);
                  }
                   
                  postAddsOnepl(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsOneLocPts/"+id,data);
                   }
                   postAddsTwopl(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsTwoLocPts/"+id,data);
                   }
                   postAddsThreepl(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsThreePtsLoc/"+id,data);
                   }
                   postAddsFourpl(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsFourLocPts/"+id,data);
                   }
                   postAddsFivepl(id,data){
                    return   this.http.post(environment.apiUrl +"/animalsAddsFiveLocPts/"+id,data);
                   }
                   

                   animalsAddsOneLocDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsOneLocDelete/"+id);
                  }
                
                  animalsAddsTwoLocDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsTwoLocDelete/"+id);
                  }
                  animalsAddsThreeLocDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsThreeDeleteLoc/"+id);
                  }
                  animalsAddsFourLocDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsFourLocDelete/"+id);
                  }
                  animalsAddsFiveLocDelete(id){
    
                    return this.http.delete(environment.apiUrl +"/animalsAddsFiveLocDelete/"+id);
                  }
                   
 }

