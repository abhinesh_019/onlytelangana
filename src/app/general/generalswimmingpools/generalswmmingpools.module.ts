import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralswimmingpoolsComponent } from './generalswimmingpools.component';
import { GeneralswmmingpoolsService } from './generalswmmingpools.service';
 

const routes:Routes=[{path:'generalswimmings',component:GeneralswimmingpoolsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralswimmingpoolsComponent
  ],
  providers: [ GeneralswmmingpoolsService],
})
 
export class GeneralswmmingpoolsModule { }
