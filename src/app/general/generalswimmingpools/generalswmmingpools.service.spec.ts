import { TestBed, inject } from '@angular/core/testing';

import { GeneralswmmingpoolsService } from './generalswmmingpools.service';

describe('GeneralswmmingpoolsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralswmmingpoolsService]
    });
  });

  it('should be created', inject([GeneralswmmingpoolsService], (service: GeneralswmmingpoolsService) => {
    expect(service).toBeTruthy();
  }));
});
