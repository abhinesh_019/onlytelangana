import { Component, OnInit } from '@angular/core';
import { GeneralclassifiedService } from './generalclassified.service';

@Component({
  selector: 'app-general-classsifides',
  templateUrl: './general-classsifides.component.html',
  styleUrls: ['./general-classsifides.component.css']
})
export class GeneralClasssifidesComponent implements OnInit {
  public locationName:any
public locationsget:any
public its:any
public locationss:any
 public landingsgets:any
public landingsName:any
public landingsNameId:any
public imageName:any
public videoName:any
public imageNames:any
public videoNames:any
public adminupdatesp:any
public searchesjobs:any
public statusNameId:any
public statusName:any
public statuseGets:any
public locationsNameId:any
public locationsName:any
public landingsgetsgenerals:any
public landingsgeneralsNameId:any
public landingsgeneralsName:any
public generalsDetails:any
public imageNamel:any
public searchesjobsc:any
public generalsDetailsc:any
public statuseGetsc:any


  dates={
    dates:""
  }
  lans={
    mainTitle:""
  }
  gen={
    mainTitle:""
  }
  status={
    status:""
  }
  searchjob={
    title:"",
    description:"",
    images:"",
    viedoes:""
  }
  generals={
    title:"",
    description:"",
    images:"",
    viedoes:""
  }
  loc={
    locations:"",
    images:""
  }
  constructor(private swim:GeneralclassifiedService) { }

  ngOnInit() {
    this.getlandings()
    this.locationsGet()
    this.statusGetc()
  }
  


  landssubmit(){
    this.swim.landingsubmits(this.lans).subscribe((res)=>{
       })
       this.lans.mainTitle=""
  }
 




    getlandings(){
      this.swim.landingsget().subscribe((res)=>{
      this.landingsgets=res
      
       var id =this.landingsgets[0];
          
      })  
      
    }
    
    landings(get?){
      this.landingsName=get.mainTitle
      this.landingsNameId=get._id 
  this.searchejobs() 
  this.searchejobsc()
     }
  
  
  handleFileInputBabycares($event:any,images){
    this.imageName= File = $event.target.files[0];
    // this.formData.append(images, imageFile);
  }
  handleFileInputBabycaresv($event:any,viedoes){
    this.videoName= File = $event.target.files[0];
  }
  FurnituresUpdatePostData( ){
    let formData: FormData = new FormData();
    formData.append('images', this.imageName);
    formData.append('viedoes', this.videoName);
    formData.append('description', this.searchjob.description);
    formData.append('title', this.searchjob.title);
  
    this.swim.postsdataupBabycares(this.landingsNameId,formData).subscribe((res)=>{
   })
        this.searchjob.title="",
        this.searchjob.description="",
        this.searchjob.images="",
        this.searchjob.viedoes=""
  }




  searchejobs(){
    this.swim.searchdjob(this.landingsNameId).subscribe((res)=>{
    this.searchesjobs=res
    })  
  }
  searchesjobes(get?){
    this.statusName=get.mainTitle
    this.statusNameId=get._id
  this.statusGet()
  this.statusGetc()
   }
   searchejobsc(){
    this.swim.searchdjobc(this.landingsNameId).subscribe((res)=>{
    this.searchesjobsc=res
    })  
  }
   poststatus(){
    this.swim.poststatu(this.statusNameId,this.status).subscribe((res)=>{
       })
       this.status.status=""
  }
  statusGet(){
    this.swim.statusGets(this.statusNameId).subscribe((res)=>{
    this.statuseGets=res
     })  
  }
  statusGetc(){
    this.swim.statusGetsc().subscribe((res)=>{
    this.statuseGetsc=res
     })  
  }
  handleFileInputL($event:any,images){
    this.imageNamel= File = $event.target.files[0];
    // this.formData.append(images, imageFile);
  }
  
  locationsPost(){
    let formData: FormData = new FormData();
    formData.append('images', this.imageNamel);
    formData.append('locations', this.loc.locations)
 
  
    this.swim.locationsPosts(formData).subscribe((res)=>{
         })
        this.loc.locations="",
        this.loc.images=""
    
  }

  
  locationsGet(){
    this.swim.locationsGets().subscribe((res)=>{
    this.locationsget=res
    var lov=this.locationsget[0]
    this.locations(lov)
    })  
  }
  locations(get?){
    this.locationsName=get.locations
    this.locationsNameId=get._id 
    this.getlandingsgenerals()
    }

    gensubmit(){
      this.swim.gensubmits(this.locationsNameId,this.gen).subscribe((res)=>{
         })
         this.gen.mainTitle=""
    }
  
    getlandingsgenerals(){
      this.swim.landingsgetgenerals(this.locationsNameId).subscribe((res)=>{
      this.landingsgetsgenerals=res
      
       var id =this.landingsgetsgenerals[0];
          
      })  
    }
    
    landingsgenerals(get?){
      this.landingsgeneralsName=get.mainTitle
      this.landingsgeneralsNameId=get._id 
  this.generalesDetails() 
  this.generalesDetailsc()    
     }




     handleFileInputGenerals($event:any,images){
      this.imageNames= File = $event.target.files[0];
      // this.formData.append(images, imageFile);
    }
    handleFileInputGeneralsv($event:any,viedoes){
      this.videoNames= File = $event.target.files[0];
    }
    GeneralsUpdatePostData(){
      let formData: FormData = new FormData();
      formData.append('images', this.imageNames);
      formData.append('viedoes', this.videoNames);
      formData.append('description', this.generals.description);
      formData.append('title', this.generals.title);
    
      this.swim.postsdataupGenerals(this.landingsgeneralsNameId,formData).subscribe((res)=>{
           })
          this.generals.title="",
          this.generals.description="",
          this.generals.images="",
          this.generals.viedoes=""
    }
  
  
    generalesDetails(){
      this.swim.generals(this.landingsgeneralsNameId).subscribe((res)=>{
      this.generalsDetails=res
      })  
    }
    generalesDetailsc(){
      this.swim.generalsc(this.landingsgeneralsNameId).subscribe((res)=>{
      this.generalsDetailsc=res
      })  
    }












}
