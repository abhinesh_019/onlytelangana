import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class GeneralclassifiedService {

  constructor(private http: HttpClient) { }
  datesubmits(data){
    return this.http.post(environment.apiUrl + "/classifidesDatep",data);
     }
     landingsubmits(data){
      return this.http.post(environment.apiUrl + "/classifideslandsp/",data);
       }
       locationsPosts(data){
        return this.http.post(environment.apiUrl + "/clasdifiedsGeneralsLocations/",data);
         }
         gensubmits(id,data){
        return this.http.post(environment.apiUrl + "/classifidsGeneralsLandingp/"+id,data);
         }
         poststatu(id,data){
          return this.http.post(environment.apiUrl + "/classifidesStatusp/"+id,data);
           }
   locationsget(){
    return this.http.get(environment.apiUrl +"/classifidesDate");
                }
                landingsget(){
                  return this.http.get(environment.apiUrl +"/classifidesLands");
                              }
                              postsdataupBabycares(id,data){
                                return this.http.post(environment.apiUrl + "/classifideSearchJobsp/"+id,data);
                                 }
                                 postsdataupGenerals(id,data){
                                  return this.http.post(environment.apiUrl + "/classifideGeneralsp/"+id,data);
                                   }
                                 searchdjob(id){
                                  return this.http.get(environment.apiUrl +"/classifideSearchJobsg/"+id);
                                              }
                                              searchdjobc(id){
                                                return this.http.get(environment.apiUrl +"/classifideSearchJobsgc/"+id);
                                                            }
                                              statusGets(id){
                                                return this.http.get(environment.apiUrl +"/classifidesStatus/"+id);
                                                            }
                                                            statusGetsc(){
                                                              return this.http.get(environment.apiUrl +"/classifidesStatusc");
                                                                          }
                                                            locationsGets(){
                                                              return this.http.get(environment.apiUrl +"/clasdifiedsGeneralsLocations");
                                                                          }
                                                                          
                                                                          landingsgetgenerals(id){
                                                                            return this.http.get(environment.apiUrl +"/classifidsGeneralsLanding/"+id);
                                                                                        }
                                                                                        generals(id){
                                                                                          return this.http.get(environment.apiUrl +"/classifideGeneralsg/"+id);
                                                                                                      }
                                                                                                      generalsc(id){
                                                                                                        return this.http.get(environment.apiUrl +"/classifideGeneralsgc/"+id);
                                                                                                                    }
}
