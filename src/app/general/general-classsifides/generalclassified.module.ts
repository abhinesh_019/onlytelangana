import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralClasssifidesComponent } from './general-classsifides.component';
import { GeneralclassifiedService } from './generalclassified.service';
 


const routes:Routes=[{path:'generalsClassifieds',component:GeneralClasssifidesComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  declarations: [
    GeneralClasssifidesComponent,
  ],
  providers: [GeneralclassifiedService],
})
export class GeneralclassifiedModule { }
