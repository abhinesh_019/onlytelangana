import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneraltravelsComponent } from './generaltravels.component';

describe('GeneraltravelsComponent', () => {
  let component: GeneraltravelsComponent;
  let fixture: ComponentFixture<GeneraltravelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneraltravelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneraltravelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
