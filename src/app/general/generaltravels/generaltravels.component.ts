import { Component, OnInit } from '@angular/core';
import { GeneraltravelsService } from './generaltravels.service';

@Component({
  selector: 'app-generaltravels',
  templateUrl: './generaltravels.component.html',
  styleUrls: ['./generaltravels.component.css']
})
export class GeneraltravelsComponent implements OnInit {

  locationsdata={
    "locations":""
  }
  areas={
    "area":""
  }
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  constructor(private travels:GeneraltravelsService) { }

  ngOnInit() {
    this.getlocations()
  }
  postlocations(){
 
    this.travels.locationspost(this.locationsdata).subscribe((res)=>{
    
      console.log(res);
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.travels.locationsget().subscribe((res)=>{
      this.locationsgets=res
      console.log(this.locationsgets);
      var id =this.locationsgets[0];
      console.log(this.locationsgets[0]);
      
      this.locations(id)
         
      })  }
      locations(get?){
        this.locationName=get.locations
        this.its=get._id
        this.locationss=get.locations
        console.log(this.locationss);
        console.log(this.its);
        this.allAreas()
      }
      allAreas(){
           
        this.travels.areaapi(this.its).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0];
          console.log(this.areasAll[0]);
          
          this.selectedAreas(id)
          
        })
       
      }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
        console.log(this.selectArea);
        console.log(this.selectAreaId);
         
      }
      
      postarea(){
        this.travels.areapost(this.its,this.areas).subscribe((res)=>{
           console.log(res);
          this.areas.area=""
        }) 
      }
      
}

