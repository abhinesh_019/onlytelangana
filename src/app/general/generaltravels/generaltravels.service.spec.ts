import { TestBed, inject } from '@angular/core/testing';

import { GeneraltravelsService } from './generaltravels.service';

describe('GeneraltravelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneraltravelsService]
    });
  });

  it('should be created', inject([GeneraltravelsService], (service: GeneraltravelsService) => {
    expect(service).toBeTruthy();
  }));
});
