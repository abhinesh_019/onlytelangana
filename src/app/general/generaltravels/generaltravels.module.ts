import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 
import { GeneraltravelsComponent } from './generaltravels.component';
import { GeneraltravelsService } from './generaltravels.service';


const routes:Routes=[{path:'generalstravels',component:GeneraltravelsComponent}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneraltravelsComponent
  ],
  providers: [ GeneraltravelsService],
})
 
export class GeneraltravelsModule { }
