import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class MainmenuService {

  constructor(private http: HttpClient) { }
  addsget(){
    return this.http.get(environment.apiUrl +"/advertiseOnly");
                }
                feedbacks(){
                  return this.http.get(environment.apiUrl +"/feedbackOnly");

                }
                addsgetc(){
                  return this.http.get(environment.apiUrl +"/advertiseOnlyc");
                              }
                              feedbacksc(){
                                return this.http.get(environment.apiUrl +"/feedbackOnlyc");
              
                              }
                              addOne(data){
                                return this.http.post(environment.apiUrl + "/advertiseOnep",data);
                                 }
                                 addtwo(data){
                                  return this.http.post(environment.apiUrl + "/advertiseTwop",data);
                                   }
                                   addthree(data){
                                    return this.http.post(environment.apiUrl + "/advertiseThreep",data);
                                     }
                                     addfour(data){
                                      return this.http.post(environment.apiUrl + "/advertiseFourp",data);
                                       }
                                       addfive(data){
                                        return this.http.post(environment.apiUrl + "/advertiseFivep",data);
                                         }

                                         addsix(data){
                                          return this.http.post(environment.apiUrl + "/advertiseSixp",data);
                                           }
                                           addseven(data){
                                            return this.http.post(environment.apiUrl + "/advertiseSevenp",data);
                                             }
                                             addeight(data){
                                              return this.http.post(environment.apiUrl + "/advertiseEightp",data);
                                               }
                                               addnine(data){
                                                return this.http.post(environment.apiUrl + "/advertiseNinep",data);
                                                 }
                                                 addten(data){
                                                  return this.http.post(environment.apiUrl + "/advertiseTenp",data);
                                                   }


                                 addsonegets(){
                                  return this.http.get(environment.apiUrl +"/advertiseOne");
                
                                }
                                addstwogets(){
                                  return this.http.get(environment.apiUrl +"/advertiseTwo");
                
                                }
                                addsthreegets(){
                                  return this.http.get(environment.apiUrl +"/advertiseThree");
                
                                }
                                addsfourgets(){
                                  return this.http.get(environment.apiUrl +"/advertiseFour");
                
                                }
                                addsfivegets(){
                                  return this.http.get(environment.apiUrl +"/advertiseFive");
                
                                }
                                addssixgets(){
                                  return this.http.get(environment.apiUrl +"/advertiseSix");
                
                                }


                                addssevengets(){
                                  return this.http.get(environment.apiUrl +"/advertiseSeven");
                
                                }
                                addseightgets(){
                                  return this.http.get(environment.apiUrl +"/advertiseEight");
                
                                }
                                addsninegets(){
                                  return this.http.get(environment.apiUrl +"/advertiseNine");
                
                                }
                                addstengets(){
                                  return this.http.get(environment.apiUrl +"/advertiseTen");
                
                                }

                                addonedelete(data){
                                  return this.http.delete(environment.apiUrl + "/advertiseOned/"+data);
                                   }
                                   
                                   addtwodelete(data){
                                    return this.http.delete(environment.apiUrl + "/advertisetwod/"+data);
                                     }
                                     addthreedelete(data){
                                      return this.http.delete(environment.apiUrl + "/advertisethreed/"+data);
                                       }
                                       addfourdelete(data){
                                        return this.http.delete(environment.apiUrl + "/advertisefourd/"+data);
                                         }
                                         addfivedelete(data){
                                          return this.http.delete(environment.apiUrl + "/advertisefived/"+data);
                                           }
                                           addsixdelete(data){
                                            return this.http.delete(environment.apiUrl + "/advertiseSixd/"+data);
                                             }
                                             addsevendelete(data){
                                              return this.http.delete(environment.apiUrl + "/advertiseSevend/"+data);
                                               }
                                               addeightdelete(data){
                                                return this.http.delete(environment.apiUrl + "/advertiseeightd/"+data);
                                                 }
                                                 addninedelete(data){
                                                  return this.http.delete(environment.apiUrl + "/advertisenined/"+data);
                                                   }
                                                   addtendelete(data){
                                                    return this.http.delete(environment.apiUrl + "/advertisetend/"+data);
                                                     }

                                                  
}
