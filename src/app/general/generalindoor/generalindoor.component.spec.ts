import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralindoorComponent } from './generalindoor.component';

describe('GeneralindoorComponent', () => {
  let component: GeneralindoorComponent;
  let fixture: ComponentFixture<GeneralindoorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralindoorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralindoorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
