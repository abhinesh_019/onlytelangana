import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralfightsComponent } from './generalfights.component';

describe('GeneralfightsComponent', () => {
  let component: GeneralfightsComponent;
  let fixture: ComponentFixture<GeneralfightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralfightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralfightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
