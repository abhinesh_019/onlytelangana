import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {GeneralTutionsService} from'./general-tutions.service'
@Component({
  selector: 'app-generals-tutions',
  templateUrl: './generals-tutions.component.html',
  styleUrls: ['./generals-tutions.component.css']
})
export class GeneralsTutionsComponent implements OnInit {
  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  public babycaresAddsOneLocl:any


  areaAddsOne={
    "addsOneLink":"",
    "addsOneImg":"",
  }
  areaAddsTwo={
    "addsTwoLink":"",
    "addsTwoImg":"",
  }
  areaAddsThree={
    "addsThreeLink":"",
    "addsThreeImg":"",
  }
  areaAddsFour={
    "addsFourLink":"",
    "addsFourImg":"",
  }
   
  
  areaAddsOneLoc={
    "addsOneLinkLoc":"",
    "addsOneImgLoc":"",
  }
  areaAddsTwoLoc={
    "addsTwoLinkLoc":"",
    "addsTwoImgLoc":"",
  }
  areaAddsThreeLoc={
    "addsThreeLinkLoc":"",
    "addsThreeImgLoc":"",
  }
  areaAddsFourLoc={
    "addsFourLinkLoc":"",
    "addsFourImgLoc":"",
  }
  areaAddsFiveLoc={
    "addsFiveLinkLoc":"",
    "addsFiveImgLoc":"",
  }
  public imagesName:any
  formData: FormData = new FormData(); 
  catdata={
  "categories":"",
  "images1":"",
 }
 locationsdata={
  "locations":""
}
areas={
  "area":""
}


usersMain={
  "name":"",
  "tutionsName":"",
  "myProfileDescriptionOne":"",
  "myProfileDescriptionTwo":"",
  "ourAimDescriptionOne":"",
  "ourAimDescriptionTwo":"",
  "parentsInformation":"",
 
    "fecilities1":"",
    "fecilities2"   :"",
     "fecilities3" :"",
    "fecilities4"   :"",
    "fecilities5" :"",
"fecilities6":"",
  "fecilities7"  :"",
  "fecilities8" :"",
  "fecilities9"  :"",
  "fecilities10":"",
 "fecilities11":"",
  "fecilities12":"",
  "fecilities13":"",
  "fecilities14":"",
  "fecilities15":"",
 
  "day1":"",
  "day2":"",
  "day3":"",
  "day4":"",
  "day5":"",
  "day6":"",
  "day7":"",
  "allTimes":"",
 "houseNo":"",
  "subArea":"",
  "landmark":"",
  "city":"",
"distict":"",
 "state":"",
   "pincode":"",
  "officeNo":"",
   "mobileNo":"",
   "whatsupno":"",
   "emailId":"",
 "images1":"",
"images2":"",
"images3":"",
"images4":"",
"images5":"",
 "latitude":"",
 "longitude":""
}



 public catList:any
 public catName:any
 public its:any

 public locationsgets:any
 public locationName:any
 public locationss:any
 public idss:any
 public areasAll:any
public selectAreaId:any

public selectArea:any


  constructor(private tutions:GeneralTutionsService) { }

  ngOnInit() {
this.getcatagories()
   }
 
// *********************catagories***************   
   handleFile($event:any,images){
  this.imagesName= File = $event.target.files[0]; 
 
}

postcat(){
 
 this.formData.append('images1', this.imagesName);
 
 this.formData.append('categories',this.catdata.categories); 

 this.tutions.catpost(this.formData).subscribe((res)=>{
     
     })

     this.catdata.categories="",
     this.catdata.images1=""
     
}

getcatagories(){
  this.tutions.catapiget().subscribe((res)=>{
this.catList=res 

var id =this.catList[0]; 
      this.catagoriescal(id)

        })  }
      catagoriescal(get?){
        this.catName=get.categories
        this.its=get._id 
        
        this.getlocations()
       }
      
      // ************************************   

      // *********************** locations post ****************
      
      postlocations(){
 
       this.tutions.locationspost(this.its,this.locationsdata).subscribe((res)=>{ 
       this.locationsdata.locations=""
       })  }
    
        getlocations(){
          this.tutions.locationsget(this.its).subscribe((res)=>{
          this.locationsgets=res
           var id =this.locationsgets[0];
           
          this.locations(id)
             
          })  }
          locations(get?){
            this.locationName=get.locations
            this.idss=get._id
             this.allAreas()
             this.babycaressareasAddsOneLoc()
             this.babycaressareasAddsTwoLoc()
             this.babycaressareasAddsThreeLoc()
             this.babycaressareasAddsFourLoc()
             this.babycaressareasAddsFiveLoc()
           }
// ************************************************************************************************************************************


//  **********************************area *************************************
postarea(){
  this.tutions.areapost(this.idss,this.areas).subscribe((res)=>{
    
    this.areas.area=""
  }) 
}


allAreas(){
           
  this.tutions.areaapi(this.idss).subscribe((res)=>{
    this.areasAll=res
    var id =this.areasAll[0];
    this.selectedAreas(id)
    
  })
 
}

selectedAreas(result){
  this.selectAreaId=result._id
  this.selectArea=result.area 
  this.babycaressareasAddsOnea()
  this.babycaressareasAddsTwoa()
  this.babycaressareasAddsThreea()
  this.babycaressareasAddsFoura() 
}

// ************************************************************************
// ***************************************post users ********************************  
handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile);
 
  
}
postUsers(){
  
   
    this.formData.append('name',this.usersMain.name);
    this.formData.append('tutionsName',this.usersMain.tutionsName);
    this.formData.append('myProfileDescriptionOne',this.usersMain.myProfileDescriptionOne);
    this.formData.append('myProfileDescriptionTwo',this.usersMain.myProfileDescriptionTwo);
     this.formData.append('ourAimDescriptionOne',this.usersMain.ourAimDescriptionOne);
    this.formData.append('ourAimDescriptionTwo',this.usersMain.ourAimDescriptionTwo);
    this.formData.append('parentsInformation',this.usersMain.parentsInformation);
    this.formData.append('fecilities1',this.usersMain.fecilities1);
    this.formData.append('fecilities2',this.usersMain.fecilities2);
    this.formData.append('fecilities3',this.usersMain.fecilities3);
    this.formData.append('fecilities4',this.usersMain.fecilities4);
    this.formData.append('fecilities5',this.usersMain.fecilities5);
    this.formData.append('fecilities6',this.usersMain.fecilities6);
    this.formData.append('fecilities7',this.usersMain.fecilities7);
    this.formData.append('fecilities8',this.usersMain.fecilities8);
    this.formData.append('fecilities9',this.usersMain.fecilities9);
    this.formData.append('fecilities10',this.usersMain.fecilities10);
    this.formData.append('fecilities11',this.usersMain.fecilities11);
    this.formData.append('fecilities12',this.usersMain.fecilities12);
    this.formData.append('fecilities13',this.usersMain.fecilities13);
    this.formData.append('fecilities14',this.usersMain.fecilities13);
    this.formData.append('fecilities15',this.usersMain.fecilities13);
    this.formData.append('day1',this.usersMain.day1);
    this.formData.append('day2',this.usersMain.day2);
    this.formData.append('day3',this.usersMain.day3);
    this.formData.append('day4',this.usersMain.day4);
    this.formData.append('day5',this.usersMain.day5);
    this.formData.append('day6',this.usersMain.day6);
    this.formData.append('day7',this.usersMain.day7);
    this.formData.append('allTimes',this.usersMain.allTimes);
    this.formData.append('houseNo',this.usersMain.houseNo);
    this.formData.append('mainArea',this.selectArea);
    this.formData.append('subArea',this.usersMain.subArea);
    this.formData.append('landmark',this.usersMain.landmark);
    this.formData.append('city',this.usersMain.city);
    this.formData.append('distict',this.locationName);
    this.formData.append('state',this.usersMain.state);
    this.formData.append('pincode',this.usersMain.pincode);
    this.formData.append('officeNo',this.usersMain.officeNo);
    this.formData.append('mobileNo',this.usersMain.mobileNo);
    this.formData.append('whatsupno',this.usersMain.whatsupno);
    this.formData.append('emailId',this.usersMain.emailId);
    this.formData.append('latitude',this.usersMain.latitude);
    this.formData.append('longitude',this.usersMain.longitude);


  this.usersMain.name=""
  this.usersMain.tutionsName=""
  this.usersMain.myProfileDescriptionOne="",
  this.usersMain.myProfileDescriptionTwo="",
  this.usersMain.ourAimDescriptionOne="",
  this.usersMain.ourAimDescriptionTwo="",
  this.usersMain.parentsInformation="",
   
  
  this.usersMain.fecilities1="",
  this.usersMain.fecilities2="",
  this.usersMain.fecilities3="",
  this.usersMain.fecilities4="",
  this.usersMain.fecilities5="",
  this.usersMain.fecilities6="",
  this.usersMain.fecilities7="",
  this.usersMain.fecilities8="",
  this.usersMain.fecilities9="",
  this.usersMain.fecilities10="",
  this.usersMain.fecilities11="",
  this.usersMain.fecilities12="",
  this.usersMain.fecilities13="",
  this.usersMain.fecilities14="",
  this.usersMain.fecilities15="",


  this.usersMain.day1="",
  this.usersMain.day2="",
  this.usersMain.day3="",
  this.usersMain.day4="",
  this.usersMain.day5="",
  this.usersMain.day6="",
  this.usersMain.day7="",
  this.usersMain.allTimes="",
   
  this.usersMain.houseNo="",
  this.usersMain.subArea="",
  this.usersMain.landmark="",
  this.usersMain.city="",
  
  
   this.usersMain.state="",
  this.usersMain.pincode="",
  this.usersMain.officeNo="",
  this.usersMain.mobileNo="",
  this.usersMain.whatsupno="",
  this.usersMain.emailId="",
  this.usersMain.latitude="",
  this.usersMain.longitude="",
  
  this.usersMain.images1="",
  this.usersMain.images2="",
  this.usersMain.images3="",
  this.usersMain.images4="",
  this.usersMain.images5="",
   
  
      
        this.tutions.areapostusers(this.selectAreaId,this.formData).subscribe((res)=>{
          
          this.areas.area=""
        }) 
      
  
  
  }

// **************************************************************************************
       

// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.tutions.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this.tutions.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.tutions.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this.tutions.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.tutions.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this.tutions.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.tutions.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this.tutions.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.tutions.postAddsOnepl(this.idss,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this.tutions.babycaresAddsOnel(this.idss).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.tutions.postAddsTwopl(this.idss,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this.tutions.babycaresAddsTwol(this.idss).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.tutions.postAddsThreepl(this.idss,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this.tutions.babycaresAddsThreel(this.idss).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.tutions.postAddsFourpl(this.idss,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this.tutions.babycaresAddsFourl(this.idss).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.tutions.postAddsFivepl(this.idss,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this.tutions.babycaresAddsFivel(this.idss).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.tutions.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}

