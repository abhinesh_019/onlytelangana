import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralmusicComponent } from './generalmusic.component';
import { GeneralmusicService } from './generalmusic.service';
 
 
const routes:Routes=[{path:'generalsmusic',component:GeneralmusicComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralmusicComponent
  ],
  providers: [ GeneralmusicService],
})
 
export class GeneralmusicModule { }
