import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()
export class GeneralmusicService {

  constructor(private http: HttpClient) { }

  
  locationspost(data) {
    return   this.http.post(environment.apiUrl +"/musicLocations",data);

 
    }

    locationsget(){
       return this.http.get(environment.apiUrl +"/musicLocations");
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/musicAreas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAreas/"+id,data);
                   }
                   areapostusers(id,data){
                    return   this.http.post(environment.apiUrl +"/musicClientsPost/"+id,data);
                   }
 
                   babycaresAddsOnea(id){

                    return this.http.get(environment.apiUrl +"/musicAddsOneGeta/"+id);
                  }
                  babycaresAddsTwoa(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsTwoGeta/"+id);
                  }
                  babycaresAddsThreea(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsThreeGeta/"+id);
                  }
                  babycaresAddsFoura(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsFourGeta/"+id);
                  }
                   
                  babycaresAddsOnel(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsOneLocGet/"+id);
                  }
                  babycaresAddsTwol(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsTwoLocGet/"+id);
                  }
                  babycaresAddsThreel(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsThreeGetLoc/"+id);
                  }
                  babycaresAddsFourl(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsFourLocGet/"+id);
                  }
                
                  babycaresAddsFivel(id){
                    
                    return this.http.get(environment.apiUrl +"/musicAddsFiveLocGet/"+id);
                  }
                   
                  postAddsOne(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsOnePtsa/"+id,data);
                   }
                   postAddsTwo(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsTwoPtsa/"+id,data);
                   }
                   postAddsThree(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsThreePtsa/"+id,data);
                   }
                   postAddsFour(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsFourPtsa/"+id,data);
                   }
                   
                
                
                  postAddsOnepl(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsOneLocPts/"+id,data);
                   }
                   postAddsTwopl(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsTwoLocPts/"+id,data);
                   }
                   postAddsThreepl(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsThreePtsLoc/"+id,data);
                   }
                   postAddsFourpl(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsFourLocPts/"+id,data);
                   }
                   postAddsFivepl(id,data){
                    return   this.http.post(environment.apiUrl +"/musicAddsFiveLocPts/"+id,data);
                   }
                   
                
                
                   babycaresAddsOneaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsOneDeletea/"+id);
                  }
                 
                  babycaresAddsTwoaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsTwoDeletea/"+id);
                  }
                  babycaresAddsThreeaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsThreeDeletea/"+id);
                  }
                  babycaresAddsFouraDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsFourDeletea/"+id);
                  }
                
                
                   babycaresAddsOneLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsOneLocDelete/"+id);
                  }
                
                  babycaresAddsTwoLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsTwoLocDelete/"+id);
                  }
                  babycaresAddsThreeLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsThreeDeleteLoc/"+id);
                  }
                  babycaresAddsFourLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsFourLocDelete/"+id);
                  }
                  babycaresAddsFiveLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/musicAddsFiveLocDelete/"+id);
                  }
                   
                 }
                 