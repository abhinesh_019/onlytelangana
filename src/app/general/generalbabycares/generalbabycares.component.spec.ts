import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralbabycaresComponent } from './generalbabycares.component';

describe('GeneralbabycaresComponent', () => {
  let component: GeneralbabycaresComponent;
  let fixture: ComponentFixture<GeneralbabycaresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralbabycaresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralbabycaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
