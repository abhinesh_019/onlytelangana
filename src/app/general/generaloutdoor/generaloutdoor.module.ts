import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneraloutdoorComponent } from './generaloutdoor.component';
import { GeneraloutdoorService } from './generaloutdoor.service';
 
const routes:Routes=[{path:'generaloutdoorgames',component:GeneraloutdoorComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneraloutdoorComponent
  ],
  providers: [ GeneraloutdoorService],
})
 
export class GeneraloutdoorModule { }
