import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()


export class GeneralpackersMoversService {
  constructor(private http: HttpClient) { }

  
  locationspost(data) {
    return   this.http.post(environment.apiUrl +"/packersMoversLocations",data);

 
    }

    locationsget(){
       return this.http.get(environment.apiUrl +"/packersMoversLocations");
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/packersMoversareas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversareas/"+id,data);
                   }
                   areapostusers(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversClientsPost/"+id,data);
                   }
 
                   babycaresAddsOnea(id){

                    return this.http.get(environment.apiUrl +"/packersMoversAddsOneGeta/"+id);
                  }
                  babycaresAddsTwoa(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsTwoGeta/"+id);
                  }
                  babycaresAddsThreea(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsThreeGeta/"+id);
                  }
                  babycaresAddsFoura(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsFourGeta/"+id);
                  }
                   
                  babycaresAddsOnel(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsOneLocGet/"+id);
                  }
                  babycaresAddsTwol(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsTwoLocGet/"+id);
                  }
                  babycaresAddsThreel(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsThreeGetLoc/"+id);
                  }
                  babycaresAddsFourl(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsFourLocGet/"+id);
                  }
                
                  babycaresAddsFivel(id){
                    
                    return this.http.get(environment.apiUrl +"/packersMoversAddsFiveLocGet/"+id);
                  }
                   
                  postAddsOne(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsOnePtsa/"+id,data);
                   }
                   postAddsTwo(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsTwoPtsa/"+id,data);
                   }
                   postAddsThree(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsThreePtsa/"+id,data);
                   }
                   postAddsFour(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsFourPtsa/"+id,data);
                   }
                   
                
                
                  postAddsOnepl(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsOneLocPts/"+id,data);
                   }
                   postAddsTwopl(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsTwoLocPts/"+id,data);
                   }
                   postAddsThreepl(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsThreePtsLoc/"+id,data);
                   }
                   postAddsFourpl(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsFourLocPts/"+id,data);
                   }
                   postAddsFivepl(id,data){
                    return   this.http.post(environment.apiUrl +"/packersMoversAddsFiveLocPts/"+id,data);
                   }
                   
                
                
                   babycaresAddsOneaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsOneDeletea/"+id);
                  }
                 
                  babycaresAddsTwoaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsTwoDeletea/"+id);
                  }
                  babycaresAddsThreeaDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsThreeDeletea/"+id);
                  }
                  babycaresAddsFouraDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsFourDeletea/"+id);
                  }
                
                
                   babycaresAddsOneLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsOneLocDelete/"+id);
                  }
                
                  babycaresAddsTwoLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsTwoLocDelete/"+id);
                  }
                  babycaresAddsThreeLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsThreeDeleteLoc/"+id);
                  }
                  babycaresAddsFourLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsFourLocDelete/"+id);
                  }
                  babycaresAddsFiveLocDelete(id){
                
                    return this.http.delete(environment.apiUrl +"/packersMoversAddsFiveLocDelete/"+id);
                  }
                   
                 }