import { TestBed, inject } from '@angular/core/testing';

import { GeneralpackersMoversService } from './generalpackers-movers.service';

describe('GeneralpackersMoversService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralpackersMoversService]
    });
  });

  it('should be created', inject([GeneralpackersMoversService], (service: GeneralpackersMoversService) => {
    expect(service).toBeTruthy();
  }));
});
