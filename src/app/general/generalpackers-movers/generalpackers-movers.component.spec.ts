import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralpackersMoversComponent } from './generalpackers-movers.component';

describe('GeneralpackersMoversComponent', () => {
  let component: GeneralpackersMoversComponent;
  let fixture: ComponentFixture<GeneralpackersMoversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralpackersMoversComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralpackersMoversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
