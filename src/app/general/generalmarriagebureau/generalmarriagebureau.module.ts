import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralmarriagebureauComponent } from './generalmarriagebureau.component';
import { GeneralmarriagebureauService } from './generalmarriagebureau.service';
 
const routes:Routes=[{path:'generalmarriageBureau',component:GeneralmarriagebureauComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralmarriagebureauComponent
  ],
  providers: [ GeneralmarriagebureauService],
})
 
export class GeneralmarriagebureauModule { }
