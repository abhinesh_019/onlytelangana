import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralmarriagebureauComponent } from './generalmarriagebureau.component';

describe('GeneralmarriagebureauComponent', () => {
  let component: GeneralmarriagebureauComponent;
  let fixture: ComponentFixture<GeneralmarriagebureauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralmarriagebureauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralmarriagebureauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
