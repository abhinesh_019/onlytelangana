import { Component, OnInit } from '@angular/core';
import { FurnituresService } from './generalfurnitures.service';
import { ActivatedRoute } from '@angular/router';
 
@Component({
  selector: 'app-general-furnitures',
  templateUrl: './general-furnitures.component.html',
  styleUrls: ['./general-furnitures.component.css']
})
export class GeneralFurnituresComponent implements OnInit {
  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  public babycaresAddsOneLocl:any


  areaAddsOne={
    "addsOneLink":"",
    "addsOneImg":"",
  }
  areaAddsTwo={
    "addsTwoLink":"",
    "addsTwoImg":"",
  }
  areaAddsThree={
    "addsThreeLink":"",
    "addsThreeImg":"",
  }
  areaAddsFour={
    "addsFourLink":"",
    "addsFourImg":"",
  }
   
  
  areaAddsOneLoc={
    "addsOneLinkLoc":"",
    "addsOneImgLoc":"",
  }
  areaAddsTwoLoc={
    "addsTwoLinkLoc":"",
    "addsTwoImgLoc":"",
  }
  areaAddsThreeLoc={
    "addsThreeLinkLoc":"",
    "addsThreeImgLoc":"",
  }
  areaAddsFourLoc={
    "addsFourLinkLoc":"",
    "addsFourImgLoc":"",
  }
  areaAddsFiveLoc={
    "addsFiveLinkLoc":"",
    "addsFiveImgLoc":"",
  }
   
public furniturlocations:any
public selectAreaId:any
public fileids:any
public selectArea:any
public locationsget:any
public locationName:any
public its:any;
public locationget:any
 
public areasAll:any
locationsdata={
  "locations":""
}

areas={
  "area":""
}


products={
  images:"",
  name:"",
  price:"",
  productDescription:"",
  guarantee:"",
}


furnitures={
  "name":"",
  "shopname":"",
  "aboutusdescriptionone":"",
  "aboutusdescriptiontwo":"",
  "informationtopublic":"",
  "mon":"",
  "tue":"",
  "wed":"",
    "thu" :"",
    "fri" :"",
    "sat":"",
    "sun"   :"",
     "totaltiming" :"",
    "hno"   :" ",
    "area" :" ",
"landmark":"",
  "city"  :"",
  "distict" :" ",
  "state"  :" ",
  "pincode":" ",

 "officeno":"",
  "mobileno":" ",
  "whatsappno":" ",
  "email" :" ",
   "images1":"",
  "images2":"",
  "images3":"",
  "images4":"",
  "images5":"",
  "latitude":"",
  "longitude":"",

}
public formData: any = new FormData();

  constructor(private route:ActivatedRoute,private furniture:FurnituresService) { }
  ngOnInit() {
    this.getlocations()
  }
  
  postlocations(){
 
    this.furniture.locationspost(this.locationsdata).subscribe((res)=>{
      this.furniturlocations=res 
      
    })  }



    //  ************************main users details *********************

handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile);
  
}


    
 OnSubmit( ){
  
  this.formData.append('name',this.furnitures.name);
  this.formData.append('shopname',this.furnitures.shopname);
  this.formData.append('email',this.furnitures.email);
  this.formData.append('aboutusdescriptionone',this.furnitures.aboutusdescriptionone);
  this.formData.append('aboutusdescriptiontwo',this.furnitures.aboutusdescriptiontwo);
  this.formData.append('informationtopublic',this.furnitures.informationtopublic);
  this.formData.append('mon',this.furnitures.mon);
  this.formData.append('tue',this.furnitures.tue);
  this.formData.append('fri',this.furnitures.fri);
  this.formData.append('sat',this.furnitures.sat);
  this.formData.append('sun',this.furnitures.sun);
  this.formData.append('totaltiming ',this.furnitures.totaltiming);
  this.formData.append('totaltiming ',this.furnitures.totaltiming);
  this.formData.append('hno',this.furnitures.hno);
  this.formData.append('area',this.furnitures.area);
  this.formData.append('landmark',this.furnitures.landmark);
  this.formData.append('city',this.furnitures.city);
  this.formData.append('distict',this.locationName);
  this.formData.append('state',this.furnitures.state);
  this.formData.append('pincode',this.furnitures.pincode);
  this.formData.append('officeno',this.furnitures.officeno);
  this.formData.append('mobileno',this.furnitures.mobileno);
  this.formData.append('whatsappno',this.furnitures.whatsappno);
  this.formData.append('email',this.furnitures.email);
  this.formData.append('email',this.furnitures.email);
  this.formData.append('mainArea',this.selectArea);
  this.formData.append('latitude',this.furnitures.latitude);
  this.formData.append('longitude',this.furnitures.longitude);
this.furnitures.name=""
this.furnitures.shopname=""
this.furnitures.email="",
this.furnitures.aboutusdescriptionone="",
this.furnitures.aboutusdescriptiontwo="",
this.furnitures.informationtopublic="",
this.furnitures.mon="",
this.furnitures.tue="",
this.furnitures.wed="",
this.furnitures.thu="",
this.furnitures.fri="",
this.furnitures.sat="",
this.furnitures.sun="",
this.furnitures.totaltiming="",
 
this.furnitures.hno="",
this.furnitures.area="",
this.furnitures.landmark="",
this.furnitures.city="",


this.furnitures.distict="",
this.furnitures.state="",
this.furnitures.pincode="",
this.furnitures.officeno="",
this.furnitures.mobileno="",
this.furnitures.whatsappno="",
this.furnitures.email="",

this.furnitures.images1="",
this.furnitures.images2="",
this.furnitures.images3="",
this.furnitures.images4="",
this.furnitures.images5="",
  this.furniture.postFiles(this.selectAreaId,this.formData).subscribe(
    data =>{
    
      
    }
  );
 }
 
 

// locations*****************
getlocations(){
  this.furniture.locationapi().subscribe((res)=>{
    this.locationsget=res;
    
    var id =this.locationsget[0]; 
    
    this.locations(id)
  })

}
 
locations(get?){
  this.locationName=get.locations
  this.its=get._id
  this.locationget=get.locations
        this.babycaressareasAddsOneLoc()
        this.babycaressareasAddsTwoLoc()
        this.babycaressareasAddsThreeLoc()
        this.babycaressareasAddsFourLoc()
        this.babycaressareasAddsFiveLoc()
  
  this.allAreas()
}
allAreas(){
     
  this.furniture.areaapi(this.its).subscribe((res)=>{
    this.areasAll=res
    var id =this.areasAll[0]; 
    
    this.selectedAreas(id)
    
  })
 
}

selectedAreas(result){
  this.selectAreaId=result._id
  this.selectArea=result.area
  this.babycaressareasAddsOnea()
  this.babycaressareasAddsTwoa()
  this.babycaressareasAddsThreea()
  this.babycaressareasAddsFoura()
   
}

postarea(){
  this.furniture.areapost(this.its,this.areas).subscribe((res)=>{
    this.furniturlocations=res 
    this.areas.area=""
  }) 
}
 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.furniture.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this.furniture.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.furniture.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this.furniture.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.furniture.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this.furniture.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.furniture.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this.furniture.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.furniture.postAddsOnepl(this.its,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this.furniture.babycaresAddsOnel(this.its).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.furniture.postAddsTwopl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this.furniture.babycaresAddsTwol(this.its).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.furniture.postAddsThreepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this.furniture.babycaresAddsThreel(this.its).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.furniture.postAddsFourpl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this.furniture.babycaresAddsFourl(this.its).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.furniture.postAddsFivepl(this.its,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this.furniture.babycaresAddsFivel(this.its).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.furniture.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}
