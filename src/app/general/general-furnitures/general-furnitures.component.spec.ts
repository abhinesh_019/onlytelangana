import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralFurnituresComponent } from './general-furnitures.component';

describe('GeneralFurnituresComponent', () => {
  let component: GeneralFurnituresComponent;
  let fixture: ComponentFixture<GeneralFurnituresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralFurnituresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralFurnituresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
