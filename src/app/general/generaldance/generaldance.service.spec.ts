import { TestBed, inject } from '@angular/core/testing';

import { GeneraldanceService } from './generaldance.service';

describe('GeneraldanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneraldanceService]
    });
  });

  it('should be created', inject([GeneraldanceService], (service: GeneraldanceService) => {
    expect(service).toBeTruthy();
  }));
});
