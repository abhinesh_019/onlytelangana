import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneraldanceComponent } from './generaldance.component';
import { GeneraldanceService } from './generaldance.service';
 
const routes:Routes=[{path:'generaldances',component:GeneraldanceComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneraldanceComponent
  ],
  providers: [ GeneraldanceService],
})
 
export class GeneraldanceModule { }
