import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneraldanceComponent } from './generaldance.component';

describe('GeneraldanceComponent', () => {
  let component: GeneraldanceComponent;
  let fixture: ComponentFixture<GeneraldanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneraldanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneraldanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
