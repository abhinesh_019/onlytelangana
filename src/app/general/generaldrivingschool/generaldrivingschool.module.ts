import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneraldrivingschoolComponent } from './generaldrivingschool.component';
import { GeneraldrivingschoolService } from './generaldrivingschool.service';
 
const routes:Routes=[{path:'generalDrivingSchools',component:GeneraldrivingschoolComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneraldrivingschoolComponent
  ],
  providers: [ GeneraldrivingschoolService],
})
export class GeneraldrivingschoolModule { }
