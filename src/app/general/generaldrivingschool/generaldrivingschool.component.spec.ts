import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneraldrivingschoolComponent } from './generaldrivingschool.component';

describe('GeneraldrivingschoolComponent', () => {
  let component: GeneraldrivingschoolComponent;
  let fixture: ComponentFixture<GeneraldrivingschoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneraldrivingschoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneraldrivingschoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
