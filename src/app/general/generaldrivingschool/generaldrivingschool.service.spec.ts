import { TestBed, inject } from '@angular/core/testing';

import { GeneraldrivingschoolService } from './generaldrivingschool.service';

describe('GeneraldrivingschoolService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneraldrivingschoolService]
    });
  });

  it('should be created', inject([GeneraldrivingschoolService], (service: GeneraldrivingschoolService) => {
    expect(service).toBeTruthy();
  }));
});
