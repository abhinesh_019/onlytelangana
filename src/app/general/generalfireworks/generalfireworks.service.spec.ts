import { TestBed, inject } from '@angular/core/testing';

import { GeneralfireworksService } from './generalfireworks.service';

describe('GeneralfireworksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralfireworksService]
    });
  });

  it('should be created', inject([GeneralfireworksService], (service: GeneralfireworksService) => {
    expect(service).toBeTruthy();
  }));
});
