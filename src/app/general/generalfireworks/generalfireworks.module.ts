import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { GeneralfireworksComponent } from './generalfireworks.component';
import { GeneralfireworksService } from './generalfireworks.service';
 
const routes:Routes=[{path:'generalFireWorks',component:GeneralfireworksComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralfireworksComponent
  ],
  providers: [ GeneralfireworksService],
})
export class GeneralfireworksModule { }
