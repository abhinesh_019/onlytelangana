import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralrealestateComponent } from './generalrealestate.component';
import { GeneralrealestateService } from './generalrealestate.service';
 
 
 
const routes:Routes=[{path:'generalsrealestate',component:GeneralrealestateComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
   ],
  declarations: [
    GeneralrealestateComponent
  ],
  providers: [ GeneralrealestateService ],
})
 
export class GeneralrealestateModule { }
