import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralrealestateComponent } from './generalrealestate.component';

describe('GeneralrealestateComponent', () => {
  let component: GeneralrealestateComponent;
  let fixture: ComponentFixture<GeneralrealestateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralrealestateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralrealestateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
