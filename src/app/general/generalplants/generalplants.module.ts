import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { GeneralplantsComponent } from './generalplants.component';
import { GeneralplantsService } from './generalplants.service';
 
 
const routes:Routes=[{path:'generalsPlants',component:GeneralplantsComponent},]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
   ],
  declarations: [
    GeneralplantsComponent
  ],
  providers: [ GeneralplantsService ],
})
 
export class GeneralplantsModule { }
