import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralplantsComponent } from './generalplants.component';

describe('GeneralplantsComponent', () => {
  let component: GeneralplantsComponent;
  let fixture: ComponentFixture<GeneralplantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralplantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralplantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
