import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralHostelsComponent } from './general-hostels.component';

describe('GeneralHostelsComponent', () => {
  let component: GeneralHostelsComponent;
  let fixture: ComponentFixture<GeneralHostelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralHostelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralHostelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
