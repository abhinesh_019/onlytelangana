import { Component, OnInit } from '@angular/core';
import { HostelsService } from './hostels.service';

@Component({
  selector: 'app-general-hostels',
  templateUrl: './general-hostels.component.html',
  styleUrls: ['./general-hostels.component.css']
})
export class GeneralHostelsComponent implements OnInit {
   
  public imageAddsOne:any
  public babycaresAddsOnes:any
   
  public babycaresAddsFours:any
  public imageAddsFour:any
  public imageAddsThree:any
  public imageAddsTwo:any
  public babycaresAddsThrees:any
  public babycaresAddsTwos:any
  
  
  public babycaresAddsFivesLoc:any
  public imageAddsFiveLoc:any
  public imageAddsFourLoc:any
  public babycaresAddsFoursLoc:any
  public imageAddsThreeLoc:any
  public babycaresAddsThreeLoc:any
  public imageAddsTwoLoc:any
  public babycaresAddsTwoLoc:any
  public imageAddsOneLoc:any
  public babycaresAddsOneLoc:any
  public babycaresAddsOneLocl:any


  areaAddsOne={
    "addsOneLink":"",
    "addsOneImg":"",
  }
  areaAddsTwo={
    "addsTwoLink":"",
    "addsTwoImg":"",
  }
  areaAddsThree={
    "addsThreeLink":"",
    "addsThreeImg":"",
  }
  areaAddsFour={
    "addsFourLink":"",
    "addsFourImg":"",
  }
   
  
  areaAddsOneLoc={
    "addsOneLinkLoc":"",
    "addsOneImgLoc":"",
  }
  areaAddsTwoLoc={
    "addsTwoLinkLoc":"",
    "addsTwoImgLoc":"",
  }
  areaAddsThreeLoc={
    "addsThreeLinkLoc":"",
    "addsThreeImgLoc":"",
  }
  areaAddsFourLoc={
    "addsFourLinkLoc":"",
    "addsFourImgLoc":"",
  }
  areaAddsFiveLoc={
    "addsFiveLinkLoc":"",
    "addsFiveImgLoc":"",
  }
   


  catageries={
    "categories":"",
    "images1":""
  }
  locationsdata={
    "locations":""
  }

  areas={
    "area":""
  }

  hostel={
    "name":"",
    "catageries":"",
    "shopname":"",
    "aboutusdescriptionone":"",
    "aboutusdescriptiontwo":"",
    "informationtopublic":"",
    "mon":"",
    "tue":"",
    "wed":"",
      "thu" :"",
      "fri" :"",
      "sat":"",
      "sun"   :"",
       "totaltiming" :"",
      "hno"   :" ",
      "area" :" ",
  "landmark":"",
    "city"  :"",
    "distict" :" ",
    "state"  :" ",
    "pincode":" ",
  
   "officeno":"",
    "mobileno":" ",
    "whatsappno":" ",
    "email" :" ",
     "images1":"",
    "images2":"",
    "images3":"",
    "images4":"",
    "images5":"",
    "latitude":"",
    "longitude":"",
  
  }
  
  public locationsgets:any
  public locationName:any
  public its:any
  public locationss:any
  public areasAll:any
  public selectAreaId:any
  public selectArea:any
  public imagesName:any
  public catList:any
  public catName:any
  public it:any

  formData: FormData = new FormData(); 

  constructor(private hostels:HostelsService) { }

  ngOnInit() {
    this.getcatagories()
    
  }

// *********************catagories***************   
handleFile($event:any,images){
  this.imagesName= File = $event.target.files[0];
 
}

postcat(){
 
 this.formData.append('images1', this.imagesName);
 
 this.formData.append('categories',this.catageries.categories);
    
 

 this.hostels.catpost(this.formData).subscribe((res)=>{
     
     })

     this.catageries.categories="",
     this.catageries.images1=""
     
}

getcatagories(){
  this.hostels.catapiget().subscribe((res)=>{
this.catList=res
 
var id =this.catList[0];
       this.catagoriescal(id)

        })  }
      catagoriescal(get?){
        this.catName=get.categories
        this.its=get._id
        this.getlocations()
       }
      
      // ************************************   


  postlocations(){
    this.hostels.locationspost(this.its,this.locationsdata).subscribe((res)=>{
      this.locationsdata.locations=""
    })  }

    getlocations(){
      this.hostels.locationsget(this.its).subscribe((res)=>{
      this.locationsgets=res
       var id =this.locationsgets[0];
      this.locations(id)
      })  }
      locations(get?){
        this.locationName=get.locations
        this.it=get._id
        this.locationss=get.locations
        this.allAreas()

      this.babycaressareasAddsOneLoc()
      this.babycaressareasAddsTwoLoc()
      this.babycaressareasAddsThreeLoc()
      this.babycaressareasAddsFourLoc()
      this.babycaressareasAddsFiveLoc()
      }

      allAreas(){
        this.hostels.areaapi(this.it).subscribe((res)=>{
          this.areasAll=res
          var id =this.areasAll[0];          
          this.selectedAreas(id)
         })
       }
      
      selectedAreas(result){
        this.selectAreaId=result._id
        this.selectArea=result.area
        this.babycaressareasAddsOnea()
        this.babycaressareasAddsTwoa()
        this.babycaressareasAddsThreea()
        this.babycaressareasAddsFoura()
      }
      
      postarea(){
        this.hostels.areapost(this.it,this.areas).subscribe((res)=>{
           this.areas.area=""
        }) 
      }

 //  ************************main users details *********************

 handleFileInput($event:any,img) {
  let imageFile= File = $event.target.files[0];
  this.formData.append(img, imageFile);
 }
 
 OnSubmit(){
  
  this.formData.append('name',this.hostel.name);
  this.formData.append('catageries',this.catName);
  this.formData.append('shopname',this.hostel.shopname);
  this.formData.append('email',this.hostel.email);
  this.formData.append('aboutusdescriptionone',this.hostel.aboutusdescriptionone);
  this.formData.append('aboutusdescriptiontwo',this.hostel.aboutusdescriptiontwo);
  this.formData.append('informationtopublic',this.hostel.informationtopublic);
  this.formData.append('mon',this.hostel.mon);
  this.formData.append('tue',this.hostel.tue);
  this.formData.append('wed',this.hostel.tue);
  this.formData.append('thu',this.hostel.tue);
  this.formData.append('fri',this.hostel.fri);
  this.formData.append('sat',this.hostel.sat);
  this.formData.append('sun',this.hostel.sun);
  this.formData.append('totaltiming',this.hostel.totaltiming);
   this.formData.append('hno',this.hostel.hno);
  this.formData.append('area',this.hostel.area);
  this.formData.append('landmark',this.hostel.landmark);
  this.formData.append('city',this.hostel.city);
  this.formData.append('distict',this.locationName);
  this.formData.append('state',this.hostel.state);
  this.formData.append('pincode',this.hostel.pincode);
  this.formData.append('officeno',this.hostel.officeno);
  this.formData.append('mobileno',this.hostel.mobileno);
  this.formData.append('whatsappno',this.hostel.whatsappno);
  this.formData.append('mainArea',this.selectArea);
  this.formData.append('latitude',this.hostel.latitude);
  this.formData.append('longitude',this.hostel.longitude);
this.hostel.name=""
this.hostel.shopname=""
this.hostel.email="",
this.hostel.aboutusdescriptionone="",
this.hostel.aboutusdescriptiontwo="",
this.hostel.informationtopublic="",
this.hostel.mon="",
this.hostel.tue="",
this.hostel.wed="",
this.hostel.thu="",
this.hostel.fri="",
this.hostel.sat="",
this.hostel.sun="",
this.hostel.totaltiming="",
this.hostel.hno="",
this.hostel.area="",
this.hostel.landmark="",
this.hostel.city="",
this.hostel.distict="",
this.hostel.state="",
this.hostel.pincode="",
this.hostel.officeno="",
this.hostel.mobileno="",
this.hostel.whatsappno="",
this.hostel.email="",
this.hostel.images1="",
this.hostel.images2="",
this.hostel.images3="",
this.hostel.images4="",
this.hostel.images5="",
  this.hostels.postFiles(this.selectAreaId,this.formData).subscribe(
    data =>{
      
      
    }
  );
 }

       
// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdates($event:any,images){
  this.imageAddsOne= File = $event.target.files[0];
 }
 

postAddsOne(){
this.formData.append('addsOneImg',this.imageAddsOne);
this.formData.append('addsOneLink',this.areaAddsOne.addsOneLink); 
 
 this.hostels.postAddsOne(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsOne.addsOneLink="",
     this.areaAddsOne.addsOneImg=""
}
babycaressareasAddsOnea(){
  this.hostels.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
   })
}
babycaressareasAddsOneaDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsOneaDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwo($event:any,images){
  this.imageAddsTwo= File = $event.target.files[0];
 }
 

postAddsTwo(){
this.formData.append('addsTwoImg',this.imageAddsTwo);
this.formData.append('addsTwoLink',this.areaAddsTwo.addsTwoLink); 
 
 this.hostels.postAddsTwo(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwo.addsTwoLink="",
     this.areaAddsTwo.addsTwoImg=""
}
babycaressareasAddsTwoa(){
  this.hostels.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
   })
}
babycaressareasAddsTwoaDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsTwoaDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three ***********************************************************************

handleFileInputUpdatesThree($event:any,images){
  this.imageAddsThree= File = $event.target.files[0];
 }
 

postAddsThree(){
this.formData.append('addsThreeImg',this.imageAddsThree);
this.formData.append('addsThreeLink',this.areaAddsThree.addsThreeLink); 
 
 this.hostels.postAddsThree(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsThree.addsThreeLink="",
     this.areaAddsThree.addsThreeImg=""
}
babycaressareasAddsThreea(){
  this.hostels.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
   })
}
babycaressareasAddsThreeaDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsThreeaDelete(areaIds).subscribe((res)=>{  
  })
}


// ********************************************************* Adds Areas Four ***********************************************************************

handleFileInputUpdatesFour($event:any,images){
  this.imageAddsFour= File = $event.target.files[0];
 }
 

postAddsFour(){
this.formData.append('addsFourImg',this.imageAddsFour);
this.formData.append('addsFourLink',this.areaAddsFour.addsFourLink); 
 
 this.hostels.postAddsFour(this.selectAreaId,this.formData).subscribe((res)=>{
     })
     this.areaAddsFour.addsFourLink="",
     this.areaAddsFour.addsFourImg=""
}
babycaressareasAddsFoura(){
  this.hostels.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res
   })
}
babycaressareasAddsFouraDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsFouraDelete(areaIds).subscribe((res)=>{
  })
}

 

 


// ********************************************************* Adds Areas One ***********************************************************************

handleFileInputUpdatesOneLoc($event:any,images){
  this.imageAddsOneLoc= File = $event.target.files[0];
 }
 

postAddsOneLoc(){
this.formData.append('addsOneImgLoc',this.imageAddsOneLoc);
this.formData.append('addsOneLinkLoc',this.areaAddsOneLoc.addsOneLinkLoc); 
 
 this.hostels.postAddsOnepl(this.it,this.formData).subscribe((res)=>{
    
})
     this.areaAddsOneLoc.addsOneImgLoc="",
     this.areaAddsOneLoc.addsOneLinkLoc=""
}
babycaressareasAddsOneLoc(){
  this.hostels.babycaresAddsOnel(this.it).subscribe((res)=>{
     this.babycaresAddsOneLocl=res
   })
}
babycaressareasAddsOneLocDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsOneLocDelete(areaIds).subscribe((res)=>{
    
  })
}

// ********************************************************* Adds Areas Two ***********************************************************************

handleFileInputUpdatesTwoLoc($event:any,images){
  this.imageAddsTwoLoc= File = $event.target.files[0];
 }
 

postAddsTwoLoc(){
this.formData.append('addsTwoImgLoc',this.imageAddsTwoLoc);
this.formData.append('addsTwoLinkLoc',this.areaAddsTwoLoc.addsTwoLinkLoc); 
 
 this.hostels.postAddsTwopl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsTwoLoc.addsTwoLinkLoc="",
     this.areaAddsTwoLoc.addsTwoImgLoc=""
}
babycaressareasAddsTwoLoc(){
  this.hostels.babycaresAddsTwol(this.it).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
   })
}
babycaressareasAddsTwoLocDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsTwoLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Three Loc***********************************************************************

handleFileInputUpdatesThreeLoc($event:any,images){
  this.imageAddsThreeLoc= File = $event.target.files[0];
 }
 

postAddsThreeLoc(){
this.formData.append('addsThreeImgLoc',this.imageAddsThreeLoc);
this.formData.append('addsThreeLinkLoc',this.areaAddsThreeLoc.addsThreeLinkLoc); 
 
 this.hostels.postAddsThreepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsThreeLoc.addsThreeImgLoc="",
     this.areaAddsThreeLoc.addsThreeLinkLoc=""
}
babycaressareasAddsThreeLoc(){
  this.hostels.babycaresAddsThreel(this.it).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
   })
}
babycaressareasAddsThreeLocDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsThreeLocDelete(areaIds).subscribe((res)=>{
  
  })
}


// ********************************************************* Adds Areas Four Loc***********************************************************************

handleFileInputUpdatesFourLoc($event:any,images){
  this.imageAddsFourLoc= File = $event.target.files[0];
 }
 

postAddsFourLoc(){
this.formData.append('addsFourImgLoc',this.imageAddsFourLoc);
this.formData.append('addsFourLinkLoc',this.areaAddsFourLoc.addsFourLinkLoc); 
 
 this.hostels.postAddsFourpl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFourLoc.addsFourImgLoc="",
     this.areaAddsFourLoc.addsFourLinkLoc=""
}
babycaressareasAddsFourLoc(){
  this.hostels.babycaresAddsFourl(this.it).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
   })
}
babycaressareasAddsFourLocDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsFourLocDelete(areaIds).subscribe((res)=>{
    
  })
}


// ********************************************************* Adds Areas Five Loc***********************************************************************

handleFileInputUpdatesFiveLoc($event:any,images){
  this.imageAddsFiveLoc= File = $event.target.files[0];
 }
 

postAddsFiveLoc(){
this.formData.append('addsFiveImgLoc',this.imageAddsFiveLoc);
this.formData.append('addsFiveLinkLoc',this.areaAddsFiveLoc.addsFiveLinkLoc); 
 
 this.hostels.postAddsFivepl(this.it,this.formData).subscribe((res)=>{
     })
     this.areaAddsFiveLoc.addsFiveImgLoc="",
     this.areaAddsFiveLoc.addsFiveLinkLoc=""
}
babycaressareasAddsFiveLoc(){
  this.hostels.babycaresAddsFivel(this.it).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
   })
}
babycaressareasAddsFiveLocDelete(identities){
  let areaIds=identities._id
  this.hostels.babycaresAddsFiveLocDelete(areaIds).subscribe((res)=>{
    
  })
}

 
}

