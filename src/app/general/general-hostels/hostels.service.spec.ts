import { TestBed, inject } from '@angular/core/testing';

import { HostelsService } from './hostels.service';

describe('HostelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HostelsService]
    });
  });

  it('should be created', inject([HostelsService], (service: HostelsService) => {
    expect(service).toBeTruthy();
  }));
});
