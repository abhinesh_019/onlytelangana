import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
 import { HostelsService } from './hostels.service';
import { GeneralHostelsComponent } from './general-hostels.component';


const routes:Routes=[{path:'generalshostels',component:GeneralHostelsComponent}]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
    

  ],
  declarations: [
    GeneralHostelsComponent
  ],
  providers: [ HostelsService],
   
})
export class HostelsModule { }
