import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
 import { environment } from '../../../environments/environment.prod';

@Injectable()


export class HostelsService {

  constructor(private http: HttpClient) { }
  catpost(data) {
    return   this.http.post(environment.apiUrl +"/hostelsCategoriesp",data);
 }

 catapiget(){
       return this.http.get(environment.apiUrl +"/hostelsCategorie/");
                   }
 
  locationspost(id,data) {
    return   this.http.post(environment.apiUrl +"/hostelsLocations/"+id,data);

 
    }

    locationsget(id){
       return this.http.get(environment.apiUrl +"/hostelsLocations/"+id);
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/hostelsAreas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/hostelsAreas/"+id,data);
                   }

                   postFiles(id,data) {
                    return   this.http.post(environment.apiUrl +"/boysHostelsp/"+id,data);
               }
 
               babycaresAddsOnea(id){

                return this.http.get(environment.apiUrl +"/hostelsAddsOneGeta/"+id);
              }
              babycaresAddsTwoa(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsTwoGeta/"+id);
              }
              babycaresAddsThreea(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsThreeGeta/"+id);
              }
              babycaresAddsFoura(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsFourGeta/"+id);
              }
               
              babycaresAddsOnel(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsOneLocGet/"+id);
              }
              babycaresAddsTwol(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsTwoLocGet/"+id);
              }
              babycaresAddsThreel(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsThreeGetLoc/"+id);
              }
              babycaresAddsFourl(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsFourLocGet/"+id);
              }
            
              babycaresAddsFivel(id){
                
                return this.http.get(environment.apiUrl +"/hostelsAddsFiveLocGet/"+id);
              }
               
              postAddsOne(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsOnePtsa/"+id,data);
               }
               postAddsTwo(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsTwoPtsa/"+id,data);
               }
               postAddsThree(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsThreePtsa/"+id,data);
               }
               postAddsFour(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsFourPtsa/"+id,data);
               }
               
            
            
              postAddsOnepl(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsOneLocPts/"+id,data);
               }
               postAddsTwopl(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsTwoLocPts/"+id,data);
               }
               postAddsThreepl(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsThreePtsLoc/"+id,data);
               }
               postAddsFourpl(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsFourLocPts/"+id,data);
               }
               postAddsFivepl(id,data){
                return   this.http.post(environment.apiUrl +"/hostelsAddsFiveLocPts/"+id,data);
               }
               
            
            
               babycaresAddsOneaDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsOneDeletea/"+id);
              }
             
              babycaresAddsTwoaDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsTwoDeletea/"+id);
              }
              babycaresAddsThreeaDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsThreeDeletea/"+id);
              }
              babycaresAddsFouraDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsFourDeletea/"+id);
              }
            
            
               babycaresAddsOneLocDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsOneLocDelete/"+id);
              }
            
              babycaresAddsTwoLocDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsTwoLocDelete/"+id);
              }
              babycaresAddsThreeLocDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsThreeDeleteLoc/"+id);
              }
              babycaresAddsFourLocDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsFourLocDelete/"+id);
              }
              babycaresAddsFiveLocDelete(id){
            
                return this.http.delete(environment.apiUrl +"/hostelsAddsFiveLocDelete/"+id);
              }
               
             }
            