import { TestBed, inject } from '@angular/core/testing';

import { GeneralrestaurantsService } from './generalrestaurants.service';

describe('GeneralrestaurantsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeneralrestaurantsService]
    });
  });

  it('should be created', inject([GeneralrestaurantsService], (service: GeneralrestaurantsService) => {
    expect(service).toBeTruthy();
  }));
});
