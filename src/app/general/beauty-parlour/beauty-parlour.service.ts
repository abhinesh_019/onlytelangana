import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
 
@Injectable()


export class BeautyParlourService {
  constructor(private http: HttpClient) { }
  catpost(data) {
    return   this.http.post(environment.apiUrl +"/parlourCategoriesp",data);
 }

 catapiget(){
       return this.http.get(environment.apiUrl +"/parlourCategories/");
                   }
 
  locationspost(id,data) {
    return   this.http.post(environment.apiUrl +"/parlourlocationsp/"+id,data);
 }

    locationsget(id){
       return this.http.get(environment.apiUrl +"/parlourlocations/"+id);
                   }
                   
                   areaapi(id){
    
                    return this.http.get(environment.apiUrl +"/beautyParlourAreas/"+id);
                  }

                  areapost(id,data){
                    return   this.http.post(environment.apiUrl +"/beautyParlourAreas/"+id,data);
                   }
                   areapostusers(id,data){
                    return   this.http.post(environment.apiUrl +"/bpClients/"+id,data);

                   }


  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/beutyParlourAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/beutyParlourAddsFiveLocGet/"+id);
  }
   
  postAddsOne(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsOnePtsa/"+id,data);
   }
   postAddsTwo(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsTwoPtsa/"+id,data);
   }
   postAddsThree(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsThreePtsa/"+id,data);
   }
   postAddsFour(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsFourPtsa/"+id,data);
   }
   


  postAddsOnepl(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsOneLocPts/"+id,data);
   }
   postAddsTwopl(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsTwoLocPts/"+id,data);
   }
   postAddsThreepl(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsThreePtsLoc/"+id,data);
   }
   postAddsFourpl(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsFourLocPts/"+id,data);
   }
   postAddsFivepl(id,data){
    return   this.http.post(environment.apiUrl +"/beutyParlourAddsFiveLocPts/"+id,data);
   }
   


   babycaresAddsOneaDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsOneDeletea/"+id);
  }
 
  babycaresAddsTwoaDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsTwoDeletea/"+id);
  }
  babycaresAddsThreeaDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsThreeDeletea/"+id);
  }
  babycaresAddsFouraDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsFourDeletea/"+id);
  }


   babycaresAddsOneLocDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsOneLocDelete/"+id);
  }

  babycaresAddsTwoLocDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsTwoLocDelete/"+id);
  }
  babycaresAddsThreeLocDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsThreeDeleteLoc/"+id);
  }
  babycaresAddsFourLocDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsFourLocDelete/"+id);
  }
  babycaresAddsFiveLocDelete(id){

    return this.http.delete(environment.apiUrl +"/beutyParlourAddsFiveLocDelete/"+id);
  }
   
 }
