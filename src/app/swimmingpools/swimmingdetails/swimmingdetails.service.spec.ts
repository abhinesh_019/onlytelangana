import { TestBed, inject } from '@angular/core/testing';

import { SwimmingdetailsService } from './swimmingdetails.service';

describe('SwimmingdetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SwimmingdetailsService]
    });
  });

  it('should be created', inject([SwimmingdetailsService], (service: SwimmingdetailsService) => {
    expect(service).toBeTruthy();
  }));
});
