import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimmingdetailsComponent } from './swimmingdetails.component';

describe('SwimmingdetailsComponent', () => {
  let component: SwimmingdetailsComponent;
  let fixture: ComponentFixture<SwimmingdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwimmingdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimmingdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
