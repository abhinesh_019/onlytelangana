import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';
    

@Injectable()
export class FightusersService {

  constructor(private http: HttpClient) { }
    
  individualcat(id){
    return this.http.get(environment.apiUrl +"/fightsCategorie/"+id);

  }
   locationsget(id){
    return this.http.get(environment.apiUrl +"/fightsLocations/"+id);
                }
                areaapi(id){
     
                 return this.http.get(environment.apiUrl +"/fightsAreas/"+id);
               }   

               catageriesAllCounts(){
     
                return this.http.get(environment.apiUrl +"/fightsClientscatAll");
              }       
    
              locationsAll(){
     
                return this.http.get(environment.apiUrl +"/fightsUsersLocations");
              } 
              
              Client(id,data){
    
                return this.http.get(environment.apiUrl +"/fightsClientsget/"+id,{params:data});
              } 
              clientsCoounts(id){
    
                return this.http.get(environment.apiUrl +"/fightsClientsgetCounts/"+id);
              } 
              clientsSubAreasCnt(id){
    
                return this.http.get(environment.apiUrl +"/fightsClientsgetAreaCounts/"+id);
              } 
                                 
              babycaresAddsOnea(id){

                return this.http.get(environment.apiUrl +"/fightsAddsOneGeta/"+id);
              }
              babycaresAddsTwoa(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsTwoGeta/"+id);
              }
              babycaresAddsThreea(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsThreeGeta/"+id);
              }
              babycaresAddsFoura(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsFourGeta/"+id);
              }
               
              babycaresAddsOnel(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsOneLocGet/"+id);
              }
              babycaresAddsTwol(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsTwoLocGet/"+id);
              }
              babycaresAddsThreel(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsThreeGetLoc/"+id);
              }
              babycaresAddsFourl(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsFourLocGet/"+id);
              }
            
              babycaresAddsFivel(id){
                
                return this.http.get(environment.apiUrl +"/fightsAddsFiveLocGet/"+id);
              }
               

}
