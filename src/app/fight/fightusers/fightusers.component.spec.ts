import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FightusersComponent } from './fightusers.component';

describe('FightusersComponent', () => {
  let component: FightusersComponent;
  let fixture: ComponentFixture<FightusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FightusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FightusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
