import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsdetailsService } from './restaurantsdetails.service';

@Component({
  selector: 'app-restaurantsdetails',
  templateUrl: './restaurantsdetails.component.html',
  styleUrls: ['./restaurantsdetails.component.css']
})
export class RestaurantsdetailsComponent implements OnInit {
public ids:any
public resClients:any
 
public servicesOnes:any
public selectDishName:any
public selectDishId:any
public dishTypeGets:any
public selectFoodItemsId:any
public selectFoodItemsName:any
public selectItemsTwoGets:any
public masterName:any
public masterIds:any
public mainSelectItemGets:any


public replyid:any
public replies:any
public animalsTypesCnts:any
public clientUpdatesCnts:any

public share:boolean=false
public comments:boolean=false
public viewcomments:boolean=false
public replyfrom:boolean=false 
public openshareid:any
public clientUpdates:any
public clientUpdatesCount:any
public commentspostsId:any
public getUsersComments:any
public getUsersCommentsCounts:any
public viewcommentsid:any
// public masterName:any
// public masterName:any
// public masterName:any
// public masterName:any
// public masterName:any
// public masterName:any
// public masterName:any


postComments={
  "descriptions":""
}
commentspost={
  "name":"",
  "email":"",
  "contactNo":"",
  "leaveComment":"",
    }


constructor(private res:RestaurantsdetailsService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
this.servicesOne()
this.clientsUpdates()
this.clientsUpdatesCount()
   }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}

usersDetails(){ 
this.res.usersDetails(this.ids).subscribe((res)=>{
this.resClients=res 
})
}
servicesOne(){ 
this.res.servicesOne(this.ids).subscribe((res)=>{
this.servicesOnes=res
var vsd=this.servicesOnes[0]
this.onSelectDish(vsd)
})
}
onSelectDish(up){
  this.selectDishId=up._id
  this.selectDishName=up.foodType1
this.dishTypeGet()
}
 dishTypeGet(){ 
this.res.dishTypeGet(this.selectDishId).subscribe((res)=>{
this.dishTypeGets=res
var disy=this.dishTypeGets[0]
this.onSelectItemTypes(disy)
})
}
onSelectItemTypes(up){
  this.selectFoodItemsId=up._id
  this.selectFoodItemsName=up.foodDish 
this.selectItemsTwoGet()
}
selectItemsTwoGet(){ 
  this.res.selectItemsTwoGet(this.selectFoodItemsId).subscribe((res)=>{
  this.selectItemsTwoGets=res
    })
  }
  onClickMasteritems(up){
    this.masterIds=up._id
    this.masterName=up.foodDish 
    this.mainSelectItemGet()
  }

  mainSelectItemGet(){ 
    this.res.mainSelectItemGet(this.masterIds).subscribe((res)=>{
    this.mainSelectItemGets=res
      })
    }


// ******************************************updates present ***************************************
shareClick(up){
  this.share=!this.share
  this.comments=false
  this.viewcomments=false
  this.replyfrom=false
  this.openshareid=up._id
}
commentsClick(up){
  this.comments=!this.comments
  this.share=false
  this.viewcomments=false
  this.commentspostsId=up._id
   this.commentsGettoClints()
   this.commentsGettoClintsCounts()
 }

viewCommentsClick(up){
  this.viewcomments=!this.viewcomments
  this.share=false
  this.viewcommentsid=up._id
}
replyFromm(comms){
  this.replyfrom=!this.replyfrom
  this.share=false
  this.replyid=comms._id
  this.replyFromCommentesGet()
  console.log(this.replyid)
  console.log(this.replyfrom)
}

 clientsUpdates(){   
  this.res.clientsUpdatesGet(this.ids).subscribe((res)=>{
    this.clientUpdates=res
   })
 }
 
 clientsUpdatesCount(){   
  this.res.clientsUpdatesGetCounts(this.ids).subscribe((res)=>{
    this.clientUpdatesCount=res
     })
 }

 commentsPoststoClints(){   
  this.res.commentsPoststoClints(this.commentspostsId,this.postComments).subscribe((res)=>{
     })
     this.postComments.descriptions=""
 }
 commentsGettoClints(){   
  this.res.commentsGettoClints(this.commentspostsId).subscribe((res)=>{
    this.getUsersComments=res
   })
 }

 commentsGettoClintsCounts(){   
  this.res.commentsGettoClintsCounts(this.commentspostsId).subscribe((res)=>{
    this.getUsersCommentsCounts=res
    })
 }

 replyFromCommentesGet(){   
  this.res.replyFromCommentesGet(this.replyid).subscribe((res)=>{
    this.replies=res 
    })
 }


  // ***********0verallcomments************
  overallcomment(){
        
    this.res.overallscommentspost(this.ids,this.commentspost).subscribe((res)=>{
     
     this.commentspost.name="",
     this.commentspost.contactNo="",
     this.commentspost.email="",
     this.commentspost.leaveComment=""
    })
  }
}
