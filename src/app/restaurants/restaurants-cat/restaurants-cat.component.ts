import { Component, OnInit } from '@angular/core';
import { RestaurantsCatService } from './restaurants-cat.service';

@Component({
  selector: 'app-restaurants-cat',
  templateUrl: './restaurants-cat.component.html',
  styleUrls: ['./restaurants-cat.component.css']
})
export class RestaurantsCatComponent implements OnInit {

  public catList:any
  
  constructor(private resCat:RestaurantsCatService) { }
   

  ngOnInit() {
    this.getcatagories()
    
  }
  
  getcatagories(){
    this.resCat.catapigetdetails().subscribe((res)=>{
  this.catList=res 
 })  }
         
     
}
