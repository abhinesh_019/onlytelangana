import { TestBed, inject } from '@angular/core/testing';

import { RestaurantsCatService } from './restaurants-cat.service';

describe('RestaurantsCatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestaurantsCatService]
    });
  });

  it('should be created', inject([RestaurantsCatService], (service: RestaurantsCatService) => {
    expect(service).toBeTruthy();
  }));
});
