import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantsCatComponent } from './restaurants-cat.component';

describe('RestaurantsCatComponent', () => {
  let component: RestaurantsCatComponent;
  let fixture: ComponentFixture<RestaurantsCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantsCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantsCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
