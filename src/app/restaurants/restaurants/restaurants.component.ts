import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RestaurantsService } from './restaurants.service';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit {

  public clients:any
  public ids:any
  public mainuser:any
  public catdeatils:any
  public locationsgets:any
  public areasAll:any
  public locationName:any
  public idss:any
  public selectAreaId:any
  public selectArea:any
  public subsArea:any
  public areasCount:any
  public catAllCont:any
  public mensC:any
  public womensCo:any
  public searchString:any
  public banquietCount:any
  public funHall:any
  public realestateUsers:any
public locationsCountAlls:any
public resCatss:any
public clientsAreaCountsInd:any
public clientsAreaCounts:any

 
 // **********************************************************
 
 public imageAddsOne:any
 public babycaresAddsOnes:any
  
 public babycaresAddsFours:any
 public imageAddsFour:any
 public imageAddsThree:any
 public imageAddsTwo:any
 public babycaresAddsThrees:any
 public babycaresAddsTwos:any
 
 
 public babycaresAddsFivesLoc:any
 public imageAddsFiveLoc:any
 public imageAddsFourLoc:any
 public babycaresAddsFoursLoc:any
 public imageAddsThreeLoc:any
 public babycaresAddsThreeLoc:any
 public imageAddsTwoLoc:any
 public babycaresAddsTwoLoc:any
 public imageAddsOneLoc:any
 public babycaresAddsOneLoc:any
  constructor(private res:RestaurantsService,private route:ActivatedRoute) { }
  

  ngOnInit() {
    this.individualdata()
    this.usersDetails()
    this.getlocations()
    this.resLoc()  
    this.resCat() 
  }
  individualdata(){
    this.ids=this.route.snapshot.params['_id'];

}


usersDetails(){ 
  this.res.individualcat(this.ids).subscribe((res)=>{
    this.catdeatils=res
      })
}

      // *********************** locations ****************
      
         getlocations(){
           this.res.locationsget(this.ids).subscribe((res)=>{
           this.locationsgets=res
            var id =this.locationsgets[0];
            this.locations(id) 
           })  }


           locations(get?){
             this.locationName=get.locations
             this.idss=get._id
              this.allAreas()
              this.babycaressareasAddsOneLoc()
              this.babycaressareasAddsTwoLoc()
              this.babycaressareasAddsThreeLoc()
              this.babycaressareasAddsFourLoc()
              this.babycaressareasAddsFiveLoc()
           }

 // ************************************************************************************************************************************
 resLoc(){ 
  this.res.resLocs().subscribe((res)=>{
    this.locationsCountAlls=res
      })
}

 resCat(){ 
  this.res.resCats().subscribe((res)=>{
    this.resCatss=res
      })
}
//  **********************************area *************************************

allAreas(){
        
this.res.areaapi(this.idss).subscribe((res)=>{
 this.areasAll=res
 var id =this.areasAll[0];
 this.selectedAreas(id)
})
}

selectedAreas(result){
this.selectAreaId=result._id
this.selectArea=result.area
this.realestateUsersall()
this.resArea()
this.resSubArea()
this.babycaressareasAddsOnea()
this.babycaressareasAddsTwoa()
this.babycaressareasAddsThreea()
this.babycaressareasAddsFoura()
}

// ************************************************************************

realestateUsersall(){
  var data:any =  {}
  if(this.searchString){
    data.search=this.searchString
    }
  this.res.realestateUsersall(this.selectAreaId,data).subscribe((res)=>{
    this.realestateUsers=res 
    })
 }

searchFilter(){
  this.realestateUsersall()
}

resArea(){ 
  this.res.resAreas(this.selectAreaId).subscribe((res)=>{
    this.clientsAreaCountsInd=res
      })
}
resSubArea(){ 
  this.res.resSubreas(this.selectAreaId).subscribe((res)=>{
    this.clientsAreaCounts=res
      })
}

// ****************************************************

babycaressareasAddsOnea(){
  this.res.babycaresAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsOnes=res
 
     
   })
}
babycaressareasAddsTwoa(){
  this.res.babycaresAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsTwos=res
  
   })
}
babycaressareasAddsThreea(){
  this.res.babycaresAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsThrees=res
  
   })
}

babycaressareasAddsFoura(){
  this.res.babycaresAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.babycaresAddsFours=res

   })
}

babycaressareasAddsOneLoc(){
  this.res.babycaresAddsOnel(this.idss).subscribe((res)=>{
     this.babycaresAddsOneLoc=res 
   })
}
babycaressareasAddsTwoLoc(){
  this.res.babycaresAddsTwol(this.idss).subscribe((res)=>{
     this.babycaresAddsTwoLoc=res
    
   })
}
babycaressareasAddsThreeLoc(){
  this.res.babycaresAddsThreel(this.idss).subscribe((res)=>{
     this.babycaresAddsThreeLoc=res
     
   })
}
babycaressareasAddsFourLoc(){
  this.res.babycaresAddsFourl(this.idss).subscribe((res)=>{
     this.babycaresAddsFoursLoc=res
     
   })
}
babycaressareasAddsFiveLoc(){
  this.res.babycaresAddsFivel(this.idss).subscribe((res)=>{
     this.babycaresAddsFivesLoc=res
    
   })
}

}
