import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class RestaurantsService {

  constructor(private http: HttpClient) { }
  individualcat(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsCatageriesInd/"+id);
  }  

  locationsget(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsLocations/"+id);
  } 
  areaapi(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAreas/"+id);
  } 

  realestateUsersall(id,data){
    
    return this.http.get(environment.apiUrl +"/restaurantsClientsget/"+id,{params:data});
  }
  
  resLocs(){
    
    return this.http.get(environment.apiUrl +"/restaurantsUsersLocations");
  }

  resCats(){
    
    return this.http.get(environment.apiUrl +"/restaurantsUsersCatageries");
  }
  resAreas(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsClientsMainAreas/"+id);
  }
  resSubreas(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsClientsAreas/"+id);
  }
  
  babycaresAddsOnea(id){

    return this.http.get(environment.apiUrl +"/restaurantsAddsOneGeta/"+id);
  }
  babycaresAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsTwoGeta/"+id);
  }
  babycaresAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsThreeGeta/"+id);
  }
  babycaresAddsFoura(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsFourGeta/"+id);
  }
   
  babycaresAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsOneLocGet/"+id);
  }
  babycaresAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsTwoLocGet/"+id);
  }
  babycaresAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsThreeGetLoc/"+id);
  }
  babycaresAddsFourl(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsFourLocGet/"+id);
  }

  babycaresAddsFivel(id){
    
    return this.http.get(environment.apiUrl +"/restaurantsAddsFiveLocGet/"+id);
  }
   
}