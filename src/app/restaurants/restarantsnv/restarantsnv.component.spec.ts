import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestarantsnvComponent } from './restarantsnv.component';

describe('RestarantsnvComponent', () => {
  let component: RestarantsnvComponent;
  let fixture: ComponentFixture<RestarantsnvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestarantsnvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestarantsnvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
