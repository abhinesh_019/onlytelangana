import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AnimalsComponent } from './animals.component';
import { AnimalsdetailsModule } from '../animalsdetails/animalsdetails.module';
import { AnimalsService } from '../animals.service';
 

const routes: Routes = [
  {
    path: '', component: AnimalsComponent, children:
      [
      { path: 'animalsdetails/:_id/:name', loadChildren: 'app/animals/animalsdetails/animalsdetails.module#AnimalsdetailsModule'},
      ]
  }]



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
     SharedModule,
     AnimalsdetailsModule  
    ],
  declarations: [
    AnimalsComponent,
  ],
  providers:[AnimalsService]
})
export class AnimalsModule { }
