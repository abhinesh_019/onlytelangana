import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AnimalsService } from '../animals.service';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent implements OnInit {

  public area:any
  public searchString:String;
  searchTerm:String;
  public locationsget:any
  public locations:any
  public locationName:any
public its:any
public locationget:any
public areasAll:any
public selectAreaId:any
public selectArea:any
public animalsuser:any
public animalsusercount:any
public animalsusercounttot:any
public popularareas:any
public animalsusercountLocations:any
public animalsAddsOnes:any
 public animalsAddsTwoa:any
 public animalsAddsThreea:any
 public animalsAddsOnesl:any
 public animalsAddsTwol:any
 public animalsAddsThreel:any
 public animalsAddsFourl:any
public animalsAddsFivesLoc:any
public animalsAddsFours:any

  constructor(private router:Router,private route:ActivatedRoute,private animals:AnimalsService) { }

  ngOnInit() {
    this.getlocations()
    this.animalsuserscounttot()
    this.animalsuserscountLocations()
  }

    
  // locations*****************
getlocations(){
  this.animals.locationapi().subscribe((res)=>{
    this.locationsget=res;
 
    var id =this.locationsget[0];
     this.location(id)
     
  })}


  location(get?){
    this.locationName=get.locations
    this.its=get._id
     this.allAreas()
     this.animalssareasAddsOneLoc()
     this.animalssareasAddsTwoLoc()
     this.animalssareasAddsThreeLoc()
     this.animalssareasAddsFourLoc()
     this.animalssareasAddsFiveLoc()
   }

   allAreas(){   
    this.animals.areaapi(this.its).subscribe((res)=>{
      this.areasAll=res
      var id =this.areasAll[0];
       this.selectedAre(id)
     })
   }

selectedAre(result){
this.selectAreaId=result._id
this.selectArea=result.area
 this.animalsuserscount()   
 this.animalsusers()
this.animalssareascount()  
this.animalssareasAddsOnea()
this.animalssareasAddsTwoa()
this.animalssareasAddsThreea()
this.animalssareasAddsFoura()
}

animalsusers(){
  var data:any =  {}
  if(this.searchString){
    data.search=this.searchString
   }
  this.animals.animalsusers(this.selectAreaId,data).subscribe((res)=>{
    this.animalsuser=res
   })
 }

searchFilter(){
this.animalsusers()
}
// ***************************************************************
animalsuserscount(){
  this.animals.animalsuserscounts(this.selectAreaId).subscribe((res)=>{
    this.animalsusercount=res 
  })
 }
 animalsuserscountLocations(){
  this.animals.animalsusersForLocations().subscribe((res)=>{
    this.animalsusercountLocations=res  
  })
 }
// ***************************************************************
 animalsuserscounttot(){
  this.animals.animalsuserscountstot().subscribe((res)=>{
    this.animalsusercounttot=res 
  })
 }

 animalssareascount(){
   this.animals.animalsuserscountsarea(this.selectAreaId).subscribe((res)=>{
      this.popularareas=res
   })
 }
 animalssareasAddsOnea(){
  this.animals.animalsAddsOnea(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsOnes=res
   
  })
}
animalssareasAddsTwoa(){
  this.animals.animalsAddsTwoa(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsTwoa=res
   
  })
}
 animalssareasAddsThreea(){
  this.animals.animalsAddsThreea(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsThreea=res
  
  })
}
animalssareasAddsFoura(){
  this.animals.animalsAddsFoura(this.selectAreaId).subscribe((res)=>{
     this.animalsAddsFours=res
   })
}

animalssareasAddsOneLoc(){
  this.animals.animalsAddsOnel(this.its).subscribe((res)=>{
     this.animalsAddsOnesl=res
 
  })
}
animalssareasAddsTwoLoc(){
  this.animals.animalsAddsTwol(this.its).subscribe((res)=>{
     this.animalsAddsTwol=res
     
  })
}
 animalssareasAddsThreeLoc(){
  this.animals.animalsAddsThreel(this.its).subscribe((res)=>{
     this.animalsAddsThreel=res
  })
}
animalssareasAddsFourLoc(){
  this.animals.animalsAddsFour(this.its).subscribe((res)=>{
     this.animalsAddsFourl=res
  })
}
animalssareasAddsFiveLoc(){
  this.animals.animalsAddsFivel(this.its).subscribe((res)=>{
     this.animalsAddsFivesLoc=res
   })
}
// ****************************************************
}

