import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
 import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment.prod';
  

@Injectable()
export class AnimalsService {

  constructor(private http: HttpClient) { }
  locationapi( ){
    return   this.http.get(environment.apiUrl +"/animalsLocations");
   }
   areaapi(id){
    return   this.http.get(environment.apiUrl +"/animalsAreas/"+id);
   }

  animalsusers(id,data){
    
    return this.http.get(environment.apiUrl +"/animalsClients/"+id,{params:data});
  }
  animalsusersForLocations(){
    
    return this.http.get(environment.apiUrl +"/animalsCountsLocations");
  }
  animalsusersonly(id){
    
    return this.http.get(environment.apiUrl +"/animalsClientInd/"+id);
  }
  
  animalsuserscounts(id){
    return this.http.get(environment.apiUrl +"/animalsClientsCountsByArea/"+id);
  }
  animalsuserscountstot(){
    
    return this.http.get(environment.apiUrl +"/animalsClientsAll");
  }
  animalsuserscountsarea(id){
    
    return this.http.get(environment.apiUrl +"/animalsCountsAreas/"+id);
  }
  animalsTypes(id){
    
    return this.http.get(environment.apiUrl +"/animalsTypes/"+id);
  }
  animalsListsTypeCnt(id){
    
    return this.http.get(environment.apiUrl +"/animalsTypesdesCont/"+id);
  }
  
  animalsListDetail(id){
    
    return this.http.get(environment.apiUrl +"/animalsTypesdes/"+id);
  }
  animalsListDetailIndv(id){
    
    return this.http.get(environment.apiUrl +"/animalsTypedesInd/"+id);
  }
  clientsUpdatesGet(id){
    
    return this.http.get(environment.apiUrl +"/animalsupdatesget/"+id);
  }
  clientsUpdatesCnts(id){
    
    return this.http.get(environment.apiUrl +"/animalsupdatesgetCounts/"+id);
  }
  clientsUpdatesGetCounts(id){
    
    return this.http.get(environment.apiUrl +"/animalsupdatesgetCounts/"+id);
  }
  commentsPoststoClints(id,data){
    
    return this.http.post(environment.apiUrl +"/animalsupdatesCommentspost/"+id,data);
  }
  commentsGettoClints(id){
    
    return this.http.get(environment.apiUrl +"/animalsupdatesCommentsget/"+id);
  }
  commentsGettoClintsCounts(id){
    
    return this.http.get(environment.apiUrl +"/animalsupdatesCommentsgetcounts/"+id);
  }
  replyFromCommentesGet(id){
    
    return this.http.get(environment.apiUrl +"/animalsupdatesCommentReplysGet/"+id);
  }

  overallscommentspost(id,data){
    
    return this.http.post(environment.apiUrl +"/animalsuserscomments/"+id,data);
  }
  // ********************************
  plantsFertilizersGets(id){
    
    return this.http.get(environment.apiUrl +"/animalsfoodget/"+id);
  }
  PlantsEquipmentsGets(id){
      
    return this.http.get(environment.apiUrl +"/animalsNecessariesGet/"+id);
  }

  animalsAddsOnea(id){
    
    return this.http.get(environment.apiUrl +"/animalsAddsOneGeta/"+id);
  }
  animalsAddsTwoa(id){
    
    return this.http.get(environment.apiUrl +"/animalsAddsTwoGeta/"+id);
  }
  animalsAddsThreea(id){
    
    return this.http.get(environment.apiUrl +"/animalsAddsThreeGeta/"+id);
  }
  animalsAddsFoura(id){
                    
    return this.http.get(environment.apiUrl +"/animalsAddsFourGeta/"+id);
  }

  

  animalsAddsOnel(id){
    
    return this.http.get(environment.apiUrl +"/animalsAddsOneLocGet/"+id);
  }
  animalsAddsTwol(id){
    
    return this.http.get(environment.apiUrl +"/animalsAddsTwoLocGet/"+id);
  }
  animalsAddsThreel(id){
    
    return this.http.get(environment.apiUrl +"/animalsAddsThreeGetLoc/"+id);
  }
  animalsAddsFour(id){
    
    return this.http.get(environment.apiUrl +"/animalsAddsFourLocGet/"+id);
  }
  animalsAddsFivel(id){
                    
    return this.http.get(environment.apiUrl +"/animalsAddsFiveLocGet/"+id);
  }
}
