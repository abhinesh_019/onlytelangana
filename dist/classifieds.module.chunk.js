webpackJsonp(["classifieds.module"],{

/***/ "./src/app/classifieds/classifieds/classifieds.component.css":
/***/ (function(module, exports) {

module.exports = ".catstyle{text-align: center;font-size: 40px;background-color: blanchedalmond;border-radius: 45px;padding: 12px;}\n.catstyles{text-align: center;font-size: 40px;background-color: blanchedalmond;border-radius: 45px;padding:3px;margin-bottom:25px}"

/***/ }),

/***/ "./src/app/classifieds/classifieds/classifieds.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n<div class=\"col-md-12 col-xs-12\">\n  <div class=\"col-sm-12\">\n       <a [routerLink]= \"['/classifieds/classifiedGeneral']\">\n\n      <div class=\"panel panel-primary dff boxshaws\">\n           <div class=\"panel-body dffff\">\n          <div class=\"col-sm-6 col-sm-offset-3 imgs\">\n              <h2 class=\"catstyle\"> classifieds General </h2>\n              <img src=\"assets/images/images.png\" alt=\"about us\" style=\"width:100%; height: 260px;\"> \n          </div>  \n        \n          </div>\n        </div>\n</a>\n      </div>\n\n</div> \n\n\n\n<div class=\"col-md-12 col-xs-12\">\n  <div class=\"col-sm-12\">\n       <a [routerLink]= \"['/classifieds/searchjobs']\">\n\n      <div class=\"panel panel-primary dff boxshaws\">\n           <div class=\"panel-body dffff\">\n          <div class=\"col-sm-6 col-sm-offset-3 imgs\">\n              <h2 class=\"catstyles\"> I Need Job </h2>\n              <img src=\"assets/images/job.jpeg\" alt=\"about us\" style=\"width:100%; height: 260px;\"> \n          </div>  \n        \n          </div>\n        </div>\n</a>\n      </div>\n\n</div> "

/***/ }),

/***/ "./src/app/classifieds/classifieds/classifieds.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassifiedsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ClassifiedsComponent = /** @class */ (function () {
    function ClassifiedsComponent() {
    }
    ClassifiedsComponent.prototype.ngOnInit = function () {
    };
    ClassifiedsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-classifieds',
            template: __webpack_require__("./src/app/classifieds/classifieds/classifieds.component.html"),
            styles: [__webpack_require__("./src/app/classifieds/classifieds/classifieds.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ClassifiedsComponent);
    return ClassifiedsComponent;
}());



/***/ }),

/***/ "./src/app/classifieds/classifieds/classifieds.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClassifiedsModule", function() { return ClassifiedsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__classifieds_component__ = __webpack_require__("./src/app/classifieds/classifieds/classifieds.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__classifieds_service__ = __webpack_require__("./src/app/classifieds/classifieds/classifieds.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__classifieds_search_jobs_classified_search_job_module__ = __webpack_require__("./src/app/classifieds/classifieds-search-jobs/classified-search-job.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__classified_generals_classified_general_module__ = __webpack_require__("./src/app/classifieds/classified-generals/classified-general.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__classifieds_generals_ids_classifieds_generals_ids_module__ = __webpack_require__("./src/app/classifieds/classifieds-generals-ids/classifieds-generals-ids.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    {
        path: '', component: __WEBPACK_IMPORTED_MODULE_5__classifieds_component__["a" /* ClassifiedsComponent */], children: [
            { path: 'searchjobs', loadChildren: 'app/classifieds/classifieds-search-jobs/classified-search-job.module#ClassifiedSearchJobModule' },
            { path: 'classifiedGeneral', loadChildren: 'app/classifieds/classified-generals/classified-general.module#ClassifiedGeneralModule' },
            { path: 'classifiedsGeneralsIds/:_id/:name', loadChildren: 'app/classifieds/classifieds-generals-ids/classifieds-generals-ids.module#ClassifiedsGeneralsIdsModule' },
        ]
    }
];
var ClassifiedsModule = /** @class */ (function () {
    function ClassifiedsModule() {
    }
    ClassifiedsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["h" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_7__classifieds_search_jobs_classified_search_job_module__["ClassifiedSearchJobModule"],
                __WEBPACK_IMPORTED_MODULE_8__classified_generals_classified_general_module__["ClassifiedGeneralModule"],
                __WEBPACK_IMPORTED_MODULE_9__classifieds_generals_ids_classifieds_generals_ids_module__["ClassifiedsGeneralsIdsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__classifieds_component__["a" /* ClassifiedsComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_6__classifieds_service__["a" /* ClassifiedsService */]]
        })
    ], ClassifiedsModule);
    return ClassifiedsModule;
}());



/***/ }),

/***/ "./src/app/classifieds/classifieds/classifieds.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassifiedsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ClassifiedsService = /** @class */ (function () {
    function ClassifiedsService() {
    }
    ClassifiedsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ClassifiedsService);
    return ClassifiedsService;
}());



/***/ })

});
//# sourceMappingURL=classifieds.module.chunk.js.map